#if !defined(LesParametresExternes)
	#define LesParametresExternes

#include <string>
#include "Options.h"
using namespace std;

//-------------------------------------------
// struct TPeriodes :
// param�tres d'une p�riode
//-------------------------------------------
struct TPeriode 
{
    double debut; // Date de d�but de la p�riode
    double dt; // Pas de temps de la p�riode (correspondant au taux de pr�cipitations indiqu�)
    double taux_precip; // Taux de pr�cipitations de la p�riode
    string fich_precip; // Nom du fichier contenant le taux de pr�cipitations pour cette p�riode
    double* taux_uplift; // Taux d'uplift par zone
    string fich_uplift; // Nom des fichier contenant le taux de soul�vement pour cette p�riode
    double niveaudebase; // Niveau de base de la p�riode
    double frac_temps_pluie; // Fraction du temps de pluie CLIMAT STOCHASTIQUE ou NON
    double variabilite; // Exposant alpha de la loi de puissance (Molnar 2001)
    // ou parametre k de la loi "Heavy Tail" de Crave and Davy, 2001.
    // Tous les deux expriment la variabilite du  CLIMAT STOCHASTIQUE
    double TemperatureSurface; //temp�rature au niveau 0 pour la p�riode, ajoute le 04/04/11
};

//-------------------------------------------
// struct TChronologie :
// d�coupage en p�riodes
//-------------------------------------------
struct TChronologie
{
    int nb; // Nombre de p�riodes
    TPeriode* TabPeriode; // Param�tres des p�riodes
					// (n+1 pour que la derni�re contienne la date de fin)
};

//-------------------------------------------
// struct TLimites :
// conditions aux limites des bords
//-------------------------------------------

struct TLimites
{
    BORD Type_bord[4]; // Types des 4 bords
    int ij_exut[2]; // Indices (i,j) de l'exutoire si 4 bords fermes
    double base[4]; // Niveaux de base des bords
    double base_min;	// Niveau de base minimum
};

//-------------------------------------------
// struct TNivBase :
// niveau de base interieur � la grille
//-------------------------------------------

struct TNivBase
{
    MODE mode_nivbase; // Mode d'initialisation du niveau de base

    double NivBase; // Niveau de base (max si variable)
    bool Sinus; // Option de variations sinusoidales du niveau de base
    double Periode; // P�riode du sinus dans le cas OUI

    string fich_nivbase; // Fichier contenant la s�quence temporelle de niveau de base
    int nbdates; // Nombre d'entr�es dans le fichier - ou nb de p�riodes
    double* date; // dates des niveaux impos�s
    double* niveau; // niveaux impos�s correspondants - ou niveaux / p�riode
    double alpha; // z = alpha + beta*t
    double beta; // jusqu'� la prochaine date :
    double nextdate; // celle-ci, qui correspond au niveau
    int nextnumdate;		// num�ro cela
};

//----------------------------------------------
//	struct TSorties : param�tres de sortie
//----------------------------------------------

struct TSorties
{
    double dt_denud; // Intervalle de sortie du taux de d�nudation
    double dt_bassin; // Intervalle de sortie des infos grille du bassin
    int ind_bassin; // Num�ro du dernier fichier de sortie de bassin
    string DirSortie; // Nom du r�pertoire de sortie des r�sultats
    string FichierDenud; // Nom du fichier de sortie des taux de d�nudation
    string FichierBassin; // Radical du fichier de sortie des infos grille du bassin
    string FichierRiviere; // Radical du fichier de sortie des rivieres
    string FichierRestart; // Nom du fichier restart
    string FichierFluxChimique; // Nom du fichier concentant les flux chimiques au cours du temps
    string FichierMoyenneEcartType; // Nom du fichier contenant la repartition des clasts au cours du temps


    bool OptionZone; // Taux calcul�s sur une zone tecto particuli�re
    int numzone; // Numero de cette zone
    bool OptionMasque; // Taux calcul�s sur un masque
    string fich_masque; // Nom du fichier contenant la grille masque
    bool OptionRiviere; // Rivi�res principales extraites en m�me temps que les bassins
    int nbrivieres; // Nb de rivi�res extraites
};

//----------------------------------------------
//	struct ParamClimat : param�tres de climat
//----------------------------------------------

struct ParamClimat
{
    MODE mode_precip; // Mode d'initialisation des pr�cipitations

    bool Sinus; // Option de variations sinusoidales des pr�cipitations
    double Periode; // P�riode du sinus dans le cas OUI
    bool LinAlti; // Option de variations des pr�cipitations avec l'altitude (lin�aire croissant)
    double GradientAlti; // Gradient altitudinal de precipitation
    double AltiMax; // Altitude o� les pr�cipitations max sont atteintes
    // (et ne varient plus au-dessus) NB: pass� en VOLUME tout de suite dans CModele

    bool GaussAlti; //option de calcul des pr�cipitation avec une gaussienne, ajoute le 01/03/11
    double GaussA;
    double GaussB;
    double GaussC;

    //ajoute le 28/03/11
    double DiminutionTemperature;


    bool Option_DtFctPrecip; // Option : pas de temps adapt� aux pr�cipitations [variables]
    double dtmax_precip; // Pas de temps maximal de la p�riode (cas pas de temps adapt� aux pr�cip)

    bool Stochastique; // Option de distribution stochastique des pr�cipitations au cours du temps
    PROBA LoiProbabilite; // Type de loi de probabilite (climat stochastique)
};

//----------------------------------------------
//	struct ParamTecto : param�tres de tectonique
//----------------------------------------------

struct ParamTecto
{
    MODE mode_tecto; // Mode d'initialisation de la tectonique

    bool OptionUplift; // Soul�vement ou non ?
    bool OptionDecro; // D�crochement ou non ?
    int LigneDecro; // Num�ro de la ligne o� il y a d�crochement
    double VitesseDecro; // Vitesse de d�crochement (de + par rapport � -)

    int nbzone; // Nombre de zones tectoniques
    int* numzone; // Num�ros des colonnes s�parant ces zones
    UPLIFT* type_uplift; // Type d'uplift par zone
};

//----------------------------------------------
//	ParamErosion : param�tres de transport / d'incision
//----------------------------------------------

struct ParamErosion 
{
    double K; // Coefficient de'erosion alluvial / d'incision bedrock
    double Ktemp; // Coefficient d'erosion alluvialle / d'incision bedrock
    // li� au temps (temporaire, valable pour le pas de temps)
    double MQ; // Exposant M du d�bit Q dans la loi d'erosion alluvale / d'incision bedrock
    double NS; // Exposant N de la pente S dans la loi d'erosion alluvale / d'incision bedrock
    double Seuil; // Seuil d'erosion alluvale / d'incision bedrock
    double Exposantshearstress; // Exposant a dans (Tau - Tauc)**a dans la loi de transport/incision // le 13 juillet 07
    double Kmanning; // Coefficient intervenant dans le calcul du shear stress
    double Nmanning; // Coefficient de manning  2022 en unités SI (m,s)
    double KErosionLaterale; // coefficient d'erosion laterale 7 aout 2008
    double CoeffLongTransport; // coefficient de la longueur de transport fluvial janvier 2013
    double CoeffLongTransporttemp; // coefficient de la longueur de transport fluvial prenant en compte le pas de temps et le coefficient variabilite des pluies [1/L**2] 2022
};

//----------------------------------------------
//	ParamLargeur : param�tres de prise en compte
//		de la largeur de rivi�re li�e au d�bit
//----------------------------------------------

struct ParamLargeur
{
    bool OuiNon; // Flag de prise en compte
    double K;		// Coefficient de la loi W = K*(Q**0.5) / pas spatial
};

//----------------------------------------------
//	ParamDiff : param�tres de diffusion
//----------------------------------------------

struct ParamDiff 
{
    DIFF Type_diff; // Type de diffusion
    double K; // Coefficient de diffusion
    double Ktemp; // Coefficient de diffusion
    // li� au temps (temporaire, valable pour le pas de temps)
    double Seuil; // Pente-seuil de diffusion
    double Seuil2; // = seuil*seuil
    double AlphaSeuil; // = seuil*alpha
    double AlphaSeuilSur; // = seuil*alpha/(1-scsq)
    double pas3; // cube du pas spatial
};

//----------------------------------------------
//	ParamEffondr : param�tres d'effondrements
//----------------------------------------------

struct ParamEffondr
{
    double Seuil; // Pente � partir de laquelle il peut y avoir effondrement
    double Retour; // P�riode de retour des effondrements
    double K; // Fraction du diff�rentiel d'altitude qui tombe (0<K<1)
    int Nmax; // Nombre de mailles maximal qui peuvent s'effondrer lors d'un �v�nement
    bool TalusBroye; // Option : ce qui reste de la falaise devient s�diments ou non
};

//----------------------------------------------
//	ParamAlter : param�tres d'alt�ration
//----------------------------------------------

struct ParamAlter 
{
    double Kw; // Coefficient Kw de l'alteration
    double Kwdt; // Coefficient Kw de l'alteration mulitplie par dt
    double d1; // facteur de decroissance de la premiere exponentielle
    double d2; // facteur de decroissance de la deuxieme exponentielle.  01/02/11
    double k1; // Coefficient sans dimension devant la seconde exponentielle.  01/02/11
    double NbMailleRunoffLimite; // Nombre de mailles drainees definissant un seuil de saturation du sol en eau.  01/02/11
};

//----------------------------------------------
//	Tlitho : param�tres d'une lithologie
//----------------------------------------------

struct Tlitho
{
    ParamAlter* alter; // Param�tres d'alt�ration
    ParamDiff* diff; // Param�tres de diffusion
    ParamErosion* incis; // Param�tres d'incision
    int nbMineral; //Ajout Pierre
    vector<string> mineraux; //Ajout Pierre
    vector<double> proportionMineral;       //Ajout Pierre
};

//----------------------------------------------------
//	ParamCouche : param�tres d'une couche g�ologique
//----------------------------------------------------

struct ParamCouche
{
    int indlitho; // Num�ro de la lithologie de chaque couche
    double epaisseur; // Epaisseur de chaque couche
    string fich;		// Nom du fichier contenant l'�paisseur de chaque couche
};

//-----------------------------------------------------
//	TStratigraphie : param�tres de la pile stratigraphique
//-----------------------------------------------------

struct TStratigraphie
{
    MODE mode_sed; // Mode d'initialisation de l'�paisseur initiale des s�diments
    double epaiss_sed; // Epaisseur initiale des s�diments (si uniforme)
    string FichierEpaissSed; // prise en compte de la diffusion des s�diments ou non ?

    MODE mode_couches; // Mode d'initialisation de l'�paisseur initiale des couches substratum
    int nbstrati; // Nombre de couches g�ologiques (substratum)
    ParamCouche* TabCouche;	// Liste des couches strati
};

//---------------------------------------------
//	TTopoInit : param�tres de la topo initiale
//---------------------------------------------

struct TTopoInit
{
    int nbcol; // NX
    int nblig; // NY
    double pas; // Pas spatial (largeur d'une maille)
    double coteoctogone; // Cote de l'octogone inscrit ajout le 25 octobre 2007
    string FichierAlti;		// Nom du fichier contenant la topographie initiale
};

//---------------------------------------------
//	TPluieStoch : param�tres associe a une valeur de precipitation de la distribution statistique CLIMAT STOCHASTIQUE
//---------------------------------------------

struct TPluieStoch
{
    int    indice;           // Indice de la valeur de precipitation dans le tableau contenant la distribution stat des pluies
    double precipitation; // Taux de precipitation evenementiel
    //	double precipmax;        // Valeur max de la distribution
    double tempsretour; // Temps de retour en annees de la pluie evenementielle
    double pasdetemps;				// Pas de temps associe a cette evenement
};

//---------------------------------------------
//    ParamLocalEntry : parametres concernant l'entrée locale d'eau et de sediment (ex pour un flume)
//---------------------------------------------
// 2022
struct ParamLocalEntry
{
    int    inputimin;     // numero de colonne min d'entree d'eau et de sediment
    int    inputimax;     // numero de colonne max d'entree d'eau et de sediment
    int    inputjmin;     // numero de ligne min d'entree d'eau et de sediment
    int    inputjmax;     // numero de ligne max d'entree d'eau et de sediment
    double inputwaterflux; // flux d'eau en entrée sur chaque pixel de la zone d'entrée
    double inputsedflux; // flux de sediment en entrée sur chaque pixel de la zone d'entrée
};
#endif
