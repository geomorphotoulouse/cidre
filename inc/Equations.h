#if !defined(LesEquations)
#define LesEquations

#include <math.h>
#include "Param_externes.h"
#include "Options.h" // ajout le 11 mars 2008
#include "CMaille.h"

//------------------------------------------------------
//	diverses facons de calculer la longueur de transport pour la diffusion en un point - janvier 2013
//------------------------------------------------------
// Lin�aire

inline double Longueur_Lineaire(const double &pente, ParamDiff* param) 
{
    return 1.;
}


// Non-lin�aire avec pente critique

inline double Longueur_NonLineaire(const double &pente, ParamDiff* param) 
{
    if (pente >= param->Seuil)
        return 1000000000.; // longueur "infinie"
    else
        return (1. / (1. - (pente * pente / param->Seuil2)));
}

//------------------------------------------------------
//	differentes longueurs de transport pour le transport fluvial - janvier 2013
//------------------------------------------------------
// Rmq : pour etre tout a fait rigoureux, il faudrait calculer Q/width, mais width
// n'est pas toujours calcule dans le modele selon les parametres des equations d'erosion.
// ca introduit une incoherence si on a choisi de ne pas calculer la largueur de l'ecoulement
// au quel cas la longueur de transport devrait etre proportionnelle a fluxeau. A voir si
// rafine plus tard. janvier 2013

inline double Longueur_transport(const double &fluxeau, const double &coefflongtransport) 
{
    //return mymax(coefflongtransport*sqrt(fluxeau),1.);
    return coefflongtransport*fluxeau;

}

inline double Longueur_transport_1(const double &fluxeau, const double &coefflongtransport) 
{
    return 1.;
}

inline double Longueur_transport_infinie(const double &fluxeau, const double &coefflongtransport) 
{
    return 1000000000.;
}

//------------------------------------------------------
//	diverses facons d'eroder par diffusion en tenant compte ou pas d'une pente critique de stabilite
// Lin�aire
//------------------------------------------------------

// Erosion pour ajuster la pente a la  pente critique

inline double Diffusion_RetourPenteCritique(const double &pente, ParamDiff* param) 
{
    return (param->pas3 * (pente - param->Seuil));
}

// Lin�aire

inline double Diffusion(const double &pente, const double &coeffdiff) // modif juin 2014
{
    return (coeffdiff * pente);
}



//---------------------------------------------------
//	Capacite de Detachement du bedrock et des sediment par l'eau - modifs et suppression janvier 2013... et juin 2014: bedrock ET sediments
//---------------------------------------------------

//$$$$$$$$$$$$$$$$$$$$$$$$$$
//       SANS seuil
//$$$$$$$$$$$$$$$$$$$$$$$$$$

inline double CapaciteErosion_SansSeuil(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    return ( Ktemp2 * pow(pente, param->NS) * pow(fluxeau, param->MQ));
}

inline double CapaciteErosion_SansSeuil_AvecLargeurRiv(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double fluxeauspecifique = fluxeau / width;
    return ( width * Ktemp2 * pow(pente, param->NS) * pow(fluxeauspecifique, param->MQ));
}


/////////////
//  N = 1  //
/////////////
// SANS seuil

inline double CapaciteErosion_SansSeuil_N1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    return ( Ktemp2 * pente * pow(fluxeau, param->MQ));
}

inline double CapaciteErosion_SansSeuil_N1_AvecLargeurRiv(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double fluxeauspecifique = fluxeau / width;
    return ( width * Ktemp2 * pente * pow(fluxeauspecifique, param->MQ));
}


//////////////////////
//  M = 0.5, N = 1  //
//////////////////////
// SANS seuil

inline double CapaciteErosion_SansSeuil_M0_5N1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    return ( Ktemp2 * pente * sqrt(fluxeau));
}

inline double CapaciteErosion_SansSeuil_M0_5N1_AvecLargeurRiv(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double fluxeauspecifique = fluxeau / width;
    return ( width * Ktemp2 * pente * sqrt(fluxeauspecifique));
}

////////////////////
//  M = 1, N = 1  //
////////////////////
// SANS seuil

inline double CapaciteErosion_SansSeuil_M1N1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    return ( Ktemp2 * pente * fluxeau);
}

inline double CapaciteErosion_SansSeuil_M1N1_AvecLargeurRiv(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double fluxeauspecifique = fluxeau / width;
    return ( width * Ktemp2 * pente * fluxeauspecifique);
}


//////////////////////
//  M = 1.5, N = 1  //
//////////////////////
// SANS seuil

inline double CapaciteErosion_SansSeuil_M1_5N1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    return ( Ktemp2 * pente * fluxeau * sqrt(fluxeau));
}

inline double CapaciteErosion_SansSeuil_M1_5N1_AvecLargeurRiv(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double fluxeauspecifique = fluxeau / width;
    return ( width * Ktemp2 * pente * fluxeauspecifique * sqrt(fluxeauspecifique));
}


//$$$$$$$$$$$$$$$$$$$$$$$$$$
//       AVEC seuil
//$$$$$$$$$$$$$$$$$$$$$$$$$$

inline double CapaciteErosion_AvecSeuil(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double excessshearstress = shearstress - param->Seuil; // modifie le 10 mars 2008
    double capa = Ktemp2 *
            pow(excessshearstress, param->Exposantshearstress);
    return mymax(capa, 0.);
}

inline double CapaciteErosion_AvecSeuil_Exposantshearstress1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double excessshearstress = shearstress - param->Seuil; // modifie le 10 mars 2008
    double capa = Ktemp2 *
            excessshearstress;
    return mymax(capa, 0.);
}

inline double CapaciteErosion_AvecSeuil_Exposantshearstress1_5(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double excessshearstress = shearstress - param->Seuil; // modifie le 10 mars 2008
    double capa = Ktemp2 *
            sqrt(excessshearstress) *
            excessshearstress;
    return mymax(capa, 0.);
}

inline double CapaciteErosion_AvecSeuil_Exposantshearstress2(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double excessshearstress = shearstress - param->Seuil; // modifie le 10 mars 2008
    double capa = Ktemp2 *
            excessshearstress *
            excessshearstress;
    return mymax(capa, 0.);
}

// prise en compte largeur riviere

inline double CapaciteErosion_AvecSeuil_AvecLargeurRiv(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) // modifs 25 octobre 2007
{
    double excessshearstress = shearstress - param->Seuil;
    double capa = Ktemp2 * pow(excessshearstress, param->Exposantshearstress)
            * width;
    return mymax(capa, 0.);
}

inline double CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) // modifs 25 octobre 2007
{
    double excessshearstress = shearstress - param->Seuil;
    double capa = Ktemp2 * excessshearstress
            * width;
    return mymax(capa, 0.);
}

inline double CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress1_5(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) // modifs 25 octobre 2007
{
    double excessshearstress = shearstress - param->Seuil;
    double capa = Ktemp2 *
            sqrt(excessshearstress) *
            excessshearstress
            * width;
    return mymax(capa, 0.);
}

inline double CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress2(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress) 
{
    double excessshearstress = shearstress - param->Seuil;
    double capa = Ktemp2 *
            excessshearstress *
            excessshearstress *
            width;
    return mymax(capa, 0.);
}



//----------------------------
//	distance entre deux points
//----------------------------

inline double Distance(const TCoord xy1, const TCoord xy2) 
{
    return sqrt((xy1.x - xy2.x)*(xy1.x - xy2.x) + (xy1.y - xy2.y)*(xy1.y - xy2.y));
}


//----------------------------
//	calcul de la largeur d'ecoulement
//----------------------------

inline double LargeurFonctionDuFluxEau(const double &fluxeau, ParamLargeur* largeur, const double &pas) // ajout 25 octobre 2007
{
    double width = largeur->K * sqrt(fluxeau); // calcul de la largeur de l'ecoulement pour le cas ou on tient compte 25 octobre 2007
    width = mymin(width, pas);
    return width;
}

inline double NeRecalculePasLaLargeur(const double &fluxeau, ParamLargeur* largeur, const double &pas) // ajout 25 octobre 2007
{
    double width = pas;
    return width;
}


//----------------------------
//	calcul du Shear Stress
//----------------------------
// Avec calcul de largeur et donc de flux specifique

inline double CalculeLeShearStress(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // ajout 25 octobre 2007
{
    double FluxeauSpecifique = fluxeau / width;
    double shearstress = param->Kmanning * pow(pente, param->NS) * pow(FluxeauSpecifique, param->MQ);
    //cout << param->Kmanning<< " "<<param->NS << " "<<param->MQ << "\n";
    return shearstress;
}

inline double CalculeLeShearStress_M1_N1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // decembre 2018
{
    double FluxeauSpecifique = fluxeau / width;
    double shearstress = param->Kmanning * pente * FluxeauSpecifique;
    //cout << param->Kmanning<< " "<<param->NS << " "<<param->MQ << "\n";
    return shearstress;
}

inline double CalculeLeShearStress_M0_5_N1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // decembre 2018
{
    double FluxeauSpecifique = fluxeau / width;
    double shearstress = param->Kmanning * pente * sqrt(FluxeauSpecifique);
    //cout << param->Kmanning<< " "<<param->NS << " "<<param->MQ << "\n";
    return shearstress;
}

inline double CalculeLeShearStress_M0_5_N0_5(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // decembre 2018
{
    double FluxeauSpecifique = fluxeau / width;
    double shearstress = param->Kmanning * sqrt(pente) * sqrt(FluxeauSpecifique);
    //cout << param->Kmanning<< " "<<param->NS << " "<<param->MQ << "\n";
    return shearstress;
}

inline double CalculeLeShearStress_M1_5_N1_5(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // decembre 2018
{
    double FluxeauSpecifique = fluxeau / width;
    double shearstress = param->Kmanning * sqrt(pente) * pente * sqrt(FluxeauSpecifique) * FluxeauSpecifique;
    //cout << param->Kmanning<< " "<<param->NS << " "<<param->MQ << "\n";
    return shearstress;
}

// Sans calcul de largeur et donc de flux specifique

inline double CalculeLeShearStressSansLargeur(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // ajout 25 octobre 2007
{
    double shearstress = param->Kmanning * pow(pente, param->NS) * pow(fluxeau, param->MQ);
//if (fluxeau>0.) cout << "calcul shearstress "<< param->Kmanning<< " " << pente<< "  "<< fluxeau<< "  "<< shearstress<<endl;
    return shearstress;
}

inline double CalculeLeShearStressSansLargeur_M1_N1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // ajout 25 octobre 2007
{
    double shearstress = param->Kmanning * pente * fluxeau;
    return shearstress;
}

inline double CalculeLeShearStressSansLargeur_M0_5_N1(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // ajout 25 octobre 2007
{
    double shearstress = param->Kmanning * pente * sqrt(fluxeau);
    return shearstress;
}

inline double CalculeLeShearStressSansLargeur_M0_5_N0_5(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // ajout 25 octobre 2007
{
    double shearstress = param->Kmanning * sqrt(pente) * sqrt(fluxeau);
    return shearstress;
}

inline double CalculeLeShearStressSansLargeur_M1_5_N1_5(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // ajout 25 octobre 2007
{
    double shearstress = param->Kmanning * sqrt(pente) * pente * sqrt(fluxeau) * fluxeau;
    return shearstress;
}

inline double NeCalculePasLeShearStress(const double &pente, const double &fluxeau,
        ParamErosion* param, const double &width) // ajout 25 octobre 2007
// dans le cas ou le seuil de transport est nul et ou les parametres des lois d'erosion/transport sont regroupes dans Cmodel::Reformulation.
// Alors finalement, le calcul de la capacite de transport ou d'erosion ne passe pas par le calcul du shearstress,
// on utilise une formule du type capa = k q**m S**n
{
    return 0.;
}


#endif
