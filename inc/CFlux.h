#if !defined(LesFlux)
	#define LesFlux

#include "Options.h"
#include <math.h>
#include "CModele.h"
#include "CMaillage.h"
#include "Param_externes.h"
using namespace std;

//------------------------------------------------
//
//	classe CFlux :
//
//	g√ãre les flux (eau, s√àdiments...)
//	-> actualisations
//	-> acc√ãs
//
//------------------------------------------------

class CFlux
{
//++++++++
protected:
//++++++++

	//===========================
    // Trucs qui servent souvent, qu'on r√àcup√ãre au d√àbut du Maillage et du Mod√ãle
    //===========================

    int nbmailles;
    int nbcol;
    int nblig; // 2022
    double pas; // 2022
    double pas2;
    ParamClimat climat; // Param√ãtres climatiques
    double AltiMax;
    double GradientAlti; // 19/03/12
    double PeriodeSinus;
    double GaussA;
    double GaussB;
    double GaussC;

    //ajoute le 28/03/11
    double TemperatureReference;
    double DiminutionTemperature;

    //ajoute en decembre 2014
    //double Diffusivite;     // sert pour les cailloux. A la dimension de L3 (Kxdx2*dt)

    int nbperiodes; // nb de p√àriodes (ie taille des tableaux suivants)
    GrilleDble* TabPrecip; // Liste des grilles de pr√àcipitations (une par p√àriode)
    // NB : Les pr√àcipitations sont en VOLUME (donc *pas2 est d√àj‚Ä° fait)

    int nblitho; // Nb de lithologies
    ParamAlter** alter; // Param√ãtres de d√àtachement des lithologies

    CMaillage* MonMaillage; // Le maillage lui-m√çme
    CMaille* MaMaille;
    double ReliefMin; // Le diff√àrentiel d'altitude minimal, dans le patch de correction
    // des d√àp√ôts (pour √àviter les trop gros d√àp√ôts)
    
    CModele* MonModele; // 2022
    
    ParamErosion* incisSed; // 2022

    //===========
    // variables
    //===========

    int NumPeriode; // Num√àro de la p√àriode en cours
    GrilleDble Precip; // Ptr sur la grille de pr√àcipitations de la p√àriode
    GrilleDble Precip2;

    int jdecro; // Num√àro de la ligne de d√àcrochement (s'il y en a une)
    double dt_decro; // Intervalle entre deux incr√àments de d√àcrochement
    double temps_decro; // Temps √àcoul√à depuis le dernier d√àcrochement (d'une largeur de maille)

    GrilleDble StockSed; // Volume de s√àdiments stock√às sur la grille
    // au d√àbut du pas de temps courant
    GrilleInt NumCouche; // Num√àro de la premi√ãre couche de bedrock sous les s√àdiments
    GrilleInt NumLitho; // Indice de lithologie de cette premi√ãre couche
    GrilleDble VolumeCouche; // Volume de la premi√ãre couche de bedrock sous les s√àdiments
    GrilleDble FluxEau; // Flux d'eau de pluie (...) recue par pas de temps au temps t [L3/T]
    double PluvMoy; // Pluviom√àtrie moyenne sur la grille
    GrilleDble EauCourante; // Flux d'eau libre passant par chaque maille au temps t [L3/T]

    double EauCouranteMax; // Flux d'eau max sur le pas de temps (max de la grille)

    double VolumeSed_Out; // Volume de s√àdiments qui sort de la grille pendant le pas de temps
    double VolumeSed_In; // Volume de s√àdiments qui entre dans la grille pendant le pas de temps
    double FluxEau_Out; // Flux d'eau qui sort de la grille pendant le pas de temps
    double FluxEau_In; // Flux d'eau qui entre dans la grille pendant le pas de temps

    GrilleDble Depot; // Volume de s√àdiments depose sur chaque maille - janvier 2013
    GrilleDble DernierDepot; // Volume de s√àdiments depose en dernier (pour ne pas le considerer ensuite dans le calcul production regolite)

    GrilleDble Erosion; // Volume de matiere erodee sur chaque maille - janvier 2013
    GrilleDble DepotDiff; //AJOUT PIERRE : la quantité déposé due à la diffusion
    GrilleDble ErosionDiff; //AJOUT PIERRE : la quantité érodé due à la diffusion
    GrilleDble* VolumeDiffExporte; //AJOUT PIERRE : la quantité trnsféré due à la diffusion

    GrilleDble VolumeDiff; // Volume de s√àdiments re√Åus par diffusion sur chaque maille
    // pendant le pas de temps
    GrilleDble VolumeFluv; // Volume de s√àdiments transport√às par l'eau courante sur chaque maille
    // pendant le pas de temps
    GrilleDble* VolumeFluvExporte; // Volume de s√àdiments transport√às par l'eau courante sur chaque maille vers une maille basse
    // pendant le pas de temps. C'est donc redondant par rapport a VolumeFluv sauf que ca permet de
    // savoir combien va d'une maille vers une autre. Utile pour erosion laterale 7 aout 2008
    GrilleDble DeltaErosion; // delta_z (sur le pas de temps) en chaque point

    GrilleDble VolumeRegolitheCree; // Volume de regolithe cree par alteration. 01/02/11

    double HauteurEau; // Hauteur d'eau calculee en utilisant une loi de friction type Manning 30 juin 2008

    bool Debordement; // Flag pour savoir s'il y a debordement d'une maille vers une maille topographiquement plus haute

    GrilleDble FluxEauEnAttente; // flux d'eau reporte au pas de temps suivant en cas de debordement. 30 juin 2008 [L3/T]
    
    GrilleDble TemperatureSurface;

    //##### TEMPORAIRE : sortie des capacit√às de d√àtachement
    GrilleDble CapaDetach;
    bool AfficheStockSedPositif();
    //##### FIN de TEMPORAIRE

    ParamLocalEntry localentry; // 2022
    
    //===================
    // variables locales
    //===================

    vector <int> MaillesLac; // Num√àro des mailles du lac (sous l'eau)
    vector <double> ZMaillesLac; // Altitudes des mailles du lac
    GrilleBool DejaLac; // Tableau de bool√àen indiquant si on est d√àj‚Ä° pass√à ou non
    vector <int> BordDuLac; // Num√àro des mailles du lac (sous l'eau)
    vector <int> MilieuDuLac; // Num√àro des mailles du lac (sous l'eau)

    int Nb_CorrectionsDepot; // Nombre de corrections effectu√àes quant aux d√àp√ôts
    int Nb_RemplissageSedLac; // Nombre de d√àp√ôts dans des lacs
    int Nb_RemplissageSedPlat; // Nombre de d√àp√ôts sur des plats

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CFlux(CMaillage* maillage, CModele* modele);
    ~CFlux();

    // fonctions

    //==================
    // fonctions INLINE
    //==================

    void SetStockSed(const int &i, const double &volume);
    void NeBroiePas(const int &i, const double &volfal);
    void Broie(const int &i, const double &volfal);
    void AddStockSed(const int &i, const double &volume);
    void AnnuleStockSed(const int &i);
    double GetStockSed(const int &i);
    void AddStockSed_VolFluv(const int &i);
    double AddStockSed_DispoFluv(const int &i);

    int GetNumCouche(const int &i);
    void SetNumCouche(const int &i, const int &num);
    double IncrementeCoucheLitho(const int &i, CMaille* maille);
    void SetNumCoucheEtLitho(const int &i, CMaille* maille, const int &numcouche);
    int GetNumLitho(const int &i);
    void SetNumLitho(const int &i, const int &num);
    void SetVolumeCouche(const int &i, const double &volume);
    double GetVolumeCouche(const int &i);
    void SoustraitVolumeCouche(const int &i, const double &vol);

    void IncrementeAlti_ActualiseStock(const int &i, CMaille* maille, const double &voldep);

    void SetVolumeSed_Out(const double &volume);
    void AddVolumeSed_Out(const double &volume);
    double GetVolumeSed_Out();
    void SetVolumeSed_In(const double &volume);
    void AddVolumeSed_In(const double &volume);
    double GetVolumeSed_In();

    double GetFluxEau(const int &i);
    void SetFluxEau(const int &i, const double &volume);
    void AnnuleFluxEau(const int &i);
    void AnnuleFluxEau_etCourante(const int &i);
    void SetFluxEau_AnnuleCourante(const int &i, const double &volume);
    void AddFluxEau(const int &recoit, const int &donne);
    void AddFluxEau(const int &recoit, const int &donne, const double &facteur);

    double GetEauCourante(const int &i);
    void SetEauCourante(const int &i, const double &volume);
    bool DrainePlus(const int &i1, const int &i2);

    void AddFluxEau_Out(const int &donne);
    void AddFluxEau_Out(const int &donne, const double &facteur);
    void AddFluxEau_In(const int &donne);
    void AddFluxEau_In(const int &donne, const double &facteur);
    
    void AddFluxEau_OutWaterbot(const double &flux);

    void AddVolumeDiff(const int &i, const double &volume);
    void AddVolumeFluv(const int &i, const double &volume);
    void AddVolumeFluv_VideStockSed(const int &i, const double &volume);
    double GetVolumeDiff(const int &i);
    double GetVolumeFluv(const int &i);
    double GetVolumeDiffFluv(const int &i);
    double GetVolumeFluvExporte(const int &nummaille, const int &i);

    double GetDeltaErosion(const int &i);
    void AddDeltaErosion(const int &i, const double &dz);
    double ActualiseErosion(const int &nummaille, CMaille* maille);
    void ActualiseErosionDepot(const int &nummaille, const double &depot, const double &erosion); // janvier 2013
    void ActualiseErosionDepotDiff(const int &nummaille, const double &depot, const double &erosion); //Ajout Pierre

    void ActualiseCoeffAlter(const double &dt, const double &fractempspluie);
    void NAlterePas(const int &i, CMaille* maille);

    void PasDeTecto(const double &dt);

    void ActualiseNumPeriode(const int &numperiode);
    void ActualisePluieDeLaPeriode(const int &numperiode); // ajout le 24 04 2007 cf commentaire dans la fonction
    //##### Modif le 06/10/2005: d√àplacement des grilles de pr√àcipitations dans CFlux
    void AlloueGrillePrecip();
    // MODIFICATION POUR PRISE EN COMPTE DE FRACTEMPSPLUIE (stochastique ou non)
    bool LitGrillePrecip(const int &numperiode, const string FichierIn, const double fractempspluie);
    // FIN MODIF
    void EvacuationEffondrements(const int &nb, int* nummailles);
    void SetGrillePrecipUniforme(const int &numperiode, const double &precip);

    void CalculeFluxEauEntrant(); // 30 juin 2008
    double GetFluxEauEntrant(); // 30 juin 2008
    double GetFluxEauSortant(); // 30 juin 2008
    void SetDebordementFalse(); // 30 juin 2008
    void SetDebordementTrue(); // 30 juin 2008
    bool GetDebordement(); // 30 juin 2008

    void SetFluxEauEnAttente(const int &i, const double &volume); // 30 juin 2008
    double GetFluxEauEnAttente(const int &i); // 2023
    
    //##### TEMPORAIRE : on note la capacit√à de d√àtachement
    void SetCapaDetach(const int &i, const double &capa);
    double GetCapaDetach(const int &i);
    //##### FIN du TEMPORAIRE


    double GetRegolitheCree(const int &i); //ajoute le 07/02/11

    //==================
    // Autres fonctions
    //==================

    void InitialiseStockSedLitho();
    bool InitialisePrecip(CModele* modele);
    //	void	InitialisePeriode(const int &numperiode);

    void SoustraitAlti_ActualiseStrati_MonoCouche(const int &i, CMaille* maille,
            double volume_erode);
    void SoustraitAlti_ActualiseStrati_MultiCouche(const int &i, CMaille* maille,
            double volume_erode);

    void InitFlux_RecuperePluie_NoSin_NoLin(const double &temps);
    void InitFlux_RecuperePluie_NoSin_Lin(const double &temps);
    void InitFlux_RecuperePluie_NoSin_Lin_Prototype(const double &temps); //ajoute le 21/02/11
    void InitFlux_RecuperePluie_NoSin_NoLin_Gaussienne(const double &temps); //ajoute le 21/02/11
    void InitFlux_RecuperePluie_Sin_NoLin(const double &temps);
    void InitFlux_RecuperePluie_Sin_Lin(const double &temps);

    double InitFlux_RecuperePluie_NoSin_Lin_PrecMax(const double &temps);
    double InitFlux_RecuperePluie_Sin_NoLin_PrecMax(const double &temps);
    double InitFlux_RecuperePluie_Sin_Lin_PrecMax(const double &temps);

    // Le calcul de l'alt√àration est dans les "flux" pour des raisons pratiques
    void AltereBedrock_MonoCouche(const int &i, CMaille* maille);
    void AltereBedrock_MonoCouche_TempRunoff(const int &i, CMaille* maille); // 01/02/11
    void AltereBedrock_MultiCouche(const int &i, CMaille* maille);
    void AltereBedrock_MultiCouche_TempRunoff(const int &i, CMaille* maille); // 01/02/11



    // Idem pour la propagation de l'eau vers les voisins bas
    void DeverseSurMonVoisin(const int &i, CMaille* maille);
    int DeverseSurMonVoisin_FilsLac(const int &i, CMaille* maille, int* lenumvoisin);
    int DeverseSurMonVoisin_Exutoire(const int &i, CMaille* maille,
            int* lenumvoisin, GrilleInt* ptr, const int &numlac);
    void DeverseSurMesVoisins(const int &i, CMaille* maille);
    void DeverseSurMesVoisinsEau(const int &i, CMaille* maille); // 30 juin 2008
    int DeverseSurMesVoisins_FilsLac(const int &i, CMaille* maille, int* lesnumsvoisins);
    int DeverseSurMesVoisins_Exutoire(const int &i, CMaille* maille,
            int* lesnumsvoisins, GrilleInt* ptr, const int &numlac);

    void DeverseSurMonVoisin_ILE(const int &i, CMaille* maille);
    int DeverseSurMonVoisin_FilsLac_ILE(const int &i, CMaille* maille, int* lenumvoisin);
    int DeverseSurMonVoisin_Exutoire_ILE(const int &i, CMaille* maille,
            int* lenumvoisin, GrilleInt* ptr, const int &numlac);
    void DeverseSurMesVoisins_ILE(const int &i, CMaille* maille);
    int DeverseSurMesVoisins_FilsLac_ILE(const int &i, CMaille* maille, int* lesnumsvoisins);
    int DeverseSurMesVoisins_Exutoire_ILE(const int &i, CMaille* maille,
            int* lesnumsvoisins, GrilleInt* ptr, const int &numlac);

    // Idem pour la distribution des flux de diffusion
    void ActualiseLesFluxDiffRecus(CMaille* maille, Tvoisinage_bas* voisinage,
            const double fluxout[8]); // Modif le 13 mars 2007 pour calculer la diffusion dans toutes les directions. Tvoisinage_diff devient Tvoisinage_bas et fluxout[4] devient fluxout[8]

    void ActualiseLesFluxDiffRecus_ILE(CMaille* maille, Tvoisinage_bas* voisinage,
            const double fluxout[8]); // Modif le 13 mars 2007 pour calculer la diffusion dans toutes les directions. Tvoisinage_diff devient Tvoisinage_bas et fluxout[4] devient fluxout[8]

    // Idem pour la distribution des flux d'√àrosion
    void ActualiseLesFluxTransportFluvial(const int &nummaille, CMaille* maille, Tvoisinage_bas* voisinage,
            const double fluxout[8]); // janvier 2013
    void ActualiseLesFluxTransportFluvial_ILE(const int &nummaille, CMaille* maille, Tvoisinage_bas* voisinage,
            const double fluxout[8]); // janvier 2013



    // Actualisation de l'√àrosion avec correction

    double CorrigeActu_Lac(const int &nummaille, CMaille* maille);
    double CorrigeActu_LacEtendu(const int &nummaille, CMaille* maille);
    double CorrigeActu_LacPasFond(const int &nummaille, CMaille* maille);
    double CorrigeActu_LacDeborde(const int &nummaille, CMaille* maille);
    double RemplissageSed_Lac(const int &nummaille, const int &numliste, CMaille* maille);

    // Idem pour la tecto
    void InitialiseDecro(ParamTecto param_tecto, double pas);
    void Souleve(const double &dt);
    void Decroche(const double &dt);
    void SouleveEtDecroche(const double &dt);

    void AfficheCouche();

    // Calcul Hauteur d'eau
    double CalculHauteurEau(const int &i, CMaille* maille, const double &dt); // 30 juin 2008
    double CalculHauteurEauSimple(const int &i, CMaille* maille, const double &dt); // 30 juin 2008 // modif 2022
    double CalculHauteurEauCourante(const int &i, CMaille* maille, const double &dt); // 30 juin 2008 // modif 2022


    void ActualiseVolumeFluvExporte(const double volsed, const int numeroMailleErosion, const int numeroMailleDepot); //Ajout Pierre
    void ActualiseVolumeDiffExporte(const int numeroMaille, double tableauEntree[], CMaille *maille); //Ajout Pierre
    double GetErosion(const int numeroMaille); //Ajout Pierre
    double GetDepot(const int numeroMaille); //Ajout Pierre
    double GetErosionDiff(const int numeroMaille); //Ajout Pierre
    double GetDepotDiff(const int numeroMaille); //Ajout Pierre
    void GetVolumeDiffExporte(const int numeroMaille, double tableauSorties[]); //Ajout Pierre
    double GetRainfall(const int numeroMaille);                                                  //2018
    
    
    //void Setdiffusivite(const int nummaille, double MonKtemp); // decembre 2014
    //double Getdiffusivite(const int nummaille); // decembre 2014
};





  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

//--------------------------
//	Op√àrations sur StockSed
//--------------------------
// Ajustement du stock s√àdimentaire sur une maille
inline void CFlux::SetStockSed(const int &i, const double &volume) {StockSed[i] = volume;}

// Apr√ãs un effondrement "sans broyage", l'√àrosion s'applique
// ‚Ä° la pile de s√àdiments telle quelle
inline void CFlux::NeBroiePas(const int &i, const double &volfal) {}

// Apr√ãs un effondrement "avec broyage", la falaise est "broy√àe" sur toute sa hauteur
// m√çme si tout ne s'effondre pas
inline void CFlux::Broie(const int &i, const double &volfal) {StockSed[i] = mymax(volfal,StockSed[i]);}

inline void CFlux::AddStockSed(const int &i, const double &volume) {StockSed[i] += volume;}

inline void CFlux::AnnuleStockSed(const int &i) {StockSed[i] = 0.;}

inline double CFlux::GetStockSed(const int &i) {return StockSed[i];}

//##### Modif. le 13/04/2006 : pour l'actualisation de StockSed dans les lacs
// Actualise le volume de s√àdiments avec ce qui est RECU par √àrosion fluviale
inline void CFlux::AddStockSed_VolFluv(const int &i) {StockSed[i] += VolumeFluv[i];}

// Actualise le volume de s√àdiments disponibles pour l'√àrosion fluviale
// (i.e. StockSed + les s√àdiments r√àcup√àr√às de plus haut par transport fluvial)
inline double CFlux::AddStockSed_DispoFluv(const int &i) {StockSed[i] += VolumeFluv[i]; return StockSed[i];}

//---------------------------------------------------
//	Op√àrations sur NumCouche, VolumeCouche, NumLitho
//---------------------------------------------------
// (premi√ãre couche de bedrock sous les s√àdiments)
inline int CFlux::GetNumCouche(const int &i) {return NumCouche[i];}

inline void CFlux::SetNumCouche(const int &i, const int &num) {NumCouche[i]=num;}
inline double CFlux::IncrementeCoucheLitho(const int &i, CMaille* maille)
{
	NumCouche[i]++;
	NumLitho[i] = maille->GetIndiceLitho(NumCouche[i]);
	VolumeCouche[i] = maille->GetEpaisseur(NumCouche[i]);

	return VolumeCouche[i];
}

inline void CFlux::SetNumCoucheEtLitho(const int &i, CMaille* maille, const int &numcouche)
{
	NumCouche[i] = numcouche;
	NumLitho[i] = maille->GetIndiceLitho(numcouche);
}

inline int CFlux::GetNumLitho(const int &i) {return NumLitho[i];}

inline void CFlux::SetNumLitho(const int &i, const int &num) {NumLitho[i]=num;}

inline void CFlux::SetVolumeCouche(const int &i, const double &volume) {VolumeCouche[i]=volume;}

inline double CFlux::GetVolumeCouche(const int &i) {return VolumeCouche[i];}

// Actualisation si √àrosion de s√àdiments seulement
inline void CFlux::SoustraitVolumeCouche(const int &i, const double &vol) {VolumeCouche[i] -= vol;}

//------------------------------------------------------------
//	Actualise en cas de d√àp√ôt / s√àdimentation (effondrements)
//------------------------------------------------------------
inline void CFlux::IncrementeAlti_ActualiseStock(const int &i, CMaille* maille, const double &voldep)
{
	maille->AddLesAlti(voldep);
	DeltaErosion[i] += voldep;
	StockSed[i] += voldep;
}

//---------------------------------------------
//	Op√àrations sur VolumeSed_Out, VolumeSed_In
//---------------------------------------------
inline void CFlux::SetVolumeSed_Out(const double &volume) {VolumeSed_Out = volume;}

inline void CFlux::AddVolumeSed_Out(const double &volume) {VolumeSed_Out += volume;}

inline double CFlux::GetVolumeSed_Out() {return VolumeSed_Out;}

inline void CFlux::SetVolumeSed_In(const double &volume) {VolumeSed_In = volume;}

inline void CFlux::AddVolumeSed_In(const double &volume) {VolumeSed_In += volume;}

inline double CFlux::GetVolumeSed_In() {return VolumeSed_In;}

//---------------------------
//	Op√àrations sur FluxEau
//---------------------------
inline double CFlux::GetFluxEau(const int &i) {return FluxEau[i];}

inline void CFlux::SetFluxEau(const int &i, const double &volume) {FluxEau[i] = volume;}

inline void CFlux::AnnuleFluxEau(const int &i) {FluxEau[i] = 0.;}

inline void CFlux::AnnuleFluxEau_etCourante(const int &i) {FluxEau[i] = 0.; EauCourante[i] = 0.;}

inline void CFlux::SetFluxEau_AnnuleCourante(const int &i, const double &volume)
{FluxEau[i] = volume; EauCourante[i] = 0.;}

inline void CFlux::AddFluxEau(const int &recoit, const int &donne) {FluxEau[recoit] += FluxEau[donne];}

inline void CFlux::AddFluxEau(const int &recoit, const int &donne, const double &facteur) {FluxEau[recoit] += FluxEau[donne]*facteur;}

//-----------------------------
//	Op√àrations sur EauCourante
//-----------------------------
inline double CFlux::GetEauCourante(const int &i) {return EauCourante[i];}

inline void CFlux::SetEauCourante(const int &i, const double &volume) {EauCourante[i] = volume;}

inline bool	CFlux::DrainePlus(const int &i1, const int &i2) {return (EauCourante[i1] > EauCourante[i2]);}

//---------------------------------------------
//	Op√àrations sur FluxEau_Out, FluxEau_In
//---------------------------------------------
inline void CFlux::AddFluxEau_Out(const int &donne) {FluxEau_Out += FluxEau[donne];}

inline void CFlux::AddFluxEau_Out(const int &donne, const double &facteur) {FluxEau_Out += FluxEau[donne]*facteur;}

inline void CFlux::AddFluxEau_In(const int &donne) {FluxEau_In += FluxEau[donne];}

inline void CFlux::AddFluxEau_In(const int &donne, const double &facteur) {FluxEau_In += FluxEau[donne]*facteur;}

inline void CFlux::AddFluxEau_OutWaterbot(const double &flux) {FluxEau_Out += flux;} // 2023

//----------------------------------------
//	Op√àrations sur VolumeDiff, VolumeFluv
//----------------------------------------
inline void CFlux::AddVolumeDiff(const int &i, const double &volume) {VolumeDiff[i] += volume;}

inline void CFlux::AddVolumeFluv(const int &i, const double &volume) {VolumeFluv[i] += volume;}

// Vire tout le volume de s√àdiments disponibles (√àrosion fluviale non limit√àe par le transport)
// (i.e. StockSed + les s√àdiments r√àcup√àr√às de plus haut par transport fluvial)
inline void CFlux::AddVolumeFluv_VideStockSed(const int &i, const double &volume)
{
	VolumeFluv[i] += volume - StockSed[i];
	StockSed[i] = 0.;
}

inline double CFlux::GetVolumeDiff(const int &i) {return VolumeDiff[i];}

inline double CFlux::GetVolumeFluv(const int &i) {return VolumeFluv[i];}

inline double CFlux::GetVolumeFluvExporte(const int &nummaille, const int &i) {return VolumeFluvExporte[nummaille][i];}

inline double CFlux::GetVolumeDiffFluv(const int &i) {return (VolumeDiff[i]+VolumeFluv[i]);}

//----------------------------------------
//	Op√àrations sur Erosion, Depot -  janvier 2013
//----------------------------------------
inline void CFlux::ActualiseErosionDepot(const int &nummaille, const double &depot, const double &erosion) // janvier 2013
{
	Erosion[nummaille] += erosion;
	Depot[nummaille] += depot;
	return;
}

inline void CFlux::ActualiseErosionDepotDiff(const int &nummaille, const double &depot, const double &erosion) //Ajout Pierre
{
	ErosionDiff[nummaille] += erosion;
	DepotDiff[nummaille] += depot;
	return;
}

//------------------------------
//	Op√àrations sur DeltaErosion
//------------------------------
inline double CFlux::GetDeltaErosion(const int &i) {return DeltaErosion[i];}

inline void CFlux::AddDeltaErosion(const int &i, const double &dz) {DeltaErosion[i] += dz;}

inline double CFlux::ActualiseErosion(const int &nummaille, CMaille* maille)
{
	double dz = Depot[nummaille]-Erosion[nummaille]; // janvier 2013
	DeltaErosion[nummaille] += dz;
	return dz;
}

//-------------------
//	Pas d'alt√àration
//-------------------
inline void CFlux::ActualiseCoeffAlter(const double &dt, const double &fractempspluie)
{
	for (int i=0; i < nblitho; i++)
		alter[i]->Kwdt = alter[i]->Kw*dt*fractempspluie;
}

inline void CFlux::NAlterePas(const int &i, CMaille* maille) {}

//---------------
//	Pas de tecto
//---------------
inline void CFlux::PasDeTecto(const double &dt) {}

//--------------------------------------------
//	Assignement du num√àro de p√àriode au d√àbut
//--------------------------------------------
inline void CFlux::ActualiseNumPeriode(const int &numperiode)
{
	NumPeriode = numperiode;
	//Precip = TabPrecip[numperiode]; modif le 24 04 2007
}

//--------------------------------------------
//	Remplissage de la grille de pluie utilisee pour cette periode
//--------------------------------------------
inline void CFlux::ActualisePluieDeLaPeriode(const int &numperiode)
// ajout le 24 04 2007. Cette assignation etait faite dans ActualiseNumPeriode
// mais ca entraine un pb avec le climat stochastique. On cree donc cette
// nouvelle fonction qui n'est utilise que dans le cas ou le climat n'est pas stochastique
{
	Precip = TabPrecip[numperiode];
}

//-----------------------------------------------------------
// Allocation des grilles de pr√àcipitations, une par p√àriode
//-----------------------------------------------------------
inline void CFlux::AlloueGrillePrecip()
{
	TabPrecip = new GrilleDble[nbperiodes];
	for (int i=0; i < nbperiodes; i++)
	{
		TabPrecip[i] = new double[nbmailles];
	}
	return;
}

//------------------------------------------------------------------------------
// Remplit la grille de pr√àcipitations par une valeur uniforme, pour une p√àriode
//------------------------------------------------------------------------------
inline void CFlux::SetGrillePrecipUniforme(const int &numperiode, const double &precip)
{
    //Remarque : la division des precip par fractempspluie a deja ete faite lors de la lecture du fichier l'entree
	for (int i=0; i < nbmailles; i++)
		TabPrecip[numperiode][i]=precip*pas2;
	return;
}

//------------------------------------------------------------------------------
// Remplit la grille de pr√àcipitations d'une p√àriode ‚Ä° partir d'un fichier
// numperiode : num√àro de la periode
//------------------------------------------------------------------------------
// fonction modifiee par l'introduction de fractempspluie (stochastique ou non)
inline bool CFlux::LitGrillePrecip(const int &numperiode, const string FichierIn, const double fractempspluie)
{
	double precip;

	filebuf MonFichier;
// Ouverture du flux d'entr√àe
	if(MonFichier.open(FichierIn.c_str(), ios::in)==NULL)
	{
		cerr << "The file does not exist or cannot be read !! " << FichierIn << endl;
		MonFichier.close(); return false;
	}
	else
	{
		istream FichierEntree(&MonFichier);
		AfficheMessage("Reading the rainfall file : ",FichierIn);

		for (int k=0; k < nbmailles; k++)
		{
			FichierEntree >> precip;
			TabPrecip[numperiode][k]=precip*pas2 / fractempspluie;
		}
// Fermeture du fichier
		MonFichier.close();
		AfficheMessage("*** Rainfall file read",numperiode);
		return true;
	}
}

//-------------------------------------------------------------
//	Actualisation de la topo si effondrements ET broyage
//-------------------------------------------------------------
inline void CFlux::EvacuationEffondrements(const int &nb,int* nummailles)
{
	int num;
	CMaille* maille;

	for (int k=0; k < nb; k++)
	{
		num = nummailles[k];
		maille = MonMaillage->GetMaille(num);
		maille->AddLesAlti(-StockSed[num]);
		StockSed[num] = 0.;
	}
}

//##### TEMPORAIRE : on note la capacit√à de d√àtachement
//------------------------------------------------------------------------------
// Assigne la capacit√à locale de d√àtachement (de la premi√ãre couche)
//------------------------------------------------------------------------------
inline void CFlux::SetCapaDetach(const int &i, const double &capa) {CapaDetach[i] = capa;}
inline double CFlux::GetCapaDetach(const int &i) {return CapaDetach[i];}

//------------------------------------------------------------------------------
// Affiche les mailles ‚Ä° stock s√àdim > 0, s'il y en a
//------------------------------------------------------------------------------
inline bool CFlux::AfficheStockSedPositif()
{
	bool flag=false;
	for (int k=0; k < nbmailles; k++)
	{
		if (StockSed[k] > 0.)
		{
			AfficheMessage("   - stock sed > 0, maille : ",k,
							MonMaillage->GetMaille(k)->GetAlti()/pas2,StockSed[k]/pas2);
			flag = true;
		}
	}
	return flag;
}


//##### FIN du TEMPORAIRE

inline void CFlux::CalculeFluxEauEntrant() // 30 juin 2008
{
	for (int k=0; k < nbmailles; k++)
		{
			FluxEau_In += Precip[k] ;
		}
}

inline double CFlux::GetFluxEauEntrant(){return FluxEau_In;} // 30 juin 2008

inline double CFlux::GetFluxEauSortant(){return FluxEau_Out;} // 30 juin 2008

inline void CFlux::SetDebordementFalse(){Debordement = false;} // 30 juin 2008

inline void CFlux::SetDebordementTrue(){Debordement = true;} // 30 juin 2008

inline bool CFlux::GetDebordement(){return Debordement;} // 30 juin 2008

inline void CFlux::SetFluxEauEnAttente(const int &i, const double &volume){FluxEauEnAttente[i] = volume; }// 30 juin 2008

inline double CFlux::GetFluxEauEnAttente(const int &i){return FluxEauEnAttente[i];} //2023


inline void CFlux::ActualiseVolumeFluvExporte(const double volsed, const int numeroMailleErosion, const int numeroMailleDepot)   //Ajout Pierre
{
    int dir(-1);
    int nbligne = int(nbmailles/nbcol);

    if(numeroMailleErosion-numeroMailleDepot<-1)
    {
        if(numeroMailleErosion-numeroMailleDepot==-nbcol){dir = 0;}
        else if(numeroMailleErosion-numeroMailleDepot==-nbcol-1){dir = 4;}
        else if(numeroMailleErosion-numeroMailleDepot==-nbcol+1){dir = 7;}
    }
    else if(numeroMailleErosion-numeroMailleDepot>1)
    {
        if(numeroMailleErosion-numeroMailleDepot==nbcol){dir = 2;}
        else if(numeroMailleErosion-numeroMailleDepot==nbcol-1){dir = 5;}
        else if(numeroMailleErosion-numeroMailleDepot==nbcol+1){dir = 6;}
    }
    else
    {
        if(numeroMailleErosion-numeroMailleDepot==1){dir = 3;}
        else if(numeroMailleErosion-numeroMailleDepot==-1){dir = 1;}
    }
    
    // Cas des bords boucles
    if (numeroMailleErosion%nbcol==1) // premiere colonne
    {
        if(numeroMailleErosion-numeroMailleDepot==-nbcol+1){dir = 3;}
        else if(numeroMailleErosion-numeroMailleDepot==1){dir = 6;}
        else if(numeroMailleErosion-numeroMailleDepot==-2.*nbcol+1){dir = 7;}
    }
    else if (numeroMailleErosion%nbcol==0) // derniere colonne
    {
        if(numeroMailleErosion-numeroMailleDepot==nbcol-1){dir = 1;}
        else if(numeroMailleErosion-numeroMailleDepot==2.*nbcol-1){dir = 5;}
        else if(numeroMailleErosion-numeroMailleDepot==-1){dir = 4;}
    }
    else if (numeroMailleErosion<=nbcol) // premiere ligne
    {
        if(numeroMailleErosion-numeroMailleDepot==-(nbligne-1)*nbcol){dir = 2;}
        else if(numeroMailleErosion-numeroMailleDepot==-(nbligne-1)*nbcol-1){dir = 5;}
        else if(numeroMailleErosion-numeroMailleDepot==-(nbligne-1)*nbcol+1){dir = 6;}
    }
    else if (numeroMailleErosion>nbcol*(nbligne-1)) // derniere ligne
    {
        if(numeroMailleErosion-numeroMailleDepot==(nbligne-1)*nbcol){dir = 0;}
        else if(numeroMailleErosion-numeroMailleDepot==(nbligne-1)*nbcol+1){dir = 7;}
        else if(numeroMailleErosion-numeroMailleDepot==(nbligne-1)*nbcol-1){dir = 4;}
    }
    

    if(dir>=0 && dir<8){VolumeFluvExporte[numeroMailleErosion][dir] += volsed;}
   // else{cout << "Probleme lors de la determination de la direction du voisin pour l'erosion laterale  " << numeroMailleErosion<< " " << numeroMailleDepot<< endl;}
}

inline void CFlux::ActualiseVolumeDiffExporte(const int numeroMaille, double tableauEntree[], CMaille *maille)    //Ajout Pierre
{
    for (int i(0); i<maille->GetNbVoisinsBas(); i++)
    {
        VolumeDiffExporte[numeroMaille][(maille->GetDirectionVoisinsBas(i))-1] += tableauEntree[i];
    }
}

inline double CFlux::GetErosion(const int numeroMaille){return Erosion[numeroMaille];}        //Ajout Pierre

inline double CFlux::GetDepot(const int numeroMaille){return Depot[numeroMaille];}        //Ajout Pierre

inline double CFlux::GetErosionDiff(const int numeroMaille){return ErosionDiff[numeroMaille];}        //Ajout Pierre

inline double CFlux::GetDepotDiff(const int numeroMaille){return DepotDiff[numeroMaille];}        //Ajout Pierre

inline void CFlux::GetVolumeDiffExporte(const int numeroMaille, double tableauSortie[])        //Ajout Pierre
{
    for (int i(0); i<8; i++)
    {
        tableauSortie[i] = VolumeDiffExporte[numeroMaille][i];
    }
}

inline double CFlux::GetRainfall(const int numeroMaille){return Precip2[numeroMaille];}        //2018

//inline void CFlux::Setdiffusivite(const int nummaille, double MonKtemp){Diffusivite = MonKtemp ;} // decembre 2014
//inline double CFlux::Getdiffusivite(const int nummaille){return Diffusivite ;} // decembre 2014

#endif
