#if !defined(LeMaillage)
	#define LeMaillage

#include <fstream> // pour les manipulations d'I/O sur file
#include <iostream> // pour les cerr
#include <sstream> // pour les manipulations d'I/O sur string
#include <string>		// pour les chaines de caracteres
#include <math.h>
#include "CMaille.h"
#include "Param_externes.h"
#include "Utilitaires.h"
//#include <algorithm>

using namespace std;

//------------------------------------------------
//
//	classe CMaillage :
//
//	tableau des mailles 
//
//------------------------------------------------

class CMaillage
{
//++++++++
protected:
//++++++++

    //===========
    // variables
    //===========

    int nbcol; // NX
    int nblig; // NY
    int nbmailles; // Nombre de mailles
    double* pas3; // Tableau des pas dans les 8 directions
    double pas2; // Surface d'une maille

    CMaille** TabMaille; // Tableaux des CMaille
    int* Desordre; // Liste d�sordonn�e, par d�faut pour le tri rapide
    int* Liste; // Liste des num�ros des mailles, de la plus haute � la plus basse
    int* ListeInv; // Liste inverse : classement des mailles dans la Liste
    int* ListeEau; // Liste des num�ros des mailles, de la plus haute � la plus basse en altitude de l'eau 30 juin 2008
    int* ListeEauInv; // Liste inverse : classement des mailles dans la Liste selon l'altitude de l'eau 30 juin 2008
    // NB: ListeInv est remplie dans le calcul du bilan d'eau
    Pile soustas[100]; // Sert au tri des altitudes

    TNivBase nivbase; // Param�tres de calcul du niveau de base
    double NiveauDeBase; // Valeur (�ventuellement fct de t) du niveau de base
    double temps_cycleustatic; // Temps �coul� depuis le d�but du cycle eustatique

    int nbperiodes; // nb de p�riodes (ie taille des tableaux suivants)
    GrilleDble* TabPrecip; // Liste des grilles de pr�cipitations (une par p�riode)
    // NB : Les pr�cipitations sont en VOLUME (donc *pas2 est d�j� fait)
    GrilleDble* TabUplift; // Liste des grilles de soul�vement (une par p�riode)
    GrilleDble Uplift; // Grille de soul�vement (pour un pas de temps d�fini)

    int jdecro; // Num�ro de la ligne de d�crochement (s'il y en a une)

    double MaxPente; // Pente maximale du MNT

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CMaillage();
    CMaillage(const CMaillage &maillage);
    ~CMaillage();

    //==================
    // fonctions INLINE
    //==================

    int GetNbCol();
    void SetNbCol(int ncol);
    int GetNbLig();
    void SetNbLig(int nlig);
    int GetNbMailles();
    void SetNbMailles(int nmailles);
    double* GetPas3();
    double GetPas3(int i);
    void SetPas3(int i, double nv_pas3);
    double GetPas2();
    void SetPas2(double nv_pas2);

    CMaille* GetMaille(const int &i);
    void SetMaille(int i, CMaille* maille);

    int PlusBasDesBas_SteepDesc(CMaille* maille);
    int PlusBasDesBas_MultFlow(CMaille* maille);

    void SetNivBase(TNivBase nv_parambase);
    double GetNiveauDeBase();

    void SetNbPeriodes(int nv_nb);
    double GetPrecip(int numperiode, int i);

    void ReinitVoisinages_PasNivBase(const double &t, const double &dt);
    void ReinitVoisinages_NivBaseSinus(const double &t, const double &dt);
    void ReinitVoisinages_NivBaseEustatic(const double &t, const double &dt);
    void ReinitVoisinages_NivBaseSinus_ILE(const double &t, const double &dt);
    void ReinitVoisinages_NivBaseEustatic_ILE(const double &t, const double &dt);

    int GetNumListe(const int &i);
    void SetNumListeInv(const int &i, const int &num);
    int GetNumListeInv(const int &i);
    int GetNumListeEau(const int &i); // 30 juin 2008
    void SetNumListeEauInv(const int &i, const int &num); // 30 juin 2008
    int GetNumListeEauInv(const int &i); // 30 juin 2008
    int* GetDesordre();

    bool A_PlusHautQue_B(const int &ia, const int &ib);
    bool A_PlusHautQue_B_Eau(const int &ia, const int &ib);
    bool MailleTerre(const int &i);

    void InitialiseNiveauDeBase_Constant();
    void InitialiseNiveauDeBase_Eustatic();
    void ActualiseAlti_LimiteBase(const int &nummaille, const double &delta_z);

    void PasUplift(const int &numperiode, const double &dt);

    //==================
    // Autres fonctions
    //==================

    bool LitAltitudes(const string FichierIn);
    void SetSedimentUniforme(double epaisseur);
    void SetBedrockUniforme();
    void SetBedrockUniforme(double epaisseur, int numcouche, int indlitho);
    void InitialiseCouches(int nbstrati);
    bool LitEpaisseurs(const string FichierIn);
    bool LitEpaisseurs(const string FichierIn, int numcouche, int numlitho);

    void AlloueGrilleUplift();
    bool SetGrilleUplift(int numperiode, ParamTecto tecto, double* uplift);
    bool LitGrilleUplift(const string FichierIn, int numperiode);

    void ActualiseUplift(const int &numperiode, const double &dt);

    void InitialiseEspaceTopologie(int nv_nbcol, int nv_nblig, double nv_pas);
    void InitialiseEspaceTopologieVoisinLateral(int nv_nbcol, int nv_nblig, double nv_pas); // idem pour erosion laterale 7 aout 2008
    void AlloueLesVoisinages(PENTE type_pente);
    void AffineTopologie(TLimites Limites);
    void AffineTopologieVoisinLateral(TLimites Limites); // idem pour erosion laterale 7 aout 2008

    void BordureIle();

    void TriRapideOptimal_deMailles();
    void TriParInsertion_deMailles();

    void TriRapideOptimal_deMailles_Eau(); // idem en fonction des Altitudes de la surface de l'eau 30 juin 2008
    void TriParInsertion_deMailles_Eau(); // 2018

    void InitialiseDecro(ParamTecto param_tecto);
    void Souleve();
    void Decroche();

    void SuivantBas(CMaille* maille, GrilleInt* ptr, const int &numlac,
            int& isuivant, double& zsuivant);
    void SuivantBas_ILE(CMaille* maille, GrilleInt* ptr, const int &numlac,
            int& isuivant, double& zsuivant);
    bool SuivantPlat(CMaille* maille, GrilleInt* ptr, const int &numlac,
            const double &zlac, int& isuivant);
    bool SuivantPlat_ILE(CMaille* maille, GrilleInt* ptr, const int &numlac,
            const double &zlac, int& isuivant);
    bool TesteSiBas(CMaille* maille, GrilleInt* ptr, const int &numlac,
            const double &z);
    bool TesteSiBas_ILE(CMaille* maille, GrilleInt* ptr, const int &numlac,
            const double &z);

    bool LitEustatique(const double &dtMin);

    void AfficheMaillage();
    
    void NeFaitRien(); // 2023

};




  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

//---------------------
// Sur dimensions, Pas
//---------------------
inline int CMaillage::GetNbCol() {return nbcol;}

inline void CMaillage::SetNbCol(int ncol) {nbcol=ncol;}

inline int CMaillage::GetNbLig() {return nblig;}

inline void CMaillage::SetNbLig(int nlig) {nblig=nlig;}

inline int CMaillage::GetNbMailles() {return nbmailles;}

inline void CMaillage::SetNbMailles(int nmailles) {nbmailles=nmailles;}

inline double* CMaillage::GetPas3() {return pas3;}

inline double CMaillage::GetPas3(int i) {return pas3[i];}

inline void CMaillage::SetPas3(int i, double nv_pas3) 
{
	if (nv_pas3 > 0.) 
		pas3[i]=nv_pas3;
}

inline double CMaillage::GetPas2() {return pas2;}

inline void CMaillage::SetPas2(double nv_pas2) {pas2=nv_pas2;}

//-----------------
// Sur les Mailles
//-----------------
inline CMaille* CMaillage::GetMaille(const int &i) {return TabMaille[i];}

inline void CMaillage::SetMaille(int i, CMaille* maille) {TabMaille[i]=maille;}

//----------------
//	PlusBasDesBas
//----------------
inline int CMaillage::PlusBasDesBas_SteepDesc(CMaille* maille) {return 0;}

inline int CMaillage::PlusBasDesBas_MultFlow(CMaille* maille)
{
	int ibas = 0;
	double PenteBas = maille->GetPente(0);
	double MaPente;

	for (int i=1; i < maille->GetNbVoisinsBas(); i++) 
	{
		MaPente = maille->GetPente(i);
		if (MaPente > PenteBas)
		{
			PenteBas = MaPente;
			ibas = i;
		}
	}
	return ibas;
}

//-----------------------
//	Sur le niveau de base
//-----------------------
inline void CMaillage::SetNivBase(TNivBase nv_param) {nivbase=nv_param;}

inline double CMaillage::GetNiveauDeBase() {return NiveauDeBase;}

//-----------------
//	Sur les Precip
//-----------------
inline void CMaillage::SetNbPeriodes(int nv_nb) {nbperiodes=nv_nb;}

inline double CMaillage::GetPrecip(int numperiode, int i) {return TabPrecip[numperiode][i];}

//---------------------------------------
// remise � z�ro des compteurs de voisins
//---------------------------------------
inline void CMaillage::ReinitVoisinages_PasNivBase(const double &t, const double &dt)
{
	for (int i=0; i < nbmailles; i++)
	{
		TabMaille[i]->InitialiseLeVoisinage();
        TabMaille[i]->InitialiseLeVoisinageEau(); // 2022
	}
}

inline void CMaillage::ReinitVoisinages_NivBaseSinus(const double &t, const double &dt)
{
	for (int i=0; i < nbmailles; i++)
    {
		TabMaille[i]->InitialiseLeVoisinage();
        TabMaille[i]->InitialiseLeVoisinageEau(); // 2022
    }
    NiveauDeBase = nivbase.NivBase*(1.+sin(2*PI*t/nivbase.Periode));
}
/*
inline void CMaillage::ReinitVoisinages_NivBaseEustatic(const double &t)
{
	for (int i=0; i < nbmailles; i++)
		TabMaille[i]->InitialiseLeVoisinage();
	if (t >= nivbase.nextdate)
	{
		do
		{
			nivbase.nextnumdate++;
			nivbase.nextdate = nivbase.date[nivbase.nextnumdate];
		}
		while (t >= nivbase.nextdate);
		nivbase.beta = nivbase.niveau[nivbase.nextnumdate] 
					/ (nivbase.date[nivbase.nextnumdate]-nivbase.date[nivbase.nextnumdate-1]);
		nivbase.alpha = nivbase.niveau[nivbase.nextnumdate-1] 
						- (nivbase.date[nivbase.nextnumdate-1]*nivbase.beta);
	}
	NiveauDeBase = (nivbase.alpha*t) + nivbase.beta;
}
*/
//##### Modif. le 23/01/2006 : gestion + simple, et cycles � la suite
inline void CMaillage::ReinitVoisinages_NivBaseEustatic(const double &t, const double &dt)
{
	for (int i=0; i < nbmailles; i++)
    {
		TabMaille[i]->InitialiseLeVoisinage();
        TabMaille[i]->InitialiseLeVoisinageEau(); // 2022
    }

	temps_cycleustatic += dt;
	if (temps_cycleustatic >= nivbase.nextdate)
	{
		do
		{
			nivbase.nextnumdate++;
			if (nivbase.nextnumdate >= nivbase.nbdates)
			{
				nivbase.nextnumdate = 0;
				temps_cycleustatic = 0.;
			}
			nivbase.nextdate = nivbase.date[nivbase.nextnumdate];
		}
		while (temps_cycleustatic >= nivbase.nextdate);
		nivbase.beta = nivbase.niveau[nivbase.nextnumdate] 
					/ (nivbase.date[nivbase.nextnumdate]-nivbase.date[nivbase.nextnumdate-1]);
		nivbase.alpha = nivbase.niveau[nivbase.nextnumdate-1] 
						- (nivbase.date[nivbase.nextnumdate-1]*nivbase.beta);
		NiveauDeBase = nivbase.niveau[nivbase.nextnumdate];
	}
}

inline void CMaillage::ReinitVoisinages_NivBaseSinus_ILE(const double &t, const double &dt)
{
	for (int i=0; i < nbmailles; i++)
    {
		TabMaille[i]->InitialiseLeVoisinage();
        TabMaille[i]->InitialiseLeVoisinageEau(); // 2022
    }

	NiveauDeBase = nivbase.NivBase*(1.+sin(2*PI*t/nivbase.Periode));
	BordureIle();
}

inline void CMaillage::ReinitVoisinages_NivBaseEustatic_ILE(const double &t, const double &dt)
{
	for (int i=0; i < nbmailles; i++)
    {
		TabMaille[i]->InitialiseLeVoisinage();
        TabMaille[i]->InitialiseLeVoisinageEau(); // 2022
    }
	if (t >= nivbase.nextdate)
	{
		do
		{
			nivbase.nextnumdate++;
			nivbase.nextdate = nivbase.date[nivbase.nextnumdate];
		}
		while (t >= nivbase.nextdate);
		nivbase.beta = nivbase.niveau[nivbase.nextnumdate] 
					/ (nivbase.date[nivbase.nextnumdate]-nivbase.date[nivbase.nextnumdate-1]);
		nivbase.alpha = nivbase.niveau[nivbase.nextnumdate-1] 
						- (nivbase.date[nivbase.nextnumdate-1]*nivbase.beta);
	}
	NiveauDeBase = (nivbase.alpha*t) + nivbase.beta;
	BordureIle();
}

//-----------------
//	Sur les Listes
//-----------------
// renvoie le classement par altitude de la i-�me maille
inline int CMaillage::GetNumListe(const int &i) {return Liste[i];}

// note le classement par altitude de la i-�me maille
inline void CMaillage::SetNumListeInv(const int &i, const int &num) {ListeInv[num] = i;}

// renvoie le classement par altitude de la i-�me maille
inline int CMaillage::GetNumListeInv(const int &i) {return ListeInv[i];}

// renvoie le classement par altitude de  l'eau de la i-�me maille
inline int CMaillage::GetNumListeEau(const int &i) {return ListeEau[i];} // 30 juin 2008

// note le classement par altitude de  l'eau de la i-�me maille
inline void CMaillage::SetNumListeEauInv(const int &i, const int &num) {ListeEauInv[num] = i;} // 30 juin 2008

// renvoie le classement par altitude de l'eau de la i-�me maille
inline int CMaillage::GetNumListeEauInv(const int &i) {return ListeEauInv[i];} // 30 juin 2008


// renvoie la liste en d�sordre
inline int* CMaillage::GetDesordre() {return Desordre;}
//------------------------------------------
// comparaison d'altitude entre deux mailles
//------------------------------------------
inline bool CMaillage::A_PlusHautQue_B(const int &ia, const int &ib) {return (TabMaille[ia]->PlusHaut(TabMaille[ib]));}

inline bool CMaillage::A_PlusHautQue_B_Eau(const int &ia, const int &ib) {return (TabMaille[ia]->PlusHaut_Eau(TabMaille[ib]));}

inline bool CMaillage::MailleTerre(const int &i) {return (TabMaille[i]->GetAlti() > NiveauDeBase);}

//---------------------------------
// initialisation du niveau de base
//---------------------------------
inline void CMaillage::InitialiseNiveauDeBase_Constant(){NiveauDeBase = nivbase.NivBase;}

inline void CMaillage::InitialiseNiveauDeBase_Eustatic()
{
	temps_cycleustatic = 0.;
	if (nivbase.date[0] > 0.)
	{
		nivbase.alpha = nivbase.niveau[0];
		nivbase.beta = 0.;
		nivbase.nextdate = nivbase.date[0];
		nivbase.nextnumdate = 0;
	}
	else
	{
		nivbase.alpha = nivbase.niveau[0];
		nivbase.beta = nivbase.niveau[1]/(nivbase.date[1]-nivbase.date[0]);
		nivbase.nextdate = nivbase.date[1];
		nivbase.nextnumdate = 1;
	}
	NiveauDeBase = nivbase.alpha;
}

inline void CMaillage::ActualiseAlti_LimiteBase(const int &nummaille, const double &delta_z)
{
	double new_z = TabMaille[nummaille]->GetAlti() + delta_z;
	if (new_z >= NiveauDeBase) 
		TabMaille[nummaille]->SetLesAlti(new_z);
	else
		TabMaille[nummaille]->SetLesAlti(NiveauDeBase);
}

//--------------------------
// pas de tecto � actualiser mais on actualise quand meme le niveau de base
//--------------------------
inline void CMaillage::PasUplift(const int &numperiode, const double &dt)
{
	nivbase.NivBase = nivbase.niveau[numperiode];
	NiveauDeBase = nivbase.NivBase * pas2; // multiplication par pas2 en decembre 2018;
}


#endif
