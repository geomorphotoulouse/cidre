#if !defined(LeCalculDeDiffusion)
	#define LeCalculDeDiffusion

#include "CMaillage.h"
#include "CFlux.h"
#include "Ccalc_pentes.h"
#include "Equations.h"

//========================
// Pointeurs de fonctions
//========================

typedef double (*PtrFct_LongTranspDiff)(const double &pente, ParamDiff* param);
//typedef double (*PtrFct_DiffSed)(const double &pente, ParamDiff* param, const double &Ktemp2); // modif juin 2014
typedef double (*PtrFct_Diff)(const double &pente, const double &coeffdiff);  // modif juin 2014
typedef void (CFlux::*PtrFct_FluxRecus)(CMaille* maille, Tvoisinage_bas* voisinage,
										const double fluxout[8]); // Modif le 13 mars 2007 pour calculer la diffusion dans toutes les directions. Tvoisinage_diff devient Tvoisinage_bas et fluxout[4] devient fluxout[8]
typedef void (CFlux::*PtrFct_FluxRecus_Coeff)(CMaille* maille, Tvoisinage_bas* voisinage,
											  const double fluxout[8], const double &facteur);// Modif le 13 mars 2007 pour calculer la diffusion dans toutes les directions. Tvoisinage_diff devient Tvoisinage_bas et fluxout[4] devient fluxout[8]

//------------------------------------
//
//	classe CDiffusion :
//
//	calcul de la diffusion en un point
//
//------------------------------------

class CDiffusion
{
//++++++++
protected:
//++++++++

    //===========================
    // Trucs qui servent souvent, qu'on rÈcupËre au dÈbut du Maillage
    //===========================

    double* pas3;

    CFlux* MesFlux; // Les grilles de flux

    //===================================
    // ParamËtres du modËle de diffusion
    //===================================

    int nblitho; // Nombre de lithologies
    ParamDiff* diffsed; // ParamËtres de diffusion des sediments aeriens
    ParamDiff* difflac; // ParamËtres de diffusion des sediments dans les lac
    ParamDiff** diffbedrock; // ParamËtres de diffusion des lithologies bedrock

    PtrFct_Diff MaDiffusionSed; // choix de l'équation d'Erosion par diffusion - pour les sédiments
    PtrFct_Diff* MaDiffusionBedrock; // choix de l'équation d'Erosion par diffusion - pour le bedrock. Tableau car possiblement multicouche bedrock
    PtrFct_LongTranspDiff MaLongueurTransport; // choix de l'equation pour la longueur de transport - janvier 2013
    PtrFct_LongTranspDiff MaLongueurTransportLac; // choix de l'equation pour la longueur de transport sous l'eau- decembre 2018
    PtrFct_FluxRecus MonActualiseFlux; // Actualisation des flux recus par les voisins

    //===================
    // variables locales (pour un seul calcul, sur une maille)
    //===================

    Tvoisinage_bas* MonVoisinage; // Voisinage de la maille courante pour la diffusion. Modif le 13 mars 07
    double Somme_FluxDiffOut; // Somme des volumes dans les directions des voisins de diffusion
    double facteur_distrib; // Facteur de pondÈration quand la diffusion potentielle d'une couche
    // (calculÈe avec les pentes) dÈpasse le volume disponible
    // (de cette couche)
    double facteur_temps; // Multiplicateur du pas de temps quand on a dÈj‡ passÈ une partie
    // de ce pas de temps ‡ diffuser la ou les couches sup
    double ToutleVolumeDiffuse; // Tout le volume diffusÈ au fur et ‡ mesure qu'on avance
    // dans le pas de temps (sert pour recalculer les pentes 
    // si diff sed + bedrock ou si plusieurs couches sont diffusÈes)
    double FluxDiffEntrant; // Le volume reÁu par diffusion (avant de diffuser la amille donc
    // - car Áa vient des voisins hauts)
    bool SedOptionSeuil; // test de la prise en compte de seuil de pente
    bool SedOptionSeuilLac; // test de la prise en compte de seuil de pente sous l'eau
    bool *BedrockOptionSeuil; // test de la prise en compte de seuil de pente pour les couches de bedrock

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CDiffusion(CModele* modele, CMaillage* maillage, CFlux* flux);
    ~CDiffusion();

    //==================
    // fonctions INLINE
    //==================

    void NeFaitRien(double dt);
    void ActualiseCoeffDiffLac(double dt);
    void ActualiseCoeffDiffSed(double dt);
    void ActualiseCoeffDiffBedrock(double dt);
    void NeDiffuseRien(const int &nummaille, CMaille* maille);

    //==================
    // Autres fonctions
    //==================
    void DiffuseLesSedSeulsTEST(const int &nummaille, CMaille* maille);
    void DiffuseToutTEST(const int &nummaille, CMaille* maille);
    void DiffuseLesSedSeulsLacTEST(const int &nummaille, CMaille* maille);
    void DiffuseToutLacTEST(const int &nummaille, CMaille* maille);
    void DeposeLesSediments(const int &nummaille, CMaille* maille);

};



  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

// Pas d'actualisation
inline void	CDiffusion::NeFaitRien(double dt) {}

// Actualisation du coefficient de diffusion alluviale selon le pas de temps
inline void	CDiffusion::ActualiseCoeffDiffLac(double dt) {difflac->Ktemp = difflac->K*dt;}

// Actualisation du coefficient de diffusion alluviale selon le pas de temps
inline void	CDiffusion::ActualiseCoeffDiffSed(double dt) {diffsed->Ktemp = diffsed->K*dt;}

// Actualisation des coefficients de diffusion bedrock selon le pas de temps
inline void	CDiffusion::ActualiseCoeffDiffBedrock(double dt)
{
	for (int i=0; i < nblitho; i++)
		diffbedrock[i]->Ktemp = dt*diffbedrock[i]->K;
}

// Pas de diffusion
inline void CDiffusion::NeDiffuseRien(const int &nummaille, CMaille* maille) {}

#endif
