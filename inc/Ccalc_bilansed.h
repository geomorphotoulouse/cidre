#if !defined(LeCalculDuBilanSedim)
	#define LeCalculDuBilanSedim

#include "CMaillage.h"
#include "CModele.h"
#include "CFlux.h"
#include "Ccalc_bilaneau.h"
#include "Ccalc_diffusion.h"
#include "Ccalc_erosionfluv.h"

//========================
// Pointeurs de fonctions
//========================

typedef void	(CFlux::*PtrFct_Alter)(const int &i, CMaille* MaMaille);
typedef double	(CMaille::*PtrFctAltiMaille)();
typedef void	(CDiffusion::*PtrFct_ActuDiff)(double dt);
typedef void	(CErosionFluviale::*PtrFct_ActuErosFluv)(double dt, double FracTempsPluie);
typedef bool	(*PtrFct_CalcPentesMaille)(CMaille* MaMaille, CMaillage* MonMaillage,
										   PtrFctAltiMaille MonGetAlti, double pas3[8]);
typedef void	(CDiffusion::*PtrFct_CalcDiff)(const int &nummaille, CMaille* MaMaille);
typedef void	(CErosionFluviale::*PtrFct_ErosFluv)(const int &nummaille, CMaille* MaMaille);
typedef double	(CFlux::*PtrFct_ActuEros)(const int &nummaille,  CMaille* maille);
typedef void    (CErosionFluviale::*PtrFct_ErosionLaterale)(const int &nummaille, CMaille* MaMaille, CMaillage* MonMaillage); // juin 2014

//---------------------------------------------------------
//
//	classe CBilanSed :
//
//	calcul du bilan d'�rosion / s�dimentation :
//	alt�ration, diffusion, transport fluvial et d�tachement
//
//---------------------------------------------------------

class CBilanSed
{
//++++++++
protected:
//++++++++

    //===========================
    // Trucs qui servent souvent, qu'on r�cup�re au d�but du Maillage et du Mod�le
    //===========================

    int nbmailles;
    double* pas3;

    CMaillage* MonMaillage; // Le maillage lui-m�me
    CFlux* MesFlux; // Les grilles de flux
    CBilanEau* MonBilanEau; // Le bilan hydro

    ParamDiff* diffsed; // Param�tres de diffusion des s�diments aeriens - janvier 2013
    ParamDiff* difflac; // Param�tres de diffusion des s�diments dans les lacs - janvier 2013

    //============
    // Param�tres
    //============

    double FluxCritique_paran; // Flux d'eau au-dessus duquel il y a �rosion fluviale
    double FluxCritique; // idem, rapport� au pas de temps

    //==================
    // Objets de calcul
    //==================

    CDiffusion* MaDiffusion; // Objet qui g�re la diffusion
    CErosionFluviale* MonErosionFluviale; // Objet qui g�re le transport des s�diments et
    // l'incision (d�tachement) du bedrock


    //===========================================
    // Fonctions d�pendant des options du mod�le
    //===========================================

    PtrFct_ActuDiff MonActuCoeffDiffLac; // fct qui actualise les coeffs de diffusion sous-marine
    PtrFct_ActuDiff MonActuCoeffDiffSed; // fct qui actualise les coeffs de diffusion s�dim
    PtrFct_ActuDiff MonActuCoeffDiffBedrock; // fct qui actualise les coeffs de diffusion bedrock
    PtrFct_ActuErosFluv MonActuCoeffErosTranspSediment; // fct qui actualise les coeffs de transport
    PtrFct_ActuErosFluv MonActuCoeffBedrock; // fct qui actualise les coeffs d'incision

    PtrFct_CalcPentesMaille MonCalcPentes_Maille; // fct de calcul de pentes pour une maille

    PtrFct_Alter MonAlteration; // fct qui calcule l'alt�ration de la roche en s�diments (ou pas)
    PtrFct_CalcDiff MonCalculeDiff; // fct qui calcule la diffusion en un point en actualisant tout
    // ce qu'il faut : flux, stock, litho...
    PtrFct_CalcDiff MonCalculeDiffLac; // fct qui calcule la diffusion en un point de lac
    PtrFct_ErosFluv MonCalculeErosion; // Fct qui calcule l'�rosion fluviale en un point

    PtrFct_ActuEros MonActuErosion; // Fct qui actualise DeltaZ, en corrigeant �ventuellement

    PtrFct_ErosionLaterale MonErosionLaterale; // Fct qui calcule l'erosion laterale des rivieres juin 2014

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CBilanSed(CModele* modele, CMaillage* maillage, CFlux* flux, CBilanEau* bileau);
    ~CBilanSed();

    //==================
    // fonctions INLINE
    //==================

    // MODIFS DEPUIS INTRODUCTION DU CLIMAT STOCHASTIQUE :
    void ActualiseCoeffs_SansAlter(double dt, double FracTempsPluie);
    void ActualiseCoeffs_AvecAlter(double dt, double FracTempsPluie);
    // FIN DES MODIFS

    //==================
    // Autres fonctions
    //==================

    void BilanSedim();

    void BilanSedim_Heau();

    void BilanSedim_SeuilFlux();

    void BilanSedim_Heau_SeuilFlux();
};



  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

// Actualisation des coefficients selon le pas de temps : SANS alt�ration
inline void CBilanSed::ActualiseCoeffs_SansAlter(double dt, double FracTempsPluie)
{
    (MonErosionFluviale->*MonActuCoeffErosTranspSediment)(dt, FracTempsPluie);
	(MonErosionFluviale->*MonActuCoeffBedrock)(dt, FracTempsPluie);
	(MaDiffusion->*MonActuCoeffDiffSed)(dt);
	(MaDiffusion->*MonActuCoeffDiffLac)(dt); // janvier 2013
	(MaDiffusion->*MonActuCoeffDiffBedrock)(dt);
	FluxCritique_paran = FluxCritique_paran / FracTempsPluie; // Modif 2022
}

// Actualisation des coefficients selon le pas de temps : AVEC alt�ration
inline void CBilanSed::ActualiseCoeffs_AvecAlter(double dt, double FracTempsPluie)
{
	MesFlux->ActualiseCoeffAlter(dt, FracTempsPluie);
	(MonErosionFluviale->*MonActuCoeffErosTranspSediment)(dt, FracTempsPluie);
	(MonErosionFluviale->*MonActuCoeffBedrock)(dt, FracTempsPluie);
	(MaDiffusion->*MonActuCoeffDiffSed)(dt);
	(MaDiffusion->*MonActuCoeffDiffLac)(dt); // janvier 2013
	(MaDiffusion->*MonActuCoeffDiffBedrock)(dt);
	FluxCritique_paran = FluxCritique_paran / FracTempsPluie ; // modif 2022 
}

#endif
