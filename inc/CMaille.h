#if !defined(LaMaille)
	#define LaMaille

#include <math.h> // 30 juin 2008
#include "Options.h"
#include <vector>
#include <iostream>
using namespace std;



//-------------------------------------------
//
// struct Tvoisinage :
// "voisinage" d'un point : ses voisins, ses
//	voisins bas, les pentes...
//
//-------------------------------------------
struct Tvoisinage_bas
{
    int nb; // nb de voisins bas
    int* ind; // num�ros des voisins bas
    int* direction; // direction vers laquelle est situee le voisin (1 a 8) 7 aout 2008
    double* pas3; // pas (spatial) vers les voisins bas
    double* pente; // pentes
    double* coeff; // coefficient de pond�ration vers les voisins bas
    // (= pente/somme des pentes)
    double pentemoyenne; // moyenne des pentes dans la direction downstream 30 juin 2008
    double pentemax;	// pente max (pour le test de la pente critique pour la  diffusion non-lineaire)
    double minupstreamslope; // pente du voisin au-dessus la plus petite  2022
    double minupstreamslopepas; // dx dans la direction du voisin au-dessus la plus petite  2022
    double* alti; // altitude du voisin  2023
    double altilaplusbasse; // altitude du voisin  2023
    int directionaltilaplusbasse; // direction du voisin le plus bas 2023

};


//----------------------------------------------------------
//
// struct TCouche :
// �paisseur (initiale) et indice de lithologie de la couche
//
//----------------------------------------------------------
struct TCouche
{
	double epaisseur; 		// Epaisseur de la couche
	int	ind_litho;			// Indice de la lithologie de la couche
};

//-----------------------------
//
// struct TCoord :
// coordonn�es x y de la maille
//
//-----------------------------
struct TCoord
{
	float x; 		// Abscisse de la maille
	float y;		// Ordonn�e de la maille
};


//------------------------------------------------
//
//	classe CMaille :
//
//	coordonn�es de la maille + variables associ�es 
//
//------------------------------------------------
class CMaille
{
//++++++++
protected:
//++++++++

    //===========
    // variables
    //===========

    int nummaille; // Num�ro de la maille
    double z_init; // Altitude initiale
    double z; // Altitude courante
    double z_eau; // Altitude du niveau d'eau
    double z_ref; // 2022-2023 c'est soit la surface du sol (z) ou bien surface d'eau  des lacs sur laquelle
                // coule l'eau courante (z_ref<z_eau). Cette surface est utilisée pour identifier les mailles qui ont fait l'objet d'un remplissage de lac (z_ref=z_eau ou pas z_ref=z)
    bool PasBord; // est-on sur un bord ou non ? (enfin, le contraire)
    BORD type_bord; // Type de bord de la maille
    double nivbase; // Niveau de base si on est sur un bord d'alti fixe (0 par d�faut)
    int voisin[8]; // Num�ros des 8 voisins
    int VoisinLateral[8][2]; // Numeros des 2 voisins lateraux perpendiculaires a chacune des 8 directions // 7 aout 2008
    Tvoisinage_bas* voisins_bas; // Voisinage avec pentes vers le ou les bas voisins
    Tvoisinage_bas* voisins_bas_eau; // Voisinage avec pentes vers le ou les bas voisins le long de la surface de l'eau

    double StockInitSed; // Volume initial de s�diments sur la maille
    double alti_voisin_justeaudessus_eau; // 2022
    TCouche** PileCouches; // Liste des couches strati de la pile

    TCoord coord; // Coordonn�es (x,y) de la maille
    
    int NumVoisinJusteAuDessusEau; // 2023
    int NumVoisinDeuxiemeJusteAuDessusEau; // 2023
    double AltiVoisinLePlusBasEau; // 2023
    double AltiMeanVoisinEau; // 2023
    double* alti; // 2023
    double altilaplusbasse; // 2023

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CMaille();
    CMaille(const CMaille &maille);
    ~CMaille();

    //==================
    // fonctions INLINE
    //==================

    int GetNumMaille();
    bool SetNumMaille(const int &nv_num);

    double GetAlti();
    void SetAlti(const double &Alti);
    double GetAltiInit();
    void SetAltiInit(const double &Alti);
    double GetAltiEau();
    void SetAltiEau(const double &Alti);
    void SetAltiRef(const double &Alti); // 2022
    double SetAltiEau_GetVolumeLac(const double &Alti);
    double GetAlti(ALTI TypeZ);
    void SetAlti(ALTI TypeZ, double Alti);
    void InitialiseLesAlti(const double &Alti);
    void SetLesAlti(const double &Alti);
    void AddAlti(const double &deltaZ);
    void AddLesAlti(const double &deltaZ);
    void AddLesAltiLac(const double &deltaZ);
    void AddLesAltiHeau(const double &deltaZ);
    double GetVolumeLac();
    double GetAlti_SetAltiDepot(const double &zdepot);
    double GetAltiRef();  // 2022
    
    void SetNumVoisinJusteAuDessusEau(const int &i); //2023
    int GetNumVoisinJusteAuDessusEau(); //2023
    void SetNumVoisinDeuxiemeJusteAuDessusEau(const int &i); //2023
    int GetNumVoisinDeuxiemeJusteAuDessusEau(); //2023
    
    void Ajuste_ZEau_a_Z();

    bool GetPasBord();
    void SetBord();
    void SetPasBord();
    BORD GetTypeBord();
    void SetTypeBord(BORD quel_bord);
    double GetBaseBord();
    void SetBaseBord(const double &nv_nivbase);
    bool TesteBordFluxNul();
    bool TesteBordAltiFixe();

    void InitialiseLeVoisinage();
    void InitialiseLeVoisinageEau(); // 30 juin 2008
    void InitialiseLeVoisinageDiff();
    int GetNumVoisin(const int &i);
    void SetNumVoisin(const int &i, const int &nv_num);
    int GetNumVoisinLateral(const int &i, const int &num_voisin_lateral); // 7 aout 2008
    void SetNumVoisinLateral(const int &i, const int &num_voisin_lateral, const int &nv_num); // 7 aout 2008
    int GetNbVoisinsBas();
    int GetDirectionVoisinsBas(const int &i); // 7 aout 2008
    int GetDirectionVoisinsBasEau(const int &i); // 2022
    int GetNbVoisinsBasEau(); // 30 juin 2008
    void IncrementeNbVoisinsBas();
    int GetNumVoisinBas();
    int GetNumVoisinBas(const int &i);
    int GetNumVoisinBasEau(const int &i); // 30 juin 2008
    double GetPas3VoisinBas(const int &i);
    double GetPente(const int &i);
    double GetPenteMax();
    double GetCoeff(const int &i);
    double GetCoeffEau(const int &i); // 30 juin 2008
    Tvoisinage_bas* GetVoisinageBas();
    Tvoisinage_bas* GetVoisinageBasEau(); // 30 juin 2008
    int GetNbVoisinsDiff();

    double GetStockInitSed();
    void SetStockInitSed(const double &nv_vol);

    TCouche** GetPileStrati();
    void SetPileStrati(TCouche** cettepile);
    void SetEpaisseur(const int &numc, const double &epaiss);
    double GetEpaisseur(const int &numc);
    void SetIndiceLitho(const int &numc, const int &nv_ind);
    void SetCouche(const int &numc, const double &epaiss, const int &nv_ind);
    int GetIndiceLitho(const int &numc);

    bool PlusBas(CMaille* autremaille);
    bool PlusHaut(CMaille* autremaille);
    bool PlusHaut_Eau(CMaille* autremaille); //30 juin 2008
    bool MailleLac();
    bool MailleLacHeau(); //2022 la definition d'une maille lac est un peu differente ici puisqu'on ne cherche
    // pas un exutoire. Dans le cas où on propage l'eau  courante sur une surface d'eau libre (via le calcul
    // de la hauteur d'eau), les trous se remplissent progressivement; une maille "lac" est une maille où z_ref>z

    void SetCoordonnees(float x, float y);
    TCoord GetCoordonnees();
    void GetCoordonnees3D(TCoord &xy, float &alti);

    void AddVoisinBas(const int &i, const int &ind, const int &direction, const double &pas3, // modif 7 aout 2008
            const double &pente);
    void AddVoisinBas(const int &i, const int &ind, const int &direction, const double &pas3, // modif 7 aout 2008
            const double &pente, const double &pond);
    void PondereVoisinsBas(const double &somme);
    void PondereVoisinsBas_Uniforme();

    void AddVoisinBasEau(const int &i, const int &ind, const int &direction, const double &pas3,
            const double &pente, const double &pond, const double &alti); // modif 2022 2023
    void PondereVoisinsBasEau(const double &somme);
    void PondereVoisinsBas_UniformeEau();
    void SetAltiVoisinJusteAuDessusEau(const double &alti); // 2022
    double GetAltiVoisinJusteAuDessusEau(); // 2022
    void SetMinUpstreamSlope(const double &slope); // 2022
    void SetMinUpstreamSlopePas(const double &pas); // 2022

    void SetAltiVoisinLePlusBasEau(const double &alti); // 2023
    double GetAltiVoisinLePlusBasEau(); // 2023
    void SetAltiMeanVoisinEau(const double &alti); // 2023
    double GetAltiMeanVoisinEau(); // 2023

    //==================
    // Autres fonctions
    //==================

    void AllouePileCouches(int nb);

    void AlloueVoisinageMultiple();
    void AlloueVoisinageMultipleEau(); // 30 juin 2008

    void BordDeLac(GrilleInt* ptr, const int &numlac);
    void BordDeLac_ILE(GrilleInt* ptr, const int &numlac);
};




  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

//---------------------------
//	Op�rations sur nummaille
//---------------------------
inline int CMaille::GetNumMaille() {return nummaille;}

inline bool CMaille::SetNumMaille(const int &nv_num) {nummaille=nv_num; return true;}

//-------------------------
//	Op�rations sur Alti...
//-------------------------
inline double CMaille::GetAlti() {return z;}

inline void CMaille::SetAlti(const double &Alti) {z=Alti;}

inline double CMaille::GetAltiInit() {return z_init;}

inline double CMaille::GetAltiRef() {return z_ref;} //2022

inline void CMaille::SetAltiInit(const double &Alti) {z_init=Alti;}

inline double CMaille::GetAltiEau() {return z_eau;}

inline void CMaille::SetAltiEau(const double &Alti) {z_eau=Alti;}

inline void CMaille::SetAltiRef(const double &Alti) {z_ref=Alti;} // 2022

inline double CMaille::SetAltiEau_GetVolumeLac(const double &Alti)
{z_eau=Alti; return (z_eau-z);}

inline double CMaille::GetAlti(ALTI TypeZ) 
{
	switch (TypeZ) {
		case Zreel :
			return z;
		case Zinit :
			return z_init;
		case Zeau :
			return z_eau;
        case Zref :
            return z_ref;
		default :
			return -EPAISSEUR_INF;
	}
}

inline void CMaille::SetAlti(ALTI TypeZ, double Alti)
{
	switch (TypeZ) {
		case Zreel :
			z = Alti;
			break;
		case Zinit :
			z_init = Alti;
			break;
		case Zeau :
			z_eau = Alti;
            break;
        case Zref :
            z_ref = Alti;
            break;

	}
}

inline void CMaille::InitialiseLesAlti(const double &Alti) {z = Alti; z_init = Alti; z_eau = Alti; z_ref = Alti;}

inline void CMaille::SetLesAlti(const double &Alti) {z = Alti; z_eau = Alti;}

inline void CMaille::AddAlti(const double &deltaZ) {z += deltaZ;}


//inline void CMaille::AddLesAlti(const double &deltaZ) {z += deltaZ; z_eau = z;}
inline void CMaille::AddLesAlti(const double &deltaZ)
{
    z += deltaZ;
    z_eau += deltaZ;
    z_ref += deltaZ;


} // modif 2022


//##### Modif le 15/05/2006 : on GARDE l'eau des lacs !!!
inline void CMaille::AddLesAltiLac(const double &deltaZ) {z += deltaZ; z_eau += deltaZ;}

inline void CMaille::AddLesAltiHeau(const double &deltaZ) // 2023
{
    z += deltaZ;
    if (z > z_eau) {z_eau = z; z_ref = z;}
    else if (deltaZ<0.) {z_eau += deltaZ; z_ref += deltaZ;}
    else {z_ref += deltaZ;}
    // sinon si deltaZ est positif mais pas assez pour que z depasse z_eau on ne fait rien
    
}

inline double CMaille::GetVolumeLac() {return (z_eau-z);}

inline double CMaille::GetAlti_SetAltiDepot(const double &zdepot)
{
	double OldZ = z;
	z = zdepot;
//##### Modif le 15/05/2006 : incr�mente le niveau du lac
	z_eau += zdepot-OldZ;
	return OldZ;
}

inline void CMaille::Ajuste_ZEau_a_Z() {if (z_eau > z) z_eau = z;}

//---------------------------
//	Op�rations sur les bords
//---------------------------
inline bool CMaille::GetPasBord() {return PasBord;}

inline void CMaille::SetBord() {PasBord = false;}

inline void CMaille::SetPasBord() {PasBord = true;}

inline BORD CMaille::GetTypeBord() {return type_bord;}

inline void CMaille::SetTypeBord(BORD quel_bord) {type_bord=quel_bord;}

inline double CMaille::GetBaseBord() {return nivbase;}

inline void CMaille::SetBaseBord(const double &nv_nivbase) {nivbase=nv_nivbase;}

inline bool CMaille::TesteBordFluxNul() {return (type_bord==flux_nul);}

inline bool CMaille::TesteBordAltiFixe() {return (type_bord==alti_fixe);}

//-----------------------------
//	Op�rations sur les voisins
//-----------------------------
inline void CMaille::InitialiseLeVoisinage() {voisins_bas->nb = 0;} 

inline void CMaille::InitialiseLeVoisinageEau() {voisins_bas_eau->nb = 0;} // 30 juin 2008

inline int CMaille::GetNumVoisin(const int &i) {return voisin[i];}

inline void CMaille::SetNumVoisin(const int &i, const int &nv_num) {voisin[i-1] = nv_num;}

inline int CMaille::GetNumVoisinLateral(const int &i, const int &num_voisin_lateral) {return VoisinLateral[i][num_voisin_lateral];} // 7 aout 2008

inline void CMaille::SetNumVoisinLateral(const int &i, const int &num_voisin_lateral, const int &nv_num) {VoisinLateral[i-1][num_voisin_lateral-1] = nv_num ;} // 7 aout 2008

inline int CMaille::GetNbVoisinsBas() {return voisins_bas->nb;}

inline int CMaille::GetDirectionVoisinsBas(const int &i) {return voisins_bas->direction[i];} // 7 aout 2008

inline int CMaille::GetDirectionVoisinsBasEau(const int &i) {return voisins_bas_eau->direction[i];} // 2022

inline int CMaille::GetNbVoisinsBasEau() {return voisins_bas_eau->nb;} // 30 juin 2008

inline void CMaille::IncrementeNbVoisinsBas() {voisins_bas->nb++;}

inline int CMaille::GetNumVoisinBas() {return voisins_bas->ind[0];}

inline int CMaille::GetNumVoisinBas(const int &i) {return voisins_bas->ind[i];}

inline int CMaille::GetNumVoisinBasEau(const int &i) {return voisins_bas_eau->ind[i];}  // 30 juin 2008

inline double CMaille::GetPas3VoisinBas(const int &i) {return voisins_bas->pas3[i];}

inline double CMaille::GetPente(const int &i) {return voisins_bas->pente[i];}

inline double CMaille::GetPenteMax()
{
	double pentemax=-9999.;
	double penteloc;
	for (int i=0; i < voisins_bas->nb; i++)
	{
		penteloc = voisins_bas->pente[i];
		if ( penteloc > pentemax)
			pentemax = penteloc;
	}
	return pentemax;
}

inline double CMaille::GetCoeff(const int &i) {return voisins_bas->coeff[i];}

inline double CMaille::GetCoeffEau(const int &i) {return voisins_bas_eau->coeff[i];} // 30 juin 2008

inline Tvoisinage_bas* CMaille::GetVoisinageBas() {return voisins_bas;}

inline Tvoisinage_bas* CMaille::GetVoisinageBasEau() {return voisins_bas_eau;} // 30 juin 2008

//------------------------------
//	Op�rations sur StockInitSed
//------------------------------
inline double CMaille::GetStockInitSed() {return StockInitSed;}

inline void CMaille::SetStockInitSed(const double &nv_vol) {StockInitSed=nv_vol;}

//-----------------------------
//	Op�rations sur PileCouches
//-----------------------------
inline TCouche** CMaille::GetPileStrati() {return PileCouches;}

inline void CMaille::SetPileStrati(TCouche** cettepile) {PileCouches = cettepile;}

inline void CMaille::SetEpaisseur(const int &numc, const double &epaiss)
{
	PileCouches[numc]->epaisseur = epaiss;
}

inline double CMaille::GetEpaisseur(const int &numc) {return PileCouches[numc]->epaisseur;}

inline void CMaille::SetIndiceLitho(const int &numc, const int &nv_ind)
{
	PileCouches[numc]->ind_litho = nv_ind;
}

inline void CMaille::SetCouche(const int &numc, const double &epaiss, const int &nv_ind)
{
	PileCouches[numc]->epaisseur = epaiss;
	PileCouches[numc]->ind_litho = nv_ind;
}

inline int CMaille::GetIndiceLitho(const int &numc) {return PileCouches[numc]->ind_litho;}

//-------------------------------------
//	Op�rations de comparaison (d'alti)
//-------------------------------------
inline bool CMaille::PlusBas(CMaille* autremaille) {return (z_eau < autremaille->z_eau);}

inline bool CMaille::PlusHaut(CMaille* autremaille) {return (z > autremaille->z);}

inline bool CMaille::PlusHaut_Eau(CMaille* autremaille) {return (z_eau > autremaille->z_eau);}

inline bool CMaille::MailleLac() {return (z_eau > z);}

inline bool CMaille::MailleLacHeau() {return (z_ref > z);} // 2022

//-----------------------------
//	Op�rations sur Coordonn�es
//-----------------------------
inline void CMaille::SetCoordonnees(float nv_x, float nv_y) {coord.x=nv_x; coord.y=nv_y;}

inline TCoord CMaille::GetCoordonnees() {return coord;}

inline void CMaille::GetCoordonnees3D(TCoord &xy, float &alti)
{
	xy = coord;
	alti = (float) z;
}

//-------------------------
//	Op�rations sur Voisins
//-------------------------
inline void CMaille::AddVoisinBas(const int &i, const int &ind, const int &direction, const double &pas3, // modif 7 aout 2008
								  const double &pente)
{
	voisins_bas->nb = i+1;
	voisins_bas->ind[i]=ind;
	voisins_bas->direction[i]=direction + 1 ; // ajout 7 aout 2008
	voisins_bas->pas3[i]=pas3;
	voisins_bas->pente[i]=pente;
}

inline void CMaille::AddVoisinBas(const int &i, const int &ind, const int &direction, const double &pas3, // modif 7 aout 2008
								  const double &pente, const double &coeff)
{
	voisins_bas->nb = i+1;
	voisins_bas->ind[i]=ind;
	voisins_bas->direction[i]=direction + 1; // ajout 7 aout 2008
	voisins_bas->pas3[i]=pas3;
	voisins_bas->pente[i]=pente;
	voisins_bas->coeff[i]=coeff;
}

inline void CMaille::PondereVoisinsBas(const double &somme)
{
	double pentemax = -9999.;
	for(int i=0; i < voisins_bas->nb; i++)
	{
		voisins_bas->coeff[i] /= somme;
		voisins_bas->pentemoyenne = somme / double(voisins_bas->nb) ;  // 30 juin 2008
		if (voisins_bas->pente[i] > pentemax) 
		{
			voisins_bas->pentemax = voisins_bas->pente[i];
			pentemax = 	voisins_bas->pente[i];
		}
	}
}

inline void CMaille::PondereVoisinsBas_Uniforme()
{
	double unsursomme = 1./((double) voisins_bas->nb);
	double pentemax = -9999;
	for(int i=0; i < voisins_bas->nb; i++)
	{
		voisins_bas->coeff[i] = unsursomme;
		voisins_bas->pentemoyenne = 1. ;  // 30 juin 2008. Cas n'apparait que dans le cas bord_libre pour lequel les pentes avals ne sont pas calculee
										  // La valeur de 1. est arbitraire.
		if (voisins_bas->pente[i] > pentemax) 
		{
			voisins_bas->pentemax = voisins_bas->pente[i];
			pentemax = 	voisins_bas->pente[i];
		}
	}
}

// idem pour les voisins bas le long de la surface de l'eau  30 juin 2008

inline void CMaille::AddVoisinBasEau(const int &i, const int &ind, const int &direction, const double &pas3,
								  const double &pente, const double &coeff, const double &alti) // 30 juin 2008 modif 2022
{
	voisins_bas_eau->nb = i+1;
	voisins_bas_eau->ind[i]=ind;
    voisins_bas_eau->direction[i]=direction + 1 ; // 2022
	voisins_bas_eau->pas3[i]=pas3;
	voisins_bas_eau->pente[i]=pente;
	voisins_bas_eau->coeff[i]=coeff;
    voisins_bas_eau->alti[i]=alti; // 2023
}



inline void CMaille::SetMinUpstreamSlope(const double &slope) // 2022
{
    voisins_bas_eau->minupstreamslope =slope;
}

inline void CMaille::SetMinUpstreamSlopePas(const double &pas) // 2022
{
    voisins_bas_eau->minupstreamslopepas = pas;
}


inline void CMaille::SetAltiVoisinJusteAuDessusEau(const double &alti) // 2022
{
    alti_voisin_justeaudessus_eau=alti;
}

inline double CMaille::GetAltiVoisinJusteAuDessusEau() // 2022
{
    return alti_voisin_justeaudessus_eau;
}


inline void CMaille::PondereVoisinsBasEau(const double &somme) // 30 juin 2008
{
    double pentemax = -9999;
    double altimin = 1.E5; // 2023
    int imin = 0; // 2023
	for(int i=0; i < voisins_bas_eau->nb; i++)
	{
		voisins_bas_eau->coeff[i] /= somme;
        if (voisins_bas_eau->pente[i] > pentemax) // ajout 2022
        {
            voisins_bas_eau->pentemax = voisins_bas_eau->pente[i];
            pentemax = voisins_bas_eau->pente[i];
        }
        if (voisins_bas_eau->alti[i] < altimin) {altimin=voisins_bas_eau->alti[i]; imin = i;} // 2023 
	}
    voisins_bas_eau->altilaplusbasse = altimin;
    voisins_bas_eau->directionaltilaplusbasse = imin;
      
}


inline void CMaille::PondereVoisinsBas_UniformeEau() // 30 juin 2008
{
	double unsursomme = 1./((double) voisins_bas_eau->nb);
    double pentemax = -9999;
    double altimin = 1.E5; // 2023
	for(int i=0; i < voisins_bas_eau->nb; i++)
	{
		voisins_bas_eau->coeff[i] = unsursomme;
		// voisins_bas_eau->pentemoyenne = 1. ; // ne sert a rien
        if (voisins_bas_eau->pente[i] > pentemax) // ajout 2022
        {
            voisins_bas_eau->pentemax = voisins_bas_eau->pente[i];
            pentemax =     voisins_bas_eau->pente[i];
        }
        if (voisins_bas_eau->alti[i] < altimin) {altilaplusbasse = altimin;} // 2023 sans doute pas utile finalement, a verifier si on garde
	}
}



inline void CMaille::SetNumVoisinJusteAuDessusEau(const int &i) //2023
{
    NumVoisinJusteAuDessusEau = i;
}

inline int CMaille::GetNumVoisinJusteAuDessusEau() //2023
{
    return NumVoisinJusteAuDessusEau;
}

inline void CMaille::SetNumVoisinDeuxiemeJusteAuDessusEau(const int &i) //2023
{
    NumVoisinDeuxiemeJusteAuDessusEau = i;
}

inline int CMaille::GetNumVoisinDeuxiemeJusteAuDessusEau() //2023
{
    return NumVoisinDeuxiemeJusteAuDessusEau;
}

inline void CMaille::SetAltiVoisinLePlusBasEau(const double &alti) // 2023
{
    AltiVoisinLePlusBasEau = alti;
}

inline double CMaille::GetAltiVoisinLePlusBasEau() // 2023
{
    return AltiVoisinLePlusBasEau;
}

inline void CMaille::SetAltiMeanVoisinEau(const double &alti) // 2023
{
    AltiMeanVoisinEau = alti;
}

inline double CMaille::GetAltiMeanVoisinEau() // 2023
{
    return AltiMeanVoisinEau;
}

#endif
