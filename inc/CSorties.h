#if !defined(LesSorties)
	#define LesSorties

#include <fstream> // pour les manipulations d'I/O sur file
#include <sstream> // pour les manipulations d'I/O sur string
#include <string>

#include "Ccalc_bilaneau.h"
#include "CFlux.h"
#include "CMaillage.h"
#include "CMorpho.h"
#include "Param_externes.h"
#include "Utilitaires.h"
#include "CCaillou.h"
//#include <windows.h> // a commenter pour gcc

using namespace std;

//========================
// Pointeurs de fonctions
//========================

typedef bool	(CMorpho::*PtrFctSauveDivers)(int num);
//typedef double	(*PtrFct_Capa)(const double &pente, const double &fluxeau,
//							  ParamErosion* param, const double &width, const double &shearstress); // modif 25 octobre 2007. Commente juin 2014

typedef double (*PtrFct_Detach)(const double &pente, const double &fluxeau,
ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress); // modif 25 octobre 2007. Juin 2014 : utilise pour bedrock et sediment

typedef double (*PtrFct_Width)(const double &fluxeau, ParamLargeur* largeur, const double &coteoctogone); // ajout 25 octobre 2007
typedef double (*PtrFct_ShearStress)(const double &pente, const double &fluxeau,
										ParamErosion* param, const double &width); // ajout 25 octobre 2007

//------------------------------------------------
//
//	classe CSorties :
//
//	gestion des sorties du calcul
//
//------------------------------------------------

class CSorties
{
//++++++++
protected:
//++++++++

    //===========================
    // Trucs qui servent souvent, qu'on r�cup�re au d�but du Mod�le et du Maillage
    //===========================

    int nbcol; // NX
    int nblig; // NY
    int nbmailles; // Nb de mailles dans les grilles
    double pas;
    double pas2;
    double* pas3;
    double unsurpas2; // =1/pas2
    double coteoctogone; // 25 octobre 2007
    int NumPeriod;

    CMaillage* MonMaillage; // Le maillage lui-m�me
    CFlux* MesFlux; // Les grilles de flux
    CBilanEau* MonBilanEau; // Le bilan hydro
    CModele* MonModele;

    double dt_denud; // Intervalle de sortie du taux de d�nudation
    double dt_bassin; // Intervalle de sortie des infos grille du bassin
    int ind_bassin; // Num�ro du dernier fichier de sortie de bassin
    string FichierDenud; // Nom du fichier de sortie des taux de d�nudation
    string RadicalBassin; // Radical du fichier de sortie des infos grille du bassin
    string FichierRestart; // Nom du fichier restart de SORTIE
    string FichierReprise; // Nom du fichier restart d'ENTREE
    string FichierFluxChimique; // Nom du fichier contenant les flux chimiques temporels
    string FichierMoyenneEcartType; // Nom du fichier contenant la repartition des clasts au cours du temps

    bool OptionZone; // Taux calcul�s sur une zone tecto particuli�re
    int numzone; // Numero de cette zone
    bool OptionMasque; // Taux calcul�s sur un masque
    string fich_masque; // Nom du fichier contenant la grille masque

    GrilleBool Masque; // Nom de la grille masque

    CMorpho* MaMorpho; // La classe de morphologie quantitative
    PtrFctSauveDivers MonSauveDivers; // fct qui sauve ou non les rivi�res principales
    // et les pentes max locales

    ParamErosion* incisSed; // Param�tres de transport des s�diments
    // NB: les coeffs sont actualis�s ICI � chaque pas de temps
    // mais pas dans le mod�le !!!!
    ParamLargeur* largeur; // Parametres de la largeur de la riviere 25 octobre 2007
    double width; // Largeur de la riviere 25 octobre 2007
    double shearstress; // Shear stress 25 octobre 2007

    PtrFct_Detach MonErosSedFluvPotentiel; // choix de l'�quation de transport - pour les s�diments

    PtrFct_Width MonCalculDeLargeurDecoulement; // choix du calcul de largeur, tenant compte du flux d'eau ou non (25 octobre 2007)
    PtrFct_ShearStress MonCalculDeShearStress; // choix du calcul ou non du Shear Stress en fonction des valeurs des parametres des lois d'erosion.
    // Par exemple, si Tauc=0 alors on calcule directement la capa transport en fonction de q et S sans
    // passer par le calcul du shear stress, pour gagner du temps de calcul. (25 octobre 2007)
    //=====================
    // Variables courantes du calcul
    //=====================

    double temps_denud; // temps �coul� depuis la derni�re sortie de d�nudation
    double temps_bassin; // temps �coul� depuis la derni�re sortie de bassin
    ofstream FichierSortieDenud; // flux de sortie du taux de denudation
    ofstream FichierSortieFluxChimique; // flux de sortie des flux chimiques
    ofstream FichierSortieMoyenneEcartType; //evolution des distances entre les clasts

    bool sortieFluxChimique; // test si le temps correspond la sortie de valeurs des flux chimiques, en meme temps que l'ecriture des autres parametres moyens
    bool sortieGrillesCailloux; // test si le temps correspond la sortie des grilles et listes de cailloux

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CSorties(CModele* modele, CMaillage* maillage, CFlux* flux, CBilanEau* bileau);
    ~CSorties();

    //===========
    // fonctions
    //===========

    void InitialiseSorties();
    void InitialiseSortiesCOSMO(); // 2023
    void InitialiseSortiesCailloux();
    void InitialiseSortiesCaillouxWithoutDissolution();
    void InitialiseSorties_Zone(ParamTecto tecto);
    void InitialiseSorties_Masque();
    void FinitSorties();
    void FinitSortiesCailloux();
    void FinitSortiesCaillouxWithoutDissolution();
    void ActualiseNumPeriod(const int &NumPeriod);


    bool Incremente(const double &temps, const double &dt,
            const int &NbEffondrements, const int &NbLacs);
    bool IncrementeBIN(const double &temps, const double &dt,
            const int &NbEffondrements, const int &NbLacs, const vector<CCaillou> & listeCaillou); // modif 2023
    bool IncrementeBINCOSMO(const double &temps, const double &dt,
            const int &NbEffondrements, const int &NbLacs, const vector<CCaillou> & listeCaillou); // modif 2023
    bool Incremente_Masque(const double &temps, const double &dt,
            const int &NbEffondrements, const int &NbLacs, const vector<CCaillou> & listeCaillou); // modif 2023
    bool IncrementeBIN_Masque(const double &temps, const double &dt,
            const int &NbEffondrements, const int &NbLacs, const vector<CCaillou> & listeCaillou); // modif 2023


    bool EcritRestart(const double &temps);
    bool Reprise(double &temps);

    void EcritListeCailloux(const int &temps, const vector<CCaillou> & listeCaillou); //Ajout Pierre 
    void MoyenneEcartTypeCailloux(const double &temps, const vector<CCaillou> & listeCaillou); //Ajout Pierre
    void FluxChimique(CModele *modele, const vector<CCaillou> & listeCaillou, const double &dt, const double &temps); // Ajout Pierre
    void FluxChimiqueIntegre(CModele *modele, const vector<CCaillou> & listeCaillou, const double &dt, const double &temps);
    void FluxChimiqueIntegreTEST(CModele *modele, const vector<CCaillou> & listeCaillou, const double &dt, const double &temps);

    void TriRapideOptimal_deCailloux(vector<CCaillou> & listecaillouxsurmaille, int Liste[], const int &nbcailloux);
    void FluxCations(double &Ca, double &Na, double &Si, double &K, double &Mg, double &S, string NomMineral, const double &dissoutparmineral);
    
    void NeFaitRien(CModele *modele, const vector<CCaillou> & listeCaillou, const double &dt, const double &temps);
	
};


  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////


#endif
