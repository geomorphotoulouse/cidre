#if !defined(LeCalculDesEffondrements)
    #define LeCalculDesEffondrements

#include "CMaillage.h"
#include "CFlux.h"
#include "Ccalc_pentes.h"

//========================
// Pointeurs de fonctions
//========================

typedef double (CMaille::*PtrFctAltiMaille)();
typedef bool	(*PtrFct_CalcPentesMaille)(CMaille* MaMaille, CMaillage* MonMaillage,
		PtrFctAltiMaille MonGetAlti, double pas[8]);
typedef int	(CMaillage::*PtrFct_PlusBas)(CMaille* MaMaille);
typedef void	(CFlux::*PtrFct_StockSedEffondr)(const int &i, const double &vol_falaise);
typedef void	(CFlux::*PtrFct_ActuStrati)(const int &i, CMaille* maille, double volume_erode);
//typedef void	(CFlux::*PtrFct_EvacueSed)(const int &nb_effondrements);


//------------------------------------------------
//
//	classe CEffondr :
//
//	calcul des effondrements 
//
//------------------------------------------------

class CEffondr
{
//++++++++
protected:
//++++++++

    //===========================
    // Trucs qui servent souvent, qu'on r�cup�re au d�but du Maillage
    //===========================

    int nbmailles;
    double* pas3;
    double pas2;
    ParamEffondr effondr;
    double temps_critique; // dur�e � partir de laquelle on remet le compteur-temps
    // des effondrements � 0

    CMaillage* MonMaillage; // Le maillage lui-m�me
    CFlux* MesFlux; // Les grilles de flux

    //===========================================
    // Fonctions d�pendant des options du mod�le
    //===========================================

    PtrFct_CalcPentesMaille MonCalcPentes_Maille;
    PtrFct_PlusBas MonPlusBasDesBas;
    PtrFct_StockSedEffondr MonAjusteStockSed;
    PtrFctAltiMaille MonGetAlti;
    PtrFct_ActuStrati MonActualiseStrati;


    //===========
    // variables
    //===========

    GrilleBool OuiNon; // Vaut TRUE pour les mailles tomb�es / �cras�es
    int NbMaillesTombees; // Nombre de mailles effondr�es
    int* NumMaillesTombees; // Num�ros des mailles effondr�es
    double* VolumeMoins; // Volume de s�diments perdus par effondrement
    int NbMaillesEcrasees; // Nombre de mailles �cras�es par un effondrement
    int* NumMaillesEcrasees; // Num�ros des mailles �cras�es
    double* VolumePlus; // Volume de s�diments re�us par effondrement
    double temps_effondr; // temps �coul� depuis le dernier effondrement

    //+++++
public:
    //+++++

    //===========================
    // Constructeur, destructeur
    //===========================

    CEffondr(CModele* modele, CMaillage* maillage, CFlux* flux);
    ~CEffondr();

    //==================
    // Autres fonctions
    //==================

    void InitialiseEffondr();
    void ActualiseEffondre();
    void ActualiseEffondre_ILE();
    void ActualiseEffondre_TransportInstantane();
    void ActualiseEffondre_TransportInstantane_ILE();

    //------------------------------------------------
    //	test et calcul de l'effondrement d'une maille
    //	avec ou sans "broyage" du talus selon l'option
    //------------------------------------------------

    bool EffondreSurLeVoisin(CMaille* MaMaille, const int &numtombe,
            const int &dirvoisin);
    bool Effondrement(const int &i);

    bool EffondreSurLeVoisin_ILE(CMaille* MaMaille, const int &numtombe,
            const int &dirvoisin);
    bool Effondrement_ILE(const int &i);

    //---------------------------------------------------------
    //	calculer des pentes/effondrements pour toute une grille
    //---------------------------------------------------------

    int EffondreGrille(const double &dt);
    int EffondreGrille_ILE(const double &dt);
    int EffondreGrille_TERRE(const double &dt);
    int EffondreGrille_TransportInstantane(const double &dt);
    int EffondreGrille_TransportInstantane_ILE(const double &dt);
    int EffondreGrille_TransportInstantane_TERRE(const double &dt);

    int PenteSansEffondr(const double &dt);
    int PenteSansEffondr_TERRE(const double &dt);


    void AfficheEffondrement();
};

#endif
