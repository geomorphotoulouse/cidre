#if !defined(LesOptions)
#define LesOptions
#include <math.h> // 30 juin 2008
#include <string>

//#define mymin(a,b) ((a) < (b) ? (a) : (b))
//#define mymax(a,b) ((a) > (b) ? (a) : (b))
//#define dabs(a) ((a) > (0.) ? (a) : (-a))



//////////////////////
// D�finition de types
//////////////////////

typedef double* GrilleDble;
typedef int* GrilleInt;
typedef bool* GrilleBool;

////////////////////////////
// Constantes 'universelles'
////////////////////////////

const double PI = 3.14159265358979;


////////////////////////////////////////////////
// Options de calcul, de m�thodes, de limites...
////////////////////////////////////////////////
// Facteur de conversion pour passer des secondes aux ans.
const double UNANENSECONDES = 365. * 24. * 3600.;

const double PENTE_MIN = 0.000005;
// Limitation de la taille de la GrilleDble de calcul
const int NLIG_MAX = 10000;
const int NCOL_MAX = 10000;
// Limitation de la taille de la maille
const double GRAND = 1000.;
// Limitation du nombre de p�riodes
const int NPER_MAX = 50;
// Limitation du temps de calcul total
const double TCALCUL_MAX = 100.e6;
// Limitation du pas de temps de calcul
const double DTCALCUL_MIN = 1. / UNANENSECONDES;
const double DTCALCUL_MAX = 10000.;
// Limitation du nombre de lithologies
const int NLITHO_MAX = 4;
// Limitation du nombre de couches strati en un point
const int NSTRATI_MAX = 10;
// Epaisseur "grande" par d�faut (�tre s�r qu'on n'�rodera pas tout !)
const double EPAISSEUR_INF = 5.e15;
// Limitation des precipitations
const double PRECIP_MAX = 10.;
// Limitation du soul�vement
const double TXUPLIFT_MAX = 1.;
// Limitation du d�crochement
const double TXDECRO_MAX = 1.;
// Limitation du nombre de sorties de grilles
const int NSORTIES_MAX = 200;

// Limitation de la taille du lac (en fait, p�rim�tre)
const int TAILLEMAX_LAC = 1000;

// Coefficients pour la diffusion non-lin�aire bidule
const double ALPHA = 0.999;
const double SCSQ = ALPHA*ALPHA;
const double SCSQ3 = (1. + SCSQ) / ((1. - SCSQ)*(1. - SCSQ));

// Altitude "infinie" (bord flux nul)
const double TRES_HAUT = 10000.;
// Altitude maximale admise
const double ALTI_MAX = 10000.;
// Altitude minimale admise
const double ALTI_MIN = -10000.;

// Longueur de cha�ne de caract�res
const int LONG_STRING = 50;

// Nombre maximal de rivi�res
const int NRIVIERES_MAX = 10;

// Facteur de multiplication du volume d'une falaise qui s'effondre,
// de mani�re � ce qu'en cas de broyage, on n'ait pas un plat ensuite
const double PRESQUE = 0.995;

// Limite inferieure imposee a x pour le calcul de exp(x)
const double MOINS_EXP_MIN = -20.;

// Densite de l'eau kg/m3
const double DENSITE = 1000.; // ajout le 11 mars 2008

// Gravite m/s-2
const double GRAVITE = 9.81; // ajout le 11 mars 2008

// rugosite Manning ("n")
//const double NMANNING = 0.03 / UNANENSECONDES; // 30 juin 2008  // modif 2022 rentre dans inputcidre.txt

// Energie d'activation pour le calcul de la cinetique d'alteration
const double NRJACTIV = 48200.; // 01/02/11 cf West 2012 Geology. et Brantley 2003

// Temperature de reference en Kelvin  pour le calcul de la cinetique d'alteration 
const double TEMPERATUREREFERENCE = 298.15; // 01/2016

// Constante des gazs parfaits pour le calcul de la cinetique d'alteration
const double RGAZPARFAIT = 8.314; //  01/03/11


// Options rajoutées pour production de cosmonucleide youssouf 25/10/21
const double Lambda10Be = 4.99E-7;          // Constante de desintegration (10Be)
const double Lambda14C  = 1.2E-4;          // Constante de desintegration (C14)
const double Lambda26Al = 9.67E-7;   // Constante de desintegration (26Al)
const double Lambda21Ne = 0.;            // pas de desintegration

/*const  double ContribNeutron = 0.9785;   // Contribution des Neutrons // 2023 doublon avec FracNeutronSLHL ...
const  double ContribSlowMuon = 0.015;   // Contribution des Muons stop
const  double ContribFastMuon = 0.0065;  // Contribution des Muons rapides */

const  double AttenuationNeutron = 150.E4;      // Attenuation des Neutrons (g/m2)
const  double AttenuationSlowMuon = 1500.E4;    // Attenuation des Muons stop (g/m2)
const  double AttenuationFastMuon = 4320.E4;    // Attenuation des Muons rapides (g/m2) cf Braucher et al 2003; 2011

//----------------------
// Coeff de Stone 2000 a latitude de 45deg
//----------------------
const double aStone = 56.7733;
const double bStone = 649.1343;
const double cStone = -0.160859;
const double dStone = 0.00015463;
const double eStone = -5.0330E-8;
const double MStone = 0.933; // obsolete
//----------------------
// Coeff de Stone 2000 a latitude de 50 a 60 deg //----------------------
//const double aStone= 69.0720;
//const double bStone= 832.4566;
//const double cStone= -0.199252;
//const double dStone= 1.9391E-4;
//const double eStone= -6.3653E-8;
//const double MStone= 1.;

//const double M = 0.028; // at/g/a taux prod muon SLHL

const double FracNeutronSLHL =  0.9886; // fraction taux prod due aux neutrons SLHL Braucher et al 11 (0.98% of the total production at SLHL) 2023 QUELLE VALEUR ET POURQUOI DIFF DE ContribNeutron ??
const double FracSlowMuonSLHL =  0.0027; // fraction taux prod due aux slow muons SLHL Braucher et al 11 (0.27% of the total production at SLHL) 2023
const double FracFastMuonSLHL =  0.0087; // fraction taux prod due aux  fast muons SLHL Braucher et al 11 (0.87% of the total production at SLHL) 2023

const double ProductionRateSeeLevel14C = 18.6; // at/g/a Pour le 14C
const double ProductionRateSeeLevel10Be = 4.; // at/g/a total prod rate pour le 10Be d'apres Martin et al 17
//const double ProductionRateSeeLevel21Ne= 20.; // at/g/a Pour le 21Ne
const double ProductionRateSeeLevel21Ne = 16.48 ;//4.12*4 at/g/a Pour le 21Ne (Vincent)
//const double ProductionRateSeeLevel26Al = 4.; // at/g/a Pour le 10Be d'apres Martin et al 17
const double ProductionRateSeeLevel26Al = 27.; // 6.5*4 at/g/a Pour le 10Be d'apres  (Vincent)

const  double rho=2700000.;  // densite en g/m3 pour etre homogene avec les longeur d'attenuation

// Fin ajout du 25/10/21

//-----------------------------------------------------------------
//	FORMATFICHIER type enumere : donne le format (ascii ou binaire)
//-----------------------------------------------------------------

enum FORMATFICHIER 
{
    ascii, binary
};

//----------------------------------------------------------------
//	MODE type enumere : donne le mode de construction d'une grille
//----------------------------------------------------------------

enum MODE 
{
    nothing, value, file, stochastique
};

//----------------------------------------------------------------
//	PROBA type enumere : donne le type de loi de probabilite pour le climat stochastique
//----------------------------------------------------------------
// climat stochastique

enum PROBA 
{
    pasdeloi, powerlaw, heavytail
};

//--------------------------------------------------------------
//	BORD type enumere : donne le type de bord (condition limite) 
//--------------------------------------------------------------

enum BORD 
{
    // pas_bord, sym_ax, alti_fixe, flux_nul, boucle, bord_libre
    pas_bord, alti_fixe, flux_nul, boucle // modif 2023
};

//--------------------
//	LAC type enumere :
//--------------------
//enum LAC {pas_lac,bord_lac, mil_lac,exutoire};

//---------------------------------------------
//	ALTI type enumere : donne l'altitude voulue 
//---------------------------------------------

enum ALTI 
{
    Zreel, Zinit, Zeau, Zref
};

//-----------------------------------------------------
//	PENTE type enumere : donne le type de pente voulu :
//		- multiple : vers les mailles les plus basses
//-----------------------------------------------------

enum PENTE 
{
    multiple
};

//------------------------------------------------
//	DIFF type enumere : donne le type de diffusion
//------------------------------------------------

enum DIFF 
{
    pas_diff, lineaire, non_lineaire
};

//----------------------------------------------------
//	UPLIFT type enumere : donne le type de soul�vement
//----------------------------------------------------

enum UPLIFT 
{
    creneau, gaussienne, croissant, decroissant
};

////////////////////////
// Param�tres par d�faut
////////////////////////



#endif
