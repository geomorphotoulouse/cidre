#if !defined(LeCalculDuBilanDEau)
	#define LeCalculDuBilanDEau

#include <vector>
#include "CMaillage.h"
#include "CFlux.h"
#include "Ccalc_pentes.h" // 30 juin 2008
#include "CWaterbot.h" // 2023

//========================
// Pointeurs de fonctions
//========================

typedef void	(CFlux::*PtrFct_Deverse)(const int &i, CMaille* maille);
typedef int	(CFlux::*PtrFct_Deverse_FilsLac)(const int &i, CMaille* maille, int* lenumvoisin);
typedef int	(CFlux::*PtrFct_Deverse_Exutoire)(const int &i, CMaille* maille,
		int* lenumvoisin, GrilleInt* ptr, const int &numlac);
typedef void	(CMaille::*PtrFct_BordDeLac)(GrilleInt* ptr, const int &numlac);
typedef void	(CMaillage::*PtrFct_SuivantBas)(CMaille* maille, GrilleInt* ptr, const int &numlac,
		int& isuivant, double& zsuivant);
typedef bool	(CMaillage::*PtrFct_SuivantPlat)(CMaille* maille, GrilleInt* ptr, const int &numlac,
		const double &zlac, int& isuivant);
typedef bool	(CMaillage::*PtrFct_TesteSiBas)(CMaille* maille, GrilleInt* ptr, const int &numlac,
		const double &z);
typedef double	(CFlux::*PtrFct_HauteurEau)(const int &i, CMaille* maille, const double &dt); // 30 juin 2008

//------------------------------------------------
//
//	classe CBilanEau :
//
//	calcul du bilan d'eau courante et des lacs
//
//------------------------------------------------

class CBilanEau
{
//++++++++
protected:
//++++++++

    //===========================
    // Trucs qui servent souvent, qu'on r�cup�re au d�but du Maillage et du Mod�le
    //===========================

    int nbmailles;

    double pas2;
    double* pas3; // 30 juin 2008

    CMaillage* MonMaillage; // Le maillage lui-m�me
    CFlux* MesFlux; // Les grilles de flux

    //===========================================
    // Fonctions d�pendant des options du mod�le
    //===========================================

    PtrFct_Deverse MonDeverse; // Fct qui d�verse l'eau sur les voisins
    PtrFct_Deverse_FilsLac MonDeverse_FilsLac; // Idem dans le cas d'une maille
    // qui re�oit de l'eau d'un lac
    PtrFct_Deverse_Exutoire MonDeverse_Exutoire; // Idem dans le cas d'une maille exutoire
    PtrFct_BordDeLac MonBordDeLac; // Fct qui annote les mailles en bord de lac
    PtrFct_SuivantBas MonSuivantBas; // Fct qui cherche le point le plus bas autour d'une maille
    PtrFct_SuivantPlat MonSuivantPlat; // Fct qui cherche un point � m�me alti autour d'une maille
    PtrFct_TesteSiBas MonTesteSiBas; // Fct qui regarde autour si on a un point plus bas
    PtrFct_HauteurEau MonCalculHauteurEau; // Fct qui calcule la hauteur d'eau sur la maille en utilisant une loi de friction 30 juin 2008
    
    PtrFctAltiMaille MonGetAlti; // 2023

    //===========
    // variables
    //===========

    int nblacs; // Nombre de lacs
    GrilleInt NumLac; // Tableau de num�ro des lacs, vaut :
    // # 10000		exutoire de lac (passe � 9999 une fois d�vers�)
    // # 1 � 9999	bord de lac (+Num�ro du lac)
    // # 0			hors-lac (au d�part, toute la grille)
    // # -1 � -9999	milieu de lac (-Num�ro du lac)
    // # -10000		"fils-lac" (re�oit de l'eau d'un lac - temporaire)
    GrilleInt* ptrNumLac; // le pointeur sur cette grille
    GrilleBool DejaPasse; // Tableau de bool�en indiquant si on est d�j� pass� ou non
    // par les mailles dans le calcul du bilan

    double dt; // Pas de temps de la p�riode en cours

    //===================
    // variables locales
    //===================

    vector <int> BordDuLac; // Num�ro des mailles qui sont au bord du (mais encore dans le) lac
    vector <int> MilieuDuLac; // Num�ro des mailles qui sont au milieu du lac
    int* tabnumvoisin; // Num�ros des voisins du point courant

    ofstream FichierLog; // Fichier log : note les lacs
    double tempslac; // Compteur

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CBilanEau(CModele* modele, CMaillage* maillage, CFlux* flux);
    ~CBilanEau();

    //==================
    // fonctions INLINE
    //==================

    int GetNumLac(int i);
    void ActualisePasDeTemps(double nv_dt);

    void ChercheSuivantLac(const int numlac, int& isuivant, double& zsuivant);
    bool ChercheSuivantPlat(const int numlac, const double zlac, int& isuivant);
    bool TesteSiExutoire(const int numlac, const int isuivant, const double zsuivant);

    //==================
    // Autres fonctions
    //==================

    int BilanEau();
    bool CalculeLac(const int numlac, const int i, CMaille* maille, int& redepart);

    int BilanEau_TERRE();

    int BilanEau_SansLacs();

    int BilanEau_SansLacs_CalculHeau(); // 2019

    int BilanEau_TERRE_SansLacs();

    int BilanEau_TERRE_SansLacs_CalculHeau(); // 30 juin 2008


    void AfficheMaillage();
};



  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

inline int CBilanEau::GetNumLac(int i) {return NumLac[i];}

inline void CBilanEau::ActualisePasDeTemps(double nv_dt) {dt = nv_dt;}

//----------------------------------
// Recherche le point suivant du LAC
//----------------------------------
// On part de la liste des bords et du milieu du lac.
// Renvoie le point suivant du lac avec son altitude (d'eau)
//
inline void CBilanEau::ChercheSuivantLac(const int numlac,
										 int& isuivant, double& zsuivant)
{
	CMaille* MaMaille;
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
////// On cherche le "suivant", le plus bas des points qui sont autour du lac
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	isuivant = -1;
	zsuivant = EPAISSEUR_INF;
	for (unsigned int k=0; k < BordDuLac.size(); k++)
	{
		MaMaille = MonMaillage->GetMaille(BordDuLac[k]);
		(MonMaillage->*MonSuivantBas)(MaMaille,ptrNumLac,numlac,isuivant,zsuivant);
	// Si la maille n'a aucun voisin tq : NumLac[numvoisin] != -numlac
	// --> on n'est pas au bord mais au milieu
		if (zsuivant == EPAISSEUR_INF)
		{
			MilieuDuLac.push_back(BordDuLac[k]);
			BordDuLac.erase(BordDuLac.begin()+k);
			k--;
		}
	}
	return;
}

//-----------------------------------
// Recherche le point suivant du PLAT
//-----------------------------------
// On part de la liste des bords et du milieu du plat.
// Renvoie le point suivant du plat avec son altitude
//
inline bool CBilanEau::ChercheSuivantPlat(const int numlac,
										  const double zlac, int& isuivant)
{
	CMaille* MaMaille;
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
////// On cherche le "suivant", un point autour du lac qui soit � la m�me alti
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	isuivant = -1;
	for (unsigned int k=0; k < BordDuLac.size(); k++)
	{
		MaMaille = MonMaillage->GetMaille(BordDuLac[k]);
		if ((MonMaillage->*MonSuivantPlat)(MaMaille,ptrNumLac,numlac,zlac,isuivant))
			return true;
	}
	// --> pas d'autre point appartenant au plat
	return false;
}

//----------------------------------------------------
// Teste si le point est un exutoire du LAC ou du PLAT
//----------------------------------------------------
//
inline bool CBilanEau::TesteSiExutoire(const int numlac,
									   const int isuivant, const double zsuivant)
{
	CMaille* MaMaille = MonMaillage->GetMaille(isuivant);
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
////// On regarde si le point n'est pas d�j� sous l'eau (cas d'une
////// maille appartenant d�j� � un lac)
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	if (MaMaille->MailleLac())
		return false;
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
////// On regarde s'il n'y a pas un point autour plus bas que zsuivant
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	if ((MonMaillage->*MonTesteSiBas)(MaMaille,ptrNumLac,numlac,zsuivant))
		return true;
	else
		return false;
}


#endif
