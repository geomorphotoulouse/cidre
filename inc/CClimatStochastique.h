#if !defined(LeClimatStochastique)
#define LeClimatStochastique

#include <math.h> // ajout pour climat stochastique
#include "CModele.h"
#include "CMaillage.h"
#include "Param_externes.h"
#include "CFlux.h"
using namespace std;

//------------------------------------------------
//
//	classe CClimatStochastique:
//
//	Hertitage de la classe Cflux (eau, s�diments...), gere le climat stochastique
//	-> actualisations
//	-> acc�s 
//
//------------------------------------------------

class CClimatStochastique : public CFlux 
{
//++++++++
protected:
    //++++++++

    //===========================
    // Trucs qui servent souvent, qu'on r�cup�re au d�but du Maillage et du Mod�le
    //===========================


    GrilleDble* TabPrecipStoch; // Liste des grilles de pr�cipitations CAS CLIMAT STOCHASTIQUE (une par  valeur de precipitation )
    // NB : Les pr�cipitations sont en VOLUME par unite de temps (donc *pas2 est d�j� fait)
    int nbValeurPrecipStoch; // Nombre de valeurs de precipitations dans la distribution des pr�cipitations CLIMAT STOCHASTIQUE	

    //===========
    // variables
    //===========


    int NumValeurPrecipStoch; // Numero de la valeurs de precipitations dans la distribution des pr�cipitations
    // imposee en entree (sur 50 valeurs) // CLIMAT STOCHASTIQUE
    TPluieStoch* Evenement; // Tableau des Evenements de precipitation de la distribution statistique imposee.
    // Les elements de ce tableau sont des type structure TPluieStoch et ont pour 
    // variables Precipitation, tempsretour et pasdetemps. // CLIMAT STOCHASTIQUE


    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CClimatStochastique(CMaillage* maillage, CModele* modele);
    ~CClimatStochastique();

    // fonctions

    //==================
    // fonctions INLINE
    //==================


    void ActualiseNumValeurPrecipStoch(const int &numvaleurprecipstoch);
    void AlloueGrillePrecipStoch();
    void SetGrillePrecipUniformeStoch(const int &numvaleurprecipstoch, const double &PrecipStochastique);
    void CalculSequencePluieStochPowerlaw(const int &numperiode, const double &precipmoyen, const double &variabilite);
    double GetPasTempsEvenementPluie(const int &numvaleurprecipstoch);
    double GetValeurEvenementPluie(const int &numvaleurprecipstoch);
    double GetTempsRetourEvenementPluie(const int &numvaleurprecipstoch);
    //==================
    // Autres fonctions
    //==================
    void InitFlux_RecuperePluie_NoSin_NoLin_Stoch(const double &temps);
    bool InitialisePrecipStoch(CModele* modele);
};


/////////////////////////////////////////////
////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////


//--------------------------------------------
//	Assignement du num�ro de valeur de precipitation de la distribution 
//  CAS DU CLIMAT STOCHASTIQUE
//--------------------------------------------

inline void CClimatStochastique::ActualiseNumValeurPrecipStoch(const int &numvaleurprecipstoch) 
{
    NumValeurPrecipStoch = numvaleurprecipstoch;
}

//-----------------------------------------------------------
// Allocation des grilles de pr�cipitations, une par p�riode et par valeur de precipitation
// CAS CLIMAT STOCHASTIQUE
//-----------------------------------------------------------

inline void CClimatStochastique::AlloueGrillePrecipStoch() 
{
    TabPrecipStoch = new GrilleDble[nbValeurPrecipStoch];
    for (int k = 0; k < nbValeurPrecipStoch; k++) 
    {
        TabPrecipStoch[k] = new double[nbmailles];
    }
    return;
}

//------------------------------------------------------------------------------
// Remplit la grille de pr�cipitations par une valeur uniforme, pour un
// pas de temps correspondant a l'une des valeurs de la distribution temporelle
// des pluie. 50 valeurs sont selectionnees dans la distribution. Ces valeurs determinent
// chacunes un  pas de temps en fonction du temps de retour de chaque valeur de precipitation.
// Le tableau de valeur TabPrecipStoch est rempli a chaque debut de periode. 
// CAS DU CLIMAT STOCHASTIQUE
//------------------------------------------------------------------------------

inline void CClimatStochastique::SetGrillePrecipUniformeStoch(const int &numvaleurprecipstoch, const double &PrecipStochastique) 
{
    for (int i = 0; i < nbmailles; i++) 
    {
        TabPrecipStoch[numvaleurprecipstoch][i] = PrecipStochastique*pas2;
    }
    return;
}


//------------------------------------------------------------------------------
// Calcule la sequence de precipitations pour un modele donne de  distribution  statistique et une periode donnee
// CLIMAT STOCHASTIQUE
//------------------------------------------------------------------------------

inline void CClimatStochastique::CalculSequencePluieStochPowerlaw(const int &numperiode, const double &precipmoyen, const double &variabilite) 
{
    // Principe de cette fonction :
    // On suppose qu'une chronique de pluie est divisee en pas de temps journaliers. La probabilite d'avoir
    // P> a une certaine valeur (probabilite de depassement) est supposee etre une loi de puissance du type 
    // N(P)=CP^(variabilite). C est choisi tel que N(P) tende vers 1 a l'infini et vers 0 en zero (une loi de proba !). 
    // On choisit C=Pmin^(variabilite). Pmin, la precipitation min de la distribution, est calculee en fonction
    // de la precipitation moyenne precipmoyen qui est elle-meme egale a la precipitation moyenne annuelle multipliee
    // par la fraction du temps de pluie (la precip moyenne de la distribution est calculee lors
    // de la lecture du fichier d'entree). precipmoyen = integrale de Pmin a Pmax de Pp(P)dP. Pmax
    // est fixe a 3 fois le record de pluie connu sur l'ile de la reunion. p(P), la probabilite d'avoir P
    // est calculee en derivant la fonction de repartition, elle-meme egale a 1-N(P). Il en ressort que
    // Pmin=((variabilite-1.)/variabilite) * precipmoyen.
    // Connaissant Pmin et Pmax, ce domaine est divise en 50 intervalles de logarithme constant, ce qui permet
    // d'avoir un echantillonnage plus serre pour les valeurs plus probables. Pour chaque valeur centrale
    // de chaque intervalle, son temps de retour est calcule par T = 1/N(P). Le temps de retour de l'evenement
    // le plus rare fixe la duree du "cycle" climatique "historique". Sur cette duree, on calcule le pas de temps
    // associe a chaque valeur de pluviometrie. Le pas de temps est la somme des durees d'evenement de pluviometrie donnee
    // sur la duree totale du cycle. Ainsi, le rapport entre les pas de temps respecte les rapports entre temps
    // de retour, et donc respecte la fonction de probabilite choisie.

    Evenement = new TPluieStoch[nbValeurPrecipStoch];
    double precipmax = 100.; // Valeur max fixee a environ 0.3  m/j, convertie en m/a ( 1/6eme de la valeur record typhon a la reunion)
    double Probadedepassement = 0.; // Proba que l'intensite d'un ev�nement depasse une certaine valeur (permet de definir le temps de retour)
    double sum = 0.; // Somme des inverses du temps de retour (sert a cacluler le pas de temps adaptatif relatif a une Precipitation)
    double Constantepowerlawdistrib = 0.; // Constante C dans N(P)=C P^(-variabilite) 
    double intervallogprecip = 0;
    double precipmin = ((variabilite - 1.) / variabilite) * precipmoyen; // Valeur min differente de zero. Necessaire, sinon impossible de calculer un Pmoyen avec une fonction de proba en loi de puissance
    if (precipmin > 1.) 
    {
        intervallogprecip = (log(precipmax) - log(precipmin)) / double(nbValeurPrecipStoch); // Intervalle de la distribution (en  log neperien constant) de la Precipitiation (constant)                
    } 
    else 
    {
        intervallogprecip = (log(precipmax)) / double(nbValeurPrecipStoch);
    }


    for (int k = 0; k < nbValeurPrecipStoch; k++) 
    {
        // Calcul de la precipitation centrale par classe de precipitation
        Evenement[k].precipitation = precipmin * exp(double(k + 0.5) * intervallogprecip);
        // Calcul du temps de retour par evenement
        Constantepowerlawdistrib = pow(precipmin, variabilite);
        Probadedepassement = Constantepowerlawdistrib * pow(Evenement[k].precipitation, -variabilite);
        Evenement[k].tempsretour = 1. / (Probadedepassement * 365.); // en ann�es
        sum = sum + (1. / Evenement[k].tempsretour);
    }
    // Calcul du pas de temps associe a chaque evenement
    Evenement[0].pasdetemps = Evenement[nbValeurPrecipStoch - 1].tempsretour / (Evenement[0].tempsretour * sum);
    cout << "precipitation  (m/a) " << Evenement[0].precipitation << "  tretour (ans) " << Evenement[0].tempsretour << "  dt (ans) " << Evenement[0].pasdetemps << "\n";
    // test-------------------------------------
    double titi = Evenement[0].pasdetemps * Evenement[0].precipitation;
    double titi2 = Evenement[0].pasdetemps;
    // fin test-------------------------------------

    for (int k = 1; k < nbValeurPrecipStoch; k++) 
    {
        Evenement[k].pasdetemps = Evenement[nbValeurPrecipStoch - 1].tempsretour / (Evenement[k].tempsretour * sum);
        cout << "precipitation (m/a) " << Evenement[k].precipitation << "  tretour (ans) " << Evenement[k].tempsretour << "  dt (ans) " << Evenement[k].pasdetemps << "\n";

        // test-------------------------------------
        titi = titi + Evenement[k].pasdetemps * Evenement[k].precipitation;
        titi2 = titi2 + Evenement[k].pasdetemps;
        // fin test-------------------------------------
    }

    cout << "Paverage (m/a) " << titi / titi2 << "\n";
    cout << "Ttot(ans)  " << titi2 << "\n";

    // Si le temps de retour le plus grand est inferieur a 100ans,  les pas de temps
    // sont recalcules de sorte que le temps de retour le plus grand soit 50 ans.
    // On fait ca pour eviter d'avoir a effectuer 50 iterations sur une periode inferieure
    // a 100 ans, ce qui reviendrait a avoir un pas de temps moyen tres inferieur a 2 ans,
    // avec donc des temps de calculs prohibitifs.
    double tutu = 0.;
    double tutu2 = 0.;
    if (Evenement[nbValeurPrecipStoch - 1].tempsretour < 100.) 
    {
        cout << "                                                                  " << "\n";
        cout << " --------------------------------------------------------------- " << "\n";
        cout << " RECALCUL DES PAS DE TEMPS (TOTAL DE 50 ANS EN 50 ITERATIONS) :  " << "\n";
        cout << " ---------------------------------------------------------------- " << "\n";
        cout << "                                                                  " << "\n";
        for (int k = 0; k < nbValeurPrecipStoch; k++) 
        {
            Evenement[k].pasdetemps = Evenement[k].pasdetemps * (50. / Evenement[nbValeurPrecipStoch - 1].tempsretour);
            cout << "precipitation (m/a) " << Evenement[k].precipitation << "  tretour (ans) " << Evenement[k].tempsretour << "  dt (ans) " << Evenement[k].pasdetemps << "\n";
            tutu = tutu + Evenement[k].pasdetemps * Evenement[k].precipitation;
            tutu2 = tutu2 + Evenement[k].pasdetemps;
        }

    }

    tutu = tutu / 50.;
    cout << "Paverage (m/a) " << tutu << "\n";
    cout << "Ttot(ans)  " << tutu2 << "\n";

    return;
}

//------------------------------------------------------------------------------
// Recupere la valeur d'un evenement de la liste de  distribution  statistique correspondant
// au numValeurPrecipStoch'eme element.
// CLIMAT STOCHASTIQUE
//------------------------------------------------------------------------------

inline double CClimatStochastique::GetValeurEvenementPluie(const int &numvaleurprecipstoch) 
{
    return Evenement[numvaleurprecipstoch].precipitation;
}

//------------------------------------------------------------------------------
// Recupere le pas de temps d'un evenement de la liste de  distribution  statistique correspondant
// au numValeurPrecipStoch'eme element.
// CLIMAT STOCHASTIQUE
//------------------------------------------------------------------------------

inline double CClimatStochastique::GetPasTempsEvenementPluie(const int &numvaleurprecipstoch) 
{
    return Evenement[numvaleurprecipstoch].pasdetemps;
}

//------------------------------------------------------------------------------
// Recupere le temps de retour d'un evenement de la liste de  distribution  statistique correspondant
// au numValeurPrecipStoch'eme element.
// CLIMAT STOCHASTIQUE
//------------------------------------------------------------------------------

inline double CClimatStochastique::GetTempsRetourEvenementPluie(const int &numvaleurprecipstoch) 
{
    return Evenement[numvaleurprecipstoch].tempsretour;
}
#endif
