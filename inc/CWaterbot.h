//on dÈsigne les directions de cette faÁon :  7  3  6
//                                            4  -  2
//                                            8  1  5

// 2023
#ifndef CWATERBOT_H_INCLUDED
#define CWATERBOT_H_INCLUDED
#include <iostream>
#include <string>
#include <cstdlib>
#include "CFlux.h"
#include "CMaillage.h"
#include "CMaille.h"
#include "Ccalc_pentes.h" 
#include <math.h>

typedef double (CMaille::*PtrFctAltiMaille)();

class CWaterbot
{
protected:

    //Variables qui dÈfinissent le waterbot


    int m_numeroMaille;
    double m_volumeEauTransporte;
    double* m_pas3;
    int* m_numMailleAppartientLac;
    int m_numeroMaillePrecedent;
    int m_numeroMailleSuivant;
    double m_dt;
    bool m_fauxExutoire;

    double numinit;
    CFlux* m_MesFlux;  // ne faudrait il pas les definir en CClimatstochastique ?
    CMaille* m_MaMaille;
    CMaillage* m_MonMaillage;
    
    PtrFctAltiMaille MonGetAlti;
    
public:

    //Liste des mÈthodes

    //Constructeur-destructeur : constructeur par dÈfaut

    CWaterbot(CFlux *flux, CMaillage *maillage, CMaille *maille, const int ind, const double dt);
    ~CWaterbot();
    
    
    bool CombleLeTrouEtActualiseVolumeEauTransporte();
    void SetAltiPointBas();
    void DeplaceLeWaterbot();
    bool TesteSiExutoire();
    void ActualiseFluxEauEnAttente();

   

};

#endif // CWATERBOT_H_INCLUDED

