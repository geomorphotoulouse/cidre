//on dÈsigne les directions de cette faÁon :  7  3  6
//                                            4  -  2
//                                            8  1  5

#ifndef CCAILLOU_H_INCLUDED
#define CCAILLOU_H_INCLUDED
#include <iostream>
#include <string>
#include <cstdlib>
#include "CFlux.h"
#include "CClimatStochastique.h"
#include "CMaillage.h"
#include "CModele.h"
#include <math.h>

class CCaillou 
{
protected:

    //Variables qui dÈfinissent le caillou


    int m_numeroCaillou;
    int m_numeroMaille;
    int m_numeroMailleInit;
    double m_profondeur; //ATTENTION : profondeur positive vers le bas
    bool m_dansLaGrille;
    bool m_DeadInTheLastIteration; // 2023
    bool m_fantome; // Un cailloux fantome est un caillou qui a ete totalement dissout et qui dans le regolite.
    // Des qu'il est erode il est tué (-> m_dansLaGrille=false)
    double m_age; // soit l'age depuis qu'il est erode pour la premiere fois soit depuis le premier passage dans le regolithe (si option dissolution)

    double m_age_montagne; // temporaire pour distribution ages montage
    double m_pas;
    double m_pas2;
    double m_pas3;
    int m_dt; //pas de temps en annees 

    double m_rayon;
    double m_rayonInit;
    double m_rayonDebutPasdeTemps; // sert a calculer la quantite dissoute par unite de volume dans CSortie
    int m_nbMineral;
    std::string m_MineralBase;
    vector<std::string> m_nomMineral; //taille : nombre de type de mineraux
    vector<double> m_proportionMineral; //taille : nombre de type de mineraux
    vector<double> m_volumeMineral; //taille : nombre de type de mineraux
    vector<double> m_quantiteDissoute; //taille : nombre de type de mineraux

    double m_ThermoChronoAge; // age thermochronologique du mineral (ex zircon TF)

    //Variables ‡ rÈcuperer qui serviront pour le dÈplacement et l'Èvolution du caillou

    double m_quantiteErode; //On stocke la valeur erodee soit par diffusion ou par l'eau
    double m_quantiteDepose; //On stocke la valeur deposee soit lors de la diffusion ou dans l'eau
    double m_quantiteTransfere; //On stocke la valeur transfere
    double m_repartition[8]; //C'est ici qu'on stocke les directions
    double m_quantiteErodeTotale; //On stocke la valeur erodee totale sur le dt
    double m_quantiteDeposeTotale; //On stocke la valeur deposee totale sur le dt

    vector<double> m_ki; //taille : nombre de type de mineraux.
    vector<double> m_energieActivation; //taille : nombre de type de mineraux.
    vector<double> m_volumeMolaire; //taille : nombre de type de mineraux. Valeur ‡ entrer en m3/mol
    vector<double> m_rayonMineral; //taille : nombre de type de mineraux
    vector<double> m_proportionMineralInit; //taille : nombre de type de mineraux
    double m_profAlteInit;
    double m_profInit;
    double m_erosionInit; //On stocke la quantitÈ ÈrodÈ sur la maille o˘ Ètait le caillou au dÈbut du pas de temps
    ParamClimat climat;
    vector<double> m_tempSurface; //liste des temperatures de base pour tout le maillage, pour chaque periode
    vector<double> m_listeTemp; //liste des temperatures sur les mailles que parcourt le caillou au cours d'un pas de temps
    vector<double> m_listeRunoff; //liste des runoff sur les mailles que parcourt le caillou au cours d'un pas de temps



    bool m_salterepremierefois; // test pour que l'age des cailloux commence quand il s'altere pour la premiere fois.
    bool m_serodepremierefois; // test pour que l'age des cailloux commence quand il s'erode pour la premiere fois. 2023


    double m_Rugosite; // multiplicateur de la surface specifique d'alteration des mineraux identique pour tous les mineraux
    
// Débuts variables ajoutés pour le calcul de la production de cosmonucleides 20/10/2021
    double m_AltitudeMoyenne; // Altitude de la surface terrestre a l'endroit du caillou, elle est prise comme la moyenne des altitudes entre le point de départ et le point d'arriver du caillou, si le caillou a bougé. (rq: si le caillou ne bouge pas, l'altitude moyenne est l'altitude locale. )
    double m_Pressure; // Pression atmosphérique pour le calcul du taux de production
    vector<double> m_SurfaceProductionRate; // Taux de production a la surface. (voir Stone, 2000)
    double m_ScalingSpallation; // Taux de production par spallation
    double m_MuonCapture; // Taux de production par capture de muons
    double m_ScalingFastMuon; // Taux de production par reactions par muons rapide 2023
    double m_ScalingSlowMuonCapture; // Taux de production par capture de muons 2023

    vector<double> m_ProductionRate; // Taux de production au centre du caillou
    vector<double> m_IntermediateIntegral;  // Intégrale intermédiaire dans le calcul de la concentration cosmo
    vector<double> m_CosmoConcentration;    // Concentration en cosmonucleide au centre du clast en at/g/a
    vector<double> m_Lambda;// Constante de desintegration pour les cosmonucleides choisis
    vector<double> m_ProductionRateSeeLevel;//at/g/a Pour les cosmonucleides choisis
    vector<double> m_InitialCosmoConcentration; // Concentration initiale lors de l'arrivee en surface de l'echantillon (en at/g) // modif 2023
    
    int m_NbCosmonucleide; // Nombre de cosmonucleide diffèrent qu'on étudie
    
    
    double m_totalage; // age total des cailloux en annes
    int m_numeroMaillePrecedent; // Numéro de la maille de l'itération précédente.
    double m_ProfondeurAvant; // Profondeur à l'itération précédente.
    double m_ProfondeurMoyenne; // Profondeur moyenne entre l'itération précédente et l'itération actuelle
    //fin ajout Youssouf du 20/10/2021

    CFlux* m_MesFlux; // decembre 2014
    CModele* m_MonModele;
    CMaillage* m_MonMaillage; // Le maillage lui-mÍme (utile pour affecter les clasts a une couche de bedrock particuliere) 2018

public:

    
    //Liste des mÈthodes

    //Constructeur-destructeur : constructeur par dÈfaut

    CCaillou(CModele *modele, CClimatStochastique *flux, CMaillage *maillage);
    ~CCaillou();

    //Lecture des caractÈristiques initiales des cailloux

    int compterCailloux();
    void initialiserCailloux(const double n, const double x, const double y, const double z, const double r, string nom, const double age);
    void initialiserMineraux();
    void caracteristiqueMineraux();
    void RessusciteCaillou(CModele *modele, const int numperiode); // modif 2023
    void RessusciteCaillouCOSMO(CModele *modele, const int numperiode); // 2023

    //Accesseurs

    int getNumeroCaillou() const;
    int getNumeroMaille() const;
    int getNumeroMailleInit() const; // 2023
    double getProfondeur() const;
    bool getDansLaGrille() const;
    void SetDeadInTheLastIterationTrue(); // 2023
    void SetDeadInTheLastIterationFalse(); // 2023
    bool GetDeadInTheLastIteration() const; //2023
    bool getFantome() const;
    double getAge() const; // modif 2023
    double getTotalAge() const; // 2023
    double getAgeMontagne() const; // temporaire pour distribution ages montagne
    double getThermoChronoAge() const;
    double getRayon() const;
    double getRayonDebutPasdeTemps() const;
    int getNbMineraux() const;
    std::string getNomMineral(int num) const;
    double getVolumeMineral(int num) const;
    double getProportionMineral(int num) const;
    double getQuantiteDissoute(int num) const;
    double getRayonInitial() const;
   

    //Mise ‡ jour des donnÈes nÈcessaires au calcul

    void miseAZero();
    void setFluxDiff();
    void setFluxFluv();
    void setPasDeTemps(const int dt);
    void setProfAlteInit(const double prof);
    //Ensemble du calcul

    void suiviCaillouDiff(const int nbCaillou, const int numPeriode);
    void suiviCaillouFluv(const int nbCaillou, const int numPeriode);
    void suiviCaillouDiffTemp(const int nbCaillou, const int numPeriode); // l'alteration des mineraux depend de la temperature qui varie avec l'altitude
    void suiviCaillouFluvTemp(const int nbCaillou, const int numPeriode); // l'alteration des mineraux depend de la temperature qui varie avec l'altitude
    void suiviCaillouDiffWithoutDissolution(const int nbCaillou, const int numPeriode);
    void suiviCaillouFluvWithoutDissolution(const int nbCaillou, const int numPeriode);
    
    bool estErode(); //Permet de savoir si oui ou non le caillou se dÈplace
    bool directionDiff(); //Permet de dÈterminer si et dans quelle direction   se dÈplace le caillou, et met ‡ jour ses coordonnÈes
    bool directionFluv(); //Permet de dÈterminer  si et dans quelle direction   se dÈplace le caillou, et met ‡ jour ses coordonnÈes
    void direction(); //Permet de dÈterminer la direction dans laquelle se dÈplace le caillou, et met ‡ jour ses coordonnÈes
    void nouvelleProfondeur(const bool seDepose); //Calcul la nouvelle pronfondeur (calcul diffÈrent si il y a eu dÈplacement ou pas)
    void calculMineraux(const bool seDeplace, const double profAlteFin);

    // Debut Ajout Youssouf 21/10/21
    void setAltitudeMoyenne();
    void setProfondeurMoyenne(); // Calcul la profondeur moyenne du caillou entre deux itérations successives
    void    ComputePressure(); // Calcul de la pression atmospherique
    void ComputeScalingSpallation(); //Calcul le facteur de production par spallation
    void ComputeScalingMuons();  // Calcul le facteur de production par capture de muons
    void ComputeSurfaceProductionRate(); // Calcul le taux de  production en surface.
    void ComputeTotalProductionRate(); // Calcul le taux de production de cosmonucleide total
    void setTotalage();  // Calcul l'âge du caillou en années
    void InitialiserCosmonucleides(CModele *modele); // Initialise toutes les variables qu'on aura besoin pour calculer la concentration en cosmonucleide
    void ComputeIntermediateIntegral(); // Fonction qui exécute un calcul intermédiaire nécessaire pour le calcul de la concentration
    void ComputeCosmoConcentration(); // Calcul de la concentration des différents cosmonucleides
    void ComputeInitialSteadyStateCosmoConcentration(); // 2023 Calcul des concentrations initiales des différents cosmonucleides

    void ComputeCosmonuclide(); //Fonction qui centralise les autres fonctions permettant le calcule de la concentration en cosmonucleide
    vector<double> getCosmoConcentration() const;
    int getNbCosmonucleide() const;
    void NeFaitRien();
    //Fin ajout Youssouf
    
    
    //Fonction de hasard

    int hasardDeplacement();
    bool hasardSedimentation();
    bool hasardChangeMaille(int mouvement); // utilise dans le cas du detachment pour savoir si un (gros) clast sort vraiment de la maille ou reste a sa surface
    double hasardNombre();
};

#endif // CCAILLOU_H_INCLUDED
