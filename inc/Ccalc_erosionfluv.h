#if !defined(LeCalculDErosionFluviale)
	#define LeCalculDErosionFluviale

#include "CMaillage.h"
#include "CFlux.h"
#include "Equations.h"

//========================
// Pointeurs de fonctions
//========================

//typedef double (*PtrFct_Capa)(const double &pente, const double &fluxeau,
//							  ParamErosion* param, const double &width, const double &shearstress); // ajout de width 25
//octobre 2007. Commente juin 2014

typedef double (*PtrFct_Detach)(const double &pente, const double &fluxeau,
								ParamErosion* param, const double &Ktemp2, const double &width, const double &shearstress); // modif 25 octobre 2007. Juin 2014 : utilise pour bedrock et sediment
								
typedef void (CFlux::*PtrFct_ActualiseFlux)(const int &nummaille, CMaille* maille, Tvoisinage_bas* voisinage,
										const double fluxout[8]); // janvier 2013

typedef double (*PtrFct_Width)(const double &fluxeau, ParamLargeur* largeur, const double &pas); // ajout 25 octobre 2007
typedef double (*PtrFct_ShearStress)(const double &pente, const double &fluxeau,
										ParamErosion* param, const double &width); // ajout 25 octobre 2007
typedef double (*PtrFct_LongTranspFluv)(const double &fluxeau, const double &coefflongtransport); //janvier 2013

typedef Tvoisinage_bas* (CMaille::*PtrFctGetVoisinage)(); // 2023
typedef int (CMaille::*PtrFctGetDirectionVoisinsBas)(const int &i); // 2023

//------------------------------------------
//
//	classe CErosionFluviale :
//
//	calcul de l'�rosion fluviale en un point
//
//------------------------------------------

class CErosionFluviale
{
//++++++++
protected:
//++++++++

    //===========================
    // Trucs qui servent souvent, qu'on r�cup�re au d�but
    //===========================

    CFlux* MesFlux; // Les grilles de flux
    CModele* MonModele; // 25 octobre 2007
    double pas2; // 7 aout 2008

    //================================
    // Param�tres du mod�le d'�rosion
    //================================

    ParamErosion* incisSed; // Param�tres d'erosion des s�diments. Renomme en juin 2014
    // NB: les coeffs sont actualis�s ICI � chaque pas de temps
    // mais pas dans le mod�le !!!!
    int nblitho; // Nombre de lithologies
    ParamErosion** incisbedrock; // Param�tres de d�tachement des lithologies
    ParamLargeur* largeur; // Param�tres de prise en compte de la largeur

    //PtrFct_Capa MonErosSedFluvPotentiel;	// choix de l'�quation de transport - pour les s�diments
    PtrFct_Detach MonErosSedFluvPotentiel; // choix de l'�quation de transport - pour les s�diments. Modif juin 2014

    PtrFct_Detach* MonDetachePotentiel; // choix de l'�quation de d�tachement - pour le bedrock

    PtrFct_ActualiseFlux MonActualiseFlux; // choix de la routiue d'actualisation des flux de sed - janvier 2013



    PtrFct_Width MonCalculDeLargeurDecoulement; // choix du calcul de largeur, tenant compte du flux d'eau ou non (25 octobre 2007)
    PtrFct_ShearStress MonCalculDeShearStress; // choix du calcul ou non du Shear Stress en fonction des valeurs des parametres des lois d'erosion.
    // Par exemple, si Tauc=0 alors on calcule directement la capa transport en fonction de q et S sans 
    // passer par le calcul du shear stress, pour gagner du temps de calcul. (25 octobre 2007)

    PtrFct_LongTranspFluv MaLongueurTransport; // choix de l'equation pour la longueur de transport - janvier 2013

    PtrFctGetVoisinage MonGetVoisinage; // 2023
    PtrFctGetDirectionVoisinsBas MonGetDirectionVoisinsBas; // 2023

    //=============================================
    // variables locales (pour toutes les mailles)
    //=============================================

    double dt; // Pas de temps 'local' (� cet instant)
    Tvoisinage_bas* MonVoisinage; // janvier 2013
    //Tvoisinage_bas* MonVoisinageEau; // 2018

    double FluxFluvEntrant; // Le volume de sed entrant dans la maille (Qsin) - janvier 2013
    double facteur_distrib; // Proportion des sediment exportes dans une direction quand la somme
    // des sediments excede le stock disponible - janvier 2013
    double facteur_temps; // Le facteur de temps restant pour continuer a eroder - janvier 2013



    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CErosionFluviale(CModele* modele, CFlux* flux);
    ~CErosionFluviale();

    //==================
    // fonctions INLINE
    //==================

    void NeFaitRien(double nv_dt, double FracTempsPluie);
    void ActualiseCoeffSediment(double nv_dt, double FracTempsPluie);
    void ActualiseCoeffBedrock(double nv_dt, double FracTempsPluie);
    void NeCalculeRien(const int &nummaille, CMaille* maille);
    void NeErodePasLateralement(const int &nummaille, CMaille* MaMaille, CMaillage* MonMaillage); // juin 2014


    //==================
    // Autres fonctions
    //==================

    //void	DetachTransport_MultFlow_MultiCouche(const int &nummaille, CMaille* MaMaille);
    void DetachTransport_MultFlow_MultiCouche(const int &nummaille, CMaille* MaMaille);
    //void DetachTransport_MultFlow_MultiCouche_Heau(const int &nummaille, CMaille* MaMaille);

    void ErosionLaterale(const int &nummaille, CMaille* MaMaille, CMaillage* MonMaillage); // 7 aout 2008
    
    //void ErosionLateraleHeau(const int &nummaille, CMaille* MaMaille, CMaillage* MonMaillage); // 2022
    
};



  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

// Pas d'actualisation
inline void CErosionFluviale::NeFaitRien(double nv_dt, double FracTempsPluie) {}

// Actualisation du coefficients de transport selon le pas de temps
// FracTempsPluie est la fraction du temps avec pluie
inline void CErosionFluviale::ActualiseCoeffSediment(double nv_dt, double FracTempsPluie) 
{
	   incisSed->Ktemp = FracTempsPluie*nv_dt*incisSed->K;
       incisSed->CoeffLongTransporttemp = incisSed->CoeffLongTransport/FracTempsPluie; // 2022
}

// Actualisation des coefficients de d�tachement selon le pas de te ps
// FracTempsPluie est la fraction du temps avec pluie
inline void CErosionFluviale::ActualiseCoeffBedrock(double nv_dt, double FracTempsPluie)
{
	for (int i=0; i < nblitho; i++) incisbedrock[i]->Ktemp = FracTempsPluie*nv_dt*incisbedrock[i]->K;
}

// Pas d'�rosion fluviale
inline void CErosionFluviale::NeCalculeRien(const int &nummaille, CMaille* maille) { }

// Pas d'�rosion laterale
inline void CErosionFluviale::NeErodePasLateralement(const int &nummaille, CMaille* MaMaille, CMaillage* MonMaillage) { }

#endif
