#if !defined(LaMorpho)
	#define LaMorpho

#include "CFlux.h"
#include "Equations.h"
#include "Utilitaires.h"
#include "math.h"
using namespace std;

//------------------------------------------------
//
//	classe CMorpho :
//
//	caract�risation du relief et de la morpho
//
//------------------------------------------------

class CMorpho
{
//++++++++
protected:
//++++++++

    //===========================
    // Trucs qui servent souvent, qu'on r�cup�re au d�but du Maillage et du Mod�le
    //===========================

    int nbcol; // NX
    int nblig; // NY
    int nbmailles; // Nombre de mailles
    double pas; // Pas spatial
    double pasdiag; // Pas en diagonale (*sqrt(2))
    double unsurpas2; // =1/pas2
    int* Desordre; // La liste d�sordre
    string RadicalRiviere; // Radical du fichier de sortie des rivi�res
    string RadicalBassin; // Radical des fichiers de sortie grilles
    int NbRivieres; // Nombre de rivi�res demand� par l'utilisateur

    CMaillage* MonMaillage; // Le maillage lui-m�me
    CFlux* MesFlux; // Les grilles de flux

    //===========
    // variables
    //===========

    int* ListeFlux; // Liste des mailles, class�es par aire drain�e d�croissante
    bool* DejaPasse; // Grille indiquant les mailles d�j� r�pertori�es dans les rivi�res
    vector <int> TabRiv[NRIVIERES_MAX]; // Tableau des vecteurs contenant les listes de mailles
    // appartenant � chaque rivi�re
    vector <int> TabRivDiag[NRIVIERES_MAX]; // Tableau des vecteurs contenant les directions
    // dans lesquelles se trouve le point suivant
    // en fait 1 = en diagonale
    // sinon : dans une direction perpendiculaire
    int NRivieres; // Nombre de rivi�res r�pertori�es

    //===================
    // variables locales 
    //===================

    Pile soustas[100]; // Sert au tri des altitudes

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CMorpho(CMaillage* maillage, CFlux* flux, TSorties resultats);
    ~CMorpho();

    //==================
    // fonctions INLINE
    //==================

    bool SauveRien(int num);

    //==================
    // Autres fonctions
    //==================

    void TriParAireDrainee();
    void RivieresPrincipales();
    bool SauveRivieres(int num);
    bool SauvePentesMax(int num);
    bool SauveRivieres_et_PentesMax(int num);
};




  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

//---------------------------
//	Op�rations sur nummaille
//---------------------------
inline bool CMorpho::SauveRien(int num) {return true;}


#endif


