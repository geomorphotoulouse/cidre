#if !defined(LeCalcul)
#define LeCalcul
#include <iostream>
#include <fstream>
#include <string>  // pour les chaines de caracteres
#include "CCaillou.h"
#include "CModele.h"
#include "CMaillage.h"
#include "CFlux.h"
#include "Ccalc_pentes.h"
#include "Ccalc_effondr.h"
#include "Ccalc_bilaneau.h"
#include "Ccalc_bilansed.h"
#include "Ccalc_diffusion.h"
#include "Ccalc_erosionfluv.h"
#include "CSorties.h"
#include "Param_externes.h"
// RAJOUT CLIMAT STOCHASTIQUE:
#include "CClimatStochastique.h"
// FIN RAJOUT CLIMAT STOCHASTIQUE
#include <list>
using namespace std;

//========================
// Pointeurs de fonctions
//========================

typedef double (CMaille::*PtrFctAltiMaille)();
typedef void (CFlux::*PtrFct_Pluie)(const double &temps);
typedef int (CEffondr::*PtrFct_Effondr) (const double &dt);
typedef void (CBilanSed::*PtrFct_ActuCoeffs)(double dt, double FracTempsPluie);
typedef void (CMaillage::*PtrFct_ActuPrecTect)(const int &numperiode, const double &dt);
typedef void (CFlux::*PtrFct_Tecto)(const double &dt);
typedef void (CMaillage::*PtrFct_ReinitVoisinsNivbase)(const double &t, const double &dt);
typedef bool (CSorties::*PtrFct_IncrSorties)(const double &temps, const double &dt,
        const int &NbEffondrements, const int &NbLacs, const vector<CCaillou> & listeCaillou);
typedef void (CSorties::*PtrFct_SortieMax)(const double &temps, const double &dt);
typedef int (CBilanEau::*PtrFct_BilanEau)();
typedef void (CBilanSed::*PtrFct_BilanSed)();
typedef void (CSorties::*PtrFct_FluxChimiqueIntegre)(CModele *MonModele, const vector<CCaillou> &listeCaillou, const double &dt, const double &temps);
typedef void (CSorties::*PtrFct_SortiesCailloux)();

// AJOUT CLIMAT STOCHASTIQUE :
typedef void (CClimatStochastique::*PtrFct_CalcPluieStoch)(const int &numvaleurprecipstoch, const double &precipmoyen, const double &variabilite);
typedef void (CClimatStochastique::*PtrFct_PluieStoch)(const double &temps);
// FIN AJOUT CLIMAT STOCHASTIQUE
typedef void (CCaillou::*PtrFct_SuiviCaillou)(const int nbCaillou, const int numPeriode);

typedef void (CMaillage::*PtrFct_Tri)(); // 2019


//Ajout youssouf 21/10/21
typedef void (CCaillou::*PtrFct_ProductionCosmo)();
//fin ajout


//------------------------------------------------
//
//	classe CCalcul :
//
//	run � partir du mod�le et du maillage
//
//------------------------------------------------

class CCalcul 
{
//++++++++
protected:
    //++++++++

    //==============================
    // Objets qui servent au calcul
    //==============================

    CModele* MonModele; // Objet Mod�le, qui contient tout le cadre de la mod�lisation
    CMaillage* MonMaillage; // Objet Maillage, qui contient tout ce qui concerne le maillage
    // MODIF CLIMAT STOCHASTIQUE : le type de MesFlux a change de CFlux* en CClimatStochastique*
    CClimatStochastique* MesFlux; // Objet contenant les grilles de flux (eau, s�diments...)
    // FIN MODIF
    CEffondr* MesEffondrements; // Objet qui s'occupe des effondrements (et des pentes en m�me temps)
    CBilanEau* MonBilanEau; // Objet qui g�re le bilan d'eau et les lacs
    CBilanSed* MonBilanSed; // Objet qui g�re les processus de diffusion et d'�rosion fluviale
    CSorties* MesSorties; // Objet qui g�re les sorties du calcul
    int nbValeurPrecipStoch; // AJOUT CLIMAT STOCHASTIQUE
    int nbCaillouVivant;


    //===========================
    // Trucs qui servent souvent, qu'on r�cup�re au d�but du Maillage et du Mod�le
    //===========================

    int nbmailles;
    double* pas3;
    double pas2;

    //=========================
    // Pointeurs sur fonctions
    //=========================

    PtrFct_Pluie MonInitFlux_RecuperePluie; // fonction qui r�cup�re pluie+lac

    //--------------------------------------------------------------------------------------
    // RAJOUT CLIMAT STOCHASTIQUE:
    PtrFct_PluieStoch MonInitFlux_RecuperePluieStoch; // fonction qui r�cup�re pluie+lac
    PtrFct_CalcPluieStoch MonCalculPluieStoch; // fonction qui calcule les valeurs de precipitation, leur temps de retour
    // et le pas de temps associe. CLIMAT STOCHASTIQUE
    // FIN RAJOUT CLIMAT STOCHASTIQUE
    //--------------------------------------------------------------------------------------

    PtrFct_Effondr MonCalculeEffondrements; // fct qui calcule les pentes et les effondrements

    PtrFct_ActuCoeffs MonActuCoeffs; // fct qui actualise les coeffs d'alt�ration et d'�rosion

    PtrFct_ActuPrecTect MonActuTecto; // fct qui actualise les pr�cipitations (grille)
    // et �ventuellement la tecto (grille de soul�vement)

    PtrFct_Tecto MonActiviteTecto; // fct qui actualise l'effet de la tecto s'il y en a

    PtrFct_ReinitVoisinsNivbase MonActuVoisinsNivbase; // fct qui actualise le niveau de base s'il y en a
    // et remet les compteurs � z�ro

    PtrFct_IncrSorties MonIncrementeSorties; // fct qui r�alise une sortie


    PtrFct_BilanEau MonCalc_BilanEau; // fct qui calcule le bilan hydro

    PtrFct_BilanSed MonCalc_BilanSed; // fct qui calcule le bilan sedim

    PtrFct_Tri MonTriOptimalMailles; // fonction de classement decroissant des altitudes 2019
    PtrFct_Tri MonTriParInsertionMailles; // fonction de classement decroissant des altitudes 2019
    PtrFct_Tri MonDeuxiemeTriParInsertionMailles; // 2023 fonction de classement decroissant des altitudes de la hauteur d'eau lors d'un deuxieme passage

    void (CCalcul::*MaBoucle)(int NumPeriode); //AJOUT PIERRE
    void (CCalcul::*MaBoucleCaillou)(int NumPeriode, const int &nbCaillou); //AJOUT PIERRE
    PtrFct_SuiviCaillou MonSuiviCaillouDiff;
    PtrFct_SuiviCaillou MonSuiviCaillouFluv;
    void (CCalcul::*MonRessusciteCaillouMort)(const int NumPeriode);
    PtrFct_FluxChimiqueIntegre MonFluxChimiqueIntegre;
    PtrFct_SortiesCailloux MonInitSortiesCailloux;
    PtrFct_SortiesCailloux MonFinitSortiesCailloux;
    //Ajout youssouf 21/10/21
    PtrFct_ProductionCosmo MonComputeCosmonuclide; // fonction qui calcule la concentration en cosmonucleide
    // fin ajout


    //===============================
    // Variables courantes du calcul
    //===============================

    double temps; // Date courante

    double temps_decro; // temps �coul� depuis le dernier d�crochement

    double NiveauDeBase; // Niveau eustatique du pas de temps

    int NbEffondrements; // Nombre d'effondrements depuis la derni�re ligne de d�nudation
    int NbLacs; // Nombre de lacs depuis la derni�re ligne de d�nudation

    std::vector<CCaillou> listeCaillou;
    std::vector<int> indiceCaillouVivant;
    std::vector<int> indiceCaillouMort;
    
    
    // Ajout Youssouf
    std::vector<int> indiceCaillouMortAcetteIteration; // Liste des cailloux qui viennent de mourir a cette itération
    int nbCaillouMortAcetteIteration; // nombre de cailloux qui viennent de mourir a cette itération
    // fin ajout Youssouf

    //+++++
public:
    //+++++

    //===========================
    // constructeur, destructeur
    //===========================

    CCalcul(CModele* modele, CMaillage* maillage);
    ~CCalcul();

    //==================
    // Autres fonctions
    //==================

    void RunLeCalculSansCaillou(); // Modif Pierre (RunLeCalcul devient RunLeCalculSansCaillou)
    void RunLeCalculAvecCaillou(); // Ajout Pierre

    void BouclePeriode(int NumPeriode);
    //  AJOUT CLIMAT STOCHASTIQUE :
    void BouclePeriodeStoch(int NumPeriode); // boucle sur les periodes dans le cas
    //  FIN AJOUT CLIMAT STOCHASTIQUE
    //  AJOUT DES CAILLOUX (PIERRE)
    void BouclePeriodeCaillou(int NumPeriode, const int &nbCaillou);
    void BouclePeriodeStochCaillou(int NumPeriode, const int &nbCaillou);


    void MiseAJourListeCaillou();

    void RessusciteCaillouMort(const int NumPeriode); //  2023
    void RessusciteCaillouMortCOSMO(const int NumPeriode); // modif 2023

    void NeFaitRien(const int NumPeriode);  // Ajout youssouf modif 2023

    //  FIN AJOUT DES CAILLOUX
};

#endif
