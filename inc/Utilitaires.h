#if !defined(LesUtilitaires)
#define LesUtilitaires

#include <string>  // pour les chaines de caracteres
#include <algorithm> // remove(), erase()
#include "Options.h"
using namespace std;

//////////////////////////////////////////////
// Seuil de tri rapide (quicksort) / insertion
//////////////////////////////////////////////

const int SEUIL_TRI = 10;

//-----------------------------
// struct Pile :
// pour les piles du tri rapide
//-----------------------------

struct Pile 
{
    int gauche;
    int droite;
};

//========================================================================
// LitLigne :
// --------
// Fonctions pour aller lire toutes sortes de trucs dans une ligne
//
// Par d�faut il lit la valeur ou le mot qu'il y a, si on met
// un entier n (>1 !) comme 2e argument avant le 3e, il lit un tableau[n]
//------------------------------------------------------------------------
void LitLigne(const string ligne, string& toto);
void LitLigne(const string ligne, int& itoto, string& toto);
void LitLigne(const string ligne, int& itoto);
void LitLigne(const string ligne, int nbtoto, int* toto);
void LitLigne(const string ligne, int nbtoto, UPLIFT* toto);
void LitLigne(const string ligne, double& toto);
void LitLigne(const string ligne, double& toto1, double& toto2);
void LitLigne(const string ligne, int nbtoto, double* toto);
void LitLigne(const string ligne, int nbtoto, string* toto1, double* toto2); //AJOUT PIERRE
void LitLigne(const string ligne, int& itoto, double& toto);
void LitLigne(const string ligne, MODE& toto);
void LitLigne(const string ligne, PROBA& toto); // climat stochastique
void LitLigne(const string ligne, bool& toto);
void LitLigne(const string ligne, bool& toto, double& ztoto);
void LitLigne(const string ligne, bool& toto, double& ztoto1, double& ztoto2); //19/03/12
void LitLigne(const string ligne, FORMATFICHIER& toto);

void LitLigne(const string ligne, bool toto[] );


void LitFormate(const string ligne, int& itoto);
void LitFormate(const string ligne, double& toto);
void LitFormate(const string ligne, string& toto);
void LitFormate(const string ligne, MODE& toto);
void LitFormate(const string ligne, int nbtoto, UPLIFT* toto);
void LitFormate(const string ligne, int& itoto, double& toto1, double& toto2, double& toto3);
void LitFormate(const string ligne, int& itoto, double& toto);
void LitFormate(const string ligne, int& itoto1, int& itoto2);
void LitFormate(const string ligne, int& itoto1, int& itoto2, double& toto);
void LitFormate(const string ligne, int& itoto1, int& itoto2, string& toto);
void LitFormate(const string ligne, PENTE& toto);
void LitFormate(const string ligne, bool& toto);
void LitFormate(const string ligne, int& itoto, BORD& toto, double& ztoto);
void LitFormate(const string ligne, DIFF& toto);
void LitFormate(const string ligne, const int nb, int* toto);
void LitFormate(const string ligne, int& itoto, const int nb, double* toto);
void LitFormate(const string ligne, int& itoto, string& toto);
void LitFormate(const string ligne, FORMATFICHIER& toto);
void SupprimeTousLesCaracteres(string & Str, char C);

void AfficheMessage(string ligne);
void AfficheMessage(string ligne, int toto);
void AfficheMessage(string ligne, int i1, int i2);
void AfficheMessage(string ligne, int i1, int i2, int i3);
void AfficheMessage(string ligne, int i1, int i2, double toto);
void AfficheMessage(string ligne, int i1, int i2, double toto1, double toto2);
void AfficheMessage(string ligne, double toto);
void AfficheMessage(string ligne, double toto1, double toto2);
void AfficheMessage(string ligne, string toto);
void AfficheMessage(string ligne, string toto1, string toto2);
void AfficheMessage(string ligne, string toto, int itoto);
void AfficheMessage(string toto1, int itoto1, string toto2, int itoto2);
void AfficheMessage(string ligne, int i1, double toto);
void AfficheMessage(string ligne1, int i1, string ligne2, double toto);
void AfficheMessage(string ligne1, double toto, string ligne2, int i1);
void AfficheMessage(string ligne, int i1, double toto1, double toto2);
void AfficheMessage(int i1, double toto);
void AfficheMessage(int i1, double toto1, int i2, double toto2);
void AfficheMessage(int i1, double toto1, int i2, double toto2, int i3, double toto3);
void AfficheMessage(int i1, int i2, double toto);
void AfficheMessage(int i1, int i2);
void AfficheMessage(string ligne, MODE toto);

inline void Echange(int liste[], int i, int j) 
{
    int memoire;
    memoire = liste[i];
    liste[i] = liste[j];
    liste[j] = memoire;
}

void TriInsertion(const int tab[], int liste[], int n);
void TriRapideOptimal(const int tab[], int liste[], int debut, int fin);

inline double mymin(double a, double b) 
{
    return (a < b) ? a : b;
}

inline double mymax(double a, double b) 
{
    return (a > b) ? a : b;
}

inline int myintmax(int a, int b) 
{
    return (a > b) ? a : b;
}

inline double dabs(double a) 
{
    return (a) > (0.) ? (a) : (-a);
}

template <typename T> T Max(T a, T b) 
{
    return a < b ? b : a;
}
#endif
