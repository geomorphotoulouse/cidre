#if !defined(LeModele)
	#define LeModele

#include <fstream> // pour les manipulations d'I/O sur file
#include <iostream>
#include <iomanip>
#include <sstream> // pour les manipulations d'I/O sur string
#include <string>
#include "CMaillage.h"
#include "Param_externes.h"
using namespace std;

//----------------------------------------------
//
// struct TOptions :
// options du modËle et des processus d'Èrosion
//
//----------------------------------------------
struct TOptions 
{
    bool OptionIle; // Domaine de calcul qui ne touche pas les bords (Óle)
    bool OptionNivBase; // la mer ‡ l'intÈrieur ou non ?
    bool OptionSeuilFlux; // Existence d'un seuil (de flux d'eau) versant-riviËre
    bool OptionSansLacs; // Pas de calcul des lacs. Evaporation totale
    bool OptionAlter; // prise en compte de l'altÈration du bedrock en sÈdiments ou non ?
    bool OptionDiffLac; // prise en compte de la diffusion dans les lacs ou non ?
    bool OptionDiffSed; // prise en compte de la diffusion des sÈdiments ou non ?
    bool OptionDiffBedrock; // prise en compte de la diffusion dans le bedrock ou non ?
    bool OptionTransp; // prise en compte du transport fluvial des sÈdiments ou non ?
    bool OptionTool; // prise en compte du tool effect pour l'incision du bedrock ? // 8 avril 2008
    bool OptionCover; // prise en compte du tool effect pour l'incision du bedrock ? // 8 avril 2008
    bool OptionIncis; // prise en compte de l'incision dans le bedrock ou non ?
    bool OptionEffondr; // prise en compte des effondrements ou non ?
    bool OptionTecto; // tectonique ou non ?
    bool OptionReprise; // Est-ce une reprise de calcul ?
    bool OptionHauteurEau; // Calcul de la hauteur d'eau pour la distribution du flux d'eau // ajout 30 juin 2008
    bool OptionTempRunoff; //ajoute le 01/03/11  Calcul de l'altÈration en fonction de la tempÈrature et du runoff
    bool OptionRegOptimum; //ajoute le 01/03/11  Calcul de l'altÈration avec un pic ‡ une certaine profondeur sous la surface

    bool OptionAvecCailloux; //AJOUT PIERRE : présence ou non de cailloux
    bool OptionMineraux; //AJOUT PIERRE : 1 si le caillou a la composition de la couche où il est, 0 si il n'est composé que d'un seul minéral
    bool OptionRessuscite;   // ressuscite les cailloux morts en les replacant sous le regolithe a leur position initiale.
    bool OptionDissolve;   // les cailloux se dissolvent en fonction de leur minéralogie, runoff et température
    
    bool OptionCosmo; //AJOUT YOUSOUF : Option es ce que on calcule la production de nucleide cosmogenique dans les cailloux ?
    bool CosmonucleideEtudie[4]; //  Chaque indice de la liste désigne un cosmonucleide particulier, et si CosmonucleideEtudie[i]= True, on calcule la concentration pour ce Cosmonucleide.(Si i=0 c'est le 10Be, si i=1 c'est le 26Al, si i=2 c,est le 14C, et si i=3 c'est le 21Ne )
};

//------------------------------------------------
//
//	classe CModele :
//
//	paramËtres de la modÈlisation
//
//------------------------------------------------

class CModele
{
//++++++++
protected:
//++++++++

    //=======================
    // ParamËtres par dÈfaut (dÈfinis dans le constructeur)
    //=======================

    TChronologie chrono_pardefaut;
    TLimites limites_pardefaut;
    TOptions options_pardefaut;
    Tlitho litho_pardefaut;
    ParamAlter alterBr_pardefaut;
    ParamDiff diffBr_pardefaut;
    ParamErosion incisBr_pardefaut;
    ParamLargeur largeur_pardefaut;
    ParamErosion incisSed_pardefaut;
    ParamDiff diffsed_pardefaut;
    ParamDiff difflac_pardefaut;
    ParamEffondr effondr_pardefaut;
    ParamClimat climat_pardefaut;
    ParamTecto tecto_pardefaut;
    TSorties resultats_pardefaut;
    ParamLocalEntry localentry_pardefaut; // 2022

    //=======================
    // variables temporaires
    //=======================

    TTopoInit topoinitiale; // Infos sur la topo initiale et sur les dimensions des grilles
    TStratigraphie strati; // Infos sur le modËle gÈologique

    //===========
    // variables
    //===========

    double ReliefMin; // Relief minimal

    TChronologie chrono; // Structure dÈcoupage en pÈriodes
    double dtMin; // Pas de temps minimal prÈvu sur tout le calcul

    PENTE Type_pente; // Option de pente  multiple ou rien
    TLimites Limites; // Conditions aux limites des bords
    TNivBase nivbase; // Niveau de base (supposant qu'il entre dans la grille,
    // typiquement la mer occupe une partie du domaine carrÈ)

    TOptions Options; // Options des processus actifs

    double FluxCritique; // Flux d'eau au-dessus duquel il y a Èrosion fluviale [L3/T]

    int nblitho; // Nombre de lithologies
    Tlitho* ListeLitho; // Tableaux des paramËtres lithologiques
    ParamLargeur* largeur; // ParamËtres de prise en compte de la largeur

    ParamErosion* incisSed; // ParamËtres d'erosion fluviale  des sÈdiments

    ParamDiff* diffsed; // ParamËtres de diffusion des sÈdiments

    ParamDiff* difflac; // ParamËtres de diffusion dans les lacs

    ParamEffondr effondr; // ParamËtres des effondrements

    ParamClimat climat; // ParamËtres climatiques

    ParamTecto tecto; // ParamËtres tectoniques

    TSorties resultats; // ParamËtres de sortie des rÈsultats

    double ProfRessusciteCaillouxMin; // profondeur min a laquelle un cailloux mort est remis sous le regolithe 2023
    double ProfRessusciteCaillouxMax; // profondeur max a laquelle un cailloux mort est remis sous le regolithe 2023

    double Rugosite;          // facteur de multiplication de la surface specifique des grains.
    
    ParamLocalEntry localentry; // 2022

//+++++
public:
//+++++

    static const string STR_SEPAR;

    //===========================
    // constructeur, destructeur
    //===========================

    CModele();
    ~CModele();

    //==================
    // fonctions INLINE
    //==================

    TTopoInit GetTopoInit();
    void SetTopoInit(TTopoInit nvtopo);

    TStratigraphie GetStrati();

    TPeriode GetPeriode(int num);
    int GetNbPeriodes();
    // AJOUT LORS DE l'INTRODUCTION DU CLIMAT STOCHASTIQUE
    int GetNbValeurPrecipStoch();
    double GetFracTempsPluie(int num); // concerne le climat stochastique ou non
    // FIN AJOUT
    double GetFinPeriode(int num);
    double GetPasTemps(int num);
    double GetTemperatureSurface(int num); //ajoute le 04/04/11
    void SetChrono(int nv_nb, double* nv_limites, double* nv_dt);
    void SetParametreParPeriode(int num, double taux_precip, string fich_precip,
            string fich_uplift, double* taux_uplift, double Frac_temps_pluie);

    PENTE GetTypePente();
    void SetTypePente(PENTE nv_type);

    TLimites GetCondLimites();
    void SetCondLimites(BORD nv_bord[], int nv_exut[], double nv_base[]);

    double GetValeur_NivBase();
    void SetValeur_NivBase(double nv_niv);
    TNivBase GetNivBase();
    void SetNivBase(TNivBase nv_parambase);

    int GetNbLitho();
    void SetNbLitho(int nlitho);
    void AlloueLitho(int nlitho);
    Tlitho GetLitho(int num);
    void SetLitho(int num, Tlitho nv_litho);
    void SetLitho(int num, ParamAlter* nv_alter, ParamDiff* nv_diff,
            ParamErosion* nv_incis);
    void SetAlterLitho(int num, double nv_Kw, double nv_Kwdt, double nv_d1, double nv_d2, double nv_k1, double nv_NbMailleRunoffLimite);
    void SetDiffLitho(int num, DIFF nv_type, double nv_K, double nv_seuil);
    void SetIncisLitho(int num, double nv_K, double nv_N, double nv_M, double nv_seuil);
    Tlitho* GetLesLitho();
    void SetStrati(TStratigraphie NvStrati);

    double GetFluxCritique();
    void SetFluxCritique(double seuil);

    ParamEffondr GetParamEffondr();
    void SetParamEffondr(ParamEffondr nv_effondr);
    void SetParamEffondr(double nv_seuil, double nv_retour, double nv_K,
            int nv_nmax, bool nv_opttalus);
    ParamDiff* GetParamDiffLac();
    ParamDiff* GetParamDiffSed();
    void SetParamDiffSed(DIFF nv_type, double nv_K, double nv_seuil);
    ParamErosion* GetParamIncisSed();
    void SetParamTransp(double nv_K, double nv_N, double nv_M, double nv_seuil);
    ParamLargeur* GetParamLargeur();
    void SetParamLargeur(bool flag, double K);

    ParamClimat GetParamClimat();
    void SetParamClimat(ParamClimat nv_clim);
    void SetParamClimat(bool nv_optsinus, double nv_persinus,
            bool nv_optalti, double nv_altimax);

    ParamTecto GetParamTecto();
    void SetParamTecto(bool optuplift, bool optdecro, int numlig, double vit);
    void SetParamZonesTecto(int nv_nb, int* nv_num, UPLIFT* nv_type);
    void SetNbZoneTecto(int nb);
    void SetModeTecto(MODE NvMode);
    MODE GetModeTecto();
    void SetTauxUplitftParPeriode(int num, double* taux);
    ///////////////////////////////////////////////////////////////////////
    ////// ????? ‡ quoi Áa sert ? mÍme valeur pour toutes les zones ?!?!?!
    ///////////////////////////////////////////////////////////////////////
    void SetValeurTauxUplitftParPeriode(int num, double taux);
    
    ParamLocalEntry GetParamLocalEntry(); // 2022

    TSorties GetParamSorties();
    void SetParamSorties(TSorties nv_sorties);
    void SetParamSorties(double nv_dtdenud, double nv_dtbassin, int nv_indbassin,
            string nv_fichdenud, string nv_fichbassin, string nv_fichrestart);

    TOptions GetOptions();
    void SetOptions(TOptions nvoptions);
    void SetOptions(bool optile, bool optseuil, bool optalter, bool optdiffsed,
            bool optdiffbr, bool opttransplim, bool opttransp,
            bool optdetachlim, bool optincis, bool opteffondr,
            bool opttecto);
    bool TesteOptionLargeur();
    bool TesteOptionBroyage();
    bool TestePrecipSinus();
    bool TestePrecipLinAlti();
    bool TestePrecipGaussAlti(); //ajoute le 01/03/11
    bool TestePrecipStochastique(); // climat stochastique

    double GetProfRessusciteCaillouxMin(); // 2023
    double GetProfRessusciteCaillouxMax(); // 2023
    
    double GetRugositeMineral();

    string Formate(string ligne, int nb);
    string Formate(string ligne, double x);
    string Formate(string ligne, double x, string texte);
    string Formate(string ligne, string texte);
    string Formate(string ligne, int nb, string texte1, double x);
    string Formate(string ligne, int nb, string texte1, string texte2);
    string Formate(string ligne, int nb, string texte);
    string Formate(string ligne, PROBA texte); // rajout pour climat stochastique
    string Formate(string ligne, int nb, string texte1, double niv, string texte2);
    string Formate(string ligne, int nb1, int nb2);
    string Formate(string ligne, int nb1, string texte1, int nb2, string texte2, double x);
    string Formate(string ligne, int nb1, string texte1, int nb2, string texte2, string texte3);
    string FormateFich(string ligne, string texte);
    string FormateFich(string ligne, int nb, string texte);

    //==================
    // Autres fonctions
    //==================

    // fonctions qui lisent, construisent, vÈrifient le modËle ‡ partir d'un fichier

    bool LitModele(string FichierParam);
    bool SauveInfo(string FichierRecap);
    bool VerificationAvantCorrection();
    void CorrectionAuto();
    void Reformulation();
    bool InitialiseMaillage(CMaillage* maillage);

    bool ConstruitModele(CMaillage* maillage);
};




  /////////////////////////////////////////////
 ////// DEFINITION DES FONCTIONS INLINE //////
/////////////////////////////////////////////

//-------------------------------
//	OpÈrations sur topo initiale
//-------------------------------
inline TTopoInit CModele::GetTopoInit() {return topoinitiale;}

inline void	CModele::SetTopoInit(TTopoInit nvtopo) {topoinitiale = nvtopo;}

//------------------------
//	OpÈrations sur strati
//------------------------
inline TStratigraphie CModele::GetStrati() {return strati;}

//------------------------
//	OpÈrations sur chrono
//------------------------
inline TPeriode CModele::GetPeriode(int num) {return chrono.TabPeriode[num];}

inline int CModele::GetNbPeriodes() {return chrono.nb;}

inline double CModele::GetFinPeriode(int num) {return chrono.TabPeriode[num+1].debut;}

inline double CModele::GetPasTemps(int num) {return chrono.TabPeriode[num].dt;}

inline double CModele::GetTemperatureSurface(int num) {return chrono.TabPeriode[num].TemperatureSurface;} // 01/02/11

inline double CModele::GetFracTempsPluie(int num) {return chrono.TabPeriode[num].frac_temps_pluie;} // nouvelle fonction stochastique ou non

inline void CModele::SetChrono(int nv_nb, double* nv_limites, double* nv_dt)
{
	if (nv_nb != chrono.nb)
	{
		chrono.nb = nv_nb;
		chrono.TabPeriode = new TPeriode[nv_nb];
	}
	for (int i=0; i < nv_nb; i++)
	{
		chrono.TabPeriode[i].debut = nv_limites[i];
		chrono.TabPeriode[i].dt = nv_dt[i];
	}
	chrono.TabPeriode[nv_nb].debut = nv_limites[nv_nb];
}

inline void CModele::SetParametreParPeriode (int num, double taux_precip, string fich_precip,
								string fich_uplift, double* taux_uplift, double frac_temps_pluie)
{
	if (num < chrono.nb)
	{
		chrono.TabPeriode[num].taux_precip = taux_precip;
		chrono.TabPeriode[num].fich_precip = fich_precip;
		chrono.TabPeriode[num].fich_uplift = fich_uplift;
		chrono.TabPeriode[num].taux_uplift = taux_uplift;
		chrono.TabPeriode[num].frac_temps_pluie = frac_temps_pluie; // stochastique ou non
	}
}

//-----------------------------------------
//	OpÈrations sur type de calcul de pente
//-----------------------------------------
inline PENTE CModele::GetTypePente() {return Type_pente;}

inline void CModele::SetTypePente(PENTE nv_type) {Type_pente=nv_type;}

//--------------------------------------------
//	OpÈrations sur les conditions aux limites
//--------------------------------------------
inline TLimites CModele::GetCondLimites() {return Limites;}

inline void CModele::SetCondLimites(BORD nv_bord[], int nv_exut[], double nv_base[])
{
	for (int i=0; i < 4; i++)
	{
		Limites.Type_bord[i] = nv_bord[i];
		Limites.base[i] = nv_base[i];
	}
	Limites.ij_exut[0] = nv_exut[0];
	Limites.ij_exut[1] = nv_exut[1];
}

//----------------------------------
//	OpÈrations sur le niveau de base
//----------------------------------
inline double CModele::GetValeur_NivBase() {return nivbase.NivBase;}

inline void CModele::SetValeur_NivBase(double nv_niv) {nivbase.NivBase = nv_niv;}

inline TNivBase CModele::GetNivBase() {return nivbase;}

inline void CModele::SetNivBase(TNivBase nv_parambase) {nivbase = nv_parambase;}

//-----------------------
//	OpÈrations sur litho
//-----------------------
inline int CModele::GetNbLitho() {return nblitho;}

inline void CModele::SetNbLitho(int nlitho) {nblitho=nlitho;}

inline void CModele::AlloueLitho(int nlitho)
{
	int i;
	if (nlitho != nblitho)
	{
		for (i=0; i < nblitho; i++)
		{
			delete ListeLitho[i].alter;
			delete ListeLitho[i].diff;
			delete ListeLitho[i].incis;
		}
		delete [] ListeLitho;
		ListeLitho = new Tlitho[nlitho];
		for (i=0; i < nlitho; i++)
		{
			ListeLitho[i].alter = new ParamAlter;
			*ListeLitho[i].alter = alterBr_pardefaut;
			ListeLitho[i].diff = new ParamDiff;
			*ListeLitho[i].diff = diffBr_pardefaut;
			ListeLitho[i].incis = new ParamErosion;
			*ListeLitho[i].incis = incisBr_pardefaut;
		}
		nblitho=nlitho;
	}
}

inline Tlitho CModele::GetLitho(int num) {return ListeLitho[num];}

inline void CModele::SetLitho(int num, Tlitho nv_litho)
{
	*ListeLitho[num].alter = *nv_litho.alter;
	*ListeLitho[num].diff = *nv_litho.diff;
	*ListeLitho[num].incis = *nv_litho.incis;
}

inline void CModele::SetLitho(int num, ParamAlter* nv_alter, ParamDiff* nv_diff,
						ParamErosion* nv_incis)
{
	*ListeLitho[num].alter = *nv_alter;
	*ListeLitho[num].diff = *nv_diff;
	*ListeLitho[num].incis = *nv_incis;
}

// 01/02/11
inline void CModele::SetAlterLitho(int num, double nv_Kw, double nv_Kwdt, double nv_d1, double nv_d2, double nv_k1, double nv_NbMailleRunoffLimite)
{
	(ListeLitho[num].alter)->Kw = nv_Kw;
	(ListeLitho[num].alter)->Kwdt = nv_Kwdt;
	(ListeLitho[num].alter)->d1 = nv_d1;
	(ListeLitho[num].alter)->d2 = nv_d2;
	(ListeLitho[num].alter)->k1 = nv_k1;
	(ListeLitho[num].alter)->NbMailleRunoffLimite = nv_NbMailleRunoffLimite;

}

inline void CModele::SetDiffLitho(int num, DIFF nv_type, double nv_K, double nv_seuil)
{
	(ListeLitho[num].diff)->Type_diff = nv_type;
	(ListeLitho[num].diff)->K = nv_K;
	(ListeLitho[num].diff)->Seuil = nv_seuil;
}

inline void CModele::SetIncisLitho(int num, double nv_K, double nv_N, double nv_M, double nv_seuil)
{
	(ListeLitho[num].incis)->K = nv_K;
	(ListeLitho[num].incis)->MQ = nv_M;
	(ListeLitho[num].incis)->NS = nv_N;
	(ListeLitho[num].incis)->Seuil = nv_seuil;
}

inline Tlitho* CModele::GetLesLitho() {return ListeLitho;}

inline void CModele::SetStrati (TStratigraphie NvStrati)
{
	if (strati.TabCouche != NULL) delete [] strati.TabCouche;
	strati = NvStrati;
}

//-------------------------------
//	OpÈrations sur flux critique
//-------------------------------
inline double CModele::GetFluxCritique() {return FluxCritique;}

inline void CModele::SetFluxCritique(double seuil) {FluxCritique = seuil;}

//------------------------------------
//	OpÈrations sur paramËtres Èrosion
//------------------------------------
inline ParamEffondr CModele::GetParamEffondr() {return effondr;}

inline void CModele::SetParamEffondr(ParamEffondr nv_effondr)
{
	effondr.Seuil = nv_effondr.Seuil;
	effondr.Retour = nv_effondr.Retour;
	effondr.K = nv_effondr.K;
	effondr.Nmax = nv_effondr.Nmax;
	effondr.TalusBroye = nv_effondr.TalusBroye;
}

inline void CModele::SetParamEffondr(double nv_seuil, double nv_retour, double nv_K,
								int nv_nmax, bool nv_opttalus)
{
	effondr.Seuil = nv_seuil;
	effondr.Retour = nv_retour;
	effondr.K = nv_K;
	effondr.Nmax = nv_nmax;
	effondr.TalusBroye = nv_opttalus;
}

inline ParamDiff* CModele::GetParamDiffLac() {return difflac;}

inline ParamDiff* CModele::GetParamDiffSed() {return diffsed;}

inline void CModele::SetParamDiffSed(DIFF nv_type, double nv_K, double nv_seuil)
{
	diffsed->Type_diff = nv_type;
	diffsed->K = nv_K;
	diffsed->Seuil = nv_seuil;
}

inline ParamErosion* CModele::GetParamIncisSed() {return incisSed;}

inline void CModele::SetParamTransp(double nv_K, double nv_N, double nv_M, double nv_seuil)
{
	incisSed->K = nv_K;
	incisSed->MQ = nv_N;
	incisSed->NS = nv_M;
	incisSed->Seuil = nv_seuil;
}

inline ParamLargeur* CModele::GetParamLargeur() {return largeur;}

inline void CModele::SetParamLargeur(bool flag, double K)
{
	largeur->OuiNon = flag;
	largeur->K = K;
}


//-------------------------------------
//	OpÈrations sur paramËtres du climat
//-------------------------------------
inline ParamClimat CModele::GetParamClimat() {return climat;}

inline void CModele::SetParamClimat(ParamClimat nv_clim) {climat = nv_clim;}

inline void CModele::SetParamClimat(bool nv_optsinus, double nv_persinus,
						bool nv_optalti, double nv_altimax)
{
	climat.Sinus = nv_optsinus;
	climat.Periode = nv_persinus;
	climat.LinAlti = nv_optalti;
	climat.AltiMax = nv_altimax;
}

//---------------------------------
//	OpÈrations sur paramËtres tecto
//---------------------------------
inline ParamTecto CModele::GetParamTecto() {return tecto;}

inline void CModele::SetParamTecto(bool optuplift, bool optdecro, int numlig, double vit)
{
	tecto.OptionUplift = optuplift;
	tecto.OptionDecro = optdecro;
	tecto.LigneDecro = numlig;
	tecto.VitesseDecro = vit;
}

inline void CModele::SetParamZonesTecto(int nv_nb, int* nv_num, UPLIFT* nv_type)
{
	tecto.nbzone = nv_nb;
	tecto.numzone = nv_num;
	tecto.type_uplift = nv_type;
}

inline void CModele::SetNbZoneTecto(int nb) {tecto.nbzone = nb;}

inline void CModele::SetModeTecto(MODE NvMode) {tecto.mode_tecto = NvMode;}

inline MODE CModele::GetModeTecto() {return tecto.mode_tecto;}

inline void CModele::SetTauxUplitftParPeriode (int num, double* taux)
{
	chrono.TabPeriode[num].taux_uplift = taux;
}
///////////////////////////////////////////////////////////////////////
////// ????? ‡ quoi Áa sert ? mÍme valeur pour toutes les zones ?!?!?!
///////////////////////////////////////////////////////////////////////
inline void CModele::SetValeurTauxUplitftParPeriode (int num, double taux)
{
	if (num > -1 && num < chrono.nb )
	{
		for (int i =0; i<tecto.nbzone;i++)
			chrono.TabPeriode[num].taux_uplift[i] = taux;
	}
}

//-------------------------------------
//	OpÈrations sur paramËtres de sortie
//-------------------------------------
inline TSorties CModele::GetParamSorties() {return resultats;}

inline void CModele::SetParamSorties(TSorties nv_sorties)
{
	resultats.dt_denud = nv_sorties.dt_denud;
	resultats.dt_bassin = nv_sorties.dt_bassin;
	resultats.ind_bassin = nv_sorties.ind_bassin;
	resultats.FichierDenud = nv_sorties.FichierDenud;
	resultats.FichierBassin = nv_sorties.FichierBassin;
	resultats.FichierRestart = nv_sorties.FichierRestart;
}

inline void CModele::SetParamSorties(double nv_dtdenud, double nv_dtbassin, int nv_indbassin,
			string nv_fichdenud, string nv_fichbassin, string nv_fichrestart)
{
	resultats.dt_denud = nv_dtdenud;
	resultats.dt_bassin = nv_dtbassin;
	resultats.ind_bassin = nv_indbassin;
	resultats.FichierDenud = nv_fichdenud;
	resultats.FichierBassin = nv_fichbassin;
	resultats.FichierRestart = nv_fichrestart;
}

//-------------------------------------
//    Operations sur l'entree locale d'eau et sediment 2022
//-------------------------------------
inline ParamLocalEntry CModele::GetParamLocalEntry() {return localentry;}

//-------------------------------
// Tests des diffÈrentes options
//-------------------------------
inline TOptions CModele::GetOptions() {return Options;}

inline void CModele::SetOptions (TOptions nvoptions) {Options = nvoptions;}

inline void CModele::SetOptions(bool optile, bool optseuil, bool optalter, bool optdiffsed,
						bool optdiffbr, bool opttransplim, bool opttransp,
						bool optdetachlim, bool optincis, bool opteffondr,
						bool opttecto)
{
	Options.OptionIle = optile;
	Options.OptionSeuilFlux = optseuil;
	Options.OptionAlter = optalter;
	Options.OptionDiffSed = optdiffsed;
	Options.OptionDiffBedrock = optdiffbr;
	Options.OptionTransp = opttransp;
	Options.OptionIncis = optincis;
	Options.OptionEffondr = opteffondr;
	Options.OptionTecto = opttecto;
}



inline double CModele::GetProfRessusciteCaillouxMin() {return ProfRessusciteCaillouxMin;} // 2023
inline double CModele::GetProfRessusciteCaillouxMax() {return ProfRessusciteCaillouxMax;} // 2023

inline double CModele::GetRugositeMineral() {return Rugosite;}

inline bool CModele::TesteOptionLargeur() {return largeur->OuiNon;}

inline bool CModele::TesteOptionBroyage() {return effondr.TalusBroye;}

inline bool CModele::TestePrecipSinus() {return climat.Sinus;}

inline bool CModele::TestePrecipLinAlti() {return climat.LinAlti;}

inline bool CModele::TestePrecipGaussAlti() {return climat.GaussAlti;} //ajoute le 01/03/11

inline bool CModele::TestePrecipStochastique() {return climat.Stochastique;} //CLIMAT STOCHASTIQUE
//--------------------------------------
// Sauvegarde des param dans un fichier
//--------------------------------------
inline string CModele::Formate(string ligne, int nb)
{
	ostringstream oss;
	oss << left << setfill('-') << setw(LONG_STRING) << ligne.c_str() << " " << nb << endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, double x)
{
	ostringstream oss;
	oss << left << setfill('-') << setw(LONG_STRING) << ligne.c_str() << " " << x << endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, double x, string texte)
{
	ostringstream oss;
	oss << left << setfill('-') << setw(LONG_STRING) << ligne.c_str() << " " << x << texte << endl;
	return oss.str();
}

/*	inline string CModele::Formate(string ligne, double x, string texte)
{
	ostringstream oss;
	oss << ligne << " : " << right << setfill(' ')
			<< scientific << setw(10) << x << fixed << texte << endl;
	return oss.str();
}*/

inline string CModele::Formate(string ligne, string texte)
{
	ostringstream oss;
	oss << left << setfill('-') << setw(LONG_STRING) << ligne.c_str() << " " << texte << endl;
	return oss.str();
}

// stochastique
inline string CModele::Formate(string ligne, PROBA texte)
{
	ostringstream oss;
	oss << left << setfill('-') << setw(LONG_STRING) << ligne.c_str() << " " << texte << endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, int nb, string texte1, double x)
{
	ostringstream oss;
	oss << left << setfill(' ') << setw(8) << ligne.c_str()
			<< right << setw(2) << nb << left
			<< setfill('-') << setw(39) << texte1.c_str() << " " << x << endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, int nb, string texte1, string texte2)
{
	ostringstream oss;
	oss << left << setfill(' ') << setw(8) << ligne.c_str()
			<< right << setw(2) << nb << left
			<< setfill('-') << setw(39) << texte1.c_str() << " " << texte2 << endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, int nb, string texte)
{
	ostringstream oss;
	oss << ligne.c_str() << nb << " : " << setfill(' ') << setw(15) << texte.c_str() << endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, int nb, string texte1, double niv, string texte2)
{
	ostringstream oss;
	oss << ligne.c_str() << nb << " : " << texte1.c_str() << right << setfill(' ') << setw(15)
			<< ",  niv. base = " << right << setprecision(1) << setw(5) << niv << texte2
			<< endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, int nb1, int nb2)
{
	ostringstream oss;
	oss << left << setfill('-') << setw(LONG_STRING) << ligne.c_str() << " "
		<< nb1 << " " << nb2 << endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, int nb1, string texte1, int nb2, string texte2, double x)
{
	ostringstream oss;
	oss << right << setfill(' ') << setw(7) << ligne.c_str() << " : " << setw(2) << nb1
			<< setfill(' ') << setw(20) << texte1.c_str() << " : " << setw(2) << nb2
			<< setfill(' ') << setw(11) << texte2.c_str() << " : " << setprecision(1) << setw(6) << x;
	oss << " m" << endl;
	return oss.str();
}

inline string CModele::Formate(string ligne, int nb1, string texte1, int nb2, string texte2, string texte3)
{
	ostringstream oss;
	oss << right << setfill(' ') << setw(7) << ligne.c_str() << " : " << setw(2) << nb1
			<< setfill(' ') << setw(20) << texte1.c_str() << " : " << setw(2) << nb2
			<< setfill(' ') << setw(11) << texte2.c_str() << " : " << setw(6)
			<< " '" << texte3 << "'";
	oss << endl;
	return oss.str();
}

inline string CModele::FormateFich(string ligne, string texte)
{
	ostringstream oss;
	oss << left << setfill('-') << setw(LONG_STRING) << ligne.c_str() << " '"
			<< texte.c_str() << "'" << endl;
	return oss.str();
}

inline string CModele::FormateFich(string ligne, int nb, string texte)
{
	ostringstream oss;
	oss << left << setfill('-') << setw(LONG_STRING) << ligne.c_str() << " "
			<< right << setw(2) << nb << " : '" << texte.c_str() << "'" << endl;
	return oss.str();
}

//-------------------------------------
// AJOUT CLIMAT STOCHASTIQUE
//-------------------------------------
//	OpÈrations sur nombre de valeur de la distribution des precipitations
// fixe  50 pour l'instant. Peutetre a definir par l'utilisateur dans l'avenir.
inline int CModele::GetNbValeurPrecipStoch() {return 50;}
//-------------------------------------
// FIN AJOUT CLIMAT STOCHASTIQUE
//-------------------------------------
#endif
