#if !defined(LeCalculDesPentes)
#define LeCalculDesPentes

#include "CMaillage.h"

//========================
// Pointeurs de fonctions
//========================

typedef double (CMaille::*PtrFctAltiMaille)();


//----------------------------------------------------
//	diverses fa�ons de calculer les pentes en un point
//
//	(retourne "true" s'il y a au moins un voisin bas)
//----------------------------------------------------

bool PentesMaille_SteepDesc(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3);
bool PentesMaille_MultFlow(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3);
bool PentesMaille_MultFlow_SurfaceEau(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3); // 30 juin 2008
bool PentesMaille_MultFlow_ILE(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3);
bool PentesMaille_MultFlow_SurfaceEau_ILE(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3);

#endif
