#include "CClimatStochastique.h"
#include <math.h>
using namespace std;

//-------------------------------------------
//
// fonctions de la classe CClimatStochastique :
//
//-------------------------------------------

// constructeur

CClimatStochastique::CClimatStochastique(CMaillage* maillage, CModele* modele) : CFlux(maillage, modele) 
{
    nbValeurPrecipStoch = modele->GetNbValeurPrecipStoch();
}

// destructeur

CClimatStochastique::~CClimatStochastique() 
{
    //delete [] TabPrecipStoch; // comment est liberee la memoire pour ce pointeur ?
    //delete [] Evenement; // comment est liberee la memoire pour ce pointeur ?
}

//==================
// Autres fonctions
//==================


//-------------------------
// Initialise tous les flux
// Redefinition de la fonction membre de CFLUX
//-------------------------

bool CClimatStochastique::InitialisePrecipStoch(CModele* modele) 
{
    bool flag = true;
    int i;

    //******************************************************************************
    // Climat

    AlloueGrillePrecipStoch();


    // Pr�cipitations uniformes (temporellement stochastiques )

    for (i = 0; i < nbValeurPrecipStoch; i++) 
    {
        SetGrillePrecipUniformeStoch(i, modele->GetPeriode(0).taux_precip);
    }

    //******************************************************************************
    return flag;
}

//******************************************************************
//		RECUPERATION DE LA PLUIE TOMBEE PENDANT UN PAS DE TEMPS
//******************************************************************

//--------------------------------------------------
// R�cup�re la pluie tomb�e pendant le pas de temps, 
//	cas des pr�cipitations uniformes et de moyenne constante
//--------------------------------------------------
// POURQUOI LE TEMPS EST RENTRE EN ARGUMENT ICI (PAR HOMOGENEITE AVEC LA DEFINITION
// DU TYPE POINTEUR DE CES FONCTIONS DANS CALCUL.H )?

void CClimatStochastique::InitFlux_RecuperePluie_NoSin_NoLin_Stoch(const double &temps) 
{
    int k, j;
    for (k = 0; k < nbmailles; k++) 
    {
        FluxEau[k] = TabPrecipStoch[NumValeurPrecipStoch][k] + FluxEauEnAttente[k];
        Precip2[k] = Precip[k];
        //		cout <<  " FluxEauEnAttente[k] "<< FluxEauEnAttente[k]<<"\n";
        FluxEauEnAttente[k] = 0.; //  30 juin 2008
        EauCourante[k] = 0.;
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        for (j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.; // 7 aout 2008
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;
    }
    VolumeSed_Out = 0.;
    VolumeSed_In = 0.;
    FluxEau_Out = 0.;
    FluxEau_In = 0.;
    //	EauCouranteMax = 0.;
    return;
}

