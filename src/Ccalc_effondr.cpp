#include "Ccalc_effondr.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
using namespace std;


//-------------------------------------------
//
// fonctions de la classe CEffondr :
//
//-------------------------------------------

// constructeur : nbmax = effondr.Nmax

CEffondr::CEffondr(CModele* modele, CMaillage* maillage, CFlux* flux) 
{
    nbmailles = maillage->GetNbMailles();
    pas3 = maillage->GetPas3();
    pas2 = maillage->GetPas2();

    // Les objets importants
    MonMaillage = maillage;
    MesFlux = flux;

    effondr = modele->GetParamEffondr();
    OuiNon = new bool[nbmailles];
    srand((unsigned) time(NULL));
    NumMaillesTombees = new int[effondr.Nmax];
    VolumeMoins = new double[effondr.Nmax];
    NumMaillesEcrasees = new int[effondr.Nmax];
    VolumePlus = new double[effondr.Nmax];
    temps_effondr = 0.;
    temps_critique = 3. * temps_effondr;

    
    if (modele->GetOptions().OptionHauteurEau) MonGetAlti = &CMaille::GetAltiEau; // 2022
    else MonGetAlti = &CMaille::GetAlti;

    // Selon l'option de calcul de pente et ILE ou non:
    if (modele->GetOptions().OptionIle) 
    {
        MonPlusBasDesBas = &CMaillage::PlusBasDesBas_MultFlow; // Rmq 2022 si on reprend les effondrements il faudra ajouter une surcharge pour le cas OptionHauteurEau
        if (modele->GetOptions().OptionHauteurEau)
            MonCalcPentes_Maille = &PentesMaille_MultFlow_SurfaceEau_ILE; //2022
        else
            MonCalcPentes_Maille = &PentesMaille_MultFlow_ILE;
    } 
    else 
    {
        MonPlusBasDesBas = &CMaillage::PlusBasDesBas_MultFlow; // Rmq 2022 si on reprend les effondrement il faudra ajouter une surcharge pour le cas OptionHauteurEau
        if (modele->GetOptions().OptionHauteurEau)
            MonCalcPentes_Maille = &PentesMaille_MultFlow_SurfaceEau; // 2022
        else
            MonCalcPentes_Maille = &PentesMaille_MultFlow;
    }

    // Selon l'option d'effondrements :
    if (modele->TesteOptionBroyage())
        MonAjusteStockSed = &CFlux::Broie;
    else
        MonAjusteStockSed = &CFlux::NeBroiePas;

    // Selon le nombre de lithos (1 ou +) :
    if (modele->GetNbLitho() <= 1)
        MonActualiseStrati = &CFlux::SoustraitAlti_ActualiseStrati_MonoCouche;
    else
        MonActualiseStrati = &CFlux::SoustraitAlti_ActualiseStrati_MultiCouche;

}

// destructeur

CEffondr::~CEffondr() 
{
    delete [] OuiNon;
    delete [] NumMaillesTombees;
    delete [] VolumeMoins;
    delete [] NumMaillesEcrasees;
    delete [] VolumePlus;
}

// autres fonctions

void CEffondr::InitialiseEffondr() 
{
    for (int i = 0; i < nbmailles; i++) 
    {
        OuiNon[i] = false;
    }
    NbMaillesTombees = 0;
    NbMaillesEcrasees = 0;
    return;
}

//------------------------------------------
// Actualisation des altitudes et des pentes
//------------------------------------------

void CEffondr::ActualiseEffondre() 
{
    CMaille* MaMaille;
    int i, j, nummaille, numvoisin;
    // Actualise l'altitude et la strati des mailles tomb�es
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        nummaille = NumMaillesTombees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        (MesFlux->*MonActualiseStrati)(nummaille, MaMaille, VolumeMoins[i]);
    }
    // Actualise l'altitude et la strati des mailles �cras�es
    for (i = 0; i < NbMaillesEcrasees; i++) 
    {
        nummaille = NumMaillesEcrasees[i];
        if (nummaille >= 0) 
        {
            MaMaille = MonMaillage->GetMaille(nummaille);
            MesFlux->IncrementeAlti_ActualiseStock(nummaille, MaMaille, VolumePlus[i]);
        }
    }
    // Actualise les pentes des mailles tomb�es et de leurs voisins
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        nummaille = NumMaillesTombees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        MonCalcPentes_Maille(MaMaille, MonMaillage, MonGetAlti, pas3);

        for (j = 0; j < 8; j++) 
        {
            numvoisin = MaMaille->GetNumVoisin(j);
            if (numvoisin >= 0)
                MonCalcPentes_Maille(MonMaillage->GetMaille(numvoisin), MonMaillage,
                    MonGetAlti, pas3);
        }
    }
    // Actualise les pentes des mailles �cras�es et de leurs voisins
    for (i = 0; i < NbMaillesEcrasees; i++) 
    {
        nummaille = NumMaillesEcrasees[i];
        if (nummaille >= 0) 
        {
            MaMaille = MonMaillage->GetMaille(nummaille);
            MonCalcPentes_Maille(MaMaille, MonMaillage, MonGetAlti, pas3);
            for (j = 0; j < 8; j++) 
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                if (numvoisin >= 0)
                    MonCalcPentes_Maille(MonMaillage->GetMaille(numvoisin), MonMaillage,
                        MonGetAlti, pas3);
            }
        }
    }
    // Actualise le temps �coul� depuis le dernier effondrement
    if (temps_effondr < temps_critique)
        temps_effondr -= effondr.Retour;
    else
        temps_effondr = 0.;
    // C'est tout.
    return;
}

//------------------------------------------
// Actualisation des altitudes et des pentes - cas DL : les s�diments vont illico � l'oc�an
//------------------------------------------

void CEffondr::ActualiseEffondre_TransportInstantane() 
{
    CMaille* MaMaille;
    int i, j, nummaille, numvoisin;

    // Actualise l'altitude et la strati des mailles tomb�es
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        nummaille = NumMaillesTombees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        //##### Modif le 31/07/2006 : on peut �vacuer le stock sed dans la foul�e ici,
        // mais APRES, pour ne pas retirer 2 fois le volume tomb� !
        (MesFlux->*MonActualiseStrati)(nummaille, MaMaille, VolumeMoins[i]);
    }
    // en fait, cette ligne ne sert que dans le cas du broyage
    MesFlux->EvacuationEffondrements(NbMaillesTombees, NumMaillesTombees);
    // Actualise les pentes des mailles tomb�es et de leurs voisins
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        nummaille = NumMaillesTombees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        MonCalcPentes_Maille(MaMaille, MonMaillage, MonGetAlti, pas3);

        for (j = 0; j < 8; j++) 
        {
            numvoisin = MaMaille->GetNumVoisin(j);
            if (numvoisin >= 0)
                MonCalcPentes_Maille(MonMaillage->GetMaille(numvoisin), MonMaillage,
                    MonGetAlti, pas3);
        }
    }

    // Actualise le temps �coul� depuis le dernier effondrement
    if (temps_effondr < temps_critique)
        temps_effondr -= effondr.Retour;
    else
        temps_effondr = 0.;
    // C'est tout.
    return;
}

//----------------------------------------------------------
// Actualisation des altitudes et des pentes : cas d'une ILE
//----------------------------------------------------------

void CEffondr::ActualiseEffondre_ILE() 
{
    CMaille* MaMaille;
    int i, j, nummaille, numvoisin;
    // Actualise l'altitude et la strati des mailles tomb�es
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        nummaille = NumMaillesTombees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        (MesFlux->*MonActualiseStrati)(nummaille, MaMaille, VolumeMoins[i]);
    }
    MesFlux->EvacuationEffondrements(NbMaillesTombees, NumMaillesTombees);
    // Actualise l'altitude et la strati des mailles �cras�es
    for (i = 0; i < NbMaillesEcrasees; i++) 
    {
        nummaille = NumMaillesEcrasees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        MesFlux->IncrementeAlti_ActualiseStock(nummaille, MaMaille, VolumePlus[i]);
    }
    // Actualise les pentes des mailles tomb�es et de leurs voisins
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        nummaille = NumMaillesTombees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        MonCalcPentes_Maille(MaMaille, MonMaillage, MonGetAlti, pas3);
        for (j = 0; j < 8; j++) 
        {
            numvoisin = MaMaille->GetNumVoisin(j);
            if (MonMaillage->MailleTerre(numvoisin))
                MonCalcPentes_Maille(MonMaillage->GetMaille(numvoisin), MonMaillage,
                    MonGetAlti, pas3);
        }
    }
    // Actualise les pentes des mailles �cras�es et de leurs voisins
    for (i = 0; i < NbMaillesEcrasees; i++) 
    {
        nummaille = NumMaillesEcrasees[i];
        if (MonMaillage->MailleTerre(nummaille)) 
        {
            MaMaille = MonMaillage->GetMaille(nummaille);
            MonCalcPentes_Maille(MaMaille, MonMaillage, MonGetAlti, pas3);
            for (j = 0; j < 8; j++) 
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                if (MonMaillage->MailleTerre(numvoisin))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(numvoisin), MonMaillage,
                        MonGetAlti, pas3);
            }
        }
    }
    // Actualise le temps �coul� depuis le dernier effondrement
    if (temps_effondr < temps_critique)
        temps_effondr -= effondr.Retour;
    else
        temps_effondr = 0.;
    // C'est tout.
    return;
}

//----------------------------------------------------------
// Actualisation des altitudes et des pentes : cas d'une ILE - cas DL : les s�diments vont illico � l'oc�an
//----------------------------------------------------------

void CEffondr::ActualiseEffondre_TransportInstantane_ILE() 
{
    CMaille* MaMaille;
    int i, j, nummaille, numvoisin;
    // Actualise l'altitude et la strati des mailles tomb�es
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        nummaille = NumMaillesTombees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        (MesFlux->*MonActualiseStrati)(nummaille, MaMaille, VolumeMoins[i]);
    }
    // Actualise les pentes des mailles tomb�es et de leurs voisins
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        nummaille = NumMaillesTombees[i];
        MaMaille = MonMaillage->GetMaille(nummaille);
        MonCalcPentes_Maille(MaMaille, MonMaillage, MonGetAlti, pas3);
        for (j = 0; j < 8; j++) 
        {
            numvoisin = MaMaille->GetNumVoisin(j);
            if (MonMaillage->MailleTerre(numvoisin))
                MonCalcPentes_Maille(MonMaillage->GetMaille(numvoisin), MonMaillage,
                    MonGetAlti, pas3);
        }
    }
    // Actualise le temps �coul� depuis le dernier effondrement
    if (temps_effondr < temps_critique)
        temps_effondr -= effondr.Retour;
    else
        temps_effondr = 0.;
    // C'est tout.
    return;
}

//========================================
//
//		   CALCULS D'EFFONDREMENTS
//
//========================================

//----------------------------------------------------
// Effondrement :
//
// calcul d'effondrement explicit� pour la maille i
// NB: retourne "true" s'il y a eu un effondrement
// ( on part de la premi�re maille i ;
// SI elle s'effondre, on va regarder dans toutes les directions autour ;
// pour les autres points, on ne cherche q'un autre effondrement adjacent
// - autrement dit, chaque maille effondr�e a au plus 2 voisins effondr�s
// --> structure plus ou moins lin�aire (falaise, rempart)
//----------------------------------------------------

bool CEffondr::Effondrement(const int &i) 
{
    CMaille* MaMaille = MonMaillage->GetMaille(i);
    CMaille* MonAutreMaille;
    CMaille* AnotherMaille;
    int dirvoisin, numvoisin, numvoisin2;
    bool plus_loin;

    // Calcul de la pente (NB: avec z_eau)
    // S'il existe un voisin plus bas
    if (!MonCalcPentes_Maille(MaMaille, MonMaillage, MonGetAlti, pas3))
        return false;

    // Cette maille peut-elle s'effondrer ? (crit�re de pente)
    dirvoisin = (MonMaillage->*MonPlusBasDesBas)(MaMaille);

    if (!(MaMaille->GetPente(dirvoisin) >= effondr.Seuil)) return false;

    // Cette maille s'effondre-t-elle vraiment ? (-> si pas au bord d'un flux nul, oui)
    if (!EffondreSurLeVoisin(MaMaille, i, dirvoisin)) return false;

    if (NbMaillesTombees < effondr.Nmax) return true;
    ////// Si elle s'effondre effectivement, on fait le tour des voisins
    for (int j = 0; j < 8; j++) 
    {
        numvoisin = MaMaille->GetNumVoisin(j);
        if ((numvoisin < 0) || OuiNon[numvoisin]) continue;

        MonAutreMaille = MonMaillage->GetMaille(numvoisin);
        // Calcul de pente pour ce voisin :
        MonCalcPentes_Maille(MonAutreMaille, MonMaillage, MonGetAlti, pas3);
        dirvoisin = (MonMaillage->*MonPlusBasDesBas)(MonAutreMaille);
        if (MonAutreMaille->GetPente(dirvoisin) < effondr.Seuil) continue;

        // Si ce voisin s'effondre
        while (EffondreSurLeVoisin(MonAutreMaille, numvoisin, dirvoisin))
            // Poursuite de l'effondrement chez les voisins :
        {
            // Sauf si on a atteint le quota
            if (NbMaillesTombees >= effondr.Nmax) break;
            plus_loin = false;
            for (int jj = 0; jj < 8; jj++) 
            {
                numvoisin2 = MonAutreMaille->GetNumVoisin(jj);
                if ((numvoisin2 < 0) || OuiNon[numvoisin2]) continue;
                // Si num >= 0 et non encore effondr�e
                AnotherMaille = MonMaillage->GetMaille(numvoisin2);
                MonCalcPentes_Maille(AnotherMaille, MonMaillage, MonGetAlti, pas3);
                dirvoisin = (MonMaillage->*MonPlusBasDesBas)(AnotherMaille);
                if (AnotherMaille->GetPente(dirvoisin) >= effondr.Seuil) 
                {
                    MonAutreMaille = AnotherMaille;
                    numvoisin = numvoisin2;
                    plus_loin = true;
                    break;
                }
            }
            if (!plus_loin) break;
        }
        ////// Fin de la boucle sur les voisins de la premi�re maille
    }
    return true;
}

//-------------------------------------------------------
// EffondreSurLeVoisin :
// ===================
// Une fois qu'on sait qui s'effondre sur qui, on fait �a
// NB: retourne "true" s'il y a eu un effondrement
//-------------------------------------------------------

bool CEffondr::EffondreSurLeVoisin(CMaille* MaMaille, const int &numtombe,
        const int &dirvoisin) 
{
    double volumefalaise, volumetombe;
    int numvoisin;

    // Calcul de la pente
    // S'il existe un voisin plus bas
    numvoisin = MaMaille->GetNumVoisinBas(dirvoisin);
    // Si ce voisin n'est pas en-dehors de la grille
    if (numvoisin >= 0) 
    {
        // Si ce voisin ne s'est pas encore effondr� et n'a pas re�u d'effondrement
        if (!OuiNon[numvoisin]) 
        {
            OuiNon[numtombe] = true;
            NumMaillesTombees[NbMaillesTombees] = numtombe;
            // On note qu'il a re�u un effondrement sur la t�te...
            OuiNon[numvoisin] = true;
            NumMaillesEcrasees[NbMaillesEcrasees] = numvoisin;
            volumefalaise = MaMaille->GetPente(dirvoisin) * MaMaille->GetPas3VoisinBas(dirvoisin);
            volumetombe = effondr.K * volumefalaise;
            // ...le volume qu'il a re�u...
            VolumePlus[NbMaillesEcrasees] = volumetombe;
            // ...le volume qui s'est donc effondr�...
            VolumeMoins[NbMaillesTombees] = volumetombe;
            // ... les nouveaux nb de mailles effondr�es et �cras�es...
            NbMaillesTombees++;
            NbMaillesEcrasees++;
            // ...le stock de s�diments qu'il reste sur la maille, apr�s l'effondrement
            // mais AVANT d'avoir pris en compte le dz d'�rosion de l'effondrement.
            (MesFlux->*MonAjusteStockSed)(numtombe, PRESQUE * volumefalaise);


            return true;
        }
    }        // ou s'il est en-dehors mais qu'on n'est pas sur un bord � flux-nul
    else if (!MaMaille->TesteBordFluxNul()) 
    {
        OuiNon[numtombe] = true;
        // On note la maille qui s'est donc effondr�e...
        NumMaillesTombees[NbMaillesTombees] = numtombe;
        volumetombe = effondr.K
                * MaMaille->GetPente(dirvoisin) * MaMaille->GetPas3VoisinBas(dirvoisin);
        // On note le volume qui s'est donc effondr�...
        VolumeMoins[NbMaillesTombees] = volumetombe;
        // On incr�mente le nb de mailles effondr�es...
        NbMaillesTombees++;
        // ... vers l'ext�rieur.
        MesFlux->AddVolumeSed_Out(volumetombe);
        return true;
    }
    return false;
}

//-----------------------------------------------------
// EffondreGrille :
//
// calcul d'effondrement explicit� pour toute la grille
//
// retourne "1" s'il y a eu un effondrement (sinon 0)
//-----------------------------------------------------

int CEffondr::EffondreGrille(const double &dt) 
{
    int irand = 1 + rand() % (nbmailles - 1);
    int i = irand;
    int j;
    bool flag = false;

    // Le temps �coul� depuis le dernier effondrement...
    temps_effondr += dt;
    // Si ce temps n'est pas suffisant, on se contente de calculer les pentes
    if (temps_effondr < effondr.Retour) 
    {
        for (int i = 0; i < nbmailles; i++) 
        {
            if (MonMaillage->MailleTerre(i))
                MonCalcPentes_Maille(MonMaillage->GetMaille(i), MonMaillage,
                    MonGetAlti, pas3);
        }
        return 0;
    }

    //====================================
    // Sinon, on teste les effondrements :
    //====================================
    InitialiseEffondr();

    //*********************************************************
    // Commence par tester les mailles de IRAND � (nbmailles-1)
    //*********************************************************
    do {
        flag = Effondrement(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            // On reprend les calculs de pente l� o� on �tait rest�...
            j = i;
            while (j < nbmailles) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ... on finit la boucle de 0 � IRAND-1
            j = 0;
            while (j < irand) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j), MonMaillage,
                        MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }    while (i < nbmailles);

    //******************************************************
    // Si toujours rien, on teste les mailles de 0 � IRAND-1
    //******************************************************
    i = 0;
    while (i < irand) 
    {
        flag = Effondrement(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            // On finit la boucle de 0 � IRAND-1...
            j = i;
            while (j < irand) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j), MonMaillage,
                        MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }

    //******************************************************
    // S'il n'y a eu aucun effondrement
    return 0;
}
//-----------------------------------------------------
// EffondreGrille_TransportInstantane :
//
// dans le cas DL, on transporte tout instantan�ment pour simplifier
//-----------------------------------------------------

int CEffondr::EffondreGrille_TransportInstantane(const double &dt) 
{
    int irand = 1 + rand() % (nbmailles - 1);
    int i = irand;
    int j;
    bool flag = false;

    // Le temps �coul� depuis le dernier effondrement...
    temps_effondr += dt;
    // Si ce temps n'est pas suffisant, on se contente de calculer les pentes
    if (temps_effondr < effondr.Retour) 
    {
        for (int i = 0; i < nbmailles; i++) 
        {
            if (MonMaillage->MailleTerre(i))
                MonCalcPentes_Maille(MonMaillage->GetMaille(i), MonMaillage,
                    MonGetAlti, pas3);
        }
        return 0;
    }

    //====================================
    // Sinon, on teste les effondrements :
    //====================================
    InitialiseEffondr();

    //*********************************************************
    // Commence par tester les mailles de IRAND � (nbmailles-1)
    //*********************************************************
    do {
        flag = Effondrement(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            // On reprend les calculs de pente l� o� on �tait rest�...
            j = i;
            while (j < nbmailles) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ... on finit la boucle de 0 � IRAND-1
            j = 0;
            while (j < irand) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j), MonMaillage,
                        MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre_TransportInstantane();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }    while (i < nbmailles);

    //******************************************************
    // Si toujours rien, on teste les mailles de 0 � IRAND-1
    //******************************************************
    i = 0;
    while (i < irand) 
    {
        flag = Effondrement(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            // On finit la boucle de 0 � IRAND-1...
            j = i;
            while (j < irand) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j), MonMaillage,
                        MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre_TransportInstantane();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }

    //******************************************************
    // S'il n'y a eu aucun effondrement
    return 0;
}


//
//
//		   CAS D UNE ILE
//
//

//----------------------------------------------------
// Effondrement : cas d'une ILE
//----------------------------------------------------

bool CEffondr::Effondrement_ILE(const int &i) 
{
    if (!MonMaillage->MailleTerre(i))
        return false;

    CMaille* MaMaille = MonMaillage->GetMaille(i);
    CMaille* MonAutreMaille;
    CMaille* AnotherMaille;
    int dirvoisin, numvoisin, numvoisin2;
    bool plus_loin;

    // Calcul de la pente (NB: avec z_eau)
    // S'il existe un voisin plus bas
    if (!MonCalcPentes_Maille(MaMaille, MonMaillage, MonGetAlti, pas3))
        return false;

    // Cette maille peut-elle s'effondrer ? (crit�re de pente)
    dirvoisin = (MonMaillage->*MonPlusBasDesBas)(MaMaille);

    if (!(MaMaille->GetPente(dirvoisin) >= effondr.Seuil)) return false;

    // Cette maille s'effondre-t-elle vraiment ? (-> si pas au bord d'un flux nul, oui)
    if (!EffondreSurLeVoisin_ILE(MaMaille, i, dirvoisin))
        return false;

    if (NbMaillesTombees < effondr.Nmax) return true;
    ////// Si elle s'effondre effectivement, on fait le tour des voisins
    for (int j = 0; j < 8; j++) 
    {
        numvoisin = MaMaille->GetNumVoisin(j);
        if (OuiNon[numvoisin]) continue;
        if (!MonMaillage->MailleTerre(numvoisin)) continue;

        MonAutreMaille = MonMaillage->GetMaille(numvoisin);
        // Calcul de pente pour ce voisin :
        MonCalcPentes_Maille(MonAutreMaille, MonMaillage, MonGetAlti, pas3);
        dirvoisin = (MonMaillage->*MonPlusBasDesBas)(MonAutreMaille);
        if (MonAutreMaille->GetPente(dirvoisin) < effondr.Seuil) continue;

        // Si ce voisin s'effondre
        while (EffondreSurLeVoisin_ILE(MonAutreMaille, numvoisin, dirvoisin))
            // Poursuite de l'effondrement chez les voisins :
        {
            // Sauf si on a atteint le quota
            if (NbMaillesTombees >= effondr.Nmax) break;
            plus_loin = false;
            for (int jj = 0; jj < 8; jj++) 
            {
                numvoisin2 = MonAutreMaille->GetNumVoisin(jj);
                if (OuiNon[numvoisin2]) continue;
                // Si non encore effondr�e
                if (!MonMaillage->MailleTerre(numvoisin2)) continue;

                AnotherMaille = MonMaillage->GetMaille(numvoisin2);
                MonCalcPentes_Maille(AnotherMaille, MonMaillage, MonGetAlti, pas3);
                dirvoisin = (MonMaillage->*MonPlusBasDesBas)(AnotherMaille);
                if (AnotherMaille->GetPente(dirvoisin) >= effondr.Seuil) 
                {
                    MonAutreMaille = AnotherMaille;
                    numvoisin = numvoisin2;
                    plus_loin = true;
                    break;
                }
            }
            if (!plus_loin) break;
        }
        ////// Fin de la boucle sur les voisins de la premi�re maille
    }
    return true;
}

//-------------------------------------------------------
// EffondreSurLeVoisin : cas d'une ILE
//-------------------------------------------------------

bool CEffondr::EffondreSurLeVoisin_ILE(CMaille* MaMaille, const int &numtombe,
        const int &dirvoisin) 
{
    double volumefalaise, volumetombe;
    int numvoisin;

    // Calcul de la pente
    // S'il existe un voisin plus bas
    numvoisin = MaMaille->GetNumVoisinBas(dirvoisin);
    // Si ce voisin ne s'est pas encore effondr� et n'a pas re�u d'effondrement
    if (!OuiNon[numvoisin]) 
    {
        OuiNon[numtombe] = true;
        NumMaillesTombees[NbMaillesTombees] = numtombe;
        // On note qu'il a re�u un effondrement sur la t�te...
        OuiNon[numvoisin] = true;
        NumMaillesEcrasees[NbMaillesEcrasees] = numvoisin;
        volumefalaise = MaMaille->GetPente(dirvoisin) * MaMaille->GetPas3VoisinBas(dirvoisin);
        volumetombe = effondr.K * volumefalaise;
        // ...le volume qu'il a re�u...
        VolumePlus[NbMaillesEcrasees] = volumetombe;
        // ...le volume qui s'est donc effondr�...
        VolumeMoins[NbMaillesTombees] = volumetombe;
        // ... les nouveaux nb de mailles effondr�es et �cras�es...
        NbMaillesTombees++;
        NbMaillesEcrasees++;
        // ...le stock de s�diments qu'il reste sur la maille, apr�s l'effondrement
        // mais AVANT d'avoir pris en compte le dz d'�rosion de l'effondrement.
        (MesFlux->*MonAjusteStockSed)(numtombe, PRESQUE * volumefalaise);
        return true;
    } 
    else
        return false;
}

//-----------------------------------------------------
// EffondreGrille : cas d'une ILE
//-----------------------------------------------------

int CEffondr::EffondreGrille_ILE(const double &dt) 
{
    int irand = 1 + rand() % (nbmailles - 1);
    int i = irand;
    int j;
    bool flag = false;

    // Le temps �coul� depuis le dernier effondrement...
    temps_effondr += dt;
    // Si ce temps n'est pas suffisant, on se contente de calculer les pentes
    if (temps_effondr < effondr.Retour) 
    {
        for (int i = 0; i < nbmailles; i++) 
        {
            if (MonMaillage->MailleTerre(i))
                MonCalcPentes_Maille(MonMaillage->GetMaille(i),
                    MonMaillage, MonGetAlti, pas3);
        }
        return 0;
    }

    //====================================
    // Sinon, on teste les effondrements :
    //====================================
    InitialiseEffondr();

    //*********************************************************
    // Commence par tester les mailles de IRAND � (nbmailles-1)
    //*********************************************************
    do 
    {
        flag = Effondrement_ILE(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            //////////////////////////////////////////////////////////////////////////////
            AfficheMessage("Collapes : ", NbMaillesTombees);
            //////////////////////////////////////////////////////////////////////////////
            // On reprend les calculs de pente l� o� on �tait rest�...
            j = i;
            while (j < nbmailles) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ... on finit la boucle de 0 � IRAND-1
            j = 0;
            while (j < irand) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre_ILE();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }    while (i < nbmailles);

    //******************************************************
    // Si toujours rien, on teste les mailles de 0 � IRAND-1
    //******************************************************
    i = 0;
    while (i < irand) 
    {
        flag = Effondrement_ILE(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            //////////////////////////////////////////////////////////////////////////////
            AfficheMessage("Effondrements : ", NbMaillesTombees);
            //////////////////////////////////////////////////////////////////////////////
            // On finit la boucle de 0 � IRAND-1...
            j = i;
            while (j < irand) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre_ILE();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }

    //******************************************************
    // S'il n'y a eu aucun effondrement
    return 0;
}

//-----------------------------------------------------
// EffondreGrille_TransportInstantane : cas d'une ILE
// dans le cas DL, on transporte tout instantan�ment pour simplifier
//-----------------------------------------------------

int CEffondr::EffondreGrille_TransportInstantane_ILE(const double &dt) 
{
    int irand = 1 + rand() % (nbmailles - 1);
    int i = irand;
    int j;
    bool flag = false;

    // Le temps �coul� depuis le dernier effondrement...
    temps_effondr += dt;
    // Si ce temps n'est pas suffisant, on se contente de calculer les pentes
    if (temps_effondr < effondr.Retour) 
    {
        for (int i = 0; i < nbmailles; i++) 
        {
            if (MonMaillage->MailleTerre(i))
                MonCalcPentes_Maille(MonMaillage->GetMaille(i),
                    MonMaillage, MonGetAlti, pas3);
        }
        return 0;
    }

    //====================================
    // Sinon, on teste les effondrements :
    //====================================
    InitialiseEffondr();

    //*********************************************************
    // Commence par tester les mailles de IRAND � (nbmailles-1)
    //*********************************************************
    do 
    {
        flag = Effondrement_ILE(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            //////////////////////////////////////////////////////////////////////////////
            AfficheMessage("Effondrements : ", NbMaillesTombees);
            //////////////////////////////////////////////////////////////////////////////
            // On reprend les calculs de pente l� o� on �tait rest�...
            j = i;
            while (j < nbmailles) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ... on finit la boucle de 0 � IRAND-1
            j = 0;
            while (j < irand) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre_TransportInstantane_ILE();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }    while (i < nbmailles);

    //******************************************************
    // Si toujours rien, on teste les mailles de 0 � IRAND-1
    //******************************************************
    i = 0;
    while (i < irand) 
    {
        flag = Effondrement_ILE(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            //////////////////////////////////////////////////////////////////////////////
            AfficheMessage("Effondrements : ", NbMaillesTombees);
            //////////////////////////////////////////////////////////////////////////////
            // On finit la boucle de 0 � IRAND-1...
            j = i;
            while (j < irand) 
            {
                if (MonMaillage->MailleTerre(j))
                    MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre_TransportInstantane_ILE();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }

    //******************************************************
    // S'il n'y a eu aucun effondrement
    return 0;
}




//-----------------------------------------------------------
// fonction sans effondrement : calcule quand m�me les pentes
// toujours en notant s'il s'agit d'un minimum local ou non
//-----------------------------------------------------------

int CEffondr::PenteSansEffondr(const double &dt) 
{
    for (int i = 0; i < nbmailles; i++) 
    {
        if (MonMaillage->MailleTerre(i)) 
        {
            MonCalcPentes_Maille(MonMaillage->GetMaille(i), MonMaillage, MonGetAlti, pas3);
        }
    }
    return 0;
}

//
//
//		   CAS D UNE TERRE (pas de niveau de base)
//
//

//-----------------------------------------------------
// EffondreGrille :
//-----------------------------------------------------

int CEffondr::EffondreGrille_TERRE(const double &dt) 
{
    int irand = 1 + rand() % (nbmailles - 1);
    int i = irand;
    int j;
    bool flag = false;

    // Le temps �coul� depuis le dernier effondrement...
    temps_effondr += dt;
    // Si ce temps n'est pas suffisant, on se contente de calculer les pentes
    if (temps_effondr < effondr.Retour) 
    {
        for (int i = 0; i < nbmailles; i++)
            MonCalcPentes_Maille(MonMaillage->GetMaille(i),
                MonMaillage, MonGetAlti, pas3);
        return 0;
    }

    //====================================
    // Sinon, on teste les effondrements :
    //====================================
    InitialiseEffondr();

    //*********************************************************
    // Commence par tester les mailles de IRAND � (nbmailles-1)
    //*********************************************************
    do 
    {
        flag = Effondrement(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            // On reprend les calculs de pente l� o� on �tait rest�...
            j = i;
            while (j < nbmailles) 
            {
                MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ... on finit la boucle de 0 � IRAND-1
            j = 0;
            while (j < irand) 
            {
                MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }    while (i < nbmailles);

    //******************************************************
    // Si toujours rien, on teste les mailles de 0 � IRAND-1
    //******************************************************
    i = 0;
    while (i < irand) 
    {
        flag = Effondrement(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            // On finit la boucle de 0 � IRAND-1...
            j = i;
            while (j < irand) 
            {
                MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }

    //******************************************************
    // S'il n'y a eu aucun effondrement
    return 0;
}

//-----------------------------------------------------
// EffondreGrille_TransportInstantane : cas d'une TERRE
// dans le cas DL, on transporte tout instantan�ment pour simplifier
//-----------------------------------------------------

int CEffondr::EffondreGrille_TransportInstantane_TERRE(const double &dt) 
{
    int irand = 1 + rand() % (nbmailles - 1);
    int i = irand;
    int j;
    bool flag = false;

    // Le temps �coul� depuis le dernier effondrement...
    temps_effondr += dt;
    // Si ce temps n'est pas suffisant, on se contente de calculer les pentes
    if (temps_effondr < effondr.Retour) 
    {
        for (int i = 0; i < nbmailles; i++)
            MonCalcPentes_Maille(MonMaillage->GetMaille(i),
                MonMaillage, MonGetAlti, pas3);
        return 0;
    }

    //====================================
    // Sinon, on teste les effondrements :
    //====================================
    InitialiseEffondr();

    //*********************************************************
    // Commence par tester les mailles de IRAND � (nbmailles-1)
    //*********************************************************
    do 
    {
        flag = Effondrement(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            // On reprend les calculs de pente l� o� on �tait rest�...
            j = i;
            while (j < nbmailles) 
            {
                MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ... on finit la boucle de 0 � IRAND-1
            j = 0;
            while (j < irand) 
            {
                MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre_TransportInstantane();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }    while (i < nbmailles);

    //******************************************************
    // Si toujours rien, on teste les mailles de 0 � IRAND-1
    //******************************************************
    i = 0;
    while (i < irand) 
    {
        flag = Effondrement(i);
        i++;
        // Si on trouve un effondrement avant la fin :
        if (flag) 
        {
            // On finit la boucle de 0 � IRAND-1...
            j = i;
            while (j < irand) 
            {
                MonCalcPentes_Maille(MonMaillage->GetMaille(j),
                        MonMaillage, MonGetAlti, pas3);
                j++;
            }
            // ...on actualise les altitudes et les pentes
            ActualiseEffondre_TransportInstantane();
            // ...et on sort parce qu'il y a eu un effondrement
            return 1;
        }
    }

    //******************************************************
    // S'il n'y a eu aucun effondrement
    return 0;
}

int CEffondr::PenteSansEffondr_TERRE(const double &dt) 
{
    for (int i = 0; i < nbmailles; i++)
    {
        MonCalcPentes_Maille(MonMaillage->GetMaille(i), MonMaillage, MonGetAlti, pas3);
    }
    return 0;
}



void CEffondr::AfficheEffondrement() 
{
    int i;
    AfficheMessage("Effondrement :");
    // mailles tomb�es
    AfficheMessage("	- Cells that fell  : ", NbMaillesTombees);
    for (i = 0; i < NbMaillesTombees; i++) 
    {
        AfficheMessage(i, NumMaillesTombees[i], VolumeMoins[i]);
    }
    // mailles �cras�es
    AfficheMessage("	- Scratched cells : ", NbMaillesEcrasees);
    for (i = 0; i < NbMaillesEcrasees; i++) 
    {
        AfficheMessage(i, NumMaillesEcrasees[i], VolumePlus[i]);
    }
    return;
}






