#include "Ccalc_bilansed.h"
#include "Ccalc_pentes.h"
#include <time.h>
using namespace std;


//-------------------------------------------
//
// fonctions de la classe CBilanSed :
//
//-------------------------------------------

// constructeur : nbmax = effondr.Nmax

CBilanSed::CBilanSed(CModele* modele, CMaillage* maillage, CFlux* flux, CBilanEau* bileau) 
{
    nbmailles = maillage->GetNbMailles();
    pas3 = maillage->GetPas3();

    // Les objets importants
    MonMaillage = maillage;
    MesFlux = flux;
    MonBilanEau = bileau;

    // Les objets de calcul
    MaDiffusion = new CDiffusion(modele, MonMaillage, MesFlux);
    MonErosionFluviale = new CErosionFluviale(modele, MesFlux);

    

    // Param�tres d'alt�ration pour le bedrock
    int nblitho = modele->GetNbLitho();

    // Param�tres de diffusion des sediment dans les lacs ou aeriens	
    diffsed = modele->GetParamDiffSed(); // janvier 2013
    difflac = modele->GetParamDiffLac(); // janvier 2013

    // Selon l'option : transport s�dim ou non :
    if ((modele->GetOptions()).OptionTransp)
        MonActuCoeffErosTranspSediment = &CErosionFluviale::ActualiseCoeffSediment;
    else
        MonActuCoeffErosTranspSediment = &CErosionFluviale::NeFaitRien;

    // Selon l'option : incision bedrock ou non :
    if ((modele->GetOptions()).OptionIncis)
        MonActuCoeffBedrock = &CErosionFluviale::ActualiseCoeffBedrock;
    else
        MonActuCoeffBedrock = &CErosionFluviale::NeFaitRien;

    // Selon l'option d'alt�ration : // bloc modifie le 01/02/11
    if ((modele->GetOptions()).OptionAlter) 
    {
        if (nblitho <= 0)
            MonAlteration = &CFlux::NAlterePas;
        else if (nblitho == 1) 
        {
            if ((modele->GetOptions()).OptionTempRunoff) 
            {
                MonAlteration = &CFlux::AltereBedrock_MonoCouche_TempRunoff;
            } 
            else
                MonAlteration = &CFlux::AltereBedrock_MonoCouche;
        } 
        else
        {
            if ((modele->GetOptions()).OptionTempRunoff) 
            {
                MonAlteration = &CFlux::AltereBedrock_MultiCouche_TempRunoff;
            } 
            else            
                MonAlteration = &CFlux::AltereBedrock_MultiCouche;
        }
    } 
    else
        MonAlteration = &CFlux::NAlterePas;

    // Selon l'option : diffusion lacs ou non :
    if ((modele->GetOptions()).OptionDiffLac)
        MonActuCoeffDiffLac = &CDiffusion::ActualiseCoeffDiffLac;
    else
        MonActuCoeffDiffLac = &CDiffusion::NeFaitRien;

    // Selon l'option : diffusion s�dim ou non :
    if ((modele->GetOptions()).OptionDiffSed)
        MonActuCoeffDiffSed = &CDiffusion::ActualiseCoeffDiffSed;
    else
        MonActuCoeffDiffSed = &CDiffusion::NeFaitRien;

    // Selon l'option : diffusion bedrock ou non :
    if ((modele->GetOptions()).OptionDiffBedrock)
        MonActuCoeffDiffBedrock = &CDiffusion::ActualiseCoeffDiffBedrock;
    else
        MonActuCoeffDiffBedrock = &CDiffusion::NeFaitRien;

    // Selon les options de diffusion :
    if ((modele->GetOptions()).OptionDiffBedrock) 
    {
        if (nblitho <= 0)
            MonCalculeDiff = &CDiffusion::DiffuseLesSedSeulsTEST;
        else
            MonCalculeDiff = &CDiffusion::DiffuseToutTEST;
    }
    else
        MonCalculeDiff = &CDiffusion::DiffuseLesSedSeulsTEST;

    if ((modele->GetOptions()).OptionDiffLac) 
    {
        if ((modele->GetOptions()).OptionDiffBedrock)
            MonCalculeDiffLac = &CDiffusion::DiffuseToutLacTEST;
        else
            MonCalculeDiffLac = &CDiffusion::DiffuseLesSedSeulsLacTEST;
    }
    else
        MonCalculeDiffLac = &CDiffusion::DeposeLesSediments;

    if (!(modele->GetOptions()).OptionDiffSed) 
    {
        MonCalculeDiff = &CDiffusion::NeDiffuseRien;
    }



    MonCalculeErosion = &CErosionFluviale::DetachTransport_MultFlow_MultiCouche;
    //if ((modele->GetOptions()).OptionHauteurEau) MonCalculeErosion = &CErosionFluviale::DetachTransport_MultFlow_MultiCouche_Heau; 

    if ((!(modele->GetOptions()).OptionTransp) && (!(modele->GetOptions()).OptionIncis)) 
    {
        MonCalculeErosion = &CErosionFluviale::NeCalculeRien;
    }

    // Calcul de pente en multiple flow:

    
    if ((modele->GetOptions()).OptionHauteurEau)
        MonCalcPentes_Maille = &PentesMaille_MultFlow_SurfaceEau;
    else
        MonCalcPentes_Maille = &PentesMaille_MultFlow;

    // Selon l'option de seuil de flux (critique versants/rivi�res) :
    if ((modele->GetOptions()).OptionSeuilFlux)
        FluxCritique_paran = modele->GetFluxCritique();
    else
        FluxCritique_paran = 0.;

    MonActuErosion = &CFlux::ActualiseErosion;


    // Choix function erosion laterale. modif juin 2014
    if ((modele->GetParamIncisSed())->KErosionLaterale > 0.0000000001)
    {
        //if ((modele->GetOptions()).OptionHauteurEau) 
         //   MonErosionLaterale = &CErosionFluviale::ErosionLateraleHeau;
        //else
            MonErosionLaterale = &CErosionFluviale::ErosionLaterale;
    } 
    else 
    {
        MonErosionLaterale = &CErosionFluviale::NeErodePasLateralement;
    }
}

// destructeur

CBilanSed::~CBilanSed() 
{
    delete MaDiffusion;
    delete MonErosionFluviale;
}

//-------------------------------------------
// Calcul du bilan d'�rosion pour le maillage 
//-------------------------------------------


// profondement modifie en janvier 2013

void CBilanSed::BilanSedim() 
{
    int nummaille;
    CMaille* MaMaille;
    double volfluv = 0.;

    // Boucle "while" sur la liste des mailles classees par altitudes
    for (int i = 0; i < nbmailles; i++) 
    {
        nummaille = MonMaillage->GetNumListe(i);
        MaMaille = MonMaillage->GetMaille(nummaille);

        //;;;;;;;;;;;;;;;;;;;;;;
        //		ALTERATION
        //;;;;;;;;;;;;;;;;;;;;;;
        (MesFlux->*MonAlteration)(nummaille, MaMaille);
        //;;;;;;;;;;;;;;;;;;;;;;
        // Si on est DANS UN LAC OU DANS LA MER:
        //;;;;;;;;;;;;;;;;;;;;;;
        if (MonBilanEau->GetNumLac(nummaille) < 0) 
        {
            //;;;;;;;;;;;;;;;;;;;;;
            //		DIFFUSION
            //;;;;;;;;;;;;;;;;;;;;;
            // comme il n'y a que de la diffusion qui agit, on ajoute le flux entrant de sediments fluviatiles
            // au flux entrant de sediments issus de la diffusion, de sorte que ces sediments soient pris en compte
            // dans le bilan erosion-depot de la maille.
            volfluv = MesFlux->GetVolumeFluv(nummaille);
            MesFlux->AddVolumeDiff(nummaille, volfluv);

            // puis on calcule la diffusion			
            if (MaMaille->GetNbVoisinsBas() == 0) 
            {
                // si on est au fond d'un lac
                MaDiffusion->DeposeLesSediments(nummaille, MaMaille);
                //MaMaille->AddLesAltiLac(MesFlux->CorrigeActu_Lac(nummaille,MaMaille));
                MaMaille->AddLesAlti((MesFlux->*MonActuErosion)(nummaille, MaMaille)); // 2019
            } 
            else 
            {
                // si on n'est pas au fond d'un lac
                (MaDiffusion->*MonCalculeDiffLac)(nummaille, MaMaille);
                MaMaille->AddLesAltiLac(MesFlux->CorrigeActu_LacPasFond(nummaille, MaMaille));
            }

        }            //;;;;;;;;;;;;;;;;;;;;;;
            // Si on n'est PAS DANS SOUS L EAU (LAC OU MER):
            //;;;;;;;;;;;;;;;;;;;;;;
        else 
        {
            ////// Si on est AU BORD d'un lac ou a l'EXUTOIRE, recalcul des pentes :
            if (MonBilanEau->GetNumLac(nummaille) > 0)
            {
                MaMaille->InitialiseLeVoisinage();
                MonCalcPentes_Maille(MaMaille, MonMaillage, &CMaille::GetAltiEau, pas3);
            }
            //;;;;;;;;;;;;;;;;;;;;;
            //		DIFFUSION
            //;;;;;;;;;;;;;;;;;;;;;
            (MaDiffusion->*MonCalculeDiff)(nummaille, MaMaille);
            
            
            //;;;;;;;;;;;;;;;;;;;;
            //	EROSION FLUVIALE
            //;;;;;;;;;;;;;;;;;;;;
            (MonErosionFluviale->*MonCalculeErosion)(nummaille, MaMaille);
            
            
            // Erosion laterale
            
            (MonErosionFluviale->*MonErosionLaterale)(nummaille, MaMaille, MonMaillage); // 7 aout 2008. modif juin 2014
            //;;;;;;;;;;;;;;;;;;;;
            // ACTUALISATION DE L'ALTITUDE
            //;;;;;;;;;;;;;;;;;;;;
            
            MaMaille->AddLesAlti((MesFlux->*MonActuErosion)(nummaille, MaMaille));
        }
        
    }
    return;
}

// 2018 Surcharge dans le cas ou on calcule une hauteur d'eau qui peut amener a un debordement.
// La seule difference est le fait qu'on parcourt en cascade les mailles selon leur altitude de la surface de l'eau.
// La pente qui sert a calculer l'erosion est la pente hydraulique la plus forte. Dans un trou topo, les
// sediments peuvent donc remonter s'il y a continuite de la pente hydraulique.
// Pour transferer les sediments on utilise la ponderation selon la pente de l'eau. Donc des sediments peuvent aussi deborder.
// 2019 NE MARCHE PAS. IL FAUT TOUT REPENSER ET REECRIRE LE CAS DU CALCUL DE LA SURFACE HYDRAULIQUE
// MODIFS EN 2022. OK ! En fait pas tout a fait.
// Debut 2023 Tout est repris a zero avec la creation d'une classe Waterbot qui permet de calculer
// une surface d'eau connectee a un exutoire local (intra maillage). L'eau en exces est "en attente" sur l'exutoire pour être traitee le prochain pas de temps.
// Ce n'est pas parfait car la surface peut etre
// influencee par des mailles voisines hautes, avec une hauteur d'eau surestimee sur les bords pour un lac etendu, mais cela permet de
// connecter assez rapidement une surface tout en respectant les bilans d'eau et de sediments.

void CBilanSed::BilanSedim_Heau() 
{
    int nummaille;
    CMaille* MaMaille;
    double volfluv = 0.;


    // Boucle "while" sur la liste des mailles classees par altitudes
    for (int i = 0; i < nbmailles; i++) 
    {
        nummaille = MonMaillage->GetNumListeEau(i);
        MaMaille = MonMaillage->GetMaille(nummaille);

        //;;;;;;;;;;;;;;;;;;;;;;
        //		ALTERATION
        //;;;;;;;;;;;;;;;;;;;;;;
        (MesFlux->*MonAlteration)(nummaille, MaMaille);
        //;;;;;;;;;;;;;;;;;;;;;;
        // Si on est sous la mer
        //;;;;;;;;;;;;;;;;;;;;;;
        if (MonBilanEau->GetNumLac(nummaille) == -1) // sous la mer
        {

            //;;;;;;;;;;;;;;;;;;;;;
            //		DIFFUSION
            //;;;;;;;;;;;;;;;;;;;;;
            // On est dans la mer.  On ajoute le flux entrant de sediments fluviatiles
            // au flux entrant de sediments issus de la diffusion, de sorte que ces sediments soient pris en compte
            // dans le bilan erosion-depot de la maille.
            volfluv = MesFlux->GetVolumeFluv(nummaille);
            MesFlux->AddVolumeDiff(nummaille, volfluv);

            // puis on calcule la diffusion
            if (MaMaille->GetNbVoisinsBasEau() == 0)
            {
                // si on est dans un trou
                MaDiffusion->DeposeLesSediments(nummaille, MaMaille);
                //MaMaille->AddLesAltiLac(MesFlux->CorrigeActu_Lac(nummaille,MaMaille));
                MaMaille->AddLesAlti((MesFlux->*MonActuErosion)(nummaille, MaMaille));
            } 
            else 
            {
                (MaDiffusion->*MonCalculeDiffLac)(nummaille, MaMaille);
                MaMaille->SetAltiEau(MonMaillage->GetNiveauDeBase()); // on fixe l'altitude de l'eau au niveau de la mer pour ne pas avoir sediment au-dessus ce de niveau dans l'instruction suivante...
                MaMaille->AddLesAltiLac(MesFlux->CorrigeActu_LacPasFond(nummaille, MaMaille));
                MaMaille->SetAltiEau(MaMaille->GetAlti()); // ...puis on remet a l'alti du terrain pour que les mailles de la mer soient bien classees par altitude TERRESTRE decroissante.
            }
        }
        else // quand on rentre ici, pas besoin d'identifier les trous (dans la surface d'eau).
            // Leur flux d'eau courante est nul, de sorte que les sediments entrants sont forcement
            // deposes sur cette maille (et la pente max est nulle donc depot par diffusion aussi)
        {

            //;;;;;;;;;;;;;;;;;;;;;
            //        DIFFUSION
            //;;;;;;;;;;;;;;;;;;;;;
            (MaDiffusion->*MonCalculeDiff)(nummaille, MaMaille);
            
            //;;;;;;;;;;;;;;;;;;;;
            //    EROSION FLUVIALE
            //;;;;;;;;;;;;;;;;;;;;
            (MonErosionFluviale->*MonCalculeErosion)(nummaille, MaMaille);
            
            // Erosion laterale
            (MonErosionFluviale->*MonErosionLaterale)(nummaille, MaMaille, MonMaillage); // 7 aout 2008. modif juin 2014
     
            //;;;;;;;;;;;;;;;;;;;;
            // ACTUALISATION DE L'ALTITUDE
            //;;;;;;;;;;;;;;;;;;;;

            MaMaille->AddLesAltiHeau((MesFlux->*MonActuErosion)(nummaille, MaMaille));
            
            
        }
       
    }
    
    //double totalerosiondepot=0.,eaustockee=0.;
   // for (int i = 0; i < nbmailles; i++)
    //{
        //double totalexporte=0.;
        //for (int jj=0 ; jj<8 ; jj++) totalexporte+=MesFlux->GetVolumeFluvExporte(nummaille,jj) ;
        //cout << i<<" " << MonMaillage->GetMaille(i)->GetAlti()/400.<< " "<< MonMaillage->GetMaille(i)->GetAltiEau()/400.<< " "<<MesFlux->GetEauCourante(i)/400.<<" nbbas " << (MonMaillage->GetMaille(i))->GetNbVoisinsBasEau() << endl;
        //totalerosiondepot+=MesFlux->GetDeltaErosion(i);
        //totalerosiondepot+=MesFlux->GetDeltaErosion(i);
        //MaMaille = MonMaillage->GetMaille(nummaille);
        //eaustockee+= MaMaille->GetAltiEau()-MaMaille->GetAlti();
    //}
    //cout << " fluxin "<< MesFlux->GetVolumeSed_In()<<  " fluxout "<< MesFlux->GetVolumeSed_Out() <<  "  bilantotal " <<    MesFlux->GetVolumeSed_Out()-MesFlux->GetVolumeSed_In() +totalerosiondepot<< " totalerosiondepot " << totalerosiondepot << endl;
    
    return;
}



//+++++++++++++++++++++++++++++++++++++++++++++++
// Surcharge si FLUX CRITIQUE Versants / Rivi�res
//+++++++++++++++++++++++++++++++++++++++++++++++
//modif  janvier 2013 sur si on est dans un lac

void CBilanSed::BilanSedim_SeuilFlux() 
{
    int nummaille;
    CMaille* MaMaille;
    double volfluv = 0.;


    // Boucle "while" sur la liste des mailles class�es par altitudes
    for (int i = 0; i < nbmailles; i++) 
    {
        nummaille = MonMaillage->GetNumListe(i);
        MaMaille = MonMaillage->GetMaille(nummaille);
        //;;;;;;;;;;;;;;;;;;;;;;
        //		ALTERATION
        //;;;;;;;;;;;;;;;;;;;;;;

        (MesFlux->*MonAlteration)(nummaille, MaMaille);

        //;;;;;;;;;;;;;;;;;;;;;;
        // Si on est DANS UN LAC :
        //;;;;;;;;;;;;;;;;;;;;;;
        if (MonBilanEau->GetNumLac(nummaille) < 0) 
        {
            //;;;;;;;;;;;;;;;;;;;;;
            //		DIFFUSION
            //;;;;;;;;;;;;;;;;;;;;;
            // comme il n'y a que de la diffusion qui agit, on ajoute le flux entrant de sediments fluviatiles
            // au flux entrant de sediment issu de la diffusion, de sorte que ces sediments soient pris en compte
            // dans le bilan erosion-depot de la maille.
            volfluv = MesFlux->GetVolumeFluv(nummaille);
            MesFlux->AddVolumeDiff(nummaille, volfluv);

            // puis on calcule la diffusion			
            if (MaMaille->GetNbVoisinsBas() == 0) 
            {
                // si on est au fond d'un lac
                MaDiffusion->DeposeLesSediments(nummaille, MaMaille);
                //MaMaille->AddLesAltiLac(MesFlux->CorrigeActu_Lac(nummaille,MaMaille));
                MaMaille->AddLesAlti((MesFlux->*MonActuErosion)(nummaille, MaMaille)); // 2019
            } 
            else 
            {
                // si on n'est pas au fond d'un lac
                (MaDiffusion->*MonCalculeDiffLac)(nummaille, MaMaille);
                MaMaille->AddLesAltiLac(MesFlux->CorrigeActu_LacPasFond(nummaille, MaMaille));
            }
        }   //;;;;;;;;;;;;;;;;;;;;;;
            // Si on n'est PAS DANS UN LAC :
            //;;;;;;;;;;;;;;;;;;;;;;
        else 
        {
            ////// Si on est AU BORD d'un lac ou � l'EXUTOIRE, recalcul des pentes :		   	
            if (MonBilanEau->GetNumLac(nummaille) > 0) 
            {
                MaMaille->InitialiseLeVoisinage();
                MonCalcPentes_Maille(MaMaille, MonMaillage, &CMaille::GetAltiEau, pas3);
            }

            //;;;;;;;;;;;;;;;;;;;;;
            //		DIFFUSION
            //;;;;;;;;;;;;;;;;;;;;;

            if (MesFlux->GetEauCourante(nummaille) < FluxCritique_paran) // modif 2022 car EauCourante [L3/T]
                (MaDiffusion->*MonCalculeDiff)(nummaille, MaMaille);

                //;;;;;;;;;;;;;;;;;;;;
                //	EROSION FLUVIALE
                //;;;;;;;;;;;;;;;;;;;;
            else 
            {
                (MonErosionFluviale->*MonCalculeErosion)(nummaille, MaMaille);
                // Erosion laterale
                (MonErosionFluviale->*MonErosionLaterale)(nummaille, MaMaille, MonMaillage); // 7 aout 2008. modif juin 2014
            }

            //;;;;;;;;;;;;;;;;;;;;
            // ACTUALISATION DE L'ALTITUDE
            //;;;;;;;;;;;;;;;;;;;;

            MaMaille->AddLesAlti((MesFlux->*MonActuErosion)(nummaille, MaMaille));
        }
    }
    return;
}

//;;;;;;;;;;;;;;;;;;;;



// Surcharge pour flux seuil et calcul hauteur eau.
// 2019 NE MARCHE PAS. IL FAUT TOUT REPENSER ET REECRIRE LE CAS DU CALCUL DE LA SURFACE HYDRAULIQUE
// MODIFS EN 2022 puis 2023  OK

void CBilanSed::BilanSedim_Heau_SeuilFlux() 
{
    int nummaille;
    CMaille* MaMaille;
    double volfluv = 0.;
    
    // Boucle "while" sur la liste des mailles classees par altitudes
    for (int i = 0; i < nbmailles; i++)
    {
        nummaille = MonMaillage->GetNumListeEau(i);
        MaMaille = MonMaillage->GetMaille(nummaille);
        
        //;;;;;;;;;;;;;;;;;;;;;;
        //        ALTERATION
        //;;;;;;;;;;;;;;;;;;;;;;
        (MesFlux->*MonAlteration)(nummaille, MaMaille);
        //;;;;;;;;;;;;;;;;;;;;;;
        // Si on est sous la mer ou dans un lac
        //;;;;;;;;;;;;;;;;;;;;;;
        if (MonBilanEau->GetNumLac(nummaille) == -1) // sous la mer
        {
            //;;;;;;;;;;;;;;;;;;;;;
            //        DIFFUSION
            //;;;;;;;;;;;;;;;;;;;;;
            // On est dans la mer.  On ajoute le flux entrant de sediments fluviatiles
            // au flux entrant de sediments issus de la diffusion, de sorte que ces sediments soient pris en compte
            // dans le bilan erosion-depot de la maille.
            volfluv = MesFlux->GetVolumeFluv(nummaille);
            MesFlux->AddVolumeDiff(nummaille, volfluv);

            // puis on calcule la diffusion
            if (MaMaille->GetNbVoisinsBas() == 0)
            {
                // si on est dans un trou
                MaDiffusion->DeposeLesSediments(nummaille, MaMaille);
                //MaMaille->AddLesAltiLac(MesFlux->CorrigeActu_Lac(nummaille,MaMaille));
                MaMaille->AddLesAlti((MesFlux->*MonActuErosion)(nummaille, MaMaille));
            }
            else
            {
                (MaDiffusion->*MonCalculeDiffLac)(nummaille, MaMaille);
                MaMaille->SetAltiEau(MonMaillage->GetNiveauDeBase()); // on fixe l'altitude de l'eau au niveau de la mer pour ne pas avoir sediment au-dessus ce de niveau dans l'instruction suivante...
                MaMaille->AddLesAltiLac(MesFlux->CorrigeActu_LacPasFond(nummaille, MaMaille));
                MaMaille->SetAltiEau(MaMaille->GetAlti()); // ...puis on remet a l'alti du terrain pour que les mailles de la mer soient bien classees par altitude TERRESTRE decroissante.
            }
        }
       
        else // que l'on soit dans un trou ou pas ce qui compte c'est que la pente de la surface d'eau soit positive
        {
            if (MesFlux->GetEauCourante(nummaille) < FluxCritique_paran) // modif 2022 car EauCourante [L3/T]
                //;;;;;;;;;;;;;;;;;;;;;
                //        DIFFUSION
                //;;;;;;;;;;;;;;;;;;;;;
                (MaDiffusion->*MonCalculeDiff)(nummaille, MaMaille);
            else
            {
                //;;;;;;;;;;;;;;;;;;;;
                //    EROSION FLUVIALE
                //;;;;;;;;;;;;;;;;;;;;
                (MonErosionFluviale->*MonCalculeErosion)(nummaille, MaMaille);
                // Erosion laterale

                (MonErosionFluviale->*MonErosionLaterale)(nummaille, MaMaille, MonMaillage); // 7 aout 2008. modif juin 2014

            }
            //;;;;;;;;;;;;;;;;;;;;
            // ACTUALISATION DE L'ALTITUDE
            //;;;;;;;;;;;;;;;;;;;;
            
            MaMaille->AddLesAltiHeau((MesFlux->*MonActuErosion)(nummaille, MaMaille)); // 2023
            
        }
        // On change de maille
    }
    return;
}

