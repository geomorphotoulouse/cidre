#include "Ccalc_pentes.h"
#include <iostream>
#include <iomanip>
using namespace std;

//========================================
//
//			CALCULS DE PENTES
//
//========================================

//------------------------------------------------
// PentesMaille_SteepDesc :
//
// calcul de pentes explicitÈ en STEEPEST DESCENT
// pour la maille i
//------------------------------------------------

// 7 aout 2008 : modification des AddVoisinBas() et ajout de direction = j dans toutes les routines
// 2023 on supprime les cas sym_axial et bord libre car par coherents
bool PentesMaille_SteepDesc(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3) 
{
    double altiloc, altivoisin;
    double pente, pmax = -10000.;
    double pasloc;
    int numvoisin, indplusbas, direction;
    int j;

    // Je recupere ma maille locale et son altitude
    altiloc = (MaMaille->*MonGetAlti)();


    switch (MaMaille->GetTypeBord()) 
    {

        case pas_bord:
            // Si ma maille locale n'est pas au bord du maillage
            // Je cherche le voisin le plus bas
            for (j = 0; j < 8; j++) 
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();
                pente = (altiloc - altivoisin) / pas3[j];

                if (pente > pmax) 
                {
                    pmax = pente;
                    indplusbas = numvoisin;
                    direction = j;
                    pasloc = pas3[j];
                }
            }
            // Actualisation du voisin bas, dans le voisinage de la maille
            if (pmax > 0)
                MaMaille->AddVoisinBas(0, indplusbas, direction, pasloc, pmax);
            break;
       
            // Si ma maille locale est au bord d'un mur d'altitude fixe
        case alti_fixe:
            // Je cherche le voisin le plus bas
            for (j = 0; j < 8; j++) 
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                if (numvoisin >= 0)
                    altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();
                    // Du cÙtÈ du bord, l'altitude voisine est celle du bord fixe
                else
                    altivoisin = MaMaille->GetBaseBord();

                pente = (altiloc - altivoisin) / pas3[j];
                if (pente > pmax) 
                {
                    pmax = pente;
                    indplusbas = numvoisin;
                    direction = j;
                    pasloc = pas3[j];
                }
            }
            // Actualisation du voisin bas, dans le voisinage de la maille
            if (pmax > 0)
                MaMaille->AddVoisinBas(0, indplusbas, direction, pasloc, pmax);
            break;

            // Si ma maille locale est au bord de "flux nul"
        case flux_nul:
            // Je cherche le voisin le plus bas
            for (j = 0; j < 8; j++) 
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                if (numvoisin < 0) continue;
                // Du cÙtÈ du bord, on ne peut pas avoir de voisin bas
                pente = (altiloc - (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)()) / pas3[j];

                if (pente > pmax) 
                {
                    pmax = pente;
                    indplusbas = numvoisin;
                    direction = j;
                    pasloc = pas3[j];
                }
            }
            // Actualisation du voisin bas, dans le voisinage de la maille
            if (pmax > 0)
                MaMaille->AddVoisinBas(0, indplusbas, direction, pasloc, pmax);
            break;

            // Si le bord est boucle, ce n'est pas vraiment un bord car 2 bords sont senses etres colles
            // Si des bords sont boucles ils ont ete changes en pas_bord lors de la lecture des param d'entree

        case boucle:
            break;
    }
    return (pmax > 0);
}


//--------------------------------------------
// PentesMaille_MultFlow :
//
// calcul de pentes explicitÈ en MULTIPLE FLOW
// pour la maille i
//--------------------------------------------
// 2023 sym_a
bool PentesMaille_MultFlow(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3) 
{
    double altiloc, altivoisin;
    double pente, somme = 0.;
    int numvoisin;
    int nbvoisins = 0;
    int j;

    // Je recupere ma maille locale et son altitude
    altiloc = (MaMaille->*MonGetAlti)();

    // j'adapte le calcul de la pente sur les bords selon la condition limite
    switch (MaMaille->GetTypeBord()) 
    {
            // Si ma maille locale n'est pas au bord du maillage
        case pas_bord:
            // Je cherche les voisins bas
            for (j = 0; j < 8; j++) 
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();
                pente = (altiloc - altivoisin) / pas3[j];
                // Actualisation des voisins  dans le voisinage de la maille
                if (pente > 0.) 
                {
                    MaMaille->AddVoisinBas(nbvoisins, numvoisin, j, pas3[j], pente, pente);
                    nbvoisins++;
                    somme += pente;
                }
            }
            if (nbvoisins > 0) MaMaille->PondereVoisinsBas(somme);
            break;

            // Si ma maille locale est au bord d'un mur d'altitude fixe
        case alti_fixe:
            // Je cherche les voisins bas
            for (j = 0; j < 8; j++) 
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                if (numvoisin >= 0)
                    altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();
                    // Du cÙtÈ du bord, l'altitude voisine est celle du bord fixe
                else
                    altivoisin = MaMaille->GetBaseBord();

                pente = (altiloc - altivoisin) / pas3[j];
                // Actualisation des voisins bas, dans le voisinage de la maille
                if (pente > 0.) 
                {
                    MaMaille->AddVoisinBas(nbvoisins, numvoisin, j, pas3[j], pente, pente);
                    nbvoisins++;
                    somme += pente;
                }
            }
            if (nbvoisins > 0) MaMaille->PondereVoisinsBas(somme);
            break;

            // Si ma maille locale est au bord de "flux nul"
        case flux_nul:
            // Je cherche les voisins bas
            for (j = 0; j < 8; j++) 
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                if (numvoisin < 0) continue;
                // Du cÙtÈ du bord, on ne peut pas avoir de voisin bas
                altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();

                pente = (altiloc - altivoisin) / pas3[j];
                // Actualisation des voisins bas, dans le voisinage de la maille
                if (pente > 0.) 
                {
                    MaMaille->AddVoisinBas(nbvoisins, numvoisin, j, pas3[j], pente, pente);
                    nbvoisins++;
                    somme += pente;
                }
            }
            if (nbvoisins > 0) MaMaille->PondereVoisinsBas(somme);
            break;

            // Si le bord est boucle, ce n'est pas vraiment un bord car 2 bords sont senses etres colles
            // Si des bords sont boucles ils ont ete changes en pas_bord lors de la lecture des param d'entree
        case boucle:
            break;
    }
    return (nbvoisins > 0);
}

//--------------------------------------------
// PentesMaille_MultFlow_SurfaceEau :
//
// calcul de pentes explicitÈ en MULTIPLE FLOW
// pour la maille i le long de la surface de l'eau
// Uniquement pour le cas ou on tient compte de la hauteur d'eau
// pour la propagation du flux d'eau 
// 30 juin 2008
// modifs 2022
//--------------------------------------------

bool PentesMaille_MultFlow_SurfaceEau(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3)
{
    double altiloc, altivoisin;
    double pente, somme = 0.;
    double altimax = 1.E15; // 2022
    double altimin = 1.E15; // 2022
    double altivoisinleplushaut = -1.E15; // 2023
    double pasaltimin = pas3[0]; // 2022
    double mean = 0.; // 2023
    int numvoisin;
    int nbvoisins = 0;
    int j;
    int nmean = 0; // 2023


    // Je recupere ma maille locale et son altitude
    altiloc = (MaMaille->*MonGetAlti)();
    
    (MaMaille->GetVoisinageBasEau())->nb = 0;// 2023 important pour Waterbot
    // J'adapte le calcul de la pente a l'interieur ou  sur les bords selon la condition limite
    switch (MaMaille->GetTypeBord()) 
    {
            // Si ma maille locale n'est pas au bord du maillage
        case pas_bord:
            // Je cherche les voisins bas
            for (j = 0; j < 8; j++)
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();
                pente = (altiloc - altivoisin) / pas3[j];
                // Actualisation des voisins  dans le voisinage de la maille le long de la surface de l'eau
                if (pente > 0.) 
                {
                    MaMaille->AddVoisinBasEau(nbvoisins, numvoisin, j, pas3[j], pente, pente, altivoisin);
                    nbvoisins++;
                    somme += pente;
                }
                else //2022 on cherche la maille juste au-dessus selon l'altitude de l'eau et la plus haute
                {
                    if (altivoisin < altimin) {altimax=altimin; altimin=altivoisin; pasaltimin=pas3[j]; MaMaille->SetNumVoisinJusteAuDessusEau(numvoisin);} //2023
                    else if (altivoisin < altimax) {altimax=altivoisin; MaMaille->SetNumVoisinDeuxiemeJusteAuDessusEau(numvoisin); } //2023
                    if (altivoisin > altivoisinleplushaut) {altivoisinleplushaut=altivoisin;} // 2023
                    mean += altivoisin;
                }
            }
            
            if (nbvoisins==8)
            {
                MaMaille->SetMinUpstreamSlope(0.); //2022
                MaMaille->SetMinUpstreamSlopePas(pas3[0]); //2022
            }
            else
            {
                MaMaille->SetMinUpstreamSlope((-altiloc + altimin) / pasaltimin); //2022
                MaMaille->SetMinUpstreamSlopePas(pasaltimin); //2022
            }
            
                    
            if (nbvoisins > 0)
            {
                MaMaille->PondereVoisinsBasEau(somme);
                MaMaille->SetAltiVoisinJusteAuDessusEau(altiloc); //2023 on en a besoin dans CWaterbot
            }
            else
            {
                //MaMaille->SetAltiVoisinJusteAuDessusEau(altimin);  // 2022
                (MaMaille->GetVoisinageBasEau())->pentemax = 0.; // 2022
                //MaMaille->SetAltiVoisinLePlusBasEau(altiloc); //2023 on en a besoin dans CWaterbot
                // MaMaille->SetAltiMeanVoisinEau((altiloc+altivoisinleplushaut)/2.); //2023 on en a besoin dans CWaterbot
                MaMaille->SetAltiMeanVoisinEau((mean+altiloc-altivoisinleplushaut)/(8-nbvoisins)); //2023 on en a besoin dans CWaterbot
            }
            //if (MaMaille->GetNumMaille()==20965 ) cout << " 20965 "<< " mean " <<(mean+altiloc-altivoisinleplushaut)/(8-nbvoisins)/40000<< " altiloc "<< altiloc/40000 <<" leplushaut " << altivoisinleplushaut/40000<< " 8-nbvoisins " << 8-nbvoisins<< endl;
            //if (MaMaille->GetNumMaille()==21165 ) cout << " 21165 "<< " mean " << (mean+altiloc-altivoisinleplushaut)/(8-nbvoisins)/40000<< " altiloc "<< altiloc/40000 <<" leplushaut " << altivoisinleplushaut/40000<< " 8-nbvoisins " << 8-nbvoisins<< endl;

            break;


        
            // Si ma maille locale est au bord d'un mur d'altitude fixe
        case alti_fixe:
            // Je cherche les voisins bas
            for (j = 0; j < 8; j++)
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                if (numvoisin >= 0)
                    altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();
                    // Du cÙtÈ du bord, l'altitude voisine est celle du bord fixe
                else
                    altivoisin = MaMaille->GetBaseBord();//+ MaMaille->GetAltiEau() - MaMaille->GetAltiRef(); // on ajoute la hauteur d'eau  a l'alti de la
                                                                //maille externe pour avoir une pente plus realiste et eviter le surcreusement au bord
                pente = (altiloc - altivoisin) / pas3[j];
                // Actualisation des voisins bas, dans le voisinage de la maille
                if (pente > 0) 
                {
                    MaMaille->AddVoisinBasEau(nbvoisins, numvoisin, j, pas3[j], pente, pente, altivoisin);
                    nbvoisins++;
                    somme += pente;
                }
                else if (numvoisin>0)//2022 on cherche la maille juste au-dessus selon l'altitude de l'eau et la plus haute
                {
                    if (altivoisin < altimin) {altimax=altimin; altimin=altivoisin; pasaltimin=pas3[j]; MaMaille->SetNumVoisinJusteAuDessusEau(numvoisin);} //2023
                        else if (altivoisin < altimax) {altimax=altivoisin; MaMaille->SetNumVoisinDeuxiemeJusteAuDessusEau(numvoisin); } //2023
                    if (altivoisin > altivoisinleplushaut) {altivoisinleplushaut=altivoisin;} // 2023
                    mean += altivoisin;
                    nmean += 1;
                }
            }
            
            if (nbvoisins==8)
            {
                MaMaille->SetMinUpstreamSlope(0.); //2022
                MaMaille->SetMinUpstreamSlopePas(pas3[0]); //2022
            }
            else
            {
                MaMaille->SetMinUpstreamSlope((-altiloc + altimin) / pasaltimin); //2022
                MaMaille->SetMinUpstreamSlopePas(pasaltimin); //2022
            }
            
                    
            if (nbvoisins > 0)
            {
                MaMaille->PondereVoisinsBasEau(somme);
                MaMaille->SetAltiVoisinJusteAuDessusEau(altiloc); //2023 on en a besoin dans CWaterbot
            }
            else
            {
                (MaMaille->GetVoisinageBasEau())->pentemax = 0.; // 2022
                MaMaille->SetAltiMeanVoisinEau((mean+altiloc-altivoisinleplushaut)/(nmean));//((mean-altivoisinleplushaut)/(8-nbvoisins-1)); //2023 on en a besoin dans CWaterbot
            }


            break;

            // Si ma maille locale est au bord de "flux nul"
        case flux_nul:
            // Je cherche les voisins bas
            for (j = 0; j < 8; j++)
            {
                numvoisin = MaMaille->GetNumVoisin(j);
                // Du cÙtÈ du bord, on ne peut pas avoir de voisin bas
                if (numvoisin < 0)  continue;
                
                // ailleurs :
                altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();
                pente = (altiloc - altivoisin) / pas3[j];
                // Actualisation des voisins bas, dans le voisinage de la maille

                if (pente > 0) 
                {
                    MaMaille->AddVoisinBasEau(nbvoisins, numvoisin, j, pas3[j], pente, pente, altivoisin);
                    nbvoisins++;
                    somme += pente;
                }
                else //2022 on cherche la maille juste au-dessus selon l'altitude de l'eau et la plus haute
                {
                    if (altivoisin < altimin) {altimax=altimin; altimin=altivoisin; pasaltimin=pas3[j]; MaMaille->SetNumVoisinJusteAuDessusEau(numvoisin);} //2023
                        else if (altivoisin < altimax) {altimax=altivoisin; MaMaille->SetNumVoisinDeuxiemeJusteAuDessusEau(numvoisin); } //2023
                    if (altivoisin > altivoisinleplushaut) {altivoisinleplushaut=altivoisin;} // 2023
                    mean += altivoisin;
                    nmean += 1;
                }
            }
            
            if (nbvoisins==8)
            {
                MaMaille->SetMinUpstreamSlope(0.); //2022
                MaMaille->SetMinUpstreamSlopePas(pas3[0]); //2022
            }
            else
            {
                MaMaille->SetMinUpstreamSlope((-altiloc + altimin) / pasaltimin); //2022
                MaMaille->SetMinUpstreamSlopePas(pasaltimin); //2022
            }
            
                    
            if (nbvoisins > 0)
            {
                MaMaille->PondereVoisinsBasEau(somme);
                MaMaille->SetAltiVoisinJusteAuDessusEau(altiloc); //2023 on en a besoin dans CWaterbot
            }
            else
            {
                (MaMaille->GetVoisinageBasEau())->pentemax = 0.; // 2022
                MaMaille->SetAltiMeanVoisinEau((mean+altiloc-altivoisinleplushaut)/(nmean));//((mean-altivoisinleplushaut)/(nmean-1)); //2023 on en a besoin dans CWaterbot
            }
           
            
            break;


            // Si le bord est boucle, ce n'est pas vraiment un bord car 2 bords sont senses etres colles
            // Si des bords sont boucles ils ont ete changes en pas_bord lors de la lecture des param d'entree
        case boucle:
            break;
    }


    return (nbvoisins > 0);
}


//--------------------------------------------
// PentesMaille_MultFlow : cas d'une ILE
//--------------------------------------------
// 2023 : Pourquoi fait on cette distinction pour les Iles sans s'occuper des bords ? Probablement obsolete si on fait de l'erosion sous-marine. A rereflechir

bool PentesMaille_MultFlow_ILE(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3) 
{
    double altiloc;
    double pente, somme = 0.;
    int numvoisin;
    int nbvoisins = 0;
    int j;

    // Je recupere ma maille locale et son altitude
    altiloc = (MaMaille->*MonGetAlti)();

    // Je cherche les voisins bas
    for (j = 0; j < 8; j++) 
    {
        numvoisin = MaMaille->GetNumVoisin(j);
        pente = (altiloc - (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)()) / pas3[j];
        // Actualisation des voisins de diffusion, dans le voisinage de la maille
        if (pente > 0) 
        {
            MaMaille->AddVoisinBas(nbvoisins, numvoisin, j, pas3[j], pente, pente);
            nbvoisins++;
            somme += pente;
        }
    }
    if (nbvoisins > 0) //2022
        // Attention, la somme peut Ítre nulle si on n'a que des voisins nÈgatifs
    {
        if (somme > 0.)
            MaMaille->PondereVoisinsBas(somme);
        else
            MaMaille->PondereVoisinsBas_Uniforme();//2022 (pourquoi pas mis avant ?)
    }

    return (nbvoisins > 0);
}

//--------------------------------------------
// PentesMaille_MultFlow : cas d'une ILE
// Surcharge si OptionHauteurEau
//--------------------------------------------
// Modif 2023. Pourquoi fait on cette distinction pour les Iles sans s'occuper des bords ? Probablement obsolete si on fait de l'erosion sous-marine. A rereflechir
bool PentesMaille_MultFlow_SurfaceEau_ILE(CMaille* MaMaille, CMaillage* MonMaillage,
        PtrFctAltiMaille MonGetAlti, double* pas3)
{
    double altiloc, altivoisin;
    double pente, somme=0.;
    double altimax=1.E15; // 2022
    double altimin=1.E15; // 2022
    double altivoisinleplushaut=0.; // 2023
    double pasaltimin=pas3[0]; // 2022
    double mean=0.; // 2023
    int numvoisin;
    int nbvoisins = 0;
    int j;
    int nmean = 0; // 2023


    // Je recupere ma maille locale et son altitude
    altiloc = (MaMaille->*MonGetAlti)();
    
    (MaMaille->GetVoisinageBasEau())->nb = 0;// 2023 important pour Waterbot

    // Je cherche les voisins bas
    for (j = 0; j < 8; j++)
    {
        numvoisin = MaMaille->GetNumVoisin(j);
        altivoisin = (MonMaillage->GetMaille(numvoisin)->*MonGetAlti)();
        pente = (altiloc - altivoisin) / pas3[j];
        // Actualisation des voisins  dans le voisinage de la maille le long de la surface de l'eau
        if (pente > 0.)
        {
            MaMaille->AddVoisinBasEau(nbvoisins, numvoisin, j, pas3[j], pente, pente, altivoisin);
            nbvoisins++;
            somme += pente;
        }
        else //2022 on cherche la maille juste au-dessus selon l'altitude de l'eau et la plus haute
        {
            if (altivoisin < altimin) {altimax=altimin; altimin=altivoisin; pasaltimin=pas3[j]; MaMaille->SetNumVoisinJusteAuDessusEau(numvoisin);} //2023
            else if (altivoisin < altimax) {altimax=altivoisin; MaMaille->SetNumVoisinDeuxiemeJusteAuDessusEau(numvoisin); } //2023
            if (altivoisin > altivoisinleplushaut) {altivoisinleplushaut=altivoisin;} // 2023
            mean += altivoisin;
        }
    }
    
    if (nbvoisins==8)
    {
        MaMaille->SetMinUpstreamSlope(0.); //2022
        MaMaille->SetMinUpstreamSlopePas(pas3[0]); //2022
    }
    else
    {
        MaMaille->SetMinUpstreamSlope((-altiloc + altimin) / pasaltimin); //2022
        MaMaille->SetMinUpstreamSlopePas(pasaltimin); //2022
    }
    
            
    if (nbvoisins > 0)
    {
        MaMaille->PondereVoisinsBasEau(somme);
        MaMaille->SetAltiVoisinJusteAuDessusEau(altiloc); //2023 on en a besoin dans CWaterbot
        if (somme > 0.)
                MaMaille->PondereVoisinsBasEau(somme);
        else
                MaMaille->PondereVoisinsBas_UniformeEau();//2022 (pourquoi pas mis avant ?)
    }
    else
    {
        (MaMaille->GetVoisinageBasEau())->pentemax = 0.; // 2022
        MaMaille->SetAltiMeanVoisinEau((mean+altiloc-altivoisinleplushaut)/(8-nbvoisins)); //2023 on en a besoin dans CWaterbot
    }

    return (nbvoisins > 0);
}
