#include "CModele.h"
#include "Utilitaires.h"
#include <math.h>

const string CModele::STR_SEPAR = "   ";

//-------------------------------------------
//
// fonctions de la classe CModele :
//
//-------------------------------------------

// constructeur

CModele::CModele() 
{
    int i;
    // ---------------------
    // PARAMETRES PAR DEFAUT
    // ---------------------
    // DÈfinition du dÈcoupage en pÈriodes par dÈfaut
    chrono_pardefaut.nb = 1;
    chrono_pardefaut.TabPeriode = new TPeriode[2];
    chrono_pardefaut.TabPeriode[0].debut = 0.;
    chrono_pardefaut.TabPeriode[0].dt = 1.;
    chrono_pardefaut.TabPeriode[0].taux_precip = 1.;
    chrono_pardefaut.TabPeriode[1].debut = 1000.;
    chrono_pardefaut.TabPeriode[0].TemperatureSurface = 15.;
    // DÈfinition des conditions aux limites par dÈfaut
    for (i = 0; i < 4; i++) limites_pardefaut.Type_bord[i] = alti_fixe;
    for (i = 0; i < 2; i++) limites_pardefaut.ij_exut[i] = 0;
    for (i = 0; i < 4; i++) limites_pardefaut.base[i] = 0.;
    limites_pardefaut.base_min = 0.;
    // DÈfinition des paramËtres de niveau de base par dÈfaut
    nivbase.mode_nivbase = value;
    nivbase.NivBase = 0.;
    nivbase.Sinus = false;
    nivbase.Periode = 100000.;
    nivbase.nbdates = 1;
    nivbase.niveau = new double[1];
    // DÈfinition de la litho par dÈfaut
    alterBr_pardefaut.Kw = 0.;
    alterBr_pardefaut.Kwdt = 0.;
    alterBr_pardefaut.d1 = 0.;
    alterBr_pardefaut.d2 = 0.;
    alterBr_pardefaut.k1 = 0.;
    diffBr_pardefaut.Type_diff = pas_diff;
    diffBr_pardefaut.K = 0.;
    diffBr_pardefaut.Seuil = 0.5;
    incisBr_pardefaut.K = 0.;
    incisBr_pardefaut.MQ = 0.5;
    incisBr_pardefaut.NS = 1.;
    incisBr_pardefaut.Seuil = 0.;
    incisBr_pardefaut.Kmanning = 1.; // 13 juillet 07
    incisBr_pardefaut.Nmanning = 1.; // 2022
    incisBr_pardefaut.Exposantshearstress = -0.5; // 13 juillet 07
    incisBr_pardefaut.KErosionLaterale = 0.; // 7 aout 2008

    // DÈfinition des paramËtres de largeur par dÈfaut
    largeur_pardefaut.OuiNon = false;
    largeur_pardefaut.K = 0.;
    // DÈfinition des paramËtres de transport par dÈfaut
    incisSed_pardefaut.K = 2.e-4;
    incisSed_pardefaut.MQ = 1.5;
    incisSed_pardefaut.NS = 1.;
    incisSed_pardefaut.Seuil = 0.;
    incisSed_pardefaut.Kmanning = 1.; // 2022
    incisSed_pardefaut.Nmanning = 1.; // 2022
    incisSed_pardefaut.CoeffLongTransport = 0.; // janvier 2013
    incisSed_pardefaut.CoeffLongTransporttemp = 0.; // 2022
    incisSed_pardefaut.KErosionLaterale = 0.; // juin 2014

    // DÈfinition des paramËtres de diffusion alluviale par dÈfaut
    diffsed_pardefaut.Type_diff = pas_diff;
    diffsed_pardefaut.K = 0.01;
    diffsed_pardefaut.Seuil = 40.;
    // DÈfinition des paramËtres de diffusion alluviale par dÈfaut
    difflac_pardefaut = diffsed_pardefaut;
    // DÈfinition de la litho par dÈfaut
    effondr_pardefaut.Seuil = 60.;
    effondr_pardefaut.Retour = 100.;
    effondr_pardefaut.K = 0.5;
    effondr_pardefaut.Nmax = 10;
    effondr_pardefaut.TalusBroye = true;
    // DÈfinition du climat par dÈfaut
    climat_pardefaut.mode_precip = value;
    climat_pardefaut.Sinus = false;
    climat_pardefaut.Periode = 100000.;
    climat_pardefaut.LinAlti = false;
    climat_pardefaut.AltiMax = 10000.;
    climat_pardefaut.Stochastique = false; // climat stochastique
    climat_pardefaut.GaussAlti = false;
    // DÈfinition de la tecto par dÈfaut
    tecto_pardefaut.mode_tecto = nothing;
    tecto_pardefaut.OptionUplift = false;
    tecto_pardefaut.OptionDecro = false;
    tecto_pardefaut.LigneDecro = 0;
    tecto_pardefaut.VitesseDecro = 0.;
    tecto_pardefaut.nbzone = 0;
    tecto_pardefaut.numzone = NULL;
    tecto_pardefaut.type_uplift = NULL;
    // DÈfinition des paramËtres de sorties par dÈfaut
    resultats_pardefaut.dt_denud = 100.;
    resultats_pardefaut.dt_bassin = 100.;
    resultats_pardefaut.ind_bassin = 0;
    resultats_pardefaut.FichierDenud = "denudation.txt";
    resultats_pardefaut.FichierBassin = "bassinout";
    resultats_pardefaut.FichierRestart = "restart.txt";
    resultats_pardefaut.FichierFluxChimique = "FluxChimique.csv";
    resultats_pardefaut.FichierMoyenneEcartType = "MoyenneEcartTypeCailloux.txt";
    resultats_pardefaut.OptionZone = false;
    resultats_pardefaut.numzone = 1;
    resultats_pardefaut.OptionMasque = false;
    resultats_pardefaut.fich_masque = "toto.txt";
    // DÈfinition des options de calcul par dÈfaut
    options_pardefaut.OptionIle = false;
    options_pardefaut.OptionNivBase = true;
    options_pardefaut.OptionSeuilFlux = false;
    options_pardefaut.OptionSansLacs = false;
    options_pardefaut.OptionAlter = false;
    options_pardefaut.OptionDiffSed = false;
    options_pardefaut.OptionDiffBedrock = false;
    options_pardefaut.OptionTransp = true;
    options_pardefaut.OptionIncis = true;
    options_pardefaut.OptionEffondr = false;
    options_pardefaut.OptionTecto = false;
    options_pardefaut.OptionReprise = false;
    options_pardefaut.OptionTool = false; // 8 avril 2008
    options_pardefaut.OptionCover = false; // 8 avril 2008
    options_pardefaut.OptionHauteurEau = false; // 30 juin 2008
    options_pardefaut.OptionTempRunoff = false; //ajoute le 01/03/11
    options_pardefaut.OptionRegOptimum = true; //ajoute le 01/03/11

    options_pardefaut.OptionAvecCailloux = false;
    options_pardefaut.OptionMineraux = false;
    options_pardefaut.OptionRessuscite = false;
    options_pardefaut.OptionDissolve = false;
    
    options_pardefaut.OptionCosmo = false; //ajout youssouf le 20/10/2021
    for (int i=0; i<4; i++)
    {
        options_pardefaut.CosmonucleideEtudie[i]=false;
    }
    ProfRessusciteCaillouxMin = 0.; // profondeur min a la quelle un cailloux mort est replacee sour le regolithe 2023
    ProfRessusciteCaillouxMax = 0.; // profondeur max a la quelle un cailloux mort est replacee sour le regolithe 2023

    Rugosite = 1.;
    
    // Parametres par défaut concernant l'entree locale de sediment et d'eau (flume) 2022
    localentry_pardefaut.inputimin = 0;
    localentry_pardefaut.inputimax = 0;
    localentry_pardefaut.inputjmin = 0;
    localentry_pardefaut.inputjmax = 0;
    localentry_pardefaut.inputimin = 0;
    localentry_pardefaut.inputwaterflux = 0.;
    localentry_pardefaut.inputsedflux = 0.;
    
    // ------------------------
    // Initialisation du modËle
    // ------------------------
    // DÈcoupage en pÈriodes
    chrono = chrono_pardefaut;
    // Type de calcul de pentes
    Type_pente = multiple;
    // Conditions aux limites
    Limites = limites_pardefaut;
    // Seuil de flux d'eau critique pour l'Èrosion fluviale
    FluxCritique = 0.;
    // Lithologies
    nblitho = 1;
    ListeLitho = new Tlitho[1];
    ListeLitho[0].alter = new ParamAlter;
    *ListeLitho[0].alter = alterBr_pardefaut;
    ListeLitho[0].diff = new ParamDiff;
    *ListeLitho[0].diff = diffBr_pardefaut;
    ListeLitho[0].incis = new ParamErosion;
    *ListeLitho[0].incis = incisBr_pardefaut;
    // Options de calcul
    Options = options_pardefaut;
    // Prise en compte de la largeur des riviËres
    largeur = new ParamLargeur;
    *largeur = largeur_pardefaut;
    // Transport alluvial
    incisSed = new ParamErosion;
    *incisSed = incisSed_pardefaut;
    // Diffusion lacustre
    difflac = new ParamDiff;
    *difflac = difflac_pardefaut;
    // Diffusion alluviale
    diffsed = new ParamDiff;
    *diffsed = diffsed_pardefaut;
    // Effondrements
    effondr = effondr_pardefaut;
    // Climat
    climat = climat_pardefaut;
    // Tecto
    tecto = tecto_pardefaut;
    // Sorties du modËle
    resultats = resultats_pardefaut;
    // Entree locale d'eau et de sediment 2022
    localentry = localentry_pardefaut;

    // ParamËtres temporaires :
    ///////////////////////////////////
    // - modËle gÈomÈtrique
    topoinitiale.nbcol = 50;
    topoinitiale.nblig = 50;
    topoinitiale.pas = 10.;
    topoinitiale.coteoctogone = 10.; // ajout le 25 octobre 2007
    // - modËle gÈologique
    strati.mode_sed = value;
    strati.mode_couches = value;
    strati.epaiss_sed = EPAISSEUR_INF;
    strati.nbstrati = 1;
    strati.TabCouche = new ParamCouche[1];
    strati.TabCouche[0].indlitho = 0;
    strati.TabCouche[0].epaisseur = 0.;
}

// destructeur

CModele::~CModele() 
{
    int i;
    for (i = 0; i < chrono.nb; i++)
        if (chrono.TabPeriode[i].taux_uplift != NULL)
            delete [] chrono.TabPeriode[i].taux_uplift;
    delete [] chrono.TabPeriode;
    for (i = 0; i < nblitho; i++) 
    {
        delete ListeLitho[i].alter;
        delete ListeLitho[i].diff;
        delete ListeLitho[i].incis;
    }
    delete [] ListeLitho;
    delete largeur;
    delete incisSed;
    delete diffsed;
    delete [] strati.TabCouche;
    if (tecto_pardefaut.numzone != NULL) delete [] tecto_pardefaut.numzone;
    if (tecto_pardefaut.type_uplift != NULL) delete [] tecto_pardefaut.type_uplift;
}

//=================
// Autres fonctions
//=================

//--------------------------------------------------------
// Construit un modele ‡ partir d'un fichier de paramËtres
//--------------------------------------------------------

bool CModele::LitModele(string FichierParam) 
{
    string ligne, tableau[5];
    for (int i(0); i < 5; i++) 
    {
        tableau[i] = "toto";
    } //Ajout Pierre
    int i, j;
    double param[5];
    double tmptab[50];
    int ibord[4];
    int entry[4];
    //double* rigidite;

    filebuf MonFichier;
    // Ouverture du flux d'entrÈe
    if (MonFichier.open(FichierParam.c_str(), ios::in) == NULL) 
    {
        cerr << "Le fichier n'existe pas, ou n'est pas lisible !! "
                << FichierParam.c_str() << endl;
        return false;
    }
    istream FichierEntree(&MonFichier);
    AfficheMessage("> Reading input file parameters:", FichierParam);

    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    //=======================
    // PARAMETRES DU MAILLAGE
    //=======================
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);
    // Nb de colonnes
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, topoinitiale.nbcol);
    // Nb de lignes
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, topoinitiale.nblig);
    // Pas spatial (largeur de maille)
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, topoinitiale.pas);
    topoinitiale.coteoctogone = topoinitiale.pas * (sqrt(2.) - 1.); // ajout 25 octobre 2007
    ReliefMin = (topoinitiale.pas) * (topoinitiale.pas) * (topoinitiale.pas) * PENTE_MIN;

    //============================
    // FICHIER ALTITUDES INITIALES
    //============================
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, topoinitiale.FichierAlti);

    //===================
    // DECOUPAGE EN TEMPS
    //===================
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);
    //
    // LECTURE NOMBRE DE PERIODES
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, chrono.nb);
    //
    // LIMITES TEMPORELLES
    //
    delete [] chrono.TabPeriode;
    chrono.TabPeriode = new TPeriode[chrono.nb + 1];
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, chrono.nb + 1, tmptab);
    for (i = 0; i <= chrono.nb; i++)
        chrono.TabPeriode[i].debut = tmptab[i];
    // Pas de temps minimal par pÈriode
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, chrono.nb, tmptab);
    for (i = 0; i < chrono.nb; i++)
        chrono.TabPeriode[i].dt = tmptab[i];
    //
    // Option de reprise (‡ partir du fichier restart)
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionReprise);

    //========================
    // PARAMETRES DE L'EROSION ET ALTERATION
    //========================
    //
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);

    //
    // FLAG CALCUL HAUTEUR EAU //30 juin 2008
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne); // 30 juin 2008
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionHauteurEau);
    //
    // FLAG TOUCHE PAS LES BORDS
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionIle);
    //
    // NIVEAU DE BASE
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionNivBase);
    // Mode d'entree
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, nivbase.mode_nivbase);
    if (nivbase.niveau != NULL) delete [] nivbase.niveau;
    // Valeurs par periodes / fichier
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    if (nivbase.mode_nivbase == value) 
    {
        // 	- NIVEAU DE BASE CONSTANT
        nivbase.nbdates = chrono.nb;
        nivbase.niveau = new double[nivbase.nbdates];
        // 	Boucle sur le nb de periodes
        for (i = 0; i < chrono.nb; i++) 
        {
            getline(FichierEntree, ligne);
            LitLigne(ligne, nivbase.niveau[i]);
        }
    } 
    else 
    {
        // 	- LECTURE D UN FICHIER DE SEQUENCE TEMPORELLE DU NIVEAU DE BASE
        getline(FichierEntree, ligne);
        LitLigne(ligne, nivbase.fich_nivbase);
    }
    // Variations sinusoidales
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, nivbase.Sinus, nivbase.Periode);
    //
    // CONDITIONS AUX LIMITES
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 4, ibord);
    for (i = 0; i < 4; i++) 
    {
        switch (ibord[i]) 
        {
            /*case 1:
                Limites.Type_bord[i] = sym_ax;
                break;*/ // 2023
            case 2:
                Limites.Type_bord[i] = alti_fixe;
                break;
            case 3:
                Limites.Type_bord[i] = flux_nul;
                break;
            case 4:
                Limites.Type_bord[i] = boucle;
                break;
            /*case 5:
                Limites.Type_bord[i] = bord_libre;
                break;*/ // 2023
            default:
                Limites.Type_bord[i] = pas_bord;
                break;
        }
    }
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 2, Limites.ij_exut);
    //
    // NIVEAU DE BASE DES BORDS (VALABLE POUR ALTI_FIXE)
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 4, Limites.base);
    //
    // CHOIX D UN DEBIT CRITIQUE
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, FluxCritique);
    if (FluxCritique > 0)
        Options.OptionSeuilFlux = true;
    else
        Options.OptionSeuilFlux = false;
    //
    // PAS DE LACS (infiltration dans les trous)
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionSansLacs);

    //
    // EPAISSEUR INITIALE DE SEDIMENTS
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, strati.mode_sed);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    if (strati.mode_sed == value) 
    {
        // 	- EPAISSEUR UNIFORME
        getline(FichierEntree, ligne);
        LitLigne(ligne, strati.epaiss_sed);
    } 
    else 
    {
        // 	- LECTURE D UN FICHIER D EPAISSEUR
        getline(FichierEntree, ligne);
        LitLigne(ligne, strati.FichierEpaissSed);
    }
    
    //
    // ENTREE LOCALE DE SEDIMENT ET D'EAU (POUR LA SIMULATION D'UN FLUME PAR EXEMPLE)
    //  2022
    
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 4, entry);
    localentry.inputimin = entry[0];
    localentry.inputimax = entry[1];
    localentry.inputjmin = entry[2];
    localentry.inputjmax = entry[3];
    
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 2, param);
    localentry.inputwaterflux = param[0];
    localentry.inputsedflux = param[1];
    
    //
    // LITHOLOGIES
    //

    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, nblitho);
    if (nblitho < 1) 
    {
        delete [] ListeLitho;
    } 
    else 
    {
        delete ListeLitho[0].alter;
        delete ListeLitho[0].diff;
        delete ListeLitho[0].incis;
        delete [] ListeLitho;
        ListeLitho = new Tlitho[nblitho];
        for (i = 0; i < nblitho; i++) 
        {
            ListeLitho[i].alter = new ParamAlter;
            *ListeLitho[i].alter = alterBr_pardefaut;
            ListeLitho[i].diff = new ParamDiff;
            *ListeLitho[i].diff = diffBr_pardefaut;
            ListeLitho[i].incis = new ParamErosion;
            *ListeLitho[i].incis = incisBr_pardefaut;
        }
    }
    //
    // COUCHES STRATIGRAPHIQUES
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, strati.mode_couches);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, strati.nbstrati);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    if (strati.nbstrati == 1) 
    {
        // 	- UNE SEULE COUCHE
        getline(FichierEntree, ligne);
        LitLigne(ligne, strati.TabCouche[0].indlitho, strati.TabCouche[0].epaisseur);
        strati.TabCouche[0].indlitho--;
    } 
    else if (strati.nbstrati > 1) 
    {
        delete [] strati.TabCouche;
        strati.TabCouche = new ParamCouche[strati.nbstrati];
        if (strati.mode_couches == value) 
        {
            // 	- EPAISSEUR UNIFORME
            for (i = 0; i < strati.nbstrati; i++) 
            {
                getline(FichierEntree, ligne);
                LitLigne(ligne, strati.TabCouche[i].indlitho, strati.TabCouche[i].epaisseur);
                strati.TabCouche[i].indlitho--;
            }
        } 
        else 
        {
            // 	- LECTURE DES FICHIERS DE STRATI
            for (i = 0; i < strati.nbstrati; i++) 
            {
                getline(FichierEntree, ligne);
                LitLigne(ligne, strati.TabCouche[i].indlitho, strati.TabCouche[i].fich);
                strati.TabCouche[i].indlitho--;
            }
        }
    }

    //
    // PARAMETRES D'ALTERATION
    //
    // Recuperation des coef d'alteration
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    Options.OptionAlter = false;
    Options.OptionRegOptimum = false;
    // 	Boucle sur le nb de lithologies
    for (i = 0; i < nblitho; i++) 
    {
        getline(FichierEntree, ligne);
        LitLigne(ligne, 4, param);
        (ListeLitho[i].alter)->Kw = param[0];
        (ListeLitho[i].alter)->d1 = param[1];
        (ListeLitho[i].alter)->d2 = param[2]; // ajoute le 01/02/11
        (ListeLitho[i].alter)->k1 = param[3]; // ajoute le 01/02/11
        if (param[0] > 0.) Options.OptionAlter = true;
        if (param[3] > 0.) Options.OptionRegOptimum = true;
    }
    // Calcul de l'altÈration en fonction de temp et runoff ou pas, ajoute le 01/02/11
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionTempRunoff);

    // conductivite hydraulique dans les sediments
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 1, param);
    for (i = 0; i < nblitho; i++) 
    {
        (ListeLitho[i].alter)->NbMailleRunoffLimite = param[0];
    }
    //
    // PRISE EN COMPTE DE LA LARGEUR : FLAG et COEFFICIENT de la loi W~Q**0.5
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, largeur->OuiNon, largeur->K);
    //
    // PARAMETRES D'EROSION DES SEDIMENTS PAR TRANSPORT FLUVIAL
    //

    // 	Coefficient
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, incisSed->K);
    if (incisSed->K > 0.) Options.OptionTransp = true;

    //  Coefficient de Manning . ajout le 13 juillet 07
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 1, param);
    for (i = 0; i < nblitho; i++) 
    {
        incisSed->Nmanning = param[0];  // modif 2022 (Nmanning au lieu de Kmanning)
    }
    // 	Exposants du flux d'eau et de la pente
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 2, param);
    incisSed->MQ = param[0];
    incisSed->NS = param[1];
    // 	Exposants p de l'exces de shear stress. Ajout le 13 juillet 07
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 1, param);
    incisSed->Exposantshearstress = param[0];
    // 	Seuil
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, incisSed->Seuil);
    //
    // PARAMETRES D'EROSION FLUVIALE DU BEDROCK
    //
    Options.OptionIncis = false;


    // 	Coefficient
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    for (i = 0; i < nblitho; i++) 
    {
        getline(FichierEntree, ligne);
        LitLigne(ligne, (ListeLitho[i].incis)->K);
        if ((ListeLitho[i].incis)->K > 0.) Options.OptionIncis = true;
    }
    //  Coefficient de Manning. ajout le 13 juillet 07
    //  Idem capacite de transport. Ce parametre intervient dans l'expression du shear stress
    //  en fonction du flux d'eau et de la pente.
    for (i = 0; i < nblitho; i++) 
    {
        (ListeLitho[i].incis)->Nmanning = incisSed->Nmanning; // modif 2022 (Nmanning au lieu de Kmanning)
    }
    // 	Exposants du flux d'eau et de la pente modif 13 juillet 07
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 2, param); // modif janvier 2013
    for (i = 0; i < nblitho; i++) 
    {
        (ListeLitho[i].incis)->MQ = param[0];
        (ListeLitho[i].incis)->NS = param[1];
    }

    // 	Exposants des fonctions de shear stress exess - modif janvier 2013
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 1, param);
    for (i = 0; i < nblitho; i++) 
    {
        (ListeLitho[i].incis)->Exposantshearstress = param[0];
    }

    //  Seuil d'erosion pour le bedrock par lithologie
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    for (i = 0; i < nblitho; i++) 
    {
        getline(FichierEntree, ligne);
        LitLigne(ligne, (ListeLitho[i].incis)->Seuil);
    }


    // 	Coefficient de la longueur de transport fluvial -  janvier 2013
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, 1, param);
    incisSed->CoeffLongTransport = param[0];


    // 	Coefficient erosion laterale des sediments- 7 aout 2008
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, incisSed->KErosionLaterale);
    // On adapte l'erosion laterale des couches de bedrock en utilsant la valeur pour les sediments
    // et le rapport entre les coeff d'erosion K. juin 2014.
    for (i = 0; i < nblitho; i++) 
    {
        (ListeLitho[i].incis)->KErosionLaterale = incisSed->KErosionLaterale * (ListeLitho[i].incis)->K / incisSed->K;
    }


    //
    // PARAMETRES DE DIFFUSION * LACS *
    //
    Options.OptionDiffLac = false;
    // 	Loi
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, i);
    switch (i) 
    {
        case 1:
            difflac->Type_diff = non_lineaire;
            difflac->pas3 = (topoinitiale.pas)*(topoinitiale.pas)*(topoinitiale.pas);
            Options.OptionDiffLac = true;
            break;
        case 2:
            difflac->Type_diff = lineaire;
            Options.OptionDiffLac = true;
            break;
        default:
            difflac->Type_diff = pas_diff;
            break;
    }
    // 	Coefficient
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, difflac->K);
    // 	Seuil
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, difflac->Seuil);
    //
    // PARAMETRES DE DIFFUSION * ALLUVIALE *
    //
    Options.OptionDiffSed = false;
    // 	Loi
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, i);
    switch (i) 
    {
        case 1:
            diffsed->Type_diff = non_lineaire;
            diffsed->pas3 = (topoinitiale.pas)*(topoinitiale.pas)*(topoinitiale.pas);
            Options.OptionDiffSed = true;
            break;
        case 2:
            diffsed->Type_diff = lineaire;
            Options.OptionDiffSed = true;
            break;
        default:
            diffsed->Type_diff = pas_diff;
            break;
    }
    // 	Coefficient
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, diffsed->K);
    // 	Seuil
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, diffsed->Seuil);
    //
    // PARAMETRES DE DIFFUSION * BEDROCK *
    //
    Options.OptionDiffBedrock = false;
    // 	Loi
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, j);
    for (i = 0; i < nblitho; i++) 
    {
        switch (j) 
        {
            case 1:
                (ListeLitho[i].diff)->Type_diff = non_lineaire;
                (ListeLitho[i].diff)->pas3 = (topoinitiale.pas)*(topoinitiale.pas)*(topoinitiale.pas);
                Options.OptionDiffBedrock = true;
                break;
            case 2:
                (ListeLitho[i].diff)->Type_diff = lineaire;
                Options.OptionDiffBedrock = true;
                break;
            default:
                (ListeLitho[i].diff)->Type_diff = pas_diff;
                break;
        }
    }
    // 	Coefficient
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    for (i = 0; i < nblitho; i++) 
    {
        getline(FichierEntree, ligne);
        LitLigne(ligne, (ListeLitho[i].diff)->K);
    }
    // 	Seuil
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    for (i = 0; i < nblitho; i++) 
    {
        getline(FichierEntree, ligne);
        LitLigne(ligne, (ListeLitho[i].diff)->Seuil);
    }

    //
    // PARAMETRES D'EFFONDREMENT
    //
    // 	Option
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionEffondr);
    // 	Seuil
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, effondr.Seuil);
    // 	PÈriode de retour
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, effondr.Retour);
    // 	Coefficient
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, effondr.K);
    // 	Nb de mailles max
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, effondr.Nmax);
    // 	Option de talus
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, effondr.TalusBroye);

    //=====================
    // PARAMETRES DU CLIMAT
    //=====================
    //
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);
    //
    // MODELE CLIMATIQUE
    //
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, climat.mode_precip);
    if (climat.mode_precip == value) 
    {
        // 	- CONSTRUCTION
        for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        // 	Boucle sur le nb de periodes
        for (i = 0; i < chrono.nb; i++) 
        {
            getline(FichierEntree, ligne);
            LitLigne(ligne, chrono.TabPeriode[i].taux_precip);
        }
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, climat.LinAlti, climat.GradientAlti, climat.AltiMax);

        //RÈcup param Gaussienne, ajoute le 01/03/11
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, climat.GaussAlti);

        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, 3, param);
        (climat.GaussA) = param[0];
        (climat.GaussB) = param[1];
        (climat.GaussC) = param[2];

        for (i = 0; i < 6; i++) getline(FichierEntree, ligne); // saut de bloc

        // 	OPTION DE PRECIP SINUSOIDALES
        for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, climat.Sinus, climat.Periode);
        // 	OPTION DE PAS DE TEMPS ADAPTATIF AUX PRECIPITATIONS
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, climat.Option_DtFctPrecip, climat.dtmax_precip);

        for (i = 0; i < 14; i++) getline(FichierEntree, ligne); // saut de bloc
    } 
    else if (climat.mode_precip == file) 
    {
        // 	- LECTURE D UNE GRILLE DE PRECIPITATIONS DANS UN FICHIER
        for (i = 0; i < 18; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        // 	Boucle sur le nb de periodes
        for (i = 0; i < chrono.nb; i++) 
        {
            getline(FichierEntree, ligne);
            LitLigne(ligne, chrono.TabPeriode[i].fich_precip);
        }

        // 	OPTION DE PRECIP SINUSOIDALES
        for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, climat.Sinus, climat.Periode);

        // 	OPTION DE PAS DE TEMPS ADAPTATIF AUX PRECIPITATIONS
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, climat.Option_DtFctPrecip, climat.dtmax_precip);

        for (i = 0; i < 14; i++) getline(FichierEntree, ligne); // saut de bloc
    } 
    else if (climat.mode_precip == stochastique) 
    {
        // 	- OPTION DE PRECIPITATIONS STOCHASTIQUES
        climat.Stochastique = true;
        for (i = 0; i < 34; i++) getline(FichierEntree, ligne);

        // 	CHOIX DU MODELE DE DISTRIBUTION STATISTIQUE DES INTENSITES
        for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, climat.LoiProbabilite);

        // 	MOYENNES ANNUELLES PLUVIOMETRIQUES (une ligne par periode)
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        // 	Boucle sur le nb de periodes
        for (i = 0; i < chrono.nb; i++) 
        {
            getline(FichierEntree, ligne);
            LitLigne(ligne, chrono.TabPeriode[i].taux_precip);
        }
        // VARIABILITE (une ligne par periode)
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        for (i = 0; i < chrono.nb; i++) 
        {
            getline(FichierEntree, ligne);
            LitLigne(ligne, chrono.TabPeriode[i].variabilite);
        }
    }

    // FRACTION DU TEMPS AVEC PLUIE
    for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    // 	Boucle sur le nb de periodes
    for (i = 0; i < chrono.nb; i++) 
    {
        getline(FichierEntree, ligne);
        LitLigne(ligne, chrono.TabPeriode[i].frac_temps_pluie);
    }

    // On redefinit la pluviometrie comme la pluviometrie des periodes de pluie,
    // la valeur lue dans le fichier d'entree etant consideree comme pluviometrie moyenne
    for (i = 0; i < chrono.nb; i++) 
    {
        chrono.TabPeriode[i].taux_precip = chrono.TabPeriode[i].taux_precip / chrono.TabPeriode[i].frac_temps_pluie;
    }

    //On rÈcupËre les valeurs de tempÈrature, ajoute le 28/03/11

    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);

    // 	Boucle sur le nb de periodes
    for (i = 0; i < chrono.nb; i++) 
    {
        getline(FichierEntree, ligne);
        LitLigne(ligne, chrono.TabPeriode[i].TemperatureSurface);
    }

    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, climat.DiminutionTemperature);

    //=======================
    // PARAMETRES DE LA TECTO
    //=======================
    //
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);
    //
    // MODELE DE SOULEVEMENT
    //
    Options.OptionTecto = false;
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, tecto.mode_tecto);
    if (tecto.mode_tecto == value) 
    {
        // 	- CONSTRUCTION
        tecto.OptionUplift = true;
        Options.OptionTecto = true;
        for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, tecto.nbzone);
        tecto.numzone = new int[tecto.nbzone + 1];
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, tecto.nbzone + 1, tecto.numzone);
        tecto.type_uplift = new UPLIFT[tecto.nbzone];
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, tecto.nbzone, tecto.type_uplift);
        for (i = 0; i < chrono.nb; i++)
            chrono.TabPeriode[i].taux_uplift = new double[tecto.nbzone];

        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        for (i = 0; i < chrono.nb; i++) 
        {
            getline(FichierEntree, ligne);
            LitLigne(ligne, tecto.nbzone, tmptab);
            for (j = 0; j < tecto.nbzone; j++) chrono.TabPeriode[i].taux_uplift[j] = tmptab[j];
        }
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Pour l'instant, on ne gËre pas l'isostasie, et donc pas la rigiditÈ !
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //rigidite = new double[tecto.nbzone];
        //for (i=0; i < 3; i++) getline(FichierEntree,ligne);
        //getline(FichierEntree,ligne);
        //		LitLigne(ligne,tecto.nbzone,rigidite);
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    } 
    else if (tecto.mode_tecto == file) 
    {
        // 	- LECTURE D UNE GRILLE DE SOULEVEMENT DANS UN FICHIER
        tecto.OptionUplift = true;
        Options.OptionTecto = true;
        for (i = 0; i < 18; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        // 	Boucle sur le nb de periodes
        for (i = 0; i < chrono.nb; i++) 
        {
            getline(FichierEntree, ligne);
            LitLigne(ligne, chrono.TabPeriode[i].fich_uplift);
        }
    } 
    else 
    {
        tecto.OptionUplift = false;
        for (i = 0; i < 23; i++) getline(FichierEntree, ligne);
    }
    //
    // MODELE DE DECROCHEMENT
    //
    for (i = 0; i < 2; i++) getline(FichierEntree, ligne);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, tecto.OptionDecro);
    if (!tecto.OptionDecro) 
    {
        for (i = 0; i < 8; i++) getline(FichierEntree, ligne);
    } 
    else 
    {
        Options.OptionTecto = true;
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, tecto.LigneDecro);
        for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
        getline(FichierEntree, ligne);
        LitLigne(ligne, tecto.VitesseDecro);
    }
    //=====================
    // PARAMETRES DE SORTIE
    //=====================
    //
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.dt_denud);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.dt_bassin);
    resultats.ind_bassin = 0;
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.OptionRiviere);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.nbrivieres);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.OptionZone);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.numzone);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.OptionMasque);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.fich_masque);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.DirSortie);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.FichierDenud);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.FichierBassin);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.FichierRiviere);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, resultats.FichierRestart);


    //===================
    // SUIVI DES CAILLOUX
    //===================
    //
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionAvecCailloux);

    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionMineraux);

    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, nblitho, param);

    for (i = 0; i < nblitho; i++) 
    {
        ListeLitho[i].nbMineral = param[0];
    }

    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    for (i = 0; i < nblitho; i++) 
    {
        int nb(ListeLitho[i].nbMineral);
        getline(FichierEntree, ligne);
        LitLigne(ligne, nb, tableau, param);
        for (j = 0; j < nb; j++) (ListeLitho[i].mineraux).push_back(tableau[j]);
        for (j = 0; j < nb; j++) (ListeLitho[i].proportionMineral).push_back(param[j]);
    }

    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionRessuscite, ProfRessusciteCaillouxMin, ProfRessusciteCaillouxMax);
    
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionDissolve);
    
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Rugosite);
    // Option calcul de la production de nucleotide cosmogenique ajout 20/10/2021 Youssouf
    for (i = 0; i < 3; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne, Options.OptionCosmo);
    for (i = 0; i < 4; i++) getline(FichierEntree, ligne);
    getline(FichierEntree, ligne);
    LitLigne(ligne,Options.CosmonucleideEtudie);
    // fin ajout youssouf
    // Fermeture du fichier
    MonFichier.close();
    AfficheMessage("          *** Input file read ***\n");

    return true;
}

//=============================================================================
// RÈcapitulatif des paramËtres dans "param.cid"
//=============================================================================

bool CModele::SauveInfo(string FichierRecap) 
{

    if (!VerificationAvantCorrection()) 
    {
        AfficheMessage("##### It is absolutely necessary to do this modifications ! Verify that no line was added or removed in the input_cidre.txt or that opening it and changing its parameters has not modified the ASCII format (do not use word nor notepad). Try a dos2unix input_cidre.txt ");
        return false;
    }

    CorrectionAuto();

    int i;
    ofstream FichierSortie;
    FichierSortie.open(FichierRecap.c_str(), ios::out | ios::trunc);
    // Ouverture du flux de sortie
    if (FichierSortie.bad()) 
    {
        cerr << "Impossible to ouput files !! " << FichierRecap << endl;
        FichierSortie.close();
        return false;
    }

    AfficheMessage("> Saving parameters of this simulation...");

    FichierSortie << "==================================================" << endl;
    FichierSortie << "   RECAPITULATIF DES PARAMETRES DE CALCUL CIDRE   " << endl;
    FichierSortie << "==================================================" << endl;
    FichierSortie << endl;

    FichierSortie << "==========================" << endl;
    FichierSortie << "= PARAMETRES DU MAILLAGE =" << endl;
    FichierSortie << "==========================" << endl;
    FichierSortie << Formate("Nombre de colonnes", topoinitiale.nbcol);
    FichierSortie << Formate("Nombre de lignes", topoinitiale.nblig);
    FichierSortie << Formate("Pas spatial [m]", topoinitiale.pas);
    FichierSortie << FormateFich("Fichier de topographie initiale", topoinitiale.FichierAlti);
    FichierSortie << endl;

    FichierSortie << "======================" << endl;
    FichierSortie << "= DECOUPAGE EN TEMPS =" << endl;
    FichierSortie << "======================" << endl;
    FichierSortie << Formate("Nombre de periodes", chrono.nb);
    if (climat.mode_precip == value)
        FichierSortie << Formate("Mode d'entree des precipitations", "valeur");
    else
        FichierSortie << Formate("Mode d'entree des precipitations", "fichier");
    if (tecto.mode_tecto == value)
        FichierSortie << Formate("Mode d'entree du soulevement tectonique", "valeur");
    else if (tecto.mode_tecto == file)
        FichierSortie << Formate("Mode d'entree du soulevement tectonique", "fichier");
    else
        FichierSortie << Formate("Mode d'entree du soulevement tectonique", "rien");
    FichierSortie << endl;
    FichierSortie << " Periode |   Debut   |    Fin    | Pas de temps [ans]" << endl;
    FichierSortie << left << setfill('-') << setw(52) << "" << endl;
    for (i = 0; i < chrono.nb; i++) 
    {
        FichierSortie << " " << right << setfill(' ') << setw(7) << i + 1;
        FichierSortie << STR_SEPAR << fixed << setprecision(0) << setw(8) << chrono.TabPeriode[i].debut;
        FichierSortie << " " << STR_SEPAR << setprecision(0) << setw(8) << chrono.TabPeriode[i + 1].debut;
        FichierSortie << "    " << STR_SEPAR << setprecision(1) << setw(6) << chrono.TabPeriode[i].dt;
        FichierSortie << endl;
    }
    FichierSortie << endl;

    FichierSortie << "=====================" << endl;
    FichierSortie << "= OPTIONS DE CALCUL =" << endl;
    FichierSortie << "=====================" << endl;

    FichierSortie << Formate("Distribution des flux", "multiple");
    FichierSortie << "(ie: flux distribués vers les mailles plus basses)" << endl;

    if (Options.OptionHauteurEau) // 30 juin 2008
        FichierSortie << Formate("Calcul hauteur eau", "OUI");
    else
        FichierSortie << Formate("Calcul hauteur eau", "NON");
    if (Options.OptionIle)
        FichierSortie << Formate("Calcul sans les bords (ile)", "OUI");
    else
        FichierSortie << Formate("Calcul sans les bords (ile)", "NON");
    if (Options.OptionNivBase)
        FichierSortie << Formate("La mer peut envahir le domaine", "OUI");
    else
        FichierSortie << Formate("La mer peut envahir le domaine", "NON");
    if (Options.OptionSeuilFlux)
        FichierSortie << Formate("Flux critique versant/riviere", "OUI");
    else
        FichierSortie << Formate("Flux critique versant/riviere", "NON");
    if (Options.OptionSansLacs)
        FichierSortie << Formate("Pas de lacs", "OUI");
    else
        FichierSortie << Formate("Pas de lacs", "NON");
    if (largeur->OuiNon) 
    {
        FichierSortie << Formate("Calcul d'une largeur pour les rivieres", "OUI");
        FichierSortie << "(ie: largeur estimee a partir du debit)" << endl;
    } 
    else 
    {
        FichierSortie << Formate("Calcul d'une largeur pour les rivieres", "NON");
        FichierSortie << "(ie: largeur fixee a la largeur d'une maille)" << endl;
    }
    if (Options.OptionAlter)
        FichierSortie << Formate("Alteration du bedrock en sediments", "OUI");
    else
        FichierSortie << Formate("Alteration du bedrock en sediments", "NON");
    if (Options.OptionTempRunoff) //ajoute le 07/03/11
        FichierSortie << Formate("Alteration depend de temp. et runoff", "OUI");
    else
        FichierSortie << Formate("Alteration depend de temp. et runoff", "NON");
    if (Options.OptionDiffLac)
        FichierSortie << Formate("Diffusion dans les lacs", "OUI");
    else
        FichierSortie << Formate("Diffusion dans les lacs", "NON");
    if (Options.OptionDiffSed)
        FichierSortie << Formate("Diffusion des sediments", "OUI");
    else
        FichierSortie << Formate("Diffusion des sediments", "NON");
    if (Options.OptionDiffBedrock)
        FichierSortie << Formate("Diffusion du bedrock", "OUI");
    else
        FichierSortie << Formate("Diffusion du bedrock", "NON");
    if (Options.OptionTransp)
        FichierSortie << Formate("Transport alluvial", "OUI");
    else
        FichierSortie << Formate("Transport alluvial", "NON");
    if (Options.OptionIncis)
        FichierSortie << Formate("Detachement (incision) du bedrock", "OUI");
    else
        FichierSortie << Formate("Detachement (incision) du bedrock", "NON");
    if (Options.OptionEffondr)
        FichierSortie << Formate("Effondrements", "OUI");
    else
        FichierSortie << Formate("Effondrements", "NON");
    if (Options.OptionTecto)
        FichierSortie << Formate("Activite tectonique", "OUI");
    else
        FichierSortie << Formate("Activite tectonique", "NON");
    if (Options.OptionAvecCailloux) //Ajout Youssouf
        FichierSortie << Formate("Suivi des cailloux", "OUI");
    else //Ajout Youssouf
        FichierSortie << Formate("Suivi des cailloux", "NON");
    if (Options.OptionReprise)
        FichierSortie << Formate("Reprise de calcul deja commence", "OUI");
    else
        FichierSortie << Formate("Reprise de calcul deja commence", "NON");
    FichierSortie << endl;

    FichierSortie << "==========================" << endl;
    FichierSortie << "= CONDITIONS AUX LIMITES =" << endl;
    FichierSortie << "==========================" << endl;
    FichierSortie << " Bord |  Type de limite  | Niveau de base [m]" << endl;
    FichierSortie << left << setfill('-') << setw(46) << "" << endl;
    for (i = 0; i < 4; i++) 
    {
        FichierSortie << " " << right << setfill(' ') << setw(3) << i + 1 << fixed << STR_SEPAR;
        switch (Limites.Type_bord[i]) 
        {
           /* case sym_ax:
                FichierSortie << left << setfill(' ') << setw(16) << "SYMETRIE_AXIALE";
                FichierSortie << STR_SEPAR << right << setprecision(1) << setw(8) << Limites.base[i];
                break;*/ // 2023
            case alti_fixe:
                FichierSortie << left << setfill(' ') << setw(16) << "ALTITUDE_FIXE";
                FichierSortie << STR_SEPAR << right << setprecision(1) << setw(8) << Limites.base[i];
                break;
            case flux_nul:
                FichierSortie << left << setfill(' ') << setw(16) << "FLUX_NUL";
                FichierSortie << STR_SEPAR << "      no";
                break;
            case boucle:
                FichierSortie << left << setfill(' ') << setw(16) << "BOUCLE";
                FichierSortie << STR_SEPAR << "      no";
                break;
            /*case bord_libre:
                FichierSortie << left << setfill(' ') << setw(16) << "BORD_LIBRE";
                FichierSortie << STR_SEPAR << "      no";
                break;*/ // 2023
            case pas_bord: // ne sert qu'a eviter un warning a la compilation
                break;
        }
        FichierSortie << endl;
    }
    FichierSortie << endl;

    if ((Limites.Type_bord[0] == flux_nul && Limites.Type_bord[1] == flux_nul
        && Limites.Type_bord[2] == flux_nul && Limites.Type_bord[3] == flux_nul)) 
    {
        FichierSortie << "Coordonnees de l'exutoire :" << endl;
        FichierSortie << Limites.ij_exut[0] << " " << Limites.ij_exut[1] << endl;
        FichierSortie << endl;
    }

    if (Options.OptionNivBase) 
    {
        if (nivbase.mode_nivbase == value) 
        {
            FichierSortie << Formate("Mode d'entree du niveau de base", "valeur");
            if (nivbase.Sinus) 
            {
                FichierSortie << Formate("Niveau eustatique sinusoidal (fct de t)", "OUI");
                FichierSortie << Formate("Periode du cycle 'eustatique' [ans]", nivbase.Periode);
            } 
            else
                FichierSortie << Formate("Niveau eustatique sinusoidal (fct de t)", "NON");
            FichierSortie << endl;
            FichierSortie << " Periode | Niveau de base [m]" << endl;
            FichierSortie << left << setfill('-') << setw(31) << "" << endl;
            for (i = 0; i < chrono.nb; i++) 
            {
                FichierSortie << " " << right << setfill(' ') << setw(7) << i + 1;
                FichierSortie << "      " << STR_SEPAR;
                FichierSortie << right << setprecision(1) << setw(8) <<
                        nivbase.niveau[i] << "      ";
                FichierSortie << STR_SEPAR << endl;
            }
        } 
        else 
        {
            FichierSortie << Formate("Mode d'entree du niveau de base", "fichier");
            FichierSortie << FormateFich("Fichier de la sequence temporelle", nivbase.fich_nivbase);
        }
        FichierSortie << endl;
    }

    if (Options.OptionSeuilFlux) 
    {
        FichierSortie << "=================" << endl;
        FichierSortie << "= FLUX CRITIQUE =" << endl;
        FichierSortie << "=================" << endl;
        FichierSortie << Formate("Seuil de flux d'eau versant/riviere [m3/an]",
                FluxCritique);
        FichierSortie << endl;
    }

    FichierSortie << "===================================" << endl;
    FichierSortie << "= EPAISSEUR INITIALE DE SEDIMENTS =" << endl;
    FichierSortie << "===================================" << endl;
    if (strati.mode_sed == value)
        FichierSortie << Formate("Mode d'entree des epaisseurs", "valeur");
    else
        FichierSortie << Formate("Mode d'entree des epaisseurs", "fichier");
    if (strati.mode_sed == value)
        FichierSortie << Formate("==> epaisseur uniforme [m]", strati.epaiss_sed);
    else
        FichierSortie << FormateFich("==> lecture dans un fichier", strati.FichierEpaissSed);
    FichierSortie << endl;

    FichierSortie << "============" << endl;
    FichierSortie << "= GEOLOGIE =" << endl;
    FichierSortie << "============" << endl;
    FichierSortie << Formate("Nombre de lithologies", nblitho);
    if (nblitho >= 1)
        FichierSortie << Formate("Nombre de couches geologiques", strati.nbstrati);
    if (strati.mode_couches == value)
        FichierSortie << Formate("Mode d'entree des epaisseurs", "valeur");
    else
        FichierSortie << Formate("Mode d'entree des epaisseurs", "fichier");
    if (strati.nbstrati >= 1) 
    {
        if (strati.mode_couches == value)
            // 	- EPAISSEUR UNIFORME
        {
            FichierSortie << endl;
            FichierSortie << " Couche | Lithologie | Epaisseur [m]" << endl;
            FichierSortie << left << setfill('-') << setw(37) << "" << endl;
            for (i = 0; i < strati.nbstrati; i++) 
            {
                FichierSortie << " " << right << setfill(' ') << setw(4) << i + 1;
                FichierSortie << "  " << STR_SEPAR << setw(6) << strati.TabCouche[i].indlitho;
                if (i == strati.nbstrati - 1)
                    FichierSortie << "    " << STR_SEPAR << setw(8) << "inf";
                else
                    FichierSortie << "    " << STR_SEPAR
                        << fixed << setprecision(1) << setw(8) << strati.TabCouche[i].epaisseur;
                FichierSortie << endl;
            }
        } 
        else 
        {
            // 	- LECTURE DES FICHIERS DE STRATI
            FichierSortie << endl;
            FichierSortie << " Couche | Lithologie | Nom du fichier " << endl;
            FichierSortie << left << setfill('-') << setw(37) << "" << endl;
            for (i = 0; i < strati.nbstrati; i++) 
            {
                FichierSortie << " " << right << setfill(' ') << setw(4) << i + 1;
                FichierSortie << "  " << STR_SEPAR << setw(6) << strati.TabCouche[i].indlitho;
                if (i == strati.nbstrati - 1)
                    FichierSortie << "    " << STR_SEPAR << setprecision(1) << setw(8) << EPAISSEUR_INF;
                else
                    FichierSortie << "    " << STR_SEPAR << setw(14) << strati.TabCouche[i].fich.c_str();
                FichierSortie << endl;
            }
        }
    }
    FichierSortie << endl;

    FichierSortie << "=======================" << endl;
    FichierSortie << "= PROCESSUS D'EROSION =" << endl;
    FichierSortie << "=======================" << endl;
    if (largeur->OuiNon) 
    {
        FichierSortie << Formate("Coeff de la loi largeur~sqrt(flux d'eau)", largeur->K);
        FichierSortie << "#" << endl;
    }
    if (Options.OptionDiffLac) 
    {
        FichierSortie << "Diffusion 'sous-marine' des sediments" << endl;
        switch (difflac->Type_diff) 
        {
            case non_lineaire:
                FichierSortie << Formate(" - loi de diffusion", "NON_LINEAIRE");
                FichierSortie << Formate(" - coefficient de diffusion", difflac->K);
                FichierSortie << Formate(" - angle-seuil de diffusion [∞]", difflac->Seuil);
                break;
            case lineaire:
                FichierSortie << Formate(" - loi de diffusion", "LINEAIRE");
                FichierSortie << Formate(" - coefficient de diffusion", difflac->K);
                break;
            case pas_diff: // sert juste a eviter warning a la compilation
                break;
        }
        FichierSortie << "#" << endl;
    }
    if (Options.OptionDiffSed) 
    {
        FichierSortie << "Diffusion 'subaerienne' des sediments" << endl;
        switch (diffsed->Type_diff) 
        {
            case non_lineaire:
                FichierSortie << Formate(" - loi de diffusion", "NON_LINEAIRE");
                FichierSortie << Formate(" - coefficient de diffusion", diffsed->K);
                FichierSortie << Formate(" - angle-seuil de diffusion [∞]", diffsed->Seuil);
                break;
            case lineaire:
                FichierSortie << Formate(" - loi de diffusion", "LINEAIRE");
                FichierSortie << Formate(" - coefficient de diffusion", diffsed->K);
                break;
            case pas_diff: // sert juste a eviter warning a la compilation
                break;
        }
        FichierSortie << "#" << endl;
    }
    if (Options.OptionTransp) 
    {
        FichierSortie << "Transport alluvial" << endl;
        FichierSortie << Formate(" - coefficient de transport", incisSed->K);
        FichierSortie << Formate(" - exposant du flux d'eau", incisSed->MQ);
        FichierSortie << Formate(" - exposant de la pente", incisSed->NS);
        FichierSortie << Formate(" - seuil de transport", incisSed->Seuil);
        FichierSortie << Formate(" - Exposant shear stress", incisSed->Exposantshearstress);
        FichierSortie << "#" << endl;
    }
    if (Options.OptionAlter) 
    {
        FichierSortie << "Alteration du substratum rocheux en sol/sediment" << endl;
        for (i = 0; i < nblitho; i++) 
        {
            FichierSortie << Formate(" - litho ", i, ", parametre Kw", (ListeLitho[i].alter)->Kw);
            FichierSortie << Formate(" - litho ", i, ", parametre d1", (ListeLitho[i].alter)->d1);
            FichierSortie << Formate(" - litho ", i, ", parametre d2", (ListeLitho[i].alter)->d2); //ajoute le 07/03/11
            FichierSortie << Formate(" - litho ", i, ", parametre k1", (ListeLitho[i].alter)->k1);
            FichierSortie << Formate(" - litho ", i, ", mailles drainees max pour calcul", (ListeLitho[i].alter)->NbMailleRunoffLimite);
        }
        FichierSortie << "#" << endl;
    }
    if (Options.OptionDiffBedrock) 
    {
        FichierSortie << "Diffusion du substratum rocheux" << endl;
        for (i = 0; i < nblitho; i++) 
        {
            switch ((ListeLitho[i].diff)->Type_diff) 
            {
                case non_lineaire:
                    FichierSortie << Formate(" - litho ", i, ", loi de diffusion", "NON_LINEAIRE");
                    FichierSortie << Formate(" - litho ", i, ", coefficient de diffusion",
                            (ListeLitho[i].diff)->K);
                    FichierSortie << Formate(" - litho ", i, ", angle-seuil de diffusion [∞]",
                            (ListeLitho[i].diff)->Seuil);
                    break;
                case lineaire:
                    FichierSortie << Formate(" - litho ", i, ", loi de diffusion", "LINEAIRE");
                    FichierSortie << Formate(" - litho ", i, ", coefficient de diffusion",
                            (ListeLitho[i].diff)->K);
                    break;
                default:
                    FichierSortie << Formate(" - litho ", i, ", loi de diffusion", "AUCUNE");
            }
        }
        FichierSortie << "#" << endl;
    }
    if (Options.OptionIncis)
        //debut des modifs 13 juillet 07
    {
        FichierSortie << "Incision du substratum rocheux" << endl;
        for (i = 0; i < nblitho; i++) 
        {
            FichierSortie << Formate(" - litho ", i, ", coefficient de detachement",
                    (ListeLitho[i].incis)->K);

        }
        FichierSortie << Formate(" - litho ", 0, ", coefficient contenant le parametre n de manning",
                (ListeLitho[0].incis)->Kmanning);
        FichierSortie << Formate(" - litho ", 0, ", exposant du flux d'eau",
                (ListeLitho[0].incis)->MQ);
        FichierSortie << Formate(" - litho ", 0, ", exposant de la pente",
                (ListeLitho[0].incis)->NS);
        FichierSortie << Formate(" - litho ", 0, ", seuil de transport",
                (ListeLitho[0].incis)->Seuil);
        FichierSortie << Formate(" - litho ", 0, ", exposant de la fonction du shear stress",
                (ListeLitho[0].incis)->Exposantshearstress);
        //fin des modifs 13 juillet 07
        FichierSortie << "#" << endl;
    }
    if (Options.OptionEffondr) 
    {
        FichierSortie << "Effondrements" << endl;
        FichierSortie << Formate(" - angle critique d'effondrement [∞]", effondr.Seuil);
        FichierSortie << Formate(" - periode de retour des effondrements [ans]", effondr.Retour);
        FichierSortie << Formate(" - coefficient d'effondrement", effondr.K);
        FichierSortie << Formate(" - nombre max de mailles par effondrement", effondr.Nmax);
        if (effondr.TalusBroye)
            FichierSortie << Formate(" - option de talus broye", "OUI");
        else
            FichierSortie << Formate(" - option de talus broye", "NON");
        FichierSortie << "#" << endl;
    }
    FichierSortie << endl;

    FichierSortie << "==================" << endl;
    FichierSortie << "= PRECIPITATIONS =" << endl;
    FichierSortie << "==================" << endl;
    if (climat.Stochastique) 
    {
        FichierSortie << Formate("Precipitations stochastique", "OUI");
        FichierSortie << Formate("ModËle de loi de probabilitÈ", "powerlaw");
    }
    if (climat.LinAlti)
    {
        FichierSortie << Formate("Precipitations croissant avec l'altitude", "OUI");
        FichierSortie << Formate("Altitude de precipitation maximale [m]", climat.AltiMax);
    } else
        FichierSortie << Formate("Precipitations croissant avec l'altitude", "NON");
    if (climat.GaussAlti) //ajoute le 07/03/11
    {
        FichierSortie << Formate("Precipitations variant suivant une gaussienne", "OUI");
        FichierSortie << Formate("ParamËtre a", climat.GaussA);
        FichierSortie << Formate("ParamËtre b", climat.GaussB);
        FichierSortie << Formate("ParamËtre c", climat.GaussC);
    } 
    else
        FichierSortie << Formate("Precipitations variant suivant une gaussienne", "NON");

    if (climat.Sinus) 
    {
        FichierSortie << Formate("Precipitations sinusoidales (fct de t)", "OUI");
        FichierSortie << Formate("Periode du cycle 'climatique' [ans]", climat.Periode);
    } 
    else
        FichierSortie << Formate("Precipitations sinusoidales (fct de t)", "NON");
    FichierSortie << endl;

    FichierSortie << " Periode | Precipitations [mm/an] | Fraction temps pluie | Variabilite" << endl;
    FichierSortie << left << setfill('-') << setw(61) << "" << endl;
    for (i = 0; i < chrono.nb; i++) 
    {
        FichierSortie << " " << right << setfill(' ') << setw(7) << i + 1;
        FichierSortie << "      " << STR_SEPAR;
        if (climat.mode_precip == value || climat.mode_precip == stochastique)
            FichierSortie << right << setprecision(1) << setw(8) <<
            chrono.TabPeriode[i].taux_precip * chrono.TabPeriode[i].frac_temps_pluie * 1000. << "      ";
        else if (climat.mode_precip == file)
            FichierSortie << left << setw(14) << chrono.TabPeriode[i].fich_precip.c_str();

        FichierSortie << right << setprecision(1) << setw(15) <<
                chrono.TabPeriode[i].frac_temps_pluie << "      ";
        if (climat.mode_precip == stochastique)
            FichierSortie << right << setprecision(1) << setw(11) <<
            chrono.TabPeriode[i].variabilite << "      ";

        FichierSortie << STR_SEPAR;
        FichierSortie << endl;
    }
    FichierSortie << endl;
    for (i = 0; i < chrono.nb; i++)
        FichierSortie << Formate(" - pÈriode ", i + 1, ", TempÈrature au niveau 0",
            (chrono.TabPeriode[i].TemperatureSurface));


    FichierSortie << endl;
    //FichierSortie << Formate("TempÈrature en surface",climat.TemperatureSurface);
    FichierSortie << Formate("Gradient altitudinal de tempÈrature", climat.DiminutionTemperature);

    FichierSortie << endl;

    if (Options.OptionTecto) 
    {
        FichierSortie << "=======================" << endl;
        FichierSortie << "= ACTIVITE TECTONIQUE =" << endl;
        FichierSortie << "=======================" << endl;
        if (tecto.OptionUplift) 
        {
            FichierSortie << Formate("Soulevement", "OUI");
            if (tecto.mode_tecto == value) 
            {
                FichierSortie << Formate("Nombre de zones actives", tecto.nbzone);
                for (i = 0; i < tecto.nbzone; i++)
                    FichierSortie << tecto.numzone[i] << " " << tecto.numzone[i + 1] << endl;
                for (i = 0; i < tecto.nbzone; i++) 
                {
                    switch (tecto.type_uplift[i]) 
                    {
                        case creneau:
                            FichierSortie << "creneau ";
                            break;
                        case gaussienne:
                            FichierSortie << "gaussienne ";
                            break;
                        case croissant:
                            FichierSortie << "croissant ";
                        default:
                            FichierSortie << "decroissant ";
                    }
                }
                FichierSortie << endl;
            }
            FichierSortie << endl;
            if (tecto.mode_tecto == value)
                FichierSortie << " Periode | Soulevement [mm/an]" << endl;
            else
                FichierSortie << " Periode |     Fichier" << endl;
            FichierSortie << left << setfill('-') << setw(31) << "" << endl;
            for (i = 0; i < chrono.nb; i++) 
            {
                FichierSortie << " " << right << setfill(' ') << setw(7) << i + 1;
                FichierSortie << "      " << STR_SEPAR;
                if (tecto.mode_tecto == value) 
                {
                    FichierSortie << right << setprecision(1) << setw(8);
                    for (int j = 0; j < tecto.nbzone; j++)
                        FichierSortie << chrono.TabPeriode[i].taux_uplift[j]*1000. << " ";
                } else if (tecto.mode_tecto == file)
                    FichierSortie << left << setw(14) << chrono.TabPeriode[i].fich_uplift.c_str();
                FichierSortie << endl;
            }
            FichierSortie << endl;
        } 
        else
            FichierSortie << Formate("Soulevement", "NON");
        if (tecto.OptionDecro) {
            FichierSortie << Formate("Decrochement", "OUI");
            FichierSortie << Formate("Numero dela ligne de decrochement", tecto.LigneDecro);
            FichierSortie << Formate("Taux de decrochement dextre [mm/an]", tecto.VitesseDecro * 1000.);
        }
        else
            FichierSortie << Formate("Decrochement", "NON");
        FichierSortie << endl;
    }

    // Debut ajout youssouf
    if (Options.OptionAvecCailloux)
    {
        FichierSortie << "============" << endl;
        FichierSortie << "= CAILLOUX =" << endl;
        FichierSortie << "============" << endl;
        
        if (Options.OptionMineraux)
            FichierSortie << Formate("Minéralogie déterminée par la lithologie ", "OUI");
        else
            FichierSortie << Formate("Minéralogie déterminée par la lithologie ", "NON");
        
        if (Options.OptionMineraux)
        {
            for (int i(0); i<nblitho ; i++)
            {
                FichierSortie <<"# Composition de la lithologie num "<< i<<endl;
                FichierSortie <<"Mineraux        :";

                for (int j(0); j<ListeLitho[i].nbMineral ; j++)
                {
                    FichierSortie <<" | ";
                    FichierSortie << ListeLitho[i].mineraux[j];
                }
                FichierSortie << endl;
                FichierSortie << left << setfill('-') << setw(46) << "" << endl;
                FichierSortie <<"Proportion en % :";
                for (int j(0); j<ListeLitho[i].nbMineral ; j++)
                {
                    FichierSortie <<"   ";
                                                    
                    FichierSortie << ListeLitho[i].proportionMineral[j];
                    FichierSortie <<"  ";
                }
                FichierSortie << endl;
            }
            FichierSortie << " " << endl;
        }


        

        if (Options.OptionRessuscite)
        {
            FichierSortie << Formate("Ressusciter les cailloux morts ", "OUI");
            FichierSortie << Formate("Profondeur max sous le régolithe ou les cailloux sont ressuscités", ProfRessusciteCaillouxMax);
        }
        else
        {
            FichierSortie << Formate("Ressusciter les cailloux morts ", "NON");
        }
        if (Options.OptionDissolve)
        {
            FichierSortie << Formate("Dissoudre les cailloux ", "OUI");
            FichierSortie << Formate("Paramètre de rugosité", Rugosite);
        }
        else
        {
            FichierSortie << Formate("Dissoudre les cailloux ", "NON");
        }
        if (Options.OptionCosmo)
        {
            FichierSortie << Formate("Production de cosmonucleide", "OUI");
            FichierSortie << " " << endl;
            FichierSortie <<"Nucleides :";
            FichierSortie <<" 10Be  | 26Al  |  14C  |  21Ne"<<endl;
            FichierSortie << left << setfill('-') << setw(46) << "" << endl;
            FichierSortie <<"Present   :";
            for (int i(0); i<4; i++)
            {
                if(Options.CosmonucleideEtudie[i])
                {
                    FichierSortie<<"  OUI   ";
                }
                else
                {
                    FichierSortie<<"  NON   ";

                }
            }
            
            FichierSortie<<endl;
            
        }
        
        else
        {
            FichierSortie << Formate("Production de cosmonucleide", "NON");
        }
        
            
    }// Fin ajout youssouf
    FichierSortie << " " << endl;
    FichierSortie << "===========" << endl;
    FichierSortie << "= SORTIES =" << endl;
    FichierSortie << "===========" << endl;
    FichierSortie << Formate("Intervalle de sortie du taux de denudation [ans]",
            resultats.dt_denud);
    FichierSortie << Formate("Intervalle de sortie des grilles topo [ans]",
            resultats.dt_bassin);
    if (resultats.OptionRiviere) 
    {
        FichierSortie << Formate("Identification de rivieres (par bassin)", "OUI");
        FichierSortie << Formate("Nombre de rivieres", resultats.nbrivieres);
    } 
    else
        FichierSortie << Formate("Identification de rivieres (par bassin)", "NON");
    if (resultats.OptionZone) 
    {
        FichierSortie << Formate("Taux calcules pour une zone tecto particuliere", "OUI");
        FichierSortie << Formate("Numero de cette zone", resultats.numzone);
    } 
    else
        FichierSortie << Formate("Taux calcules pour une zone tecto particuliere", "NON");
    if (resultats.OptionMasque) 
    {
        FichierSortie << Formate("Taux calcules suivant une grille-masque", "OUI");
        FichierSortie << FormateFich("Nom du fichier contenant le masque",
                resultats.fich_masque);
    } 
    else
        FichierSortie << Formate("Taux calcules suivant une grille-masque", "NON");

    FichierSortie << FormateFich("Repertoire de sortie des resultats",
            resultats.DirSortie);
    FichierSortie << FormateFich("Fichier de sortie du taux de denudation",
            resultats.FichierDenud);
    FichierSortie << FormateFich("Radical des fichiers de sortie des grilles",
            resultats.FichierBassin);
    if (resultats.OptionRiviere) 
    {
        FichierSortie << FormateFich("Radical des fichiers de sortie des rivieres",
                resultats.FichierRiviere);
    }
    FichierSortie << FormateFich("Fichier 'restart'", resultats.FichierRestart);
    // Fermeture du fichier
    FichierSortie.close();

    AfficheMessage("          *** Synthesis of parameters in ", FichierRecap, " ***\n");
    return true;
}


//=============================================================================
// VÈrification des trucs ‡ corriger absolument !!!
//=============================================================================

bool CModele::VerificationAvantCorrection() 
{
    bool flag = true;
    int i, j;
    double dtMax = 0.;
    dtMin = TCALCUL_MAX;

    AfficheMessage("> Checking parameters before launching the calculus...");


    // PARAMETRES DU MAILLAGE
    // Nb de colonnes
    if (topoinitiale.nbcol <= 0) 
    {
        AfficheMessage("  -+ Number of columns <= 0 !");
        flag = false;
    }
    if (topoinitiale.nbcol > NCOL_MAX) 
    {
        AfficheMessage("  -+ Number of columns too large ! The max is :", NCOL_MAX);
        flag = false;
    }
    // Nb de lignes
    if (topoinitiale.nblig <= 0) 
    {
        AfficheMessage("  -+ Number of rows <= 0 !");
        flag = false;
    }
    if (topoinitiale.nblig > NLIG_MAX) 
    {
        AfficheMessage("  -+ Number of rows too large ! Le max est :", NLIG_MAX);
        flag = false;
    }
    // Pas spatial (largeur de maille)
    if (topoinitiale.pas <= 0) 
    {
        AfficheMessage("  -+ Cell size <= 0 !");
        flag = false;
    }
    if (topoinitiale.pas > GRAND) 
    {
        AfficheMessage("  -+ Cell size too large ! The max is :", GRAND);
        flag = false;
    }
    if (topoinitiale.FichierAlti.size() == 0) 
    {
        AfficheMessage("  -+ There is no initial elevation file !!!");
        flag = false;
    }
    // LECTURE NOMBRE DE PERIODES
    if (chrono.nb <= 0) 
    {
        AfficheMessage("  -+ Number of periods <= 0 !");
        flag = false;
    }
    if (chrono.nb > NPER_MAX) 
    {
        AfficheMessage("  -+ Number of periods too large ! The max is :", NPER_MAX);
        flag = false;
    }
    // LIMITES TEMPORELLES
    for (i = 0; i < chrono.nb; i++) 
    {
        if (chrono.TabPeriode[i].debut >= chrono.TabPeriode[i + 1].debut) 
        {
            AfficheMessage("  -+ Problem in the definition of periods :", i);
            flag = false;
        }
    }
    if (chrono.TabPeriode[chrono.nb].debut - chrono.TabPeriode[0].debut > TCALCUL_MAX) 
    {
        AfficheMessage("  -+ Time step too long ! The max is :", TCALCUL_MAX);
        flag = false;
    }
    // Pas de temps minimal par pÈriode
    for (i = 0; i < chrono.nb; i++)
        if (chrono.TabPeriode[i].dt < dtMin)
            dtMin = chrono.TabPeriode[i].dt;
    if (dtMin < DTCALCUL_MIN) 
    {
        AfficheMessage("  -+ Time step too short ! The min is :", DTCALCUL_MIN);
        flag = false;
    }
    for (i = 0; i < chrono.nb; i++)
        if (chrono.TabPeriode[i].dt > dtMax)
            dtMax = chrono.TabPeriode[i].dt;
    if (dtMax > DTCALCUL_MAX) 
    {
        AfficheMessage("  -+ Time step too long ! The max is :", DTCALCUL_MAX);
        flag = false;
    }
    // ILE & NIVEAU DE BASE
    if (Options.OptionIle && !Options.OptionNivBase)
        AfficheMessage("  -+ The iceland option requires to specify base-levels...");
    // NIVEAU DE BASE
    if (Options.OptionNivBase) 
    {
        if (nivbase.mode_nivbase == file) 
        {
            if (nivbase.Sinus)
                AfficheMessage("  -+ The base-level can not be at the same time sinusoidal and follow an eustatic curve !");
            if (nivbase.fich_nivbase.size() == 0) 
            {
                AfficheMessage("  -+ There is no eustatic curve file !!!");
                flag = false;
            }
        }
        else if (nivbase.Sinus && (nivbase.Periode < 10. * mymax(DTCALCUL_MIN, dtMin))) 
        {
            AfficheMessage("  -+ The period for the sinusoidal evolution of base-levels is too small ! The min is :",
                    10. * mymax(DTCALCUL_MIN, dtMin));
        }
    }
    // CONDITIONS AUX LIMITES
    if ((Limites.Type_bord[0] == flux_nul && Limites.Type_bord[1] == flux_nul
            && Limites.Type_bord[2] == flux_nul && Limites.Type_bord[3] == flux_nul)) 
    {
        if (Limites.ij_exut[0] <= 0 && Limites.ij_exut[0] > topoinitiale.nbcol) 
        {
            AfficheMessage("  -+ The output has a bad column number (must be on the grid border) !");
            flag = false;
        }
        if (Limites.ij_exut[1] <= 0 && Limites.ij_exut[1] > topoinitiale.nblig) 
        {
            AfficheMessage("  -+ The output has a bad row number (must be on the grid border) !");
            flag = false;
        }
    } 
    else 
    {
        int nb_bordsboucles = 0;
        for (i = 0; i < 4; i++) 
        {
            if ((Limites.Type_bord[i] == pas_bord)
                && Limites.Type_bord[i] != alti_fixe && Limites.Type_bord[i] != flux_nul
                && Limites.Type_bord[i] != boucle) // modif 2023 les cas sym_axial et bord_libre ont ete enleves
            {
                AfficheMessage("  -+ Pb : impossible boundary condition, boundary :", i + 1);
                flag = false;
            }
            if (Limites.Type_bord[i] == alti_fixe) 
            {
                if (Limites.base[i] < ALTI_MIN) 
                {
                    AfficheMessage("  -+ Base-level too low  - MIN = ", ALTI_MIN);
                    flag = false;
                }
                if (Limites.base[i] > ALTI_MAX) 
                {
                    AfficheMessage("  -+ Base-level to high - MAX = ", ALTI_MAX);
                    flag = false;
                }
            }
            if (Limites.Type_bord[i] == boucle)
                nb_bordsboucles++;
        }
        if (nb_bordsboucles > 0) 
        {
            if (nb_bordsboucles == 1) 
            {
                AfficheMessage("  -+ Pb : only one periodic side ! 2 are needed");
                flag = false;
            } 
            else if (nb_bordsboucles > 2) 
            {
                AfficheMessage("  -+ Pb : too many periodic sides ! only 2 needed");
                flag = false;
            }
            // Si 2 bords sont bouclÈs, et que ce sont une ligne et une colonne,
            // il faut vÈrifier que nblig = nbcol !
            if ((Limites.Type_bord[0] == boucle || Limites.Type_bord[1] == boucle)
                && (Limites.Type_bord[2] == boucle || Limites.Type_bord[3] == boucle)
                && (topoinitiale.nbcol != topoinitiale.nblig)) 
            {
                AfficheMessage("  -+ 2 adjacent periodic sides requires that  NX=NY !");
                flag = false;
            }
        }
    }
    // CHOIX D UN DEBIT CRITIQUE
    if (Options.OptionSeuilFlux) 
    {
        if (FluxCritique <= 0)
            AfficheMessage("  -+ Critical discharge <= 0 !");
    }
    // GEOLOGIE
    if (strati.mode_sed == value) 
    {
        if (strati.epaiss_sed < 0.)
            AfficheMessage("  -+ Sediment thickness < 0 !");
    } 
    else 
    {
        if (strati.FichierEpaissSed.size() == 0)
            AfficheMessage("  -+ There is no file for the initial sediment/regolith thickness !");
    }
    if (strati.nbstrati > 0) 
    {
        if (strati.mode_couches == value) 
        {
            for (i = 0; i < strati.nbstrati - 1; i++) 
            {
                // si une couche est vide, faire remonter les autres d'un cran
                if (strati.TabCouche[i].epaisseur <= 0.)
                    AfficheMessage("  -+ This strata layer is empty ! layer ", i + 1);
            }
        } 
        else 
        {
            for (i = 0; i < strati.nbstrati - 1; i++) 
            {
                if (strati.TabCouche[i].fich.size() == 0) 
                {
                    AfficheMessage("  -+ There is file name for the stratigraphy", i);
                    flag = false;
                }
            }
        }
    }
    if (nblitho < 0 && strati.nbstrati > 0)
        AfficheMessage("  -+ Number of lithologies < 0 !");
    if (nblitho > NLITHO_MAX)
        AfficheMessage("  -+ Too many lithologies ! Max = ", NLITHO_MAX);
    if (strati.nbstrati < 0)
        AfficheMessage("  -+ Number of bedrock strata < 0 !");
    if (strati.nbstrati > NSTRATI_MAX)
        AfficheMessage("  -+ Too many bedrock strata ! Max = ", NSTRATI_MAX);
    if (strati.nbstrati > 1 && nblitho == 1)
        AfficheMessage("  -+ Different strata for only one lithology does not make sense !");
    if (nblitho > 1 && strati.nbstrati == 1)
        AfficheMessage("  -+ Different lithologies for only one stratum does not make sense !");
    if (nblitho <= 0)
        AfficheMessage("  -+ No bedrock lithology !");
    if (strati.nbstrati <= 0)
        AfficheMessage("  -+ No strata !");
    // EROSION
    if (largeur->OuiNon && largeur->K <= 0.)
        AfficheMessage("  -+ Coeff <= 0. in the river width law !");
    // AltÈration
    if (Options.OptionAlter) 
    {
        if (nblitho <= 0)
            AfficheMessage("  -+ There is no bedrock substratum, thus no weathering !?");
        else 
        {
            for (i = 0; i < nblitho; i++) 
            {
                if ((ListeLitho[i].alter)->Kw < 0. || (ListeLitho[i].alter)->d1 < 0.)
                    AfficheMessage("  -+ Weathering coefficient < 0 ! litho ", i);
            }
        }
    }

    // Transport
    if (Options.OptionTransp && incisSed->K <= 0.)
        AfficheMessage("  -+ Detachment coefficient <= 0 !!");
    // Incision
    if (Options.OptionIncis) 
    {
        for (i = 0; i < nblitho; i++) 
        {
            if ((ListeLitho[i].incis)->K <= 0.)
                AfficheMessage("  -+ Detachment coefficient <= 0 ! litho", i);
        }
    }
    // Diffusion
    if (difflac->K <= 0.)
        AfficheMessage("  -+ Marine diffusion coefficient <= 0 !");

    if (diffsed->K <= 0.)
        AfficheMessage("  -+ Sediment diffusion coefficient <= 0 !");

    if (Options.OptionDiffBedrock) 
    {
        for (i = 0; i < nblitho; i++) 
        {
            if ((ListeLitho[i].diff)->K <= 0.)
                AfficheMessage("  -+ Bedrock diffusion coefficient <= 0 ! litho", i);
        }
    }
    // PrÈsence de sÈdiments
    if ((!Options.OptionDiffSed) && (!Options.OptionTransp)) 
    {
        AfficheMessage("  -+ No transport nor diffusion of sediments");

        if (Options.OptionDiffBedrock) 
        {
            AfficheMessage("  -+ Becareful it seems illogical to diffuse bedrock and not sediment ");
        }
    }
    // PrÈsence de bedrock
    if ((!Options.OptionAlter) && (!Options.OptionDiffBedrock)
        && (!Options.OptionEffondr) && (!Options.OptionIncis)) 
    {
        AfficheMessage("  -+ No erosion process affecting the bedrock substratum");
        if ((!Options.OptionDiffSed) && (!Options.OptionTransp)) 
        {
            AfficheMessage("  -+ No erosion process affecting sediment neither !");
            flag = false;
        }
    }
    // Effondrements
    if (Options.OptionEffondr) 
    {
        if (effondr.K < 0.)
            AfficheMessage("  -+ Collapse coefficient = 0 !");
        else if (effondr.K > 0.5)
            AfficheMessage("  -+ Collapse coefficient > 0.5 !");
        if (effondr.Nmax < 1)
            AfficheMessage("  -+ Max number of collapsing cells < 1 !");
    }
    // CLIMAT
    if (climat.mode_precip == nothing) 
    {
        AfficheMessage("  -+ You must specify precipitation rates !");
        flag = false;
    } 
    else if (climat.mode_precip == file) 
    {
        for (i = 0; i < chrono.nb; i++) 
        {
            if (chrono.TabPeriode[i].fich_precip.size() == 0) 
            {
                AfficheMessage("  -+ There is no precipitation file for period ", i);
                flag = false;
            }
        }
    }
    if (climat.mode_precip == value) 
    {
        for (i = 0; i < chrono.nb; i++) 
        {
            if (chrono.TabPeriode[i].taux_precip < 0) 
            {
                AfficheMessage("  -+ Precipitation rates < 0 for period", i);
                flag = false;
            } 
            else if (chrono.TabPeriode[i].taux_precip * chrono.TabPeriode[i].frac_temps_pluie > PRECIP_MAX) 
            {
                AfficheMessage("  -+ Precipitation rate too large ! The max is ", PRECIP_MAX);
                flag = false;
            }
        }
    }

    if (climat.Sinus && climat.Periode < mymax(DTCALCUL_MIN, dtMin))
        AfficheMessage("  -+ Sinusoidal period too small !");

    // TECTONIQUE
    if (tecto.OptionDecro && dabs(tecto.VitesseDecro) > TXDECRO_MAX)
        AfficheMessage("  -+ Strike-slip rate too large ! The max is : ", TXDECRO_MAX);
    if (tecto.OptionUplift) 
    {
        if (tecto.mode_tecto == file) 
        {
            for (i = 0; i < chrono.nb; i++) 
            {
                if (chrono.TabPeriode[i].fich_uplift.size() == 0) 
                {
                    AfficheMessage("  -+ There is no uplift rate file for period ", i);
                    flag = false;
                }
            }
        } 
        else 
        {
            if (tecto.nbzone <= 0) 
            {
                AfficheMessage("  -+ No uplifting zone defined !");
                flag = false;
            } 
            else 
            {
                for (j = 0; j < tecto.nbzone + 1; j++) 
                {
                    if (tecto.numzone[j] < 0)
                        AfficheMessage("  -+ There is a line of the tectonic zone out of the grid (zone tecto) !");
                    if (tecto.numzone[j] > topoinitiale.nblig)
                        AfficheMessage("  -+ There is a line of the tectonic zone out of the grid (zone tecto) !");
                }
                for (j = 0; j < tecto.nbzone; j++) 
                {
                    for (i = 0; i < chrono.nb; i++) 
                    {
                        if (dabs(chrono.TabPeriode[i].taux_uplift[j]) > TXUPLIFT_MAX) 
                        {
                            AfficheMessage("  -+ Uplift rate too large ! MAX =", TXUPLIFT_MAX);
                            flag = false;
                        }
                    }
                }
            }
        }
    }
    // SORTIES
    double dureetmp = chrono.TabPeriode[chrono.nb].debut - chrono.TabPeriode[0].debut;
    if (resultats.dt_denud < dtMax)
        AfficheMessage("  -+ Interval between to mean values outputs too short  ! The min is :", dtMax);
    else if (resultats.dt_denud > dureetmp)
        AfficheMessage("  -+ No output of mean values ! The run lasts only :",
            dureetmp);
    if (resultats.dt_bassin < dureetmp / (double) NSORTIES_MAX)
        AfficheMessage("  -+ Interval between two grid outputs too short ! The min is :",
            dureetmp / (double) NSORTIES_MAX);
    else if (resultats.dt_denud > dureetmp)
        AfficheMessage("  -+ No output of grids ! The run lasts only :",
            dureetmp);
    if (resultats.OptionZone) 
    {
        if (!tecto.OptionUplift)
            AfficheMessage("  -+ No tectonic zone defined");
        if ((resultats.numzone < 1) || (resultats.numzone > tecto.nbzone))
            AfficheMessage("  -+ This selected zone for output is not defined");
    }
    if (resultats.OptionMasque && (resultats.fich_masque.size() == 0))
        AfficheMessage("  -+ There is no name for the selected region file --> no selected region");
    if (resultats.FichierDenud.size() == 0) 
    {
        AfficheMessage("  -+ There is no name for the file containing the mean values through time.");
        flag = false;
    }
    if (resultats.FichierBassin.size() == 0) 
    {
        AfficheMessage("  -+ There is no radical for the output file names.");
        flag = false;
    }
    if (resultats.FichierRestart.size() == 0) 
    {
        AfficheMessage("  -+ There is no radical for the Restart file.");
        flag = false;
    }

    if (Options.OptionHauteurEau && Options.OptionIle) // 30 juin 2008
    {
        AfficheMessage("  -+ The option water height does not operate for the island option");
        flag = false;
    }
    if (Options.OptionHauteurEau && !Options.OptionSansLacs) // 30 juin 2008
    {
        AfficheMessage("  -+ The option water height does not operate for the lakes calculation option");
        flag = false;
    }
    // Ajout youssouf 02/11/21
    if (Options.OptionAvecCailloux && Options.OptionCosmo)
    {
        int NbCosmo=0;
                for (int i=0; i<4; i++)
                {
                    if (Options.CosmonucleideEtudie[i])
                    {
                        NbCosmo+=1;
                    }
                }
        if (NbCosmo==0)
        {
            AfficheMessage("   -+ If you take the option of calculating cosmonucleide production, you must at least choose one ");
            flag = false;
        }
            
    }
        
    //fin ajout
    
    AfficheMessage("          *** END of final checking ***\n");
    return flag;
}


//=============================================================================
// Correction automatique des paramËtres que l'on peut corriger
//=============================================================================

void CModele::CorrectionAuto() 
{
    int i, j;
    double dtMax = 0.;
    //	dtMin = TCALCUL_MAX;
    bool tmptest;

    AfficheMessage("> Automatic Editing of problematic parameters...");

    // ILE & NIVEAU DE BASE
    if (Options.OptionIle && !Options.OptionNivBase)
        Options.OptionIle = false;
    // NIVEAU DE BASE
    if (Options.OptionNivBase) 
    {
        if (nivbase.mode_nivbase == file) 
        {
            if (nivbase.Sinus) 
            {
                AfficheMessage("   --- The base-level can not be at the same time sinusoidal and follow an eustatic curve");
                AfficheMessage("                       --> base-level non periodic");
                nivbase.Sinus = false;
            }
        } 
        else if (nivbase.Sinus && (nivbase.Periode < 10. * mymax(DTCALCUL_MIN, dtMin))) 
        {
            AfficheMessage("   --- Period of sinusoidal variation of base-level too small");
            AfficheMessage("                       --> base-level non periodic");
            nivbase.Sinus = false;
        }
    }
    // CHOIX D UN DEBIT CRITIQUE
    if (Options.OptionSeuilFlux) 
    {
        if (FluxCritique <= 0) 
        {
            AfficheMessage("   --- Critical discharge <= 0 --> no critical discharge", FluxCritique);
            Options.OptionSeuilFlux = false;
        }
    }
    // GEOLOGIE
    if (strati.mode_sed == value)
    {
        if (strati.epaiss_sed < 0.) 
        {
            AfficheMessage("   --- Sediment thickness < 0 --> 0.");
            strati.epaiss_sed = 0.;
        }
    } 
    else 
    {
        if (strati.FichierEpaissSed.size() == 0) 
        {
            AfficheMessage("   --- No file name of sediment thickness --> 0.");
            strati.mode_sed = value;
            strati.epaiss_sed = 0.;
        }
    }
    if (strati.nbstrati > 0) 
    {
        if (strati.mode_couches == value) 
        {
            for (i = 0; i < strati.nbstrati - 1; i++) 
            {
                // si une couche est vide, faire remonter les autres d'un cran
                if (strati.TabCouche[i].epaisseur <= 0.) 
                {
                    for (j = i; j < strati.nbstrati - 2; j++) 
                    {
                        strati.TabCouche[j].indlitho = strati.TabCouche[j + 1].indlitho;
                        strati.TabCouche[j].epaisseur = strati.TabCouche[j + 1].epaisseur;
                    }
                    i--;
                    strati.nbstrati--;
                    AfficheMessage("   --- empty strata --> strata removed ", i + 1);
                }
            }
        }
    }
    if (nblitho < 0 && strati.nbstrati > 0) 
    {
        AfficheMessage("   --- Number of bedrock lithologies < 0 --> only sediment");
        strati.nbstrati = 0;
        Options.OptionAlter = false;
        Options.OptionDiffBedrock = false;
        Options.OptionIncis = false;
        strati.mode_sed = value;
        strati.epaiss_sed = EPAISSEUR_INF;
    }
    if (strati.nbstrati < 0) 
    {
        AfficheMessage("   --- Number of strata < 0 --> only sediments");
        strati.nbstrati = 0;
        Options.OptionAlter = false;
        Options.OptionDiffBedrock = false;
        Options.OptionIncis = false;
        strati.mode_sed = value;
        strati.epaiss_sed = EPAISSEUR_INF;
    }
    if (strati.nbstrati > NSTRATI_MAX) 
    {
        AfficheMessage("   --- Too many strata --> only", NSTRATI_MAX);
        strati.nbstrati = NSTRATI_MAX;
    }
    if (strati.nbstrati > 1 && nblitho == 1) 
    {
        AfficheMessage("   --- Several strata for only one lithology --> 1 stratum");
        strati.nbstrati = 1;
    }
    if (nblitho > 1 && strati.nbstrati == 1) 
    {
        AfficheMessage("   --- Several lithologies for only one strata --> 1 litho");
        int indicelitho = strati.TabCouche[0].indlitho;
        if (indicelitho != 0) 
        {
            delete ListeLitho[0].alter;
            delete ListeLitho[0].diff;
            delete ListeLitho[0].incis;
            ListeLitho[0].alter = ListeLitho[indicelitho].alter;
            ListeLitho[0].diff = ListeLitho[indicelitho].diff;
            ListeLitho[0].incis = ListeLitho[indicelitho].incis;
        }
        for (i = 1; i < nblitho; i++) 
        {
            delete ListeLitho[i].alter;
            delete ListeLitho[i].diff;
            delete ListeLitho[i].incis;
        }
        nblitho = 1;
    }
    if ((nblitho <= 0) || (strati.nbstrati <= 0)) 
    {
        if (nblitho <= 0)
            AfficheMessage("   --- No bedrock lithology --> no bedrock");
        if (strati.nbstrati <= 0)
            AfficheMessage("   --- No strata --> no bedrock");
        Options.OptionDiffBedrock = false;
        Options.OptionIncis = false;
        Options.OptionAlter = false;
        strati.mode_sed = value;
        strati.epaiss_sed = EPAISSEUR_INF;
    }
    // EROSION
    if (largeur->OuiNon && largeur->K <= 0.) 
    {
        AfficheMessage("   --- Coeff <= 0. in the river width law --> river width fixed to pixel size :",
                topoinitiale.pas);
        largeur->OuiNon = false;
    }
    // AltÈration
    if (Options.OptionAlter) 
    {
        if (nblitho <= 0) 
        {
            AfficheMessage("   --- No bedrock --> no weathering");
            Options.OptionAlter = false;
        } 
        else 
        {
            tmptest = false;
            for (i = 0; i < nblitho; i++) 
            {
                if ((ListeLitho[i].alter)->Kw < 0. || (ListeLitho[i].alter)->d1 < 0.) 
                {
                    AfficheMessage("   --- Weathering coeff < 0 --> Kw=0. for lithologie", i);
                    (ListeLitho[i].alter)->Kw = 0.;
                }
                else
                    tmptest = true;
            }
            if (!tmptest)
                Options.OptionAlter = false;
        }
    }


    // Incision
    if (Options.OptionIncis) 
    {
        tmptest = false;
        for (i = 0; i < nblitho; i++) 
        {
            if ((ListeLitho[i].incis)->K <= 0.) 
            {
                AfficheMessage("   --- Bedrock detachment coeff <= 0 \
								--> no detachment for litho", i);
                (ListeLitho[i].incis)->K = 0.;
            } 
            else
                tmptest = true;
        }
        if (!tmptest)
            Options.OptionIncis = false;
    }
    // Diffusion
    if (difflac->K <= 0.) 
    {
        AfficheMessage("   --- Offshore diffusion coefficient <= 0 \
						--> no offshore diffusion");
        Options.OptionDiffLac = false;
    }
    if (difflac->Type_diff == pas_diff)
        Options.OptionDiffLac = false;

    if (diffsed->K <= 0.) 
    {
        AfficheMessage("   --- Sediment diffusion coefficient <= 0 \
						--> no diffusion for  sed");
        Options.OptionDiffSed = false;
    }
    if (diffsed->Type_diff == pas_diff)
        Options.OptionDiffSed = false;

    if (Options.OptionDiffBedrock) 
    {
        tmptest = false;
        for (i = 0; i < nblitho; i++) 
        {
            if ((ListeLitho[i].diff)->K <= 0.) 
            {
                AfficheMessage("   --- Bedrock diffusion coefficient <= 0 \
								--> no diffusion for litho", i);
                (ListeLitho[i].diff)->Type_diff = pas_diff;
            } 
            else if ((ListeLitho[i].diff)->Type_diff != pas_diff)
                tmptest = true;
        }
        if (!tmptest)
            Options.OptionDiffBedrock = false;
    }
    // PrÈsence de sÈdiments
    if ((!Options.OptionDiffSed) && (!Options.OptionTransp)) 
    {
        if (Options.OptionDiffBedrock) 
        {
            AfficheMessage("   --- Illogical to diffuse bedrock and not sediment \
                           verify diffusion options for bedrock ");
        }
    }
    // PrÈsence de bedrock
    if ((!Options.OptionAlter) && (!Options.OptionDiffBedrock)
        && (!Options.OptionEffondr) && (!Options.OptionIncis)) 
    {
        AfficheMessage("   --- No bedrock erosion --> only sediment \
						are eroded");
    }
    // Effondrements
    if (Options.OptionEffondr) 
    {
        if (effondr.K < 0.) 
        {
            AfficheMessage("   --- Collapse coefficient < 0 --> 0.");
            effondr.K = 0.;
        } 
        else if (effondr.K > 0.5) 
        {
            AfficheMessage("   --- Collapse coefficient > 0.5 --> 0.5");
            effondr.K = 0.5;
        }
        if (effondr.Nmax < 1) 
        {
            AfficheMessage("   --- Max number of collapsing cells < 1 --> 1");
            effondr.Nmax = 1;
        }
    }
    // CLIMAT
    if (climat.Sinus && (climat.Periode < mymax(DTCALCUL_MIN, dtMin))) 
    {
        AfficheMessage("   --- Period of sinusoidal variations too small ! --> no sinusoidal variation");
        climat.Sinus = false;
    }
    // TECTONIQUE
    if (tecto.OptionDecro && (dabs(tecto.VitesseDecro) > TXDECRO_MAX)) 
    {
        AfficheMessage("   --- Strike-slip rate too large ! --> = MAX ", TXDECRO_MAX);
        if (tecto.VitesseDecro > 0)
            tecto.VitesseDecro = TXDECRO_MAX;
        else
            tecto.VitesseDecro = -TXDECRO_MAX;
    }
    if (tecto.OptionUplift && (tecto.mode_tecto == value)) 
    {
        for (j = 0; j < tecto.nbzone + 1; j++) 
        {
            if (tecto.numzone[j] < 0) 
            {
                AfficheMessage("   --- Row outside the grid (zone tecto) --> 1");
                tecto.numzone[j] = 0;
            }
            if (tecto.numzone[j] > topoinitiale.nblig) 
            {
                AfficheMessage("   --- Row outside the grid (zone tecto) -->",
                        topoinitiale.nblig);
                tecto.numzone[j] = topoinitiale.nblig;
            }
        }
    }
    //
    // SORTIES
    //
    double dureetmp = chrono.TabPeriode[chrono.nb].debut - chrono.TabPeriode[0].debut;
    if (resultats.dt_denud < dtMax) 
    {
        AfficheMessage("   --- Interval between two mean values outputs too small --> ", dtMax);
        resultats.dt_denud = dtMax;
    } 
    else if (resultats.dt_denud > dureetmp) 
    {
        AfficheMessage("   --- No outputs of mean values ! Actually yes, every ",
                dureetmp);
        resultats.dt_denud = dureetmp;
    }
    if (resultats.dt_bassin < dureetmp / (double) NSORTIES_MAX) 
    {
        AfficheMessage("   --- Interval between two grid outputs too small ! --> ",
                dureetmp / (double) NSORTIES_MAX);
        resultats.dt_bassin = dureetmp / (double) NSORTIES_MAX;
    } 
    else if (resultats.dt_bassin > dureetmp) 
    {
        AfficheMessage("   --- No grid output ! Actually yes, every ",
                dureetmp);
        resultats.dt_bassin = dureetmp;
    }
    if (resultats.OptionZone) 
    {
        if (!tecto.OptionUplift) 
        {
            AfficheMessage("   --- No tectonic zone defined --> no uplift");
            resultats.OptionZone = false;
        } 
        else if ((resultats.numzone < 1) || (resultats.numzone > tecto.nbzone)) 
        {
            AfficheMessage("   --- No specific region defined --> no specific region",
                    resultats.numzone);
            resultats.OptionZone = false;
        }
    }
    if (resultats.OptionMasque && (resultats.fich_masque.size() == 0)) 
    {
        AfficheMessage("   --- No file name for the spectific region --> no specific region");
        resultats.OptionMasque = false;
    }
    
    // Ajout Youssouf 02/11/2021
    if (Options.OptionAvecCailloux && Options.OptionCosmo && Options.OptionRessuscite)
    {
        AfficheMessage("   --- The resuscitate dead clasts option is not considered.");
        //Options.OptionRessuscite=false; test 2023

    }
    // Fin ajout

    
    AfficheMessage("          *** END of automatic editings ***\n");
    return;
}




//=============================================================================
// Reformulation de certains paramËtres
//=============================================================================

void CModele::Reformulation() 
{
    // Dans cette routine, on reformule les parametres pour que leur dimension soit
    // compatible avec les flux en volume par unite de temps, sachant que les parametres
    // d'entree du modele sont pour des loi par unite de largeur et de temps. On le fait ici pour ne pas
    // le faire a chaque maille et a chaque pas de temps.

    double tmpseuil;
    int i;
    // Niveau de base
    //if(Options.OptionNivBase && nivbase.mode_nivbase == value)
    //		nivbase.NivBase *= (topoinitiale.pas)*(topoinitiale.pas); //commente en decembre 2018

    
    // Entree locale d'eau et sediment  2022
    localentry.inputwaterflux *= UNANENSECONDES;
    localentry.inputsedflux *= UNANENSECONDES;
    
    
    // AltÈration
    for (i = 0; i < nblitho; i++) 
    {
        (ListeLitho[i].alter)->Kw *= (topoinitiale.pas)*(topoinitiale.pas);
        (ListeLitho[i].alter)->d1 *= (topoinitiale.pas)*(topoinitiale.pas); // modif le 01/02/11
        (ListeLitho[i].alter)->d2 *= (topoinitiale.pas)*(topoinitiale.pas); // ajout le 01/02/11
    }

    //Reformulation du coeff de la longueur de transport  - janvier 2013
    incisSed->CoeffLongTransport = incisSed->CoeffLongTransport / topoinitiale.pas; // [T/L**2]

    //Reformulation des exposants si le seuil de transport est nul (cf Doc)

    incisSed->K *= topoinitiale.pas; // modif 25 octobre 2007

    
    
    if (incisSed->Seuil == 0.) // on reformule tous les param pour calculer directement la stream power law
    {
        incisSed->MQ *= incisSed->Exposantshearstress;
        incisSed->NS *= incisSed->Exposantshearstress;
        //incisSed->Kmanning = pow(incisSed->Kmanning, incisSed->Exposantshearstress); // 25 octobre 2007
        incisSed->Kmanning = 1.; // 2022
        if (not largeur->OuiNon)
            incisSed->K *=  pow(topoinitiale.pas, 1. - incisSed->MQ); // modif janvier 2013 // modif 2022
    } 
    else
        // si le seuil n'est pas nul ... - modifs janvier 2013
    {
        incisSed->Kmanning = DENSITE * GRAVITE * pow(incisSed->Nmanning,3./5.) / pow(UNANENSECONDES, incisSed->MQ) ; // 2022
        
        if (not largeur->OuiNon)
        {
            incisSed->Kmanning *= pow(topoinitiale.pas, -1. * incisSed->MQ); // modif 2022
            incisSed->K *= topoinitiale.pas; // modif janvier 2013
        }
    }


    incisSed->Nmanning /= UNANENSECONDES; // 2022 pour le calcul de la hauteur d'eau si OptionHauteurEau

    // Correction du coeff d'erosion bedrock selon l'existence d'un seuil. Tout modifie juin 2014 pour faire comme pour le bedrock
    for (i = 0; i < nblitho; i++) 
    {
        (ListeLitho[i].incis)->K *= topoinitiale.pas;
        
        if ((ListeLitho[i].incis)->Seuil == 0.) 
        {
            (ListeLitho[i].incis)->MQ *= (ListeLitho[i].incis)->Exposantshearstress;
            (ListeLitho[i].incis)->NS *= (ListeLitho[i].incis)->Exposantshearstress;
            //(ListeLitho[i].incis)->Kmanning = pow((ListeLitho[i].incis)->Kmanning, (ListeLitho[i].incis)->Exposantshearstress);
            (ListeLitho[i].incis)->Kmanning = 1.; // 2022
            if (not largeur->OuiNon)
                (ListeLitho[i].incis)->K *=  pow(topoinitiale.pas, 1. - (ListeLitho[i].incis)->MQ); // modif 2022
            
        } 
        else
            // si le seuil n'est pas nul ...
        {
            (ListeLitho[i].incis)->Kmanning = DENSITE * GRAVITE * pow(incisSed->Nmanning,3./5.) / pow(UNANENSECONDES, (ListeLitho[i].incis)->MQ) ; // 2022
            if (not largeur->OuiNon) 
            {
                (ListeLitho[i].incis)->Kmanning *= pow(topoinitiale.pas, -1. * (ListeLitho[i].incis)->MQ);
                (ListeLitho[i].incis)->K *= topoinitiale.pas;
            }
        }
        (ListeLitho[i].incis)->Nmanning /= UNANENSECONDES; //2022 pour le calcul de la hauteur d'eau si
            //OptionHauteurEau. Même si pour l'instant on considère que c'estincisSed->Nmanning qui determine la
            //hauteur d'eau
    }


    // Correction des paramËtres de diffusion sous-marine des sediments
    difflac->K *= topoinitiale.pas * topoinitiale.pas; //janvier 2013... 
    tmpseuil = tan((difflac->Seuil) * PI / 180.);
    difflac->Seuil = tmpseuil;
    if (difflac->Type_diff == non_lineaire) 
    {
        difflac->Seuil2 = tmpseuil*tmpseuil;
        difflac->AlphaSeuil = ALPHA*tmpseuil;
        difflac->AlphaSeuilSur = ALPHA * tmpseuil / (1. - SCSQ);
    }

    // Correction des paramËtres de diffusion alluviale
    diffsed->K *= topoinitiale.pas * topoinitiale.pas; // janvier 2013... 
    tmpseuil = tan((diffsed->Seuil) * PI / 180.);
    diffsed->Seuil = tmpseuil;
    if (diffsed->Type_diff == non_lineaire) 
    {
        diffsed->Seuil2 = tmpseuil*tmpseuil;
        diffsed->AlphaSeuil = ALPHA*tmpseuil;
        diffsed->AlphaSeuilSur = ALPHA * tmpseuil / (1. - SCSQ);
    }

    // Correction des paramËtres de diffusion du substratum rocheux
    for (i = 0; i < nblitho; i++) 
    {
        (ListeLitho[i].diff)->K *= topoinitiale.pas * topoinitiale.pas; //  janvier 2013...
        tmpseuil = tan(((ListeLitho[i].diff)->Seuil) * PI / 180.);
        (ListeLitho[i].diff)->Seuil = tmpseuil;
        if ((ListeLitho[i].diff)->Type_diff == non_lineaire) 
        {
            (ListeLitho[i].diff)->Seuil2 = tmpseuil*tmpseuil;
            (ListeLitho[i].diff)->AlphaSeuil = ALPHA*tmpseuil;
            (ListeLitho[i].diff)->AlphaSeuilSur = ALPHA * tmpseuil / (1. - SCSQ);
        }
    }
    // Correction des paramËtres d'effondrements
    effondr.Seuil = tan((effondr.Seuil) * PI / 180.);
    // Correction des paramËtres climatiques
    climat.AltiMax *= (topoinitiale.pas)*(topoinitiale.pas);
   


    return;
}

//-----------------------------------------------
// Initialisation du maillage ‡ partir du modËle
//-----------------------------------------------

bool CModele::InitialiseMaillage(CMaillage* maillage) 
{
    int i;
    bool flag = true;
    //******************************************************************************
    // Initialisation du tableau de mailles et de la topologie (numÈros des voisins)
    maillage->InitialiseEspaceTopologieVoisinLateral(topoinitiale.nbcol, topoinitiale.nblig, topoinitiale.pas); // 7 aout 2008 TEST
    // Initialise les voisinages des mailles
    maillage->AlloueLesVoisinages(Type_pente);
    // Initialise les paramËtres du niveau de base
    maillage->SetNivBase(nivbase);
    if (Options.OptionNivBase && nivbase.mode_nivbase == file)
        maillage->LitEustatique(dtMin);
    // Initialise la taille des tableaux de precip / tecto
    maillage->SetNbPeriodes(chrono.nb);

    // DÈtermine le plus bas niveau de base,
    // si l'un des bords au moins n'est pas en flux nul
    Limites.base_min = EPAISSEUR_INF;
    if ((Limites.Type_bord[0] != flux_nul) || (Limites.Type_bord[1] != flux_nul)
        || (Limites.Type_bord[2] != flux_nul) || (Limites.Type_bord[3] != flux_nul))
        for (i = 0; i < 4; i++) 
        {
            if (Limites.base[i] < Limites.base_min)
                Limites.base_min = Limites.base[i];
        }

    // Affine la topologie (en fonction des bords) en passant
    maillage->AffineTopologieVoisinLateral(Limites); // 7 aout 2008 TEST
    //******************************************************************************

    //******************************************************************************
    // Lecture des altitudes
    //##### Modif. le 24/01/2006 : seulement si pas reprise !
    if (!Options.OptionReprise) 
    {
        if (!maillage->LitAltitudes(topoinitiale.FichierAlti)) 
        {
            AfficheMessage("Problem reading initial elevations");
            flag = false;
        }
    }
    //******************************************************************************

    //******************************************************************************
    // GÈologie
    if (nblitho < 1)
        maillage->SetSedimentUniforme(EPAISSEUR_INF);
    // Epaisseur sedimentaire initiale : uniforme...
    if (strati.mode_sed == value) 
    {
        maillage->SetSedimentUniforme(strati.epaiss_sed);

    }        // ... ou lecture dans un fichier
    
    
    else 
    {
        //##### Modif. le 24/01/2006 : seulement si pas reprise !
        if (!Options.OptionReprise) 
        {
            if (!maillage->LitEpaisseurs(strati.FichierEpaissSed)) 
            {
                AfficheMessage("Problem reading the initial sediment thickness file ",
                        strati.FichierEpaissSed);
                flag = false;

            }
        }
    }
    // Epaisseurs des couches de bedrock : une seule couche...

    if (strati.nbstrati == 1) 
    {
        maillage->InitialiseCouches(1);
        maillage->SetBedrockUniforme();

    }        // ... ou multicouche :
    else if (strati.nbstrati > 1) 
    {
        
        maillage->InitialiseCouches(strati.nbstrati);
        if (strati.mode_couches == value) 
        {
            // Epaisseurs uniformes...
            for (i = 0; i < strati.nbstrati - 1; i++)
                if (strati.TabCouche[i].epaisseur > 0.)
                    maillage->SetBedrockUniforme(strati.TabCouche[i].epaisseur, i,
                        strati.TabCouche[i].indlitho);
            maillage->SetBedrockUniforme(EPAISSEUR_INF, strati.nbstrati - 1,
                    strati.TabCouche[strati.nbstrati - 1].indlitho);
        } 
        else 
        {
            // ... ou lecture de fichiers (par couche)

            for (i = 0; i < strati.nbstrati - 1; i++) 
            {
                AfficheMessage("Couche : ", i, ", litho : ", strati.TabCouche[i].indlitho);
                if (!maillage->LitEpaisseurs(strati.TabCouche[i].fich, i,
                        strati.TabCouche[i].indlitho)) 
                {
                    AfficheMessage("Problem reading the stratigraphy file ",
                            strati.TabCouche[i].fich);
                    flag = false;
                }
            }
            maillage->SetBedrockUniforme(EPAISSEUR_INF, strati.nbstrati - 1,
                    strati.TabCouche[strati.nbstrati - 1].indlitho);
            //			AfficheMessage("Couche : ",strati.nbstrati-1,", litho : ",strati.TabCouche[strati.nbstrati-1].indlitho);
        }
    }

    //******************************************************************************
    // Tectonique
    // Taux de soulËvement par zones...
    if (tecto.mode_tecto == value) 
    {
        maillage->AlloueGrilleUplift();
        for (i = 0; i < chrono.nb; i++) 
        {
            if (!maillage->SetGrilleUplift(i, tecto, chrono.TabPeriode[i].taux_uplift)) 
            {
                AfficheMessage("Problem in assignation of tectonic parameters");
                flag = false;
            }
        }
    }        // ... ou lecture de fichiers (par pÈriode)
    else if (tecto.mode_tecto == file) 
    {
        maillage->AlloueGrilleUplift();
        for (i = 0; i < chrono.nb; i++) 
        {
            if (!maillage->LitGrilleUplift(chrono.TabPeriode[i].fich_uplift, i)) 
            {
                AfficheMessage("Problem reading the tectonic file ",
                        chrono.TabPeriode[i].fich_uplift);
                flag = false;
            }
        }
    }
    //******************************************************************************

    return flag;
}


//------------------------------
// Construit modËle et maillage
//------------------------------

bool CModele::ConstruitModele(CMaillage* maillage) 
{

    bool flag;

    flag = SauveInfo("param.cid");
    if (!flag) 
    {
        AfficheMessage("The writing of parameter file param.cid failed.");
        return false;
    }

    Reformulation();

    flag = InitialiseMaillage(maillage);
    if (flag == false) 
    {
        AfficheMessage("Problems in the initialisation of cells");
        return false;
    }

    return true;
}
