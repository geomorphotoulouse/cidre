#include "Options.h"
#include "CFlux.h"
#include <math.h>
using namespace std;

//-------------------------------------------
//
// fonctions de la classe CFlux :
//
//-------------------------------------------

// constructeur

CFlux::CFlux(CMaillage* maillage, CModele* modele) 
{
    MonModele = modele; // 2022
    nbmailles = maillage->GetNbMailles();
    nbcol = maillage->GetNbCol();
    nblig = maillage->GetNbLig();
    pas = modele->GetTopoInit().pas; //2022
    pas2 = maillage->GetPas2();
    StockSed = new double[nbmailles];
    NumCouche = new int[nbmailles];
    NumLitho = new int[nbmailles];
    VolumeCouche = new double[nbmailles];
    FluxEau = new double[nbmailles];
    EauCourante = new double[nbmailles];
    Depot = new double[nbmailles]; // janvier 2013
    DernierDepot = new double[nbmailles];
    Erosion = new double[nbmailles]; // janvier 2013
    VolumeDiff = new double[nbmailles];
    VolumeFluv = new double[nbmailles];
    DeltaErosion = new double[nbmailles];
    Precip = new double[nbmailles]; // ajout le 24 04 2007
    Precip2 = new double[nbmailles];
    FluxEauEnAttente = new double[nbmailles]; // 30 juin 2008
    climat = modele->GetParamClimat();
    AltiMax = climat.AltiMax;
    GradientAlti = climat.GradientAlti; // 19/03/12
    PeriodeSinus = climat.Periode;
    
    localentry = modele->GetParamLocalEntry(); // 2022
    
    incisSed = modele->GetParamIncisSed(); // 2022

    //Diffusivite = 0.; // decembre 2014

    //coeff dans le constructeur, ajoute le 01/02/11
    GaussA = pas2 * climat.GaussA;
    GaussB = pas2 * climat.GaussB;
    GaussC = pas2 * climat.GaussC;

    VolumeRegolitheCree = new double[nbmailles]; //ajoute le 01/02/11
    for (int k = 0; k < nbmailles; k++) 
    {
        VolumeRegolitheCree[k] = 0.;
    }

    nbperiodes = modele->GetNbPeriodes();

    //temperature, ajoute le 01/02/11
    DiminutionTemperature = climat.DiminutionTemperature;
    TemperatureSurface = new double [nbperiodes];
    for (int k = 0; k < nbperiodes; k++) 
    {
        TemperatureSurface[k] = modele->GetTemperatureSurface(k);
    }

    nblitho = modele->GetNbLitho();
    alter = new ParamAlter*[nblitho];
    for (int i = 0; i < nblitho; i++) alter[i] = (modele->GetLitho(i)).alter;

    MonMaillage = maillage;

    MaillesLac.reserve(TAILLEMAX_LAC);
    ZMaillesLac.reserve(TAILLEMAX_LAC);
    BordDuLac.reserve(TAILLEMAX_LAC);
    MilieuDuLac.reserve(TAILLEMAX_LAC);
    DejaLac = new bool[nbmailles];

    Debordement = false; // 30 juin 2008

    //##### TEMPORAIRE : sortie des capacitÈs de dÈtachement
    CapaDetach = new double[nbmailles];

    VolumeFluvExporte = new GrilleDble[nbmailles]; // 7 aout 2008.
    for (int i = 0; i < nbmailles; i++) 
    {
        VolumeFluvExporte[i] = new double[8]; // 7 aout 2008. Chaque maille pointe vers 8 voisins possibles
        Depot[i] = 0.; // 2015 ceci pour s'assurer que DernierDepot, qui reprend la valeur de Depot du precedent dt, est bien initialise au debut du calcul
    }

    DepotDiff = new double[nbmailles]; //Ajout Pierre
    ErosionDiff = new double[nbmailles]; //Ajout Pierre
    VolumeDiffExporte = new GrilleDble[nbmailles]; //Ajout Pierre
    for (int i = 0; i < nbmailles; i++) //Ajout Pierre
    {
        VolumeDiffExporte[i] = new double[8];
    }
}

// destructeur

CFlux::~CFlux() 
{
    delete [] StockSed;
    delete [] NumCouche;
    delete [] NumLitho;
    delete [] VolumeCouche;
    delete [] FluxEau;
    delete [] EauCourante;
    delete [] Depot; // janvier 2013
    delete [] DernierDepot;
    delete [] Erosion; // janvier 2013
    delete [] VolumeDiff;
    delete [] VolumeFluv;
    delete [] DeltaErosion;
    delete [] alter;
    delete [] Precip; // ajout le 24 04 2007
    delete [] CapaDetach; // ajout 30 juin 2008
    delete [] DejaLac; // ajout 30 juin 2008
    delete [] FluxEauEnAttente; // ajout 30 juin 2008
    delete [] VolumeFluvExporte; // 7 aout 2008
    delete [] VolumeRegolitheCree; // 01/02/11
    delete [] Precip2; // 01/02/11
}

//==================
// Autres fonctions
//==================

//---------------------------------------------
// Initialise le stock sÈdimentaire
// et l'Ètat de la premiËre couche de bedrock
// ‡ partir des infos des mailles d'un maillage
//---------------------------------------------

void CFlux::InitialiseStockSedLitho() 
{
    CMaille* maille;
    int numcouche;
    for (int k = 0; k < nbmailles; k++) 
    {
        maille = MonMaillage->GetMaille(k);
        StockSed[k] = maille->GetStockInitSed();
        numcouche = 0;
        while (maille->GetEpaisseur(numcouche) == 0.) 
        {
            numcouche++;
        }
        NumCouche[k] = numcouche;
        NumLitho[k] = maille->GetIndiceLitho(numcouche);
        VolumeCouche[k] = maille->GetEpaisseur(numcouche);
    }
    return;
}

//-------------------------
// Initialise tous les flux
//-------------------------

bool CFlux::InitialisePrecip(CModele* modele) 
{
    bool flag = true;
    int i;

    //******************************************************************************
    // Climat
    AlloueGrillePrecip();
    // PrÈcipitations uniformes...
    if (climat.mode_precip == value) 
    {
        for (i = 0; i < nbperiodes; i++)
            SetGrillePrecipUniforme(i, modele->GetPeriode(i).taux_precip);
    }        // ... ou lecture de fichiers (par pÈriode)
    else {
        for (i = 0; i < nbperiodes; i++) 
        {
            if (!LitGrillePrecip(i, modele->GetPeriode(i).fich_precip, modele->GetPeriode(i).frac_temps_pluie)) 
            {
                AfficheMessage("Erreur dans la lecture du fichier precip ",
                        modele->GetPeriode(i).fich_precip);
                flag = false;
            }
        }
    }
    //******************************************************************************
    return flag;
}

//-----------------------------------------------------------------
// Initialise au dÈbut du pas de temps : garde les volumes des lacs
//-----------------------------------------------------------------
//##### Modif le 06/10/2005: Cette initialisation est faite en mÍme temps
// que RecupPrecip qui initialise FluxEau
/*
void CFlux::InitialisePasTemps()
{
        for (int k=0; k < nbmailles; k++)
        {
//##### Modif le 06/10/2005: FluxEau est assignÈ quand on rÈcupËre les prÈcips.
//		FluxEau[k] = 0.;
                EauCourante[k] = 0.;
                VolumeDiff[k] = 0.;
                VolumeFluv[k] = 0.;
                DeltaErosion[k] = 0.;
        }
        VolumeSed_Out = 0.;
        VolumeSed_In = 0.;
        FluxEau_Out = 0.;
        FluxEau_In = 0.;
        EauCouranteMax = 0.;
        return;
}
 */
//--------------------------------------------------------------------------
// Actualise en cas d'Èrosion des sÈdiments et/ou du bedrock (effondrements)
//--------------------------------------------------------------------------
//-----------
// MONOcouche
//-----------

void CFlux::SoustraitAlti_ActualiseStrati_MonoCouche(const int &i, CMaille* maille, double volume_erode) 
{
    // volume_erode est POSITIF - ‡ soustraire
    maille->AddLesAlti(-volume_erode);
    DeltaErosion[i] -= volume_erode;
    // s'il y a assez de sÈdiments prÈsents sur la maille
    if (volume_erode <= StockSed[i])
        StockSed[i] -= volume_erode;
        // si on Èrode tous les sÈdiments prÈsents sur la maille
    else
        StockSed[i] = 0.;
    return;
}
//------------
// MULTIcouche
//------------

void CFlux::SoustraitAlti_ActualiseStrati_MultiCouche(const int &i, CMaille* maille, double volume_erode) 
{
    // volume_erode est POSITIF - ‡ soustraire
    maille->AddLesAlti(-volume_erode);
    DeltaErosion[i] -= volume_erode;
    // s'il y a assez de sÈdiments prÈsents sur la maille
    if (volume_erode <= StockSed[i]) 
    {
        StockSed[i] -= volume_erode;
        return;
    }        // si on Èrode tous les sÈdiments prÈsents sur la maille
    else {
        StockSed[i] = 0.;
        volume_erode -= StockSed[i];
        while (volume_erode >= VolumeCouche[i]) 
        {
            volume_erode -= VolumeCouche[i];
            //			maille->AnnuleEpaisseur(NumCouche[i]);
            NumCouche[i]++;
            VolumeCouche[i] = maille->GetEpaisseur(NumCouche[i]);
        }
        //		maille->SoustraitEpaisseur(NumCouche[i],volume_erode);
        VolumeCouche[i] -= volume_erode;
        NumLitho[i] = maille->GetIndiceLitho(NumCouche[i]);
    }
    return;
}

//******************************************************************
//		RECUPERATION DE LA PLUIE TOMBEE PENDANT UN PAS DE TEMPS
//******************************************************************

//--------------------------------------------------
// RÈcupËre la pluie tombÈe pendant le pas de temps,
//	cas des prÈcipitations uniformes et constantes
//--------------------------------------------------

void CFlux::InitFlux_RecuperePluie_NoSin_NoLin(const double &temps) 
{
    int k, j;
    
    FluxEau_In = 0.;
    VolumeSed_In = 0.;
    for (k = 0; k < nbmailles; k++) 
    {
        //##### Modif le 15/05/2006 : L'eau des lacs est maintenant simplement gardÈe dans AltiEau-Alti
        //		FluxEau[k] += TabPrecip[NumPeriode][k];
        //##### Modif le 01/06/2006 : si on rÈcupËre aussi VolumeLac, Áa revient ‡ le compter 2 fois dans les lacs
        //	car on tient compte alors d'AltiEau...
        //		FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + Precip[k];
        // Modif 30 juin 2008 pour tenir compte de l'eau en attente dans le cas ou on calcule les hauteurs d'eau en MF
        //		FluxEau[k] = Precip[k];
        FluxEau[k] = Precip[k] + FluxEauEnAttente[k]; // 30 juin 2008
        Precip2[k] = Precip[k];
        //		cout <<  " FluxEauEnAttente[k] "<< FluxEauEnAttente[k]<<"\n";
        FluxEauEnAttente[k] = 0.; //  30 juin 2008
        EauCourante[k] = 0.;
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        for (j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.; // 7 aout 2008
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;


        if (k % nbcol+1  >= localentry.inputimin && k % nbcol +1 <= localentry.inputimax && int(k/nbcol)+1 >= localentry.inputjmin && int(k/nbcol)+1 <= localentry.inputjmax) // 2022
        {
            VolumeFluv[k] = localentry.inputsedflux * MonModele->GetPasTemps(NumPeriode); // unite m3
            FluxEau[k] += localentry.inputwaterflux ; // unite m3/a
            VolumeSed_In += VolumeFluv[k];
            FluxEau_In += FluxEau[k];
        }


        // tempo dirty test effet orientation pluie qui vient de l'est
        /*int dir = MonMaillage->GetMaille(k)->GetDirectionVoisinsBas(0);
         if ( dir==2 || dir==5 || dir==6 )
         {FluxEau[k]=Precip[k]*0.5;}
         else if( dir==8 || dir==4 || dir==7 )
         {FluxEau[k]=Precip[k]*2;}
         else
         {FluxEau[k]=Precip[k];}*/
    }
    VolumeSed_Out = 0.;
    FluxEau_Out = 0.;


    return;
}

//----------------------------------------------------
// RÈcupËre la pluie tombÈe pendant le pas de temps,
//	cas des prÈcipitations constantes mais :
//	linÈairement croissantes en fonction de l'altitude
//----------------------------------------------------

void CFlux::InitFlux_RecuperePluie_NoSin_Lin(const double &temps) 
{
    double zlocal;
    
    FluxEau_In = 0.;
    VolumeSed_In = 0.;
    for (int k = 0; k < nbmailles; k++)
    {
        zlocal = MonMaillage->GetMaille(k)->GetAlti();

        if (zlocal >= AltiMax)
            //##### Modif le 15/05/2006 : L'eau des lacs est maintenant simplement gardÈe dans AltiEau-Alti
            FluxEau[k] = Precip[k] + AltiMax * GradientAlti; //modifie le 19/03/12
        else
            Precip2[k] = Precip[k] + zlocal * GradientAlti; //modifie le 19/03/12
        FluxEau[k] = Precip2[k];
        //FluxEau[k] = zlocal * Precip[k] / AltiMax;
        EauCourante[k] = 0.;
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        

        if (k % nbcol+1  >= localentry.inputimin && k % nbcol +1 <= localentry.inputimax && int(k/nbcol)+1 >= localentry.inputjmin && int(k/nbcol)+1 <= localentry.inputjmax) // 2022
        {
            VolumeFluv[k] = localentry.inputsedflux * MonModele->GetPasTemps(NumPeriode); // unite m3
            FluxEau[k] += localentry.inputwaterflux ; // unite m3/a
            VolumeSed_In += VolumeFluv[k];
            FluxEau_In += FluxEau[k];
        }
        
        for (int j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.; // 7 aout 2008
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;
    }
    VolumeSed_Out = 0.;
    FluxEau_Out = 0.;

    /*	Nb_CorrectionsDepot = 0;
            Nb_RemplissageSedLac = 0;
            Nb_RemplissageSedPlat = 0;
     */
    return;
}


//+++++++++++++ DÈpendance prÈcipitations/altitudes GAUSSIENNE ajoute le 01/02/11 +++++++++++++++++

void CFlux::InitFlux_RecuperePluie_NoSin_NoLin_Gaussienne(const double &temps) 
{
    double zlocal;

    FluxEau_In = 0.;
    VolumeSed_In = 0.;
    for (int k = 0; k < nbmailles; k++) 
    {
        zlocal = MonMaillage->GetMaille(k)->GetAlti();

        double gaussienne = GaussA * exp(-((zlocal - GaussB)*(zlocal - GaussB)) / (2 * GaussC * GaussC));
        Precip2[k] = Precip[k] + gaussienne;

        FluxEau[k] = Precip2[k];

        EauCourante[k] = 0.;
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        
;

        if (k % nbcol+1  >= localentry.inputimin && k % nbcol +1 <= localentry.inputimax && int(k/nbcol)+1 >= localentry.inputjmin && int(k/nbcol)+1 <= localentry.inputjmax) // 2022
        {
            VolumeFluv[k] = localentry.inputsedflux * MonModele->GetPasTemps(NumPeriode); // unite m3
            FluxEau[k] += localentry.inputwaterflux ; // unite m3/a
            VolumeSed_In += VolumeFluv[k];
            FluxEau_In += FluxEau[k];
        }
        
        for (int j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.;
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;
    }
    VolumeSed_Out = 0.;
    FluxEau_Out = 0.;

    return;
}

//--------------------------------------------------
// RÈcupËre la pluie tombÈe pendant le pas de temps,
//	cas des prÈcipitations uniformes mais :
//	variant pÈriodiquement au cours du temps
//--------------------------------------------------

void CFlux::InitFlux_RecuperePluie_Sin_NoLin(const double &temps)
{
    double facteur_sin = 0.5 * sin(2. * PI * temps / PeriodeSinus) + 0.5;
    //cout << temps << " "<< facteur_sin <<endl;
    
    FluxEau_In = 0.;
    VolumeSed_In = 0.;
    for (int k = 0; k < nbmailles; k++) 
    {
        //##### Modif le 15/05/2006 : L'eau des lacs est maintenant simplement gardÈe dans AltiEau-Alti
        //		FluxEau[k] += facteur_sin * TabPrecip[NumPeriode][k];
        //		FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + (facteur_sin * Precip[k]);
        Precip2[k] = facteur_sin * Precip[k];
        FluxEau[k] = Precip2[k];

        EauCourante[k] = 0.;
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        

        if (k % nbcol+1  >= localentry.inputimin && k % nbcol +1 <= localentry.inputimax && int(k/nbcol)+1 >= localentry.inputjmin && int(k/nbcol)+1 <= localentry.inputjmax) // 2022
        {
            VolumeFluv[k] = localentry.inputsedflux * MonModele->GetPasTemps(NumPeriode); // unite m3
            FluxEau[k] += localentry.inputwaterflux ; // unite m3/a
            VolumeSed_In += VolumeFluv[k];
            FluxEau_In += FluxEau[k];
        }
        
        for (int j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.; // 7 aout 2008
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;

        // if (k >= 56 && k <=64) {VolumeFluv[k] = 20000.*facteur_sin;FluxEau[k] = 3200.*125.*125.*facteur_sin;}  // temporaire oct  2014 pour test canal !!!!!
        // tempo dirty test effet orientation pluie qui vient de l'est
        /*int dir = MonMaillage->GetMaille(k)->GetDirectionVoisinsBas(0);
         if ( dir==2 || dir==5 || dir==6 )
         {FluxEau[k]=Precip2[k]*0.5;}
         else if( dir==8 || dir==4 || dir==7 )
         {FluxEau[k]=Precip2[k]*2;}
         else
         {FluxEau[k]=Precip2[k];}*/

    }
    VolumeSed_Out = 0.;
    FluxEau_Out = 0.;


    /*	Nb_CorrectionsDepot = 0;
            Nb_RemplissageSedLac = 0;
            Nb_RemplissageSedPlat = 0;
     */
    return;
}

//----------------------------------------------------
// RÈcupËre la pluie tombÈe pendant le pas de temps,
//	cas des prÈcipitations :
//	linÈairement croissantes en fonction de l'altitude
//	et variant pÈriodiquement au cours du temps
//----------------------------------------------------

void CFlux::InitFlux_RecuperePluie_Sin_Lin(const double &temps) 
{
    double zlocal;
    double facteur_sin = 0.5 * sin(2. * PI * temps / PeriodeSinus) + 0.5;
    double facteur_sin_AltiMax = facteur_sin / AltiMax;

    FluxEau_In = 0.;
    VolumeSed_In = 0.;
    for (int k = 0; k < nbmailles; k++) 
    {
        zlocal = MonMaillage->GetMaille(k)->GetAlti();
        if (zlocal >= AltiMax) 
        {
            //##### Modif le 15/05/2006 : L'eau des lacs est maintenant simplement gardÈe dans AltiEau-Alti
            //			FluxEau[k] += facteur_sin * Precip[k];
            //			FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + (facteur_sin * Precip[k]);
            Precip2[k] = facteur_sin * (Precip[k] + AltiMax * GradientAlti);
            FluxEau[k] = Precip2[k];
        } else
            //			FluxEau[k] += facteur_sin_AltiMax * zlocal * Precip[k];
            //			FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + (facteur_sin_AltiMax * zlocal * Precip[k]);
            Precip2[k] = facteur_sin_AltiMax * (Precip[k] + zlocal * GradientAlti);
        FluxEau[k] = Precip2[k];
        EauCourante[k] = 0.;
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        

        if (k % nbcol+1  >= localentry.inputimin && k % nbcol +1 <= localentry.inputimax && int(k/nbcol)+1 >= localentry.inputjmin && int(k/nbcol)+1 <= localentry.inputjmax) // 2022
        {
            VolumeFluv[k] = localentry.inputsedflux * MonModele->GetPasTemps(NumPeriode); // unite m3
            FluxEau[k] += localentry.inputwaterflux ; // unite m3/a
            VolumeSed_In += VolumeFluv[k];
            FluxEau_In += FluxEau[k];
        }
        
        for (int j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.; // 7 aout 2008
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;
    }
    VolumeSed_Out = 0.;
    FluxEau_Out = 0.;

    /*	Nb_CorrectionsDepot = 0;
            Nb_RemplissageSedLac = 0;
            Nb_RemplissageSedPlat = 0;
     */
    return;
}

//#####################################################
//  Surcharge si on veut adapter le pas de temps
//#####################################################

double CFlux::InitFlux_RecuperePluie_NoSin_Lin_PrecMax(const double &temps) 
{
    double zlocal;
    double PrecipMax = -EPAISSEUR_INF;

    FluxEau_In = 0.;
    VolumeSed_In = 0.;
    for (int k = 0; k < nbmailles; k++) 
    {
        zlocal = MonMaillage->GetMaille(k)->GetAlti();
        if (zlocal >= AltiMax)
            //##### Modif le 15/05/2006 : L'eau des lacs est maintenant simplement gardÈe dans AltiEau-Alti
            //			FluxEau[k] += TabPrecip[NumPeriode][k];
            //			FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + Precip[k];
            FluxEau[k] = Precip[k] + AltiMax * GradientAlti;
        else
            //			FluxEau[k] += zlocal * TabPrecip[NumPeriode][k] / AltiMax;
            //			FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + (zlocal * Precip[k] / AltiMax);
            FluxEau[k] = Precip[k] + zlocal * GradientAlti;
        EauCourante[k] = 0.;
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        
        if (k % nbcol+1  >= localentry.inputimin && k % nbcol +1 <= localentry.inputimax && int(k/nbcol)+1 >= localentry.inputjmin && int(k/nbcol)+1 <= localentry.inputjmax) // 2022
        {
            VolumeFluv[k] = localentry.inputsedflux * MonModele->GetPasTemps(NumPeriode); // unite m3
            FluxEau[k] += localentry.inputwaterflux ; // unite m3/a
            VolumeSed_In += VolumeFluv[k];
            FluxEau_In += FluxEau[k];
        }
        
        for (int j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.; // 7 aout 2008
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;

        if (FluxEau[k] > PrecipMax) PrecipMax = FluxEau[k];
    }
    VolumeSed_Out = 0.;
    FluxEau_Out = 0.;
    return PrecipMax;
}
//-------------------------------------------------------------------------

double CFlux::InitFlux_RecuperePluie_Sin_NoLin_PrecMax(const double &temps) 
{
    double facteur_sin = 0.5 * sin(2. * PI * temps / PeriodeSinus) + 0.5;
    double PrecipMax = -EPAISSEUR_INF;

    FluxEau_In = 0.;
    VolumeSed_In = 0.;
    for (int k = 0; k < nbmailles; k++) 
    {
        //##### Modif le 15/05/2006 : L'eau des lacs est maintenant simplement gardÈe dans AltiEau-Alti
        //		FluxEau[k] += facteur_sin * TabPrecip[NumPeriode][k];
        //		FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + (facteur_sin * Precip[k]);
        FluxEau[k] = facteur_sin * Precip[k];
        EauCourante[k] = 0.;
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        

        if (k % nbcol+1  >= localentry.inputimin && k % nbcol +1 <= localentry.inputimax && int(k/nbcol)+1 >= localentry.inputjmin && int(k/nbcol)+1 <= localentry.inputjmax) // 2022
        {
            VolumeFluv[k] = localentry.inputsedflux * MonModele->GetPasTemps(NumPeriode); // unite m3
            FluxEau[k] += localentry.inputwaterflux ; // unite m3/a
            VolumeSed_In += VolumeFluv[k];
            FluxEau_In += FluxEau[k];
        }
        
        for (int j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.; // 7 aout 2008
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;

        if (FluxEau[k] > PrecipMax) PrecipMax = FluxEau[k];
    }
    VolumeSed_Out = 0.;
    FluxEau_Out = 0.;
    return PrecipMax;
}
//-----------------------------------------------------------------------

double CFlux::InitFlux_RecuperePluie_Sin_Lin_PrecMax(const double &temps) 
{
    double zlocal;
    double facteur_sin = 0.5 * sin(2. * PI * temps / PeriodeSinus) + 0.5;
    double facteur_sin_AltiMax = facteur_sin / AltiMax;
    double PrecipMax = -EPAISSEUR_INF;

    FluxEau_In = 0.;
    VolumeSed_In = 0.;
    for (int k = 0; k < nbmailles; k++) 
    {
        zlocal = MonMaillage->GetMaille(k)->GetAlti();
        if (zlocal > AltiMax)
            //##### Modif le 15/05/2006 : L'eau des lacs est maintenant simplement gardÈe dans AltiEau-Alti
            //			FluxEau[k] += facteur_sin * Precip[k];
            //			FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + (facteur_sin * Precip[k]);
            FluxEau[k] = facteur_sin * (Precip[k] + AltiMax * GradientAlti);
        else
            //##### Modif le 15/05/2006 : L'eau des lacs est maintenant simplement gardÈe dans AltiEau-Alti
            //			FluxEau[k] += facteur_sin_AltiMax * zlocal * Precip[k];
            //			FluxEau[k] = MonMaillage->GetMaille(k)->GetVolumeLac() + (facteur_sin_AltiMax * zlocal * Precip[k]);
            FluxEau[k] = facteur_sin_AltiMax * (Precip[k] + zlocal * GradientAlti);
        EauCourante[k] = 0.; 
        DernierDepot[k] = Depot[k];
        Depot[k] = 0.; // janvier 2013
        Erosion[k] = 0.; // janvier 2013
        DepotDiff[k] = 0.; //Ajout Pierre
        ErosionDiff[k] = 0.; //Ajout Pierre
        VolumeDiff[k] = 0.;
        VolumeFluv[k] = 0.;
        
 
        if (k % nbcol+1  >= localentry.inputimin && k % nbcol +1 <= localentry.inputimax && int(k/nbcol)+1 >= localentry.inputjmin && int(k/nbcol)+1 <= localentry.inputjmax) // 2022
        {
            VolumeFluv[k] = localentry.inputsedflux * MonModele->GetPasTemps(NumPeriode); // unite m3
            FluxEau[k] += localentry.inputwaterflux ; // unite m3/a
            VolumeSed_In += VolumeFluv[k];
            FluxEau_In += FluxEau[k];
        }
        
        for (int j = 0; j < 8; j++) 
        {
            VolumeFluvExporte[k][j] = 0.; // 7 aout 2008
            VolumeDiffExporte[k][j] = 0.; // Ajout Pierre
        }
        DeltaErosion[k] = 0.;

        if (FluxEau[k] > PrecipMax) PrecipMax = FluxEau[k];
    }
    VolumeSed_Out = 0.;
    FluxEau_Out = 0.;
;
    return PrecipMax;
}


//******************************************************************
//		ALTERATION DES COUCHES SOUS-JACENTES AUX SEDIMENTS
//******************************************************************


//--------------------------------------
// Altération du bedrock en sÈdiments - version ‡ une seule litho
// Loi avec un regolithe optimum (ex Strudley and Murray 07)
// L'alteration ne depend pas de la temperature et runoff
// 01/02/11
//--------------------------------------

void CFlux::AltereBedrock_MonoCouche(const int &i, CMaille* maille) 
{
    double F1 = 0.;
    double F2 = 0.;
    double epaisseur = StockSed[i] - DernierDepot[i];
    if (epaisseur < 0.) epaisseur = 0.;
    double x1 = -epaisseur / alter[0]->d1;
    double x2 = -epaisseur / alter[0]->d2;
    if (x1 > MOINS_EXP_MIN) F1 = exp(x1);
    if (x2 > MOINS_EXP_MIN) F2 = exp(x2);

    VolumeRegolitheCree[i] = alter[0]->Kwdt * (F1 - alter[0]->k1 * F2);
    StockSed[i] += VolumeRegolitheCree[i];
    VolumeCouche[i] -= VolumeRegolitheCree[i];
    return;
}


//--------------------------------------
// Altération du bedrock en sÈdiments - version ‡ une seule litho
// Loi avec un regolithe optimum (ex Strudley and Murray 07)
// L'alteration depend de la temperature et runoff
//  01/02/11
//--------------------------------------

void CFlux::AltereBedrock_MonoCouche_TempRunoff(const int &i, CMaille* maille) 
{
    double F1 = 0.;
    double F2 = 0.;
    double epaisseur = StockSed[i] - DernierDepot[i];
    if (epaisseur < 0.) epaisseur = 0.;
    double x1 = -epaisseur / alter[0]->d1;
    double x2 = -epaisseur / alter[0]->d2;

    double zlocal = MonMaillage->GetMaille(i)->GetAlti();
    double temperaturelocale = (273.15 + TemperatureSurface[NumPeriode])-(climat.DiminutionTemperature * (zlocal / pas2));

    // ajout d'un seuil pour le runoff sous forme d'un nombre de mailles maximum.
    // Une fois atteint, l'alteration ne depend plus du runoff. On a atteint un seuil de saturation du sol en eau.

    double runofflimite = alter[0]->NbMailleRunoffLimite * Precip2[i] / pas2;
    double runofflocal = EauCourante[i] / pas2;

    if (runofflocal > runofflimite) runofflocal = runofflimite;

    if (x1 > MOINS_EXP_MIN) F1 = exp(x1);
    if (x2 > MOINS_EXP_MIN) F2 = exp(x2);
    VolumeRegolitheCree[i] = alter[0]->Kwdt * (F1 - alter[0]->k1 * F2);
    // test braun tempo
    //if (epaisseur > 0.3*500*500)
    //{
    //cout << "av "<<VolumeRegolitheCree[i] / (500*500)<< endl;
    //VolumeRegolitheCree[i] = alter[0]->Kwdt * ( 0.1667*500.*500./epaisseur ) ;
    //cout << "ap "<<VolumeRegolitheCree[i] / (500*500)<< endl;

    //}
    // fin test braun
    VolumeRegolitheCree[i] *= runofflocal * exp((-NRJACTIV / RGAZPARFAIT)*(1. / temperaturelocale - 1. / TEMPERATUREREFERENCE));

    StockSed[i] += VolumeRegolitheCree[i];
    VolumeCouche[i] -= VolumeRegolitheCree[i];
    return;
}



//--------------------------------------
// Si altÈration du bedrock en sÈdiments - version multilitho
// Loi avec un regolithe optimum (ex Strudley and Murray 07)
// L'alteration ne depend pas de la temperature et runoff
//  01/02/11
//--------------------------------------

void CFlux::AltereBedrock_MultiCouche(const int &i, CMaille* maille) 
{
    double F1 = 0.;
    double F2 = 0.;
    double volumealtere = 0.;
    double x1 = -(StockSed[i] - DernierDepot[i]) / alter[NumLitho[i]]->d1;
    double x2 = -(StockSed[i] - DernierDepot[i]) / alter[NumLitho[i]]->d2;
    if (x1 > MOINS_EXP_MIN) F1 = exp(x1);
    if (x2 > MOINS_EXP_MIN) F2 = exp(x2);
    VolumeRegolitheCree[i] = alter[NumLitho[i]]->Kwdt * (F1 - alter[NumLitho[i]]->k1 * F2);
    volumealtere = VolumeRegolitheCree[i];

    while (volumealtere >= VolumeCouche[i]) // on passe aux couches suivantes
    {
        StockSed[i] += VolumeCouche[i];
        // On change de couche
        NumCouche[i]++;
        NumLitho[i] = maille->GetIndiceLitho(NumCouche[i]);
        double x1 = -(StockSed[i] - DernierDepot[i]) / alter[NumLitho[i]]->d1;
        double x2 = -(StockSed[i] - DernierDepot[i]) / alter[NumLitho[i]]->d2;
        if (x1 > MOINS_EXP_MIN) F1 = exp(x1);
        if (x2 > MOINS_EXP_MIN) F2 = exp(x2);
        volumealtere = alter[NumLitho[i]]->Kwdt * (F1 - alter[NumLitho[i]]->k1 * F2);
        VolumeRegolitheCree[i] += volumealtere;
        VolumeCouche[i] = maille->GetEpaisseur(NumCouche[i]);
    }

    VolumeCouche[i] -= volumealtere;
    StockSed[i] += VolumeRegolitheCree[i];
    return;
}

//--------------------------------------
// Si altÈration du bedrock en sÈdiments - version multilitho
// Loi avec un regolithe optimum (ex Strudley and Murray 07)
// L'alteration depend de la temperature et runoff
//  01/02/11
//--------------------------------------

void CFlux::AltereBedrock_MultiCouche_TempRunoff(const int &i, CMaille* maille) 
{
    double F1 = 0.;
    double F2 = 0.;
    double volumealtere = 0.;

    double zlocal = MonMaillage->GetMaille(i)->GetAlti();
    double temperaturelocale = 273.15 + mymax(0., TemperatureSurface[NumPeriode] - climat.DiminutionTemperature * (zlocal / pas2));

    // ajout d'un seuil pour le runoff sous forme d'un nombre de maille maximum.
    // Une fois atteint, l'alteration ne depend plus du runoff. On a atteint un seuil de saturation du sol en eau.

    double runofflimite = alter[0]->NbMailleRunoffLimite * Precip2[i] / pas2;
    double runofflocal = EauCourante[i] / pas2;
    if (runofflocal > runofflimite) runofflocal = runofflimite;
    double alterationdueaurunoff = runofflocal * exp((NRJACTIV / RGAZPARFAIT)*(1. / TEMPERATUREREFERENCE - 1. / temperaturelocale));

    double x1 = -(StockSed[i] - DernierDepot[i]) / alter[NumLitho[i]]->d1;
    double x2 = -(StockSed[i] - DernierDepot[i]) / alter[NumLitho[i]]->d2;
    if (x1 > MOINS_EXP_MIN) F1 = exp(x1);
    if (x2 > MOINS_EXP_MIN) F2 = exp(x2);
    VolumeRegolitheCree[i] = alter[NumLitho[i]]->Kwdt * (F1 - alter[NumLitho[i]]->k1 * F2);
    VolumeRegolitheCree[i] *= alterationdueaurunoff;
    volumealtere = VolumeRegolitheCree[i];

    while (volumealtere >= VolumeCouche[i]) // on passe aux couches suivantes
    {
        StockSed[i] += VolumeCouche[i];
        // On change de couche
        NumCouche[i]++;
        NumLitho[i] = maille->GetIndiceLitho(NumCouche[i]);
        double x1 = -(StockSed[i] - DernierDepot[i]) / alter[NumLitho[i]]->d1;
        double x2 = -(StockSed[i] - DernierDepot[i]) / alter[NumLitho[i]]->d2;
        if (x1 > MOINS_EXP_MIN) F1 = exp(x1);
        if (x2 > MOINS_EXP_MIN) F2 = exp(x2);
        volumealtere = alter[NumLitho[i]]->Kwdt * (F1 - alter[NumLitho[i]]->k1 * F2);
        volumealtere *= alterationdueaurunoff;
        VolumeRegolitheCree[i] += volumealtere;
        VolumeCouche[i] = maille->GetEpaisseur(NumCouche[i]);
    }

    VolumeCouche[i] -= volumealtere;
    StockSed[i] += VolumeRegolitheCree[i];
    return;
}





//******************************************
//		PROPAGATION DE L'EAU COURANTE
//******************************************

//-------------------------------------------------------
// Propage l'eau courante d'une maille sur son voisin bas
//-------------------------------------------------------

void CFlux::DeverseSurMonVoisin(const int &i, CMaille* maille) 
{
    int numvoisin = maille->GetNumVoisinBas();

    //##### Modif le 07/10/2005 : finalement, un seul test cf Apero
    // (en espÈrant que le maille->GetPasBord() ralentissait la manip)
    //	if ( (maille->GetPasBord()) || (numvoisin >= 0) )
    if (numvoisin >= 0)
        FluxEau[numvoisin] += FluxEau[i];
        //##### Modif le 14/09/2005 : on reprend comme dans APERO
        //##### Modif le 06/10/2005 : la diffÈrence se pose pour le cas numvoisin < 0
        // Si au bord :
    else 
    {
        FluxEau_Out += FluxEau[i];
    }
    // Ajustement de l'eau courante
    // Il faut remettre FluxEau ‡ 0 au cas o˘ on repasserait par l‡ avec de l'eau
    // dÈversÈe par un lac (pas compter deux fois l'eau qui arrivait au 1er passage)
    EauCourante[i] += FluxEau[i];
    //	if (EauCourante[i] > EauCouranteMax) EauCouranteMax = EauCourante[i];
    FluxEau[i] = 0.;
    //##### Modif le 15/05/2006 : ajuste ZEau ‡ Z s'il y avait de l'eau au pas d'avant
    maille->Ajuste_ZEau_a_Z();
    return;
}
//----------------------------------------------
// Surcharge si on est sur une maille "fils-lac"
//----------------------------------------------

int CFlux::DeverseSurMonVoisin_FilsLac(const int &i, CMaille* maille, int* lenumvoisin) 
{
    int numvoisin = maille->GetNumVoisinBas();

    //##### Modif le 07/10/2005 : finalement, un seul test cf Apero
    if (numvoisin >= 0) 
    {
        FluxEau[numvoisin] += FluxEau[i];
        EauCourante[i] += FluxEau[i];
        FluxEau[i] = 0.;
        lenumvoisin[0] = numvoisin;
        return 1;
    }        //##### Modif le 14/09/2005 : on reprend comme dans APERO
        //##### Modif le 06/10/2005 : la diffÈrence se pose pour le cas numvoisin < 0
        // Si au bord :
    else 
    {
        FluxEau_Out += FluxEau[i];
        return 0;
    }
    // Ajustement de l'eau courante
    // Il faut remettre FluxEau ‡ 0 au cas o˘ on repasserait par l‡ avec de l'eau
    // dÈversÈe par un lac (pas compter deux fois l'eau qui arrivait au 1er passage)
    //	EauCourante[i] += FluxEau[i];
    //	if (EauCourante[i] > EauCouranteMax) EauCouranteMax = EauCourante[i];
    //	FluxEau[i] = 0.;
    // On note le numÈro du voisin, qui va devenir un "fils-lac"
    //	if (numvoisin >= 0)
    //	{
    //		lenumvoisin[0] = numvoisin;
    //		return 1;
    //	}
    //	else return 0;
}
//----------------------------------------------
// Surcharge si on est sur une maille "exutoire"
//----------------------------------------------

int CFlux::DeverseSurMonVoisin_Exutoire(const int &i, CMaille* maille,
        int* lenumvoisin, GrilleInt* ptr, const int &numlac) 
{
    int numvoisin = maille->GetNumVoisinBas();

    //##### Modif le 07/10/2005 : finalement, un seul test cf Apero
    if (numvoisin >= 0) 
    {
        //##### Modif le 14/10/2005 : finalement, 1 seul test au lieu de 2
        //		if ( ((*ptr)[numvoisin] != -numlac) && (MonMaillage->A_PlusHautQue_B_Eau(i,numvoisin)) )
        if (MonMaillage->A_PlusHautQue_B_Eau(i, numvoisin)) 
        {
            //##### Modif le 10/05/2006 : NON, ce n'est pas fait dans le calcul du lac !
            //				--> seul FluxEau est assignÈ au volume dÈbordant du lac !
            EauCourante[i] += FluxEau[i];
            FluxEau[numvoisin] += FluxEau[i];
            FluxEau[i] = 0.;
            lenumvoisin[0] = numvoisin;
            return 1;
        } 
        else 
        {
            EauCourante[i] += FluxEau[i];
            FluxEau[i] = 0.;
            return 0;
        }
    }        //##### Modif le 14/09/2005 : on reprend comme dans APERO
        //##### Modif le 06/10/2005 : la diffÈrence se pose pour le cas numvoisin < 0
        // Si au bord :
    else 
    {
        FluxEau_Out += FluxEau[i];
        EauCourante[i] += FluxEau[i];
        FluxEau[i] = 0.;
        return 0;
    }
    // Pas d'ajustement de l'eau courante - dÈj‡ fait dans le calcul du lac
    // Il faut remettre FluxEau ‡ 0 au cas o˘ on repasserait par l‡ avec de l'eau
    // dÈversÈe par un lac (pas compter deux fois l'eau qui arrivait au 1er passage)
    //	FluxEau[i] = 0.;
    // On note le numÈro du voisin, qui va devenir un "fils-lac"
    //	if (numvoisin >= 0)
    //	{
    //		lenumvoisin[0] = numvoisin;
    //		return 1;
    //	}
    //	else return 0;
}
//--------------------------------------------------------
// Propage l'eau courante d'une maille sur ses voisins bas
//--------------------------------------------------------

void CFlux::DeverseSurMesVoisins(const int &i, CMaille* maille) 
{
    int numvoisin;


    //##### Modif le 07/10/2005 : mettre le test avant la boucle
    if (maille->GetPasBord()) 
    {
        for (int k = 0; k < maille->GetNbVoisinsBas(); k++)
            FluxEau[maille->GetNumVoisinBas(k)] += FluxEau[i] * maille->GetCoeff(k);
    } 
    else 
    {
        for (int k = 0; k < maille->GetNbVoisinsBas(); k++) 
        {
            numvoisin = maille->GetNumVoisinBas(k);
            if (numvoisin >= 0)
                FluxEau[numvoisin] += FluxEau[i] * maille->GetCoeff(k);
            else
                FluxEau_Out += FluxEau[i] * maille->GetCoeff(k);
        }
    }
    // Ajustement de l'eau courante
    // Il faut remettre FluxEau ‡ 0 au cas o˘ on repasserait par l‡ avec de l'eau
    // dÈversÈe par un lac (pas compter deux fois l'eau qui arrivait au 1er passage)
    EauCourante[i] += FluxEau[i];
    //	if (EauCourante[i] > EauCouranteMax) EauCouranteMax = EauCourante[i];
    FluxEau[i] = 0.;
    //##### Modif le 15/05/2006 : ajuste ZEau ‡ Z s'il y avait de l'eau au pas d'avant
    maille->Ajuste_ZEau_a_Z();

    return;
}

//--------------------------------------------------------
// Propage l'eau courante d'une maille sur ses voisins bas le long de la surface de l'eau // 30 juin 2008
//--------------------------------------------------------

void CFlux::DeverseSurMesVoisinsEau(const int &i, CMaille* maille) // modifs 2022
{
    int numvoisin;

    if (maille->GetPasBord())
    {
        for (int k = 0; k < maille->GetNbVoisinsBasEau(); k++)
            FluxEau[maille->GetNumVoisinBasEau(k)] += FluxEau[i] * maille->GetCoeffEau(k);
    }
    else
    {
        for (int k = 0; k < maille->GetNbVoisinsBasEau(); k++)
        {
            numvoisin = maille->GetNumVoisinBasEau(k);
            if (numvoisin >= 0)
                FluxEau[numvoisin] += FluxEau[i] * maille->GetCoeffEau(k);
            else
                FluxEau_Out += FluxEau[i] * maille->GetCoeffEau(k);
        }
    }
    // Ajustement de l'eau courante
    // Il faut remettre FluxEau ‡ 0 au cas o˘ on repasserait par l‡ avec de l'eau
    // dÈversÈe par un lac (pas compter deux fois l'eau qui arrivait au 1er passage)
    EauCourante[i] += FluxEau[i];
    FluxEau[i] = 0.;

    return;
}




//----------------------------------------------
// Surcharge si on est sur une maille "fils-lac"
//----------------------------------------------

int CFlux::DeverseSurMesVoisins_FilsLac(const int &i, CMaille* maille, int* lesnumsvoisins) 
{
    int numvoisin;
    int nbvoisins = 0;

    //##### Modif le 07/10/2005 : mettre le test avant la boucle
    if (maille->GetPasBord()) {
        for (int k = 0; k < maille->GetNbVoisinsBas(); k++) 
        {
            numvoisin = maille->GetNumVoisinBas(k);
            FluxEau[numvoisin] += FluxEau[i] * maille->GetCoeff(k);
            lesnumsvoisins[nbvoisins] = numvoisin;
            nbvoisins++;
        }
    } 
    else 
    {
        for (int k = 0; k < maille->GetNbVoisinsBas(); k++) 
        {
            numvoisin = maille->GetNumVoisinBas(k);
            if (numvoisin >= 0) 
            {
                FluxEau[numvoisin] += FluxEau[i] * maille->GetCoeff(k);
                lesnumsvoisins[nbvoisins] = numvoisin;
                nbvoisins++;
            } 
            else
                FluxEau_Out += FluxEau[i] * maille->GetCoeff(k);
        }
    }
    // Ajustement de l'eau courante
    // Il faut remettre FluxEau ‡ 0 au cas o˘ on repasserait par l‡ avec de l'eau
    // dÈversÈe par un lac (pas compter deux fois l'eau qui arrivait au 1er passage)
    EauCourante[i] += FluxEau[i];
    //	if (EauCourante[i] > EauCouranteMax) EauCouranteMax = EauCourante[i];
    FluxEau[i] = 0.;
    // On note le nombre et les numÈros des voisins, qui vont devenir des "fils-lac"
    return nbvoisins;
}

//----------------------------------------------
// Surcharge si on est sur une maille "exutoire"
//----------------------------------------------

int CFlux::DeverseSurMesVoisins_Exutoire(const int &i, CMaille* maille,
        int* lesnumsvoisins, GrilleInt* ptr, const int &numlac) 
{
    int numvoisin;
    int nbvoisins = 0;

    //##### Modif le 07/10/2005 : mettre le test avant la boucle
    if (maille->GetPasBord()) {
        for (int k = 0; k < maille->GetNbVoisinsBas(); k++) 
        {
            numvoisin = maille->GetNumVoisinBas(k);
            //##### Modif le 14/10/2005 : finalement, 1 seul test au lieu de 2
            //			if ( ((*ptr)[numvoisin] != -numlac) && (MonMaillage->A_PlusHautQue_B_Eau(i,numvoisin)) )
            if (MonMaillage->A_PlusHautQue_B_Eau(i, numvoisin)) 
            {
                FluxEau[numvoisin] += FluxEau[i] * maille->GetCoeff(k);
                lesnumsvoisins[nbvoisins] = numvoisin;
                nbvoisins++;
            }
        }
    } 
    else 
    {
        for (int k = 0; k < maille->GetNbVoisinsBas(); k++) 
        {
            numvoisin = maille->GetNumVoisinBas(k);
            if (numvoisin >= 0) 
            {
                //##### Modif le 14/10/2005 : finalement, 1 seul test au lieu de 2
                //				if ( ((*ptr)[numvoisin] != -numlac) && (MonMaillage->A_PlusHautQue_B_Eau(i,numvoisin)) )
                if (MonMaillage->A_PlusHautQue_B_Eau(i, numvoisin)) 
                {
                    FluxEau[numvoisin] += FluxEau[i] * maille->GetCoeff(k);
                    lesnumsvoisins[nbvoisins] = numvoisin;
                    nbvoisins++;
                }
            } 
            else
                FluxEau_Out += FluxEau[i] * maille->GetCoeff(k);
        }
    }
    // Pas d'ajustement de l'eau courante - dÈj‡ fait dans le calcul du lac
    //##### Modif le 10/05/2006 : NON, ce n'est pas fait dans le calcul du lac !
    //				--> seul FluxEau est assignÈ au volume dÈbordant du lac !
    EauCourante[i] += FluxEau[i];
    // Il faut remettre FluxEau ‡ 0 au cas o˘ on repasserait par l‡ avec de l'eau
    // dÈversÈe par un lac (pas compter deux fois l'eau qui arrivait au 1er passage)
    FluxEau[i] = 0.;
    // On note le nombre et les numÈros des voisins, qui vont devenir des "fils-lac"
    return nbvoisins;
}

//***********************************************************
//		PROPAGATION DE L'EAU COURANTE - cas d'une ILE
//***********************************************************

//-------------------------------------------------------
// Propage l'eau courante d'une maille sur son voisin bas
//-------------------------------------------------------

void CFlux::DeverseSurMonVoisin_ILE(const int &i, CMaille* maille) 
{
    FluxEau[maille->GetNumVoisinBas()] += FluxEau[i];
    EauCourante[i] += FluxEau[i];
    //	if (EauCourante[i] > EauCouranteMax) EauCouranteMax = EauCourante[i];
    FluxEau[i] = 0.;
    //##### Modif le 15/05/2006 : ajuste ZEau ‡ Z s'il y avait de l'eau au pas d'avant
    maille->Ajuste_ZEau_a_Z();
    return;
}
//----------------------------------------------
// Surcharge si on est sur une maille "fils-lac"
//----------------------------------------------

int CFlux::DeverseSurMonVoisin_FilsLac_ILE(const int &i, CMaille* maille, int* lenumvoisin) 
{
    int numvoisin = maille->GetNumVoisinBas();
    FluxEau[numvoisin] += FluxEau[i];
    EauCourante[i] += FluxEau[i];
    //	if (EauCourante[i] > EauCouranteMax) EauCouranteMax = EauCourante[i];
    FluxEau[i] = 0.;
    lenumvoisin[0] = numvoisin;
    return 1;
}
//----------------------------------------------
// Surcharge si on est sur une maille "exutoire"
//----------------------------------------------

int CFlux::DeverseSurMonVoisin_Exutoire_ILE(const int &i, CMaille* maille,
        int* lenumvoisin, GrilleInt* ptr, const int &numlac) 
{
    int numvoisin = maille->GetNumVoisinBas();
    //##### Modif le 14/10/2005 : finalement, 1 seul test au lieu de 2
    //	if ( ((*ptr)[numvoisin] != -numlac) && (MonMaillage->A_PlusHautQue_B_Eau(i,numvoisin)) )
    if (MonMaillage->A_PlusHautQue_B_Eau(i, numvoisin))
        FluxEau[numvoisin] += FluxEau[i];
    EauCourante[i] += FluxEau[i];
    FluxEau[i] = 0.;
    lenumvoisin[0] = numvoisin;
    return 1;
}
//--------------------------------------------------------
// Propage l'eau courante d'une maille sur ses voisins bas
//--------------------------------------------------------

void CFlux::DeverseSurMesVoisins_ILE(const int &i, CMaille* maille) 
{
    for (int k = 0; k < maille->GetNbVoisinsBas(); k++)
        FluxEau[maille->GetNumVoisinBas(k)] += FluxEau[i] * maille->GetCoeff(k);
    EauCourante[i] += FluxEau[i];
    //	if (EauCourante[i] > EauCouranteMax) EauCouranteMax = EauCourante[i];
    FluxEau[i] = 0.;
    //##### Modif le 15/05/2006 : ajuste ZEau ‡ Z s'il y avait de l'eau au pas d'avant
    maille->Ajuste_ZEau_a_Z();
    return;
}
//----------------------------------------------
// Surcharge si on est sur une maille "fils-lac"
//----------------------------------------------

int CFlux::DeverseSurMesVoisins_FilsLac_ILE(const int &i, CMaille* maille, int* lesnumsvoisins) 
{
    int numvoisin;
    int nbvoisins = 0;

    for (int k = 0; k < maille->GetNbVoisinsBas(); k++) 
    {
        numvoisin = maille->GetNumVoisinBas(k);
        FluxEau[numvoisin] += FluxEau[i] * maille->GetCoeff(k);
        lesnumsvoisins[nbvoisins] = numvoisin;
        nbvoisins++;
    }
    EauCourante[i] += FluxEau[i];
    //	if (EauCourante[i] > EauCouranteMax) EauCouranteMax = EauCourante[i];
    FluxEau[i] = 0.;
    return nbvoisins;
}
//----------------------------------------------
// Surcharge si on est sur une maille "exutoire"
//----------------------------------------------

int CFlux::DeverseSurMesVoisins_Exutoire_ILE(const int &i, CMaille* maille,
        int* lesnumsvoisins, GrilleInt* ptr, const int &numlac) 
{
    int numvoisin;
    int nbvoisins = 0;

    for (int k = 0; k < maille->GetNbVoisinsBas(); k++) 
    {
        numvoisin = maille->GetNumVoisinBas(k);
        //##### Modif le 14/10/2005 : finalement, 1 seul test au lieu de 2
        //		if ( ((*ptr)[numvoisin] != -numlac) && (MonMaillage->A_PlusHautQue_B_Eau(i,numvoisin)) )
        if (MonMaillage->A_PlusHautQue_B_Eau(i, numvoisin)) 
        {
            FluxEau[numvoisin] += FluxEau[i] * maille->GetCoeff(k);
            lesnumsvoisins[nbvoisins] = numvoisin;
            nbvoisins++;
        }
    }
    //##### Modif le 10/05/2006 : NON, ce n'est pas fait dans le calcul du lac !
    EauCourante[i] += FluxEau[i];
    FluxEau[i] = 0.;
    return nbvoisins;
}

//*****************************
//		FLUX DE DIFFUSION
//*****************************
//----------------------------------------------------------
// Actualisation des flux de diffusion RECUS par les voisins
//----------------------------------------------------------
//void CFlux::ActualiseLesFluxDiffRecus(CMaille* maille, Tvoisinage_diff* voisinage,
//									  const double fluxout[4]) Modif le 13 Mars 07 pour changer le type de Voisinage en Tvoisinage_bas et fluxout[4] en fluxout[8].
// le 13 mars 07 On change de strategie et on calcule la diffusion dans toutes les directions (cf la valeur de MonVoisinage dans  Ccalc_diffusion).

void CFlux::ActualiseLesFluxDiffRecus(CMaille* maille, Tvoisinage_bas* voisinage,
        const double fluxout[8]) 
{
    int i, numvoisin;
    ////// Cas d'une maille qui N'EST PAS au bord du maillage
    if (maille->GetPasBord()) 
    {
        for (i = 0; i < voisinage->nb; i++)
            VolumeDiff[voisinage->ind[i]] += fluxout[i];
    } 
    else 
    {
        for (i = 0; i < voisinage->nb; i++) 
        {
            numvoisin = voisinage->ind[i];
            if (numvoisin >= 0)
                VolumeDiff[numvoisin] += fluxout[i];
            else
                VolumeSed_Out += fluxout[i];
        }
    }
    return;
}


//----------------------------------------------------------
// Actualisation des flux de diffusion RECUS par les voisins - cas d'une ILE
//----------------------------------------------------------
//void CFlux::ActualiseLesFluxDiffRecus_ILE(CMaille* maille, Tvoisinage_diff* voisinage,
//										  const double fluxout[4]) Modif le 13 Mars 07 pour changer le type de Voisinage en Tvoisinage_bas et fluxout[4] en fluxout[8].
// le 13 mars 07 On change de strategie et on calcule la diffusion dans toutes les directions (cf la valeur de MonVoisinage dans  Ccalc_diffusion).

void CFlux::ActualiseLesFluxDiffRecus_ILE(CMaille* maille, Tvoisinage_bas* voisinage,
        const double fluxout[8]) 
{
    for (int i = 0; i < voisinage->nb; i++)
        VolumeDiff[voisinage->ind[i]] += fluxout[i];
    return;
}


// Actualisation simpliliee - janvier 2013

void CFlux::ActualiseLesFluxTransportFluvial(const int &nummaille, CMaille* maille, Tvoisinage_bas* voisinage,
        const double fluxout[8]) 
{
    int i, numvoisin;
    ////// Cas d'une maille qui N'EST PAS au bord du maillage
    if (maille->GetPasBord()) 
    {
        for (i = 0; i < voisinage->nb; i++) 
        {
            VolumeFluv[voisinage->ind[i]] += fluxout[i];
            VolumeFluvExporte[nummaille][voisinage->direction[i] - 1] += fluxout[i];
        }
    } 
    else 
    {
        for (i = 0; i < voisinage->nb; i++) 
        {
            numvoisin = voisinage->ind[i];
            VolumeFluvExporte[nummaille][voisinage->direction[i] - 1] += fluxout[i];
            if (numvoisin >= 0)
                VolumeFluv[numvoisin] += fluxout[i];
            else
                VolumeSed_Out += fluxout[i];
        }
    }

    return;
}



// Actualisation simplifiee - janvier 2013

void CFlux::ActualiseLesFluxTransportFluvial_ILE(const int &nummaille, CMaille* maille, Tvoisinage_bas* voisinage,
        const double fluxout[8]) 
{
    int i;
    for (i = 0; i < voisinage->nb; i++) 
    {
        VolumeFluv[voisinage->ind[i]] += fluxout[i];
        VolumeFluvExporte[nummaille][voisinage->direction[i] - 1] += fluxout[i];
    }
    return;
}



//==============================
//	   DIFFERENTIEL D'EROSION
//==============================
//======================================
//	    pour un FOND DE LAC
//======================================
//##### Ajout le 13/04/2006
// Etale de maniËre simplifiÈe le dÈpÙt en fond de lac : remplit ‡ hauteur d'eau, pour le fond de lac
// et tous ses voisins qui sont sous l'eau
//
// *** WARNING ***
//
// Ceci est un test : s'il y a trop de sÈdiments, pour l'instant on ne cherche pas ‡ les balancer
// au-del‡ des voisins immÈdiats du fond du lac, donc l'excÈdent se volatilise :o)
//

double CFlux::CorrigeActu_Lac(const int &nummaille, CMaille* maille) 
{
    double dz = Depot[nummaille] - Erosion[nummaille]; // janvier 2013

    double OldAlti = maille->GetAlti();
    double AltiLac = maille->GetAltiEau();
    // Calcule la hauteur d'eau au-dessus du fond du lac
    double HEau = AltiLac - OldAlti;
    double AltiDepot = 0.;

    // ATTENTION : on ne s'occupe pas ici de savoir si on est sur un plat
    if (dz > HEau) 
    {
        // Parcourt les voisins et note le volume disponible sous l'eau :
        int nbmailles_lac = 0;
        int nummailleslac[8];
        double zmailleslac[8];
        double SommeAlti = OldAlti;
        int j, numvoisin;

        for (j = 0; j < 8; j++) 
        {
            numvoisin = maille->GetNumVoisin(j);
            // Les mailles non au bord de la grille...
            if (numvoisin >= 0)
                // ...et sous l'eau :
                if (MonMaillage->GetMaille(numvoisin)->MailleLac()) 
                {
                    nummailleslac[nbmailles_lac] = numvoisin;
                    zmailleslac[nbmailles_lac] = MonMaillage->GetMaille(numvoisin)->GetAlti();
                    SommeAlti += zmailleslac[nbmailles_lac];
                    nbmailles_lac++;
                }
        }
        // Si on rÈpartit le volume ‡ dÈposer sur ces voisins :
        AltiDepot = (SommeAlti + dz) / ((double) (nbmailles_lac + 1));
        //*****
        //***** ATTENTION : pour l'instant, on se fiche que Áa dÈborde de l'eau...
        //***** (sinon on mettrait : if (AltiDepot <= AltiLac) ...)
        //*****
        // Re-parcourt les voisins pour les rÈ-actualiser :
        double dz_corrige;
        for (j = 0; j < nbmailles_lac; j++) 
        {
            numvoisin = nummailleslac[j];
            dz_corrige = AltiDepot - zmailleslac[j];
            DeltaErosion[numvoisin] += dz_corrige;
            StockSed[numvoisin] += dz_corrige;
            // Corrige aussi directement l'alti car on est dÈj‡ passÈs dessus
            MonMaillage->GetMaille(numvoisin)->SetLesAlti(AltiDepot);
        }

        dz_corrige = AltiDepot - OldAlti;
        double VolSedExces = dz - dz_corrige;
        DeltaErosion[nummaille] += dz_corrige;
        StockSed[nummaille] -= VolSedExces;

        return dz_corrige;
    } 
    else 
    {
        DeltaErosion[nummaille] += dz;
        return dz;
    }
}

//-----------------------------------------------
// idem, avec Ètalement dans tout le fond de lac
//-----------------------------------------------

double CFlux::CorrigeActu_LacEtendu(const int &nummaille, CMaille* maille) 
{
    double dz = Depot[nummaille] - Erosion[nummaille]; // janvier 2013

    double OldAlti = maille->GetAlti();
    double AltiLac = maille->GetAltiEau();
    // Calcule la hauteur d'eau au-dessus du fond du lac
    double HEau = AltiLac - OldAlti;

    // Initialisation de la grille DejaLac
    for (int i = 0; i < nbmailles; i++) DejaLac[i] = false;

    if ((HEau > 0) && (dz > 0)) 
    {
        // Au dÈpart, le lac fait une maille :
        MaillesLac.clear();
        ZMaillesLac.clear();
        MaillesLac.push_back(nummaille);
        ZMaillesLac.push_back(OldAlti);
        int nbmailles_lac = 1;
        DejaLac[nummaille] = true;


        // Le volume dispo au remplissage sera connu quand on aura une 2e
        double VolumeDispo = 0.;
        double SommeAlti = 0.;

        int j, k, jsuivant, numvoisin, jlac;
        double zsuivant, zvoisin;
        double AltiRempli, AltiDepot, dz_corrige, VolSedExces;
        CMaille* maillelac;

        zsuivant = OldAlti;

        while (VolumeDispo < dz) 
        {
            AltiRempli = zsuivant;
            jsuivant = -1;
            zsuivant = EPAISSEUR_INF;
            // On parcourt les mailles de lac dÈj‡ listÈes...
            for (k = 0; k < nbmailles_lac; k++) 
            {
                jlac = MaillesLac[k];
                maillelac = MonMaillage->GetMaille(jlac);
                // ...les voisins de ces mailles...
                for (j = 0; j < 8; j++) 
                {
                    numvoisin = maillelac->GetNumVoisin(j);
                    // Les mailles non au bord...
                    if (numvoisin >= 0) 
                    {
                        //						if (!DejaLac[numvoisin] && maillelac->MailleLac())
                        //##### Modif le 02/05/2006 : le test sur maillelac ne sert ‡ rien !

                        if (!DejaLac[numvoisin]) 
                        {
                            zvoisin = MonMaillage->GetMaille(numvoisin)->GetAlti();
                            // ...et sous l'eau, et le plus bas des voisins du lac :
                            if ((zvoisin < AltiLac) && (zvoisin < zsuivant)) 
                            {
                                jsuivant = numvoisin;
                                zsuivant = zvoisin;
                            }
                        }
                    }
                }
            }

            // Si on en a trouvÈ un (de point autour, qui ait de l'eau et soit le plus bas)
            if (jsuivant >= 0) 
            {
                // on l'ajoute dans la liste
                MaillesLac.push_back(jsuivant);
                ZMaillesLac.push_back(zsuivant);
                DejaLac[jsuivant] = true;
                // Attention : il faut comptabiliser l'espace sur la maille prÈcÈdemment ajoutÈe dans la liste du lac
                // (la toute derniËre sert de limite sup pour l'altitude, mais on ne remplit pas encore dessus)
                SommeAlti += ZMaillesLac[nbmailles_lac - 1];
                nbmailles_lac++;

                // on actualise le volume dispo au remplissage
                double dz_enplus = zsuivant - AltiRempli;
                VolumeDispo += ((double) nbmailles_lac) * dz_enplus;
            }                // Si on N'en a PAS trouvÈ : le lac est fermÈ, ou on touche ‡ l'exutoire :
                //				alors on pose tout l‡ o˘ on en est (y compris la derniËre trouvÈe)
            else 
            {
                SommeAlti += ZMaillesLac[nbmailles_lac - 1];
                AltiDepot = (SommeAlti + dz) / ((double) nbmailles_lac);
                // Le fond du lac :
                dz_corrige = AltiDepot - OldAlti;
                VolSedExces = dz - dz_corrige;
                DeltaErosion[nummaille] += dz_corrige;
                StockSed[nummaille] -= VolSedExces;
                // Re-parcourt les voisins pour les rÈ-actualiser :
                for (k = 1; k < nbmailles_lac; k++) 
                {
                    numvoisin = MaillesLac[k];
                    dz_corrige = AltiDepot - ZMaillesLac[k];
                    DeltaErosion[numvoisin] += dz_corrige;
                    StockSed[numvoisin] += dz_corrige;
                    // Corrige aussi directement l'alti car on est dÈj‡ passÈs dessus
                    MonMaillage->GetMaille(numvoisin)->SetLesAlti(AltiDepot);
                }
                return dz_corrige;
            }
        }

        // L‡, on a trouvÈ assez de mailles du lac pour dÈposer notre tas dz
        // on remplit en-dessous de la derniËre qu'on a trouvÈ, qui est trop haute :
        AltiDepot = (SommeAlti + dz) / ((double) (nbmailles_lac - 1));
        // Le fond du lac :
        dz_corrige = AltiDepot - OldAlti;
        VolSedExces = dz - dz_corrige;
        DeltaErosion[nummaille] += dz_corrige;
        StockSed[nummaille] -= VolSedExces;
        // Re-parcourt les voisins pour les rÈ-actualiser :
        double dz_local;
        for (k = 1; k < nbmailles_lac - 1; k++) 
        {
            numvoisin = MaillesLac[k];
            dz_local = AltiDepot - ZMaillesLac[k];
            DeltaErosion[numvoisin] += dz_local;
            StockSed[numvoisin] += dz_local;
            // Corrige aussi directement l'alti car on est dÈj‡ passÈs dessus
            MonMaillage->GetMaille(numvoisin)->SetLesAlti(AltiDepot);
        }

        return dz_corrige;
    } 
    else 
    {
        DeltaErosion[nummaille] += dz;
        return dz;
    }
}

//----------------------------------------------------------------------------
// idem, avec Ètalement dans tout le fond de lac et dÈbordement des sÈdiments
//----------------------------------------------------------------------------

double CFlux::CorrigeActu_LacDeborde(const int &nummaille, CMaille* maille) 
{
    double dz = Depot[nummaille] - Erosion[nummaille]; // janvier 2013

    double AltiFond = maille->GetAlti();

    // Calcule la hauteur d'eau au-dessus du fond du lac
    double HEau = maille->GetAltiEau() - AltiFond;

    // Initialisation de la grille DejaLac
    for (int i = 0; i < nbmailles; i++) DejaLac[i] = false;

    //##### Modif le 03/05/2006 : retirÈ la condition "if (dz > 0)"
    // Si c'est un lac "‡ eau", une vraie cuvette (pas un plat)
    if (HEau > 0) 
    {
        // Au dÈpart, le lac fait une maille : la maille de fond de lac
        MaillesLac.clear();
        ZMaillesLac.clear();
        MaillesLac.push_back(nummaille);
        ZMaillesLac.push_back(AltiFond);
        int nbmailles_lac = 1;
        DejaLac[nummaille] = true;

        // Le volume dispo au remplissage sera connu quand on aura une 2e :
        // on va remplir sur la 1e jusqu'au niveau de la 2e
        double VolumeDispo = 0.;
        double SommeAlti = 0.;

        int j, k, jsuivant, numvoisin, jlac;
        double zsuivant, zvoisin;
        double AltiRempli, AltiDepot, dz_corrige, VolSedExces;
        CMaille* maillelac;

        zsuivant = AltiFond;

        while (VolumeDispo < dz) 
        {
            // On remplit jusqu'au niveau de la derniËre maille du lac trouvÈe
            AltiRempli = zsuivant;

            // On va chercher le "suivant", le plus bas des voisins des mailles dÈj‡ trouvÈes
            jsuivant = -1;
            zsuivant = EPAISSEUR_INF;

            // On parcourt les mailles de lac dÈj‡ listÈes...
            for (k = 0; k < nbmailles_lac; k++) 
            {
                jlac = MaillesLac[k];
                maillelac = MonMaillage->GetMaille(jlac);
                // ...les 8 voisins de ces mailles...
                for (j = 0; j < 8; j++) 
                {
                    numvoisin = maillelac->GetNumVoisin(j);
                    // Les mailles non au bord...
                    if (numvoisin >= 0) 
                    {
                        // ...qui ne font pas dÈj‡ partie de la liste...
                        if (!DejaLac[numvoisin]) 
                        {
                            zvoisin = MonMaillage->GetMaille(numvoisin)->GetAlti();
                            // ...le plus bas des voisins du lac :
                            if (zvoisin < zsuivant) 
                            {
                                jsuivant = numvoisin;
                                zsuivant = zvoisin;
                            }
                        }
                    }
                }
            }

            //==========
            // Si on en a trouvÈ un (de point autour, qui soit le plus bas)
            //==========
            if (jsuivant >= 0) 
            {
                //----------
                // ...si en plus il est PLUS HAUT que le dernier point ajoutÈ avant :
                //----------
                if (zsuivant >= AltiRempli) 
                {
                    // on l'ajoute dans la liste
                    MaillesLac.push_back(jsuivant);
                    ZMaillesLac.push_back(zsuivant);
                    DejaLac[jsuivant] = true;
                    SommeAlti += AltiRempli;
                    nbmailles_lac++;

                    // on actualise le volume dispo au remplissage
                    double dz_enplus = zsuivant - AltiRempli;
                    VolumeDispo += ((double) nbmailles_lac) * dz_enplus;
                }                    //----------
                    // ... si au contraire il est MOINS HAUT : c'est un exutoire :
                    //----------
                else 
                {
                    //##### Modif le 02/05/2006 : on parcourt les mailles autour en cherchant le plus bas
                    //				des voisins ;
                    //				- si on trouve une maille + basse (que le dernier Z) : ok, exutoire
                    //				- sinon : on ajoute cette maille ‡ la liste et on re-cherche parmi les voisins...

                    // Sur la maille de fond du lac :
                    dz_corrige = AltiRempli - AltiFond;
                    VolSedExces = dz - dz_corrige;
                    DeltaErosion[nummaille] += dz_corrige;
                    StockSed[nummaille] -= VolSedExces;

                    // On remplit d'abord ‡ ras-bord :
                    double dz_local;

                    // Re-parcourt les voisins pour les rÈ-actualiser (PAS le fond) :
                    for (k = 1; k < nbmailles_lac; k++) 
                    {
                        jlac = MaillesLac[k];
                        dz_local = AltiRempli - ZMaillesLac[k];
                        DeltaErosion[jlac] += dz_local;
                        StockSed[jlac] += dz_local;
                        // Corrige aussi directement l'alti car on est dÈj‡ passÈs dessus
                        MonMaillage->GetMaille(jlac)->SetLesAlti(AltiRempli);
                    }

                    // Le volume en excËs pour le lac entier :
                    VolSedExces = dz - VolumeDispo;
                    // A partir du dernier point trouvÈ, on fait dÈgringoler pour retomber sur une maille
                    // sur laquelle on n'est pas encore passÈs :

                    int numredepart = jsuivant;
                    while (zsuivant >= AltiFond) 
                    {
                        DejaLac[numredepart] = true;
                        CMaille* MaMaille = MonMaillage->GetMaille(numredepart);
                        // On parcourt les voisins du dernier point :
                        jsuivant = -1;
                        zsuivant = EPAISSEUR_INF;
                        for (j = 0; j < 8; j++) 
                        {
                            numvoisin = MaMaille->GetNumVoisin(j);
                            if (numvoisin >= 0) 
                            {
                                zvoisin = MonMaillage->GetMaille(numvoisin)->GetAlti();
                                // Il ne faut pas repasser sur un point dÈj‡ testÈ !
                                if (!DejaLac[numvoisin]) 
                                {
                                    // On cherche le plus bas :
                                    if (zvoisin <= zsuivant) 
                                    {
                                        zsuivant = zvoisin;
                                        jsuivant = numvoisin;
                                    }
                                }
                            }
                        }
                        // Si on n'a rien trouvÈ, on se casse
                        if (jsuivant < 0) 
                        {
                            return dz_corrige;
                        }                            // Si on a trouvÈ, on le garde
                        else
                            numredepart = jsuivant;
                    }
                    // Quand on a trouvÈ LE point de redÈpart (sous le niveau du lac et descendant de l'exutoire) :
                    VolumeFluv[numredepart] += VolSedExces;

                    return dz_corrige;
                }
                // FIN du : if (jsuivant >= 0)
            }
                //==========
                // Sinon, c'est qu'on doit Ítre au bord de la grille (?) : euh... on s'en fout, donc :-s
                //==========
            else 
            {
                DeltaErosion[nummaille] += dz;
                return dz;
            }
            //==========

            // FIN du : while (VolumeDispo < dz)
        }

        // L‡, on a trouvÈ assez de mailles du lac pour dÈposer notre tas dz :
        // ---> cas : VolumeDispo >= dz
        // on remplit en-dessous de la derniËre qu'on a trouvÈ, qui est trop haute :
        AltiDepot = (SommeAlti + dz) / ((double) (nbmailles_lac - 1));

        // Le fond du lac :
        dz_corrige = AltiDepot - AltiFond;
        VolSedExces = dz - dz_corrige;
        DeltaErosion[nummaille] += dz_corrige;
        StockSed[nummaille] -= VolSedExces;
        // Re-parcourt les voisins pour les rÈ-actualiser :
        double dz_local;
        for (k = 1; k < nbmailles_lac - 1; k++) 
        {
            numvoisin = MaillesLac[k];
            dz_local = AltiDepot - ZMaillesLac[k];
            DeltaErosion[numvoisin] += dz_local;
            StockSed[numvoisin] += dz_local;
            // Corrige aussi directement l'alti car on est dÈj‡ passÈs dessus
            MonMaillage->GetMaille(numvoisin)->SetLesAlti(AltiDepot);
        }

        return dz_corrige;
    }        // Cas : Heau = 0 --> un PLAT
        //##### Modif le 03/05/2006 : les sÈdiments doivent aussi dÈborder !
    else 
    {
        int j, k, jsuivant, numvoisin, jlac;
        double zsuivant, zvoisin;
        CMaille* maillelac;

        MaillesLac.clear();
        ZMaillesLac.clear();
        int nbmailles_lac = 0;

        // Au dÈpart, le plat fait une maille :
        jsuivant = nummaille;
        zsuivant = AltiFond;

        while (zsuivant >= AltiFond) 
        {
            // On rajoute le dernier point trouvÈ au plat :
            MaillesLac.push_back(jsuivant);
            ZMaillesLac.push_back(zsuivant);
            DejaLac[jsuivant] = true;
            nbmailles_lac++;

            // Puis on cherche le suivant :
            jsuivant = -1;
            zsuivant = EPAISSEUR_INF;
            // On parcourt les mailles de lac dÈj‡ listÈes...
            for (k = 0; k < nbmailles_lac; k++) 
            {
                jlac = MaillesLac[k];
                maillelac = MonMaillage->GetMaille(jlac);
                // ...les voisins de ces mailles...
                for (j = 0; j < 8; j++) 
                {
                    numvoisin = maillelac->GetNumVoisin(j);
                    // Les mailles non au bord...
                    if (numvoisin >= 0) 
                    {
                        if (!DejaLac[numvoisin]) 
                        {
                            zvoisin = MonMaillage->GetMaille(numvoisin)->GetAlti();
                            // ...le plus bas des voisins du lac :
                            if (zvoisin < zsuivant) 
                            {
                                jsuivant = numvoisin;
                                zsuivant = zvoisin;
                            }
                        }
                    }
                }
            }

            if (jsuivant < 0) 
            {
                DeltaErosion[nummaille] += dz;
                return dz;
            }
            // FIN de la boucle : while (zsuivant >= AltiFond)
        }

        // Quand on a trouvÈ le point de redÈpart (sous le niveau du plat) :
        //		---> on lui refourgue tout.
        VolumeFluv[jsuivant] += dz;

        return 0.;
    }
}

//-----------------------
// Remplissage d'un LAC ‡ la mode "eau"
//-----------------------
//
// On part d'une maille du fond (0 voisin bas).
//

double CFlux::RemplissageSed_Lac(const int &nummaille, const int &numliste, CMaille* maille) 
{
    int isuivant, ivoisin, placevoisin;
    double zsuivant, zvoisin;
    int j;
    unsigned int k;
    bool TrouveExutoire = false;
    double AltiDepot = 0.;

    // Initialisation de la grille DejaLac
    for (int i = 0; i < nbmailles; i++) DejaLac[i] = false;

    double dz = VolumeDiff[nummaille] + VolumeFluv[nummaille];


    //++++++++++++++++++++++++++++++++++++++
    // Si c'est un lac ‡ EAU (pas un plat) :
    //++++++++++++++++++++++++++++++++++++++
    if (maille->MailleLac()) 
    {

        int nbmailles_lac = 0;
        CMaille* MaMaille;
        // L'altitude du lac est celle du niveau de l'eau
        // (notamment si on remplit ‡ nouveau un lac dÈj‡ existant)
        // L'altitude du fond du lac
        double AltiFond = maille->GetAlti();
        // dvol_lac est l'incrÈment du volume du lac au fur et ‡ mesure qu'on le remplit,
        // ie chaque fois qu'on ajoute un point (le "suivant" : le plus bas sur le bord)
        double dvol_lac = 0.;
        // Le volume de sÈdiments ‡ rÈpartir dans le lac (au dÈbut pour une seule cellule)
        double vol_exces = dz;
        double dz_local;

        // Vide les listes des mailles du pÈrimËtre et du milieu du lac
        BordDuLac.clear();

        //##### TEST le 16/05/2006 : on s'occupe pas de la distinction Bord/Milieu
        //		MilieuDuLac.clear();

        // L'altitude de dÈpart est celle de la premiËre maille
        zsuivant = AltiFond;
        // Le point de dÈpart est le premier de la liste
        isuivant = nummaille;

        // Boucle do car on fait la boucle au moins une fois (!)
        //		do
        //##### Modif le 24/05/2006 : Transformation de la boucle do en while : peut s'arrÍter tout de suite...
        // Donc il faut tester les voisins comme exutoire, dÈs le dÈbut :

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// TEST de l'EXUTOIRE, d'office :
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// Cas d'une maille qui N'EST PAS au bord du maillage
        if (maille->GetPasBord()) 
        {
            for (j = 0; j < 8; j++) {
                ivoisin = maille->GetNumVoisin(j);
                if (!DejaLac[ivoisin]) 
                {
                    zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                    if (zvoisin < zsuivant) 
                    {
                        TrouveExutoire = true;
                        isuivant = ivoisin;
                    }
                }
            }
        } 
        else 
        {
            for (j = 0; j < 8; j++) 
            {
                ivoisin = maille->GetNumVoisin(j);
                if (ivoisin >= 0) 
                {
                    if (!DejaLac[ivoisin]) 
                    {
                        zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                        if (zvoisin < zsuivant) 
                        {
                            TrouveExutoire = true;
                            isuivant = ivoisin;
                        }
                    }
                }
            }
        }

        while (!TrouveExutoire) 
        {
            vol_exces -= dvol_lac*nbmailles_lac;
            AltiDepot = zsuivant;

            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// On ajoute le point qu'on a trouvÈ ‡ la liste des points de bord du lac
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            DejaLac[isuivant] = true;
            BordDuLac.push_back(isuivant);
            nbmailles_lac++;

            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// On cherche le "suivant", le plus bas des points qui sont autour du lac
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            isuivant = -1;
            zsuivant = EPAISSEUR_INF;
            for (k = 0; k < BordDuLac.size(); k++) 
            {
                MaMaille = MonMaillage->GetMaille(BordDuLac[k]);

                ////// Cas d'une maille qui N'EST PAS au bord du maillage
                if (MaMaille->GetPasBord()) 
                {
                    // On parcourt les voisins de cette maille de bord de lac
                    for (j = 0; j < 8; j++) 
                    {
                        ivoisin = MaMaille->GetNumVoisin(j);
                        //##### Modif le 22/05/2006 : on a oubliÈ le niveau de base...
                        if (!MonMaillage->MailleTerre(ivoisin)) 
                        {
                            // C'est comme pour un lac fermÈ (il se vide sur ce voisin)
                            StockSed[nummaille] -= dz;
                            VolumeFluv[ivoisin] += dz;
                            StockSed[ivoisin] += dz;
                            return 0.;
                        }                            // On teste si la maille n'appartient pas dÈj‡ au lac
                        else if (!DejaLac[ivoisin]) 
                        {
                            zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                            if (zvoisin < zsuivant) 
                            {
                                zsuivant = zvoisin;
                                isuivant = ivoisin;
                            }
                        }
                    }
                }                    ////// Cas d'une maille qui EST au bord du maillage
                else 
                {
                    // On parcourt les voisins de cette maille de bord de lac
                    for (j = 0; j < 8; j++) 
                    {
                        ivoisin = MaMaille->GetNumVoisin(j);
                        // Comme on est au bord de la grille, on regarde si le voisin n'est pas dehors
                        if (ivoisin >= 0) 
                        {
                            //##### Modif le 22/05/2006 : on a oubliÈ le niveau de base...
                            if (!MonMaillage->MailleTerre(ivoisin)) 
                            {
                                // C'est comme pour un lac fermÈ (il se vide sur ce voisin)
                                StockSed[nummaille] -= dz;
                                VolumeFluv[ivoisin] += dz;
                                StockSed[ivoisin] += dz;
                                return 0.;
                            }                                // On teste si la maille n'appartient pas dÈj‡ au lac
                            else if (!DejaLac[ivoisin]) 
                            {
                                zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                                if (zvoisin < zsuivant) 
                                {
                                    zsuivant = zvoisin;
                                    isuivant = ivoisin;
                                }
                            }
                        }                            // Si le voisin est dehors, tout part dehors !!!
                        else 
                        {
                            // C'est comme pour un lac fermÈ (il se vide dehors)
                            StockSed[nummaille] -= dz;
                            return 0.;
                        }
                    }
                }

                // Si la maille n'a aucun voisin tq : DejaLac[ivoisin] = false
                // --> on n'est pas au bord mais au milieu
                /*				if (isuivant < 0)
                                                {
                                                        MilieuDuLac.push_back(BordDuLac[k]);
                                                        BordDuLac.erase(BordDuLac.begin()+k);
                                                        k--;
                                                }*/
            }

            dvol_lac = zsuivant - AltiDepot;

            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// SI : le point suivant est assez haut, le lac est fermÈ : on sort
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            if (dvol_lac * nbmailles_lac >= vol_exces) break;

            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// SINON : chaque fois qu'on a trouvÈ un point ‡ ajouter ‡ la liste,
            ////// on regarde si ce n'est pas un EXUTOIRE
            ////// (exutoire = maille du bord du lac qui a un voisin extÈrieur au lac
            ////// d'altitude strictement infÈrieure ‡ celle du lac)
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            MaMaille = MonMaillage->GetMaille(isuivant);
            //##### Modif le 24/05/2006 : il ne faut pas faire ce test, car une maille-lac peut avoir
            //	un voisin plus haut au dÈbut du pas de temps, devenu plus bas entre-temps... la voie vers la sortie
            //			if (!MaMaille->MailleLac())
            //			{
            ////// Cas d'une maille qui N'EST PAS au bord du maillage
            if (MaMaille->GetPasBord()) 
            {
                for (j = 0; j < 8; j++) 
                {
                    ivoisin = MaMaille->GetNumVoisin(j);
                    if (!DejaLac[ivoisin]) 
                    {
                        zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                        if (zvoisin < zsuivant) 
                        {
                            TrouveExutoire = true;
                            isuivant = ivoisin;
                            //##### Modif le 24/05/2006 : on se fiche du z de ce truc, ce qui compte c'est sa place dansla liste
                            //								zsuivant = zvoisin;
                            break;
                        }
                    }
                }
            } 
            else 
            {
                for (j = 0; j < 8; j++) 
                {
                    ivoisin = MaMaille->GetNumVoisin(j);
                    if (ivoisin >= 0) 
                    {
                        if (!DejaLac[ivoisin]) 
                        {
                            zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                            if (zvoisin < zsuivant) 
                            {
                                TrouveExutoire = true;
                                isuivant = ivoisin;
                                //##### Modif le 24/05/2006 : on se fiche du z de ce truc, ce qui compte c'est sa place dansla liste
                                //									zsuivant = zvoisin;
                                break;
                            }
                        }
                    }
                }
            }
            //			}

            // Fin de la boucle do...while : on teste si on n'a pas trouvÈ d'exutoire
            // (le lac dÈborde encore, on a dÈj‡ vÈrifiÈ qu'il n'Ètait pas fermÈ)
        }
        //##### Modif le 24/05/2006 : Transformation de la boucle do en while : peut s'arrÍter tout de suite...
        //		while (!TrouveExutoire);

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// Cas 1 : le lac est FERME
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // C'est le premier cas ‡ tester car on peut avoir un lac fermÈ
        // tout en ayant trouvÈ pour dernier point de la liste un 'exutoire' !
        // (mais qui ne sera pas dans le lac puisqu'il ne dÈborde plus)
        if (!TrouveExutoire) 
        {
            AltiDepot += vol_exces / nbmailles_lac;

            // Le fond du lac :
            StockSed[nummaille] -= dz;

            // On parcourt les mailles du lac qui sont au "bord" (mais dans l'eau)
            for (k = 0; k < BordDuLac.size(); k++) 
            {
                j = BordDuLac[k];
                // RecupËre l'alti et l'assigne en mÍme temps ‡ sa nouvelle valeur AltiDepot
                dz_local = AltiDepot - MonMaillage->GetMaille(j)->GetAlti_SetAltiDepot(AltiDepot);
                DeltaErosion[j] += dz_local;
                StockSed[j] += dz_local;
            }
            // Fin du bord du lac, on fait le milieu
            /*			for (k=0; k < MilieuDuLac.size(); k++)
                                    {
                                            j = MilieuDuLac[k];
                                    // RecupËre l'alti et l'assigne en mÍme temps ‡ sa nouvelle valeur AltiDepot
                                            dz_local = AltiDepot - MonMaillage->GetMaille(j)->GetAlti_SetAltiDepot(AltiDepot);
                                            DeltaErosion[j] += dz_local;
                                            StockSed[j] += dz_local ;
                                    }*/
            return (AltiDepot - AltiFond);
        }
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// Cas 2 : on a trouvÈ un exutoire (lac OUVERT qui dÈborde)
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        else 
        {
            vol_exces -= dvol_lac*nbmailles_lac;
            AltiDepot = zsuivant;

            // On parcourt les mailles du lac qui sont au bord
            for (k = 0; k < BordDuLac.size(); k++) 
            {
                j = BordDuLac[k];
                // RecupËre l'alti et l'assigne en mÍme temps ‡ sa nouvelle valeur AltiDepot
                dz_local = AltiDepot - MonMaillage->GetMaille(j)->GetAlti_SetAltiDepot(AltiDepot);
                DeltaErosion[j] += dz_local;
                StockSed[j] += dz_local;
            }
            // Fin du bord du lac, on fait le milieu
            /*			for (k=0; k < MilieuDuLac.size(); k++)
                                    {
                                            j = MilieuDuLac[k];
                                    // RecupËre l'alti et l'assigne en mÍme temps ‡ sa nouvelle valeur AltiDepot
                                            dz_local = AltiDepot - MonMaillage->GetMaille(j)->GetAlti_SetAltiDepot(AltiDepot);
                                            DeltaErosion[j] += dz_local;
                                            StockSed[j] += dz_local ;
                                    }*/

            // On s'occupe ensuite de l'EXUTOIRE :
            // il faut trouver un point plus bas que le fond de lac pour y dÈposer nos trucs

            int numredepart = isuivant;
            int placesuivant = MonMaillage->GetNumListeInv(numredepart);

            //			while (zsuivant >= AltiFond) // ou tester sur la place dans la liste des altitudes ?
            //##### Modif le 24/05/2006 : on teste sur la place dans la liste - qui est classÈe par altitudes...
            while (numliste > placesuivant) // ou tester sur la place dans la liste des altitudes ?
            {
                DejaLac[numredepart] = true;
                MaMaille = MonMaillage->GetMaille(numredepart);
                // On parcourt les voisins du dernier point :
                isuivant = -1;
                placesuivant = -1;
                ////// Cas d'une maille qui N'EST PAS au bord du maillage
                if (MaMaille->GetPasBord()) 
                {
                    // On parcourt les voisins de cette maille
                    for (j = 0; j < 8; j++) 
                    {
                        ivoisin = MaMaille->GetNumVoisin(j);
                        //##### Modif le 24/05/2006 : on a oubliÈ le niveau de base...
                        if (MonMaillage->MailleTerre(ivoisin)) 
                        {
                            // On teste si on n'est pas dÈj‡ passÈ
                            if (!DejaLac[ivoisin]) 
                            {
                                placevoisin = MonMaillage->GetNumListeInv(ivoisin);
                                //##### Modif le 24/05/2006 : il est important que l'exutoire se situe plus loin
                                //		dans la liste triÈe des mailles !
                                if (placevoisin > placesuivant) 
                                {
                                    placesuivant = placevoisin;
                                    isuivant = ivoisin;
                                }
                            }
                        } 
                        else 
                        {
                            // C'est comme pour un lac fermÈ (il se vide sur ce voisin)
                            StockSed[nummaille] -= dz;
                            VolumeFluv[ivoisin] += dz;
                            StockSed[ivoisin] += dz;
                            return 0.;
                        }
                    }
                }                    ////// Cas d'une maille qui EST au bord du maillage
                else 
                {
                    // On parcourt les voisins de cette maille
                    for (j = 0; j < 8; j++) 
                    {
                        ivoisin = MaMaille->GetNumVoisin(j);
                        // Comme on est au bord de la grille, on regarde si le voisin n'est pas dehors
                        if (ivoisin >= 0) 
                        {
                            //##### Modif le 24/05/2006 : on a oubliÈ le niveau de base...
                            if (MonMaillage->MailleTerre(ivoisin)) 
                            {
                                // On teste si on n'est pas dÈj‡ passÈ
                                if (!DejaLac[ivoisin]) 
                                {
                                    placevoisin = MonMaillage->GetNumListeInv(ivoisin);
                                    //##### Modif le 24/05/2006 : il est important que l'exutoire se situe plus loin
                                    //		dans la liste triÈe des mailles ! le reste on s'en fout
                                    if (placevoisin > placesuivant) 
                                    {
                                        placesuivant = placevoisin;
                                        isuivant = ivoisin;
                                    }
                                }
                            } 
                            else 
                            {
                                // C'est comme pour un lac fermÈ (il se vide sur ce voisin)
                                StockSed[nummaille] -= dz;
                                VolumeFluv[ivoisin] += dz;
                                StockSed[ivoisin] += dz;
                                return 0.;
                            }
                        }                            // Si le voisin est dehors, tout part dehors !!!
                        else 
                        {
                            // C'est comme pour un lac fermÈ (il se vide dehors)
                            StockSed[nummaille] -= dz;
                            return 0.;
                        }
                    }
                }

                if (isuivant < 0) 
                {
                    return (AltiDepot - AltiFond);
                }
                numredepart = isuivant;
            }

            // Quand on a trouvÈ LE point de redÈpart (sous le niveau du lac et descendant de l'exutoire) :
            VolumeFluv[numredepart] += vol_exces;

            return (AltiDepot - AltiFond);
        }
    }
        //++++++++++++++++++++
        // Si c'est un PLAT : on cherche seulement l'exutoire
        //++++++++++++++++++++
    else 
    {
        int nbmailles_plat = 0;
        CMaille* MaMaille;
        // L'altitude du fond du plat
        double AltiFond = maille->GetAlti();

        // Vide les listes des mailles du pÈrimËtre et du milieu du lac
        BordDuLac.clear();
        //		MilieuDuLac.clear();
        // Le point de dÈpart est le premier de la liste
        isuivant = nummaille;
        zsuivant = AltiFond;
        // Boucle do car on fait la boucle au moins une fois (!)
        //##### Modif le 10/05/2006 : si la 2e maille a dÈj‡ ÈtÈ ÈrodÈe (vu que c'est un plat, elles ont pu
        //	Ítre traitÈes dans n'importe quel ordre), il faut autoriser ‡ sortir tout de suite. NON !
        //	NON : il faut autoriser ‡ sortir dÈs la premiËre maille non encore traitÈe ! en passant par les plus basses...
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// TEST de l'EXUTOIRE, d'office :
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// Cas d'une maille qui N'EST PAS au bord du maillage
        if (maille->GetPasBord()) 
        {
            for (j = 0; j < 8; j++) 
            {
                ivoisin = maille->GetNumVoisin(j);
                if (!DejaLac[ivoisin]) 
                {
                    zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                    if (zvoisin < zsuivant) 
                    {
                        TrouveExutoire = true;
                        isuivant = ivoisin;
                    }
                }
            }
        }
        else 
        {
            for (j = 0; j < 8; j++) 
            {
                ivoisin = maille->GetNumVoisin(j);
                if (ivoisin >= 0) 
                {
                    if (!DejaLac[ivoisin]) 
                    {
                        zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                        if (zvoisin < zsuivant) 
                        {
                            TrouveExutoire = true;
                            isuivant = ivoisin;
                        }
                    }
                }
            }
        }
        while (!TrouveExutoire) 
        {
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// On ajoute le point qu'on a trouvÈ ‡ la liste des points de bord du lac
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            DejaLac[isuivant] = true;
            BordDuLac.push_back(isuivant);
            nbmailles_plat++;

            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// On cherche le "suivant", le plus bas des points qui sont autour du lac
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            isuivant = -1;
            zsuivant = EPAISSEUR_INF;
            for (k = 0; k < BordDuLac.size(); k++) 
            {
                MaMaille = MonMaillage->GetMaille(BordDuLac[k]);

                ////// Cas d'une maille qui N'EST PAS au bord du maillage
                if (MaMaille->GetPasBord()) 
                {
                    // On parcourt les voisins de cette maille de bord de lac
                    for (j = 0; j < 8; j++) 
                    {
                        ivoisin = MaMaille->GetNumVoisin(j);
                        // On regarde si ce point n'appartient pas dÈj‡ au plat...
                        //##### Modif le 24/05/2006 : on a oubliÈ le niveau de base...
                        if (!MonMaillage->MailleTerre(ivoisin)) 
                        {
                            // C'est comme pour un lac fermÈ (il se vide sur ce voisin)
                            StockSed[nummaille] -= dz;
                            VolumeFluv[ivoisin] += dz;
                            StockSed[ivoisin] += dz;
                            return 0.;
                        } 
                        else if (!DejaLac[ivoisin]) 
                        {
                            // On est sur un plat : on teste si le point a dÈj‡ ÈtÈ parcouru
                            // dans le bilan sÈdimentaire de la liste des mailles par altitudes :
                            //##### Modif le 10/05/2006 : il faut regarder tous les voisins, on ne sait pas comment ils sont triÈs ?
                            //							placevoisin = MonMaillage->GetNumListeInv(ivoisin);
                            //							if ( (placevoisin > 0) && (placevoisin < numliste) )
                            //							{
                            zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                            if (zvoisin < zsuivant) 
                            {
                                zsuivant = zvoisin;
                                isuivant = ivoisin;
                            }
                            //							}
                        }
                    }
                }                    ////// Cas d'une maille qui EST au bord du maillage
                else 
                {
                    // On parcourt les voisins de cette maille de bord de lac
                    for (j = 0; j < 8; j++) 
                    {
                        ivoisin = MaMaille->GetNumVoisin(j);
                        // Comme on est au bord de la grille, on regarde si le voisin n'est pas dehors
                        if (ivoisin >= 0) 
                        {
                            // On regarde si ce point n'appartient pas dÈj‡ au plat...
                            //##### Modif le 24/05/2006 : on a oubliÈ le niveau de base...
                            if (!MonMaillage->MailleTerre(ivoisin)) 
                            {
                                // C'est comme pour un lac fermÈ (il se vide sur ce voisin)
                                StockSed[nummaille] -= dz;
                                VolumeFluv[ivoisin] += dz;
                                StockSed[ivoisin] += dz;
                                return 0.;
                            } 
                            else if (!DejaLac[ivoisin]) 
                            {
                                // On teste sa place dans la liste triÈe par altitudes
                                //##### Modif le 09/05/2006 : il faut regarder tous les voisins, on ne sait pas comment ils sont triÈs ?
                                //								placevoisin = MonMaillage->GetNumListeInv(ivoisin);
                                //								if ( (placevoisin > 0) && (placevoisin < numliste) )
                                //								{
                                zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                                if (zvoisin < zsuivant) 
                                {
                                    zsuivant = zvoisin;
                                    isuivant = ivoisin;
                                }
                                //								}
                            }
                        }                            // Si le voisin est dehors, tout part dehors !!!
                        else 
                        {
                            // C'est comme pour un lac fermÈ (il se vide dehors)
                            StockSed[nummaille] -= dz;
                            return 0.;
                        }
                    }
                }

                // Si la maille n'a aucun voisin tq : DejaLac[ivoisin] = false
                // --> on n'est pas au bord mais au milieu
                /*				if (isuivant < 0)
                                                {
                                                        MilieuDuLac.push_back(BordDuLac[k]);
                                                        BordDuLac.erase(BordDuLac.begin()+k);
                                                        k--;
                                                }*/
            }

            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// chaque fois qu'on a trouvÈ un point ‡ ajouter ‡ la liste,
            ////// on regarde si ce n'est pas un EXUTOIRE
            ////// (exutoire = maille du bord du lac qui a un voisin extÈrieur au lac
            ////// d'altitude strictement infÈrieure ‡ celle du lac)
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            /*			MaMaille = MonMaillage->GetMaille(isuivant);
                                    if (!MaMaille->MailleLac())
                                    {
                                    ////// La maille n'est forcÈment PAS AU BORD, sinon on serait dÈj‡ sorti avant
                                            for (j=0; j < 8; j++)
                                            {
                                                    ivoisin = MaMaille->GetNumVoisin(j);
                                                    if (!DejaLac[ivoisin])
                                                    {
                                                            if (MonMaillage->GetMaille(ivoisin)->GetAlti() < zsuivant)
                                                            {
                                                                    TrouveExutoire = true;
                                                                    isuivant = ivoisin;
                                                                    break;
                                                            }
                                                    }
                                            }
                                    }*/

            //##### Modif le 10/05/2006 : NON, un exutoire est ici une maille sur laquelle on n'est pas encore
            //			passÈ ! (dans le bilan sÈdimentaire)

            MaMaille = MonMaillage->GetMaille(isuivant);
            ////// La maille n'est forcÈment PAS AU BORD, sinon on serait dÈj‡ sorti avant
            //  25 avril 2007 : en fait SI, il semble que ivoisin puisse etre negatif et le code plante ici.
            // pourquoi, mystere...
            for (j = 0; j < 8; j++) 
            {
                ivoisin = MaMaille->GetNumVoisin(j);
                if (ivoisin >= 0) // ajout le 25 avril 2007
                {
                    if (!DejaLac[ivoisin])
                    {

                        zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                        //					if (MonMaillage->GetNumListeInv(ivoisin) > numliste)
                        if (zvoisin < zsuivant) 
                        {
                            TrouveExutoire = true;
                            isuivant = ivoisin;
                            break;
                        }
                    }
                }                    // Si le voisin est dehors, tout part dehors !!! ajout le 25 avril 2007
                else // ajout le 25 avril 2007
                {
                    // C'est comme pour un lac fermÈ (il se vide dehors)
                    StockSed[nummaille] -= dz; //ajout le 25 avril 2007
                    return 0.; //ajout le 25 avril 2007
                }
            }

            // Fin de la boucle while...do : on teste si on n'a pas trouvÈ d'exutoire
            // (le lac dÈborde encore, on a dÈj‡ vÈrifiÈ qu'il n'Ètait pas fermÈ)
        }


        //;;;;;;;;;;;;;;;;;;
        ////// L'exutoire : le premier point plus bas que AltiFond (altitude du plat)
        //;;;;;;;;;;;;;;;;;;

        int numredepart = isuivant;
        int placesuivant = MonMaillage->GetNumListeInv(numredepart);
        //			while (zsuivant >= AltiFond) // ou tester sur la place dans la liste des altitudes ?
        //##### Modif le 24/05/2006 : on teste sur la place dans la liste - qui est classÈe par altitudes...
        while (numliste > placesuivant) // ou tester sur la place dans la liste des altitudes ?
        {
            DejaLac[numredepart] = true;
            MaMaille = MonMaillage->GetMaille(numredepart);
            // On parcourt les voisins du dernier point :
            isuivant = -1;
            //				zsuivant = EPAISSEUR_INF;
            placesuivant = -1;
            ////// Cas d'une maille qui N'EST PAS au bord du maillage
            if (MaMaille->GetPasBord()) 
            {
                // On parcourt les voisins de cette maille
                for (j = 0; j < 8; j++) 
                {
                    ivoisin = MaMaille->GetNumVoisin(j);
                    //##### Modif le 24/05/2006 : on a oubliÈ le niveau de base...
                    if (MonMaillage->MailleTerre(ivoisin)) 
                    {
                        // On teste si on n'est pas dÈj‡ passÈ
                        if (!DejaLac[ivoisin]) 
                        {
                            placevoisin = MonMaillage->GetNumListeInv(ivoisin);
                            //##### Modif le 24/05/2006 : il est important que l'exutoire se situe plus loin
                            //		dans la liste triÈe des mailles !
                            if (placevoisin > placesuivant) 
                            {
                                placesuivant = placevoisin;
                                isuivant = ivoisin;
                            }
                        }
                    } 
                    else 
                    {
                        // C'est comme pour un lac fermÈ (il se vide sur ce voisin)
                        StockSed[nummaille] -= dz;
                        VolumeFluv[ivoisin] += dz;
                        StockSed[ivoisin] += dz;
                        return 0.;
                    }
                }
            }                ////// Cas d'une maille qui EST au bord du maillage
            else 
            {
                // On parcourt les voisins de cette maille
                for (j = 0; j < 8; j++) 
                {
                    ivoisin = MaMaille->GetNumVoisin(j);
                    // Comme on est au bord de la grille, on regarde si le voisin n'est pas dehors
                    if (ivoisin >= 0) 
                    {
                        //##### Modif le 24/05/2006 : on a oubliÈ le niveau de base...
                        if (MonMaillage->MailleTerre(ivoisin)) 
                        {
                            // On teste si on n'est pas dÈj‡ passÈ
                            if (!DejaLac[ivoisin]) 
                            {
                                placevoisin = MonMaillage->GetNumListeInv(ivoisin);
                                //##### Modif le 24/05/2006 : il est important que l'exutoire se situe plus loin
                                //		dans la liste triÈe des mailles ! le reste on s'en fout
                                if (placevoisin > placesuivant) 
                                {
                                    placesuivant = placevoisin;
                                    isuivant = ivoisin;
                                }
                                /*									zvoisin = MonMaillage->GetMaille(ivoisin)->GetAlti();
                                                                                                if (zvoisin < zsuivant)
                                                                                                {
                                                                                                        zsuivant = zvoisin;
                                                                                                        isuivant = ivoisin;
                                                                                                }*/
                            }
                        } 
                        else 
                        {
                            // C'est comme pour un lac fermÈ (il se vide sur ce voisin)
                            StockSed[nummaille] -= dz;
                            VolumeFluv[ivoisin] += dz;
                            StockSed[ivoisin] += dz;
                            return 0.;
                        }
                    }                        // Si le voisin est dehors, tout part dehors !!!
                    else 
                    {
                        // C'est comme pour un lac fermÈ (il se vide dehors)
                        StockSed[nummaille] -= dz;
                        return 0.;
                    }
                }
            }
            if (isuivant < 0) 
            {
                return 0.;
            }
            numredepart = isuivant;
        }

        VolumeFluv[numredepart] += dz;
        StockSed[nummaille] -= dz;
        return 0.;
    }

}

//-------------------------------------------------------------------------------------
// remplissage, sur une maille non fond (= qui a des voisins), jusqu'au niveau de l'eau
//-------------------------------------------------------------------------------------

double CFlux::CorrigeActu_LacPasFond(const int &nummaille, CMaille* maille) 
{
    double dz = Depot[nummaille] - Erosion[nummaille]; // janvier 2013

    double OldAlti = maille->GetAlti();
    // Calcule la hauteur d'eau au-dessus du fond du lac
    double HEau = maille->GetAltiEau() - OldAlti;

    if (HEau < dz) 
    {
        // On remplit jusqu'au niveau de l'eau :
        DeltaErosion[nummaille] += HEau;
        double VolSedExces = dz - HEau;
        StockSed[nummaille] -= VolSedExces;

        // On redispatche les sÈdiments en trop vers les suivants bas
        Tvoisinage_bas* MonVoisinage = maille->GetVoisinageBasEau();
        int i, numvoisin;
        //cout << "CorrigeActu_LacPasFond Heau<dz  "<<nummaille<< " nb " <<MonVoisinage->nb<<  "  maille->GetAlti() " <<maille->GetAlti()/400<<  "  maille->GetAltiEau() " <<maille->GetAltiEau()/400<< endl;
        if (maille->GetPasBord()) 
        {
            for (i = 0; i < MonVoisinage->nb; i++)
                VolumeFluv[MonVoisinage->ind[i]] += VolSedExces * MonVoisinage->coeff[i];
        } 
        else 
        {
            for (i = 0; i < MonVoisinage->nb; i++) 
            {
                numvoisin = MonVoisinage->ind[i];
                if (numvoisin >= 0)
                    VolumeFluv[numvoisin] += VolSedExces * MonVoisinage->coeff[i];
            }
        }
        return HEau;
    } else {
        //cout << "CorrigeActu_LacPasFond Heau<dz  "<<nummaille <<  " dz "<< dz <<endl;
        DeltaErosion[nummaille] += dz;
        return dz;
    }
}

//===================
//	   TECTONIQUE
//===================
//--------------------------
// Initialisation des dÈcros
//--------------------------

void CFlux::InitialiseDecro(ParamTecto param_tecto, double pas) 
{
    jdecro = param_tecto.LigneDecro;
    dt_decro = pas / param_tecto.VitesseDecro;
    // On initialise le compteur de temps depuis le dernier dÈcrochement,
    // comme si on Ètait pile entre deux dÈcrochements
    temps_decro = dt_decro / 2.;
    return;
}

//-----------------------
// SoulËvement tectonique
//-----------------------

void CFlux::Souleve(const double &dt) 
{
    MonMaillage->Souleve();
    return;
}

//-------------
// Decrochement
//-------------

void CFlux::Decroche(const double &dt) 
{
    double tmpdb[2];
    int tmpint[2];
    int j, k;

    temps_decro += dt;
    if (temps_decro > dt_decro) 
    {
        temps_decro -= dt_decro;
        for (j = 1; j < jdecro; j++) 
        {
            k = j * nbcol - 1;
            tmpdb[0] = StockSed[k];
            //##### Modif. le 23/01/2006 : il faut aussi dÈcaler la pile strati ! donc :
            tmpdb[1] = VolumeCouche[k];
            tmpint[0] = NumCouche[k];
            tmpint[1] = NumLitho[k];
            //			for (i=nbcol; i > 1; i--)
            for (k = j * nbcol - 1; k > (j - 1) * nbcol; k--)
            {
                //				k = (j-1)*nbcol+i-1;
                //				k--;
                StockSed[k] = StockSed[k - 1];
                VolumeCouche[k] = VolumeCouche[k - 1];
                NumCouche[k] = NumCouche[k - 1];
                NumLitho[k] = NumLitho[k - 1];
            }
            StockSed[k] = tmpdb[0];
            VolumeCouche[k] = tmpdb[1];
            NumCouche[k] = tmpint[0];
            NumLitho[k] = tmpint[1];
        }
        MonMaillage->Decroche();
    }
    return;
}

//---------------------------------------
// SoulËvement tectonique et dÈcrochement
//---------------------------------------

void CFlux::SouleveEtDecroche(const double &dt) 
{
    MonMaillage->Souleve();
    Decroche(dt);
    return;
}

void CFlux::AfficheCouche() 
{
    int k = -1;
    AfficheMessage("Numeros des couches");
    for (int i = 0; i < MonMaillage->GetNbLig(); i++) 
    {
        for (int j = 0; j < MonMaillage->GetNbCol(); j++) 
        {
            k++;
            cout << NumCouche[k] << "  ";
        }
        cout << endl;
    }
    return;
}

//---------------------------------------
// Calcul hauteur d'eau
//---------------------------------------

double CFlux::CalculHauteurEau(const int &i, CMaille* maille, const double &dt) // 30 juin 2008 // modifs 2022
{
    // Heau = (Q**3/5  /  Smoy**1/2) * n * pas2 : c'est donc un volume.
    // Si on est dans un trou topographique, la hautueur d'eau est le volume d'eau
    double qsurracinepente = 0.;
    double hauteureau = FluxEau[i] * dt;
    if ((maille->GetVoisinageBas())->nb > 0 && (maille->GetVoisinageBas())->pentemoyenne > 0.0001) // pour les zones presque plates la hauteur d'eau est le flux x dt
    {
        qsurracinepente = FluxEau[i] * incisSed->Nmanning / sqrt((maille->GetVoisinageBas())->pentemoyenne) / pas;
        hauteureau = pow(qsurracinepente, 0.6)  * pas2;
    }
  
    return hauteureau;
}

double CFlux::CalculHauteurEauSimple(const int &i, CMaille* maille, const double &dt) // 30 juin 2008
{
    // Heau = (Q*dt) : c'est donc un volume.
    double hauteureau = FluxEau[i] * dt;
    return hauteureau;
}

double CFlux::CalculHauteurEauCourante(const int &i, CMaille* maille, const double &dt) // 30 juin 2008 // modifs 2022
{
    // Heau = (Q* n/pas/  Smoy**1/2)**3/5  * pas2 : c'est donc un volume.
    // modif 2019 (GetVoisinageBasEau())->pentemax au lieu de voisinage terrestre et pente moyenne. On suit la surface de l'eau)
    // modifs 2022
    double qsurracinepente;
    double hauteureau = 0.;

    if ((maille->GetVoisinageBasEau())->nb > 0 && EauCourante[i] > 0.)
    {
        qsurracinepente = EauCourante[i] * incisSed->Nmanning / sqrt((maille->GetVoisinageBasEau())->pentemax) / pas;
        hauteureau = pow(qsurracinepente, 0.6)  * pas2;
    }
    //if (i>49945 && i<49953){cout << i<<" H  "<<hauteureau/400 <<" S "<<(maille->GetVoisinageBasEau())->pentemax <<" E " <<EauCourante[i]<< endl;}
    //cout << i<<" H CalculHauteurEauCourante "<<hauteureau/400 <<" S "<<(maille->GetVoisinageBasEau())->pentemax <<" E " <<EauCourante[i]/(365*24*3600)<< endl;
    return hauteureau;
}

double CFlux::GetRegolitheCree(const int &k) //ajoute le 07/02/11
{
    return VolumeRegolitheCree[k];
}


