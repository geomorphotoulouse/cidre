#include "Utilitaires.h"
#include <sstream> // pour les manipulations d'I/O sur string
#include <iostream> // pour les manipulations d'I/O sur string

//========================================================================
// LitLigne :
// --------
// Fonctions pour aller lire toutes sortes de trucs dans une ligne
//
// Par d�faut il lit la valeur ou le mot qu'il y a, si on met
// un entier n (>1 !) comme 2e argument avant le 3e, il lit un tableau[n]
//------------------------------------------------------------------------

void LitLigne(const string ligne, string& toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> toto;
    return;
}

void LitLigne(const string ligne, int& itoto, string& toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto;
    LigneEntree >> toto;
    return;
}

void LitLigne(const string ligne, int& itoto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto;
    return;
}

void LitLigne(const string ligne, int nbtoto, int* toto) 
{
    istringstream LigneEntree(ligne);
    for (int i = 0; i < nbtoto; i++) LigneEntree >> toto[i];
    return;
}

void LitLigne(const string ligne, int nbtoto, UPLIFT* toto) 
{
    istringstream LigneEntree(ligne);
    int totoint;
    for (int i = 0; i < nbtoto; i++) 
    {
        LigneEntree >> totoint;
        if (totoint == 1)
            toto[i] = creneau;
        else if (totoint == 2)
            toto[i] = gaussienne;
        else if (totoint == 3)
            toto[i] = croissant;
        else
            toto[i] = decroissant;
    }
    return;
}

void LitLigne(const string ligne, double &toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> toto;
    return;
}

void LitLigne(const string ligne, double& toto1, double& toto2) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> toto1 >> toto2;
    return;
}

void LitLigne(const string ligne, int nbtoto, double* toto) 
{
    istringstream LigneEntree(ligne);
    for (int i = 0; i < nbtoto; i++) LigneEntree >> toto[i];
    return;
}

void LitLigne(const string ligne, int nbtoto, string* toto1, double* toto2) //Ajout Pierre
{
    istringstream LigneEntree(ligne);
    for (int i = 0; i < nbtoto; i++) LigneEntree >> toto1[i];
    for (int i = nbtoto; i < 2 * nbtoto; i++) LigneEntree >> toto2[i - nbtoto];
    return;
}

void LitLigne(const string ligne, int& itoto, double& toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto;
    LigneEntree >> toto;
    return;
}

//--------------------------------------------------------------
// MODIFICATION LORS DE L'INTRODUCTION DU CLIMAT STOCHASTIQUE
//--------------------------------------------------------------

void LitLigne(const string ligne, MODE& toto) 
{
    istringstream LigneEntree(ligne);
    string titi;
    LigneEntree >> titi;
    transform(titi.begin(), titi.end(), titi.begin(), static_cast<int (*)(int)> (tolower));
    if (titi.substr(0, 2) == "fi")
        toto = file;
    else if (titi.substr(0, 2) == "va")
        toto = value;
    else if (titi.substr(0, 2) == "st")
        toto = stochastique;
    else
        toto = nothing;
    return;
}
//--------------------------------------------------------------
// FIN MODIFICATION
//--------------------------------------------------------------

//--------------------------------------------------------------
// RAJOUT LORS DE L'INTRODUCTION DU CLIMAT STOCHASTIQUE
//--------------------------------------------------------------

void LitLigne(const string ligne, PROBA& toto) 
{
    istringstream LigneEntree(ligne);
    string titi;
    LigneEntree >> titi;
    transform(titi.begin(), titi.end(), titi.begin(), static_cast<int (*)(int)> (tolower));
    if (titi.substr(0, 2) == "po" || titi.substr(0, 2) == "PO")
        toto = powerlaw;
    else if (titi.substr(0, 2) == "he" || titi.substr(0, 2) == "HE")
        toto = heavytail;
    else
        toto = pasdeloi;
    return;
}
//--------------------------------------------------------------
// FIN DU RAJOUT
//--------------------------------------------------------------

void LitLigne(const string ligne, bool& toto) 
{
    istringstream LigneEntree(ligne);
    string titi;
    LigneEntree >> titi;
    toto = false;
    if (titi == "oui" || titi == "OUI" || titi == "Oui" || titi == "O" || titi == "o"
        || titi == "yes" || titi == "YES" || titi == "Yes" || titi == "Y" || titi == "y")
        toto = true;
    return;
}

void LitLigne(const string ligne, bool& toto, double& ztoto) 
{
    istringstream LigneEntree(ligne);
    string titi;
    LigneEntree >> titi;
    toto = false;
    if (titi == "oui" || titi == "OUI" || titi == "Oui" || titi == "O" || titi == "o"
        || titi == "yes" || titi == "YES" || titi == "Yes" || titi == "Y" || titi == "y") 
    {
        toto = true;
        LigneEntree >> ztoto;
    }
    return;
}

void LitLigne(const string ligne, bool& toto, double& ztoto1, double& ztoto2) 
{
    istringstream LigneEntree(ligne);
    string titi;
    LigneEntree >> titi;
    toto = false;
    if (titi == "oui" || titi == "OUI" || titi == "Oui" || titi == "O" || titi == "o"
        || titi == "yes" || titi == "YES" || titi == "Yes" || titi == "Y" || titi == "y") 
    {
        toto = true;
        LigneEntree >> ztoto1 >> ztoto2;
    }
    return;
}

void LitLigne(const string ligne, FORMATFICHIER& toto) 
{
    istringstream LigneEntree(ligne);
    string titi;
    LigneEntree >> titi;
    transform(titi.begin(), titi.end(), titi.begin(), static_cast<int (*)(int)> (tolower));
    if (titi.substr(0, 3) == "asc")
        toto = ascii;
    else
        toto = binary;
    return;
}

void LitLigne(const string ligne, bool toto[] )
{ // Ajout Youssouf
    istringstream LigneEntree(ligne);
    string titi;
    //string slach;
    for (int i = 0; i < 4; i++)
    {
        toto[i]=false;
        LigneEntree >> titi;
        //LigneEntree >>slach;
        if (titi == "oui" || titi == "OUI" || titi == "Oui" || titi == "O" || titi == "o"
            || titi == "yes" || titi == "YES" || titi == "Yes" || titi == "Y" || titi == "y")
        {
            toto[i]=true;
        }
        
    }
    
}

//========================================================================
// LitFormate :
// ----------
// Fonctions pour aller lire des trucs format�s dans une ligne
//------------------------------------------------------------------------

void LitFormate(const string ligne, int& itoto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    LigneEntree >> itoto;
    return;
}

void LitFormate(const string ligne, double& toto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    LigneEntree >> toto;
    return;
}

void LitFormate(const string ligne, string& toto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    LigneEntree >> toto;
    return;
}

void LitFormate(const string ligne, MODE& toto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    string titi;
    LigneEntree >> titi;
    if (titi == "value")
        toto = value;
    else if (titi == "file")
        toto = file;
    else
        toto = nothing;
    return;
}

void LitFormate(const string ligne, int nbtoto, UPLIFT* toto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    string titi;
    LigneEntree >> titi;
    for (int i = 0; i < nbtoto; i++) 
    {
        LigneEntree >> titi;
        if (titi == "creneau")
            toto[i] = creneau;
        else if (titi == "gaussienne")
            toto[i] = gaussienne;
        else
            toto[i] = croissant;
    }
    return;
}

void LitFormate(const string ligne, int& itoto, double& toto1, double& toto2, double& toto3) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto >> toto1 >> toto2 >> toto3;
    return;
}

void LitFormate(const string ligne, int& itoto, double& toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto;
    double tmpdble;
    if (LigneEntree >> tmpdble)
        toto = tmpdble;
    else
        toto = EPAISSEUR_INF;
    return;
}

void LitFormate(const string ligne, int& itoto1, int& itoto2) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto1;
    LigneEntree >> itoto2;
    return;
}

void LitFormate(const string ligne, int& itoto1, int& itoto2, double& toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto1;
    LigneEntree >> itoto2;
    LigneEntree >> toto;
    return;
}

void LitFormate(const string ligne, int& itoto1, int& itoto2, string& toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto1;
    LigneEntree >> itoto2;
    LigneEntree >> toto;
    return;
}

void LitFormate(const string ligne, PENTE& toto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    string titi;
    LigneEntree >> titi;
    if (titi == "multiple")
        toto = multiple;
    return;
}

void LitFormate(const string ligne, bool& toto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    string titi;
    LigneEntree >> titi;
    if (titi == "OUI")
        toto = true;
    else
        toto = false;
    return;
}

void LitFormate(const string ligne, int& itoto, BORD& toto, double& ztoto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto;
    string titi;
    LigneEntree >> titi;
    /*if (titi == "SYMETRIE_AXIALE")
    {
        toto = sym_ax;
        LigneEntree >> ztoto;
    }
    else*/ // 2023
    if (titi == "ALTITUDE_FIXE")
    {
        toto = alti_fixe;
        LigneEntree >> ztoto;
    } 
    else if (titi == "BOUCLE")
        toto = boucle;
    /*else if (titi == "BORD_LIBRE")
        toto = bord_libre;*/ // 2023
    else
        toto = flux_nul;
    return;
}

void LitFormate(const string ligne, DIFF& toto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    string titi;
    LigneEntree >> titi;
    if (titi == "NON_LINEAIRE")
        toto = non_lineaire;
    else if (titi == "LINEAIRE")
        toto = lineaire;
    else
        toto = pas_diff;
    return;
}

void LitFormate(const string ligne, const int nb, int* toto) 
{
    istringstream LigneEntree(ligne);
    for (int i = 0; i < nb; i++)
        LigneEntree >> toto[i];
    return;
}

void LitFormate(const string ligne, int& itoto, const int nb, double* toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto;
    for (int i = 0; i < nb; i++)
        LigneEntree >> toto[i];
    return;
}

void LitFormate(const string ligne, int& itoto, string& toto) 
{
    istringstream LigneEntree(ligne);
    LigneEntree >> itoto;
    LigneEntree >> toto;
    return;
}

void LitFormate(const string ligne, FORMATFICHIER& toto) 
{
    istringstream LigneEntree(ligne.substr(LONG_STRING + 1));
    string titi;
    LigneEntree >> titi;
    transform(titi.begin(), titi.end(), titi.begin(), static_cast<int (*)(int)> (tolower));
    if (titi.substr(0, 3) == "asc")
        toto = ascii;
    else
        toto = binary;
    return;
}

// supprime toutes les occurrences du caract�re C donn�

void SupprimeTousLesCaracteres(string & Str, char C) 
{
    Str.erase(remove(Str.begin(), Str.end(), C), Str.end());
    return;
}

//=================================================
// AfficheMessage :
// --------
// Fonctions pour afficher une cha�ne de caract�res
// et �ventuellement un nombre ensuite
//-------------------------------------------------

void AfficheMessage(string ligne) 
{
    cout << ligne << endl;
    return;
}

void AfficheMessage(string ligne, int toto)
{
    cout << ligne << " " << toto << endl;
    return;
}

void AfficheMessage(string ligne, int i1, int i2) 
{
    cout << ligne << " " << i1 << " " << i2 << endl;
    return;
}

void AfficheMessage(string ligne, int i1, int i2, int i3) 
{
    cout << ligne << " " << i1 << " " << i2 << " " << i3 << endl;
    return;
}

void AfficheMessage(string ligne, int i1, int i2, double toto) 
{
    cout << ligne << " " << i1 << " " << i2 << " " << toto << endl;
    return;
}

void AfficheMessage(string ligne, int i1, int i2, double toto1, double toto2) 
{
    cout << ligne << " " << i1 << " " << i2 << " " << toto1 << " " << toto2 << endl;
    return;
}

void AfficheMessage(string ligne, double toto) 
{
    cout << ligne << " " << toto << endl;
    return;
}

void AfficheMessage(string ligne, double toto1, double toto2) 
{
    cout << ligne << " " << toto1 << " " << toto2 << endl;
    return;
}

void AfficheMessage(string ligne, string toto) 
{
    cout << ligne << " " << toto << endl;
    return;
}

void AfficheMessage(string ligne, string toto1, string toto2) 
{
    cout << ligne << "'" << toto1 << "'" << toto2 << endl;
    return;
}

void AfficheMessage(string ligne, string toto, int itoto) 
{
    cout << ligne << " " << toto << " " << itoto << endl;
    return;
}

void AfficheMessage(string toto1, int itoto1, string toto2, int itoto2) 
{
    cout << toto1 << itoto1 << toto2 << itoto2 << endl;
    return;
}

void AfficheMessage(string ligne, int i1, double toto) 
{
    cout << ligne << " " << i1 << " " << toto << endl;
    return;
}

void AfficheMessage(string ligne1, int i1, string ligne2, double toto) 
{
    cout << ligne1 << " " << i1 << " " << ligne2 << " " << toto << endl;
    return;
}

void AfficheMessage(string ligne1, double toto, string ligne2, int i1) 
{
    cout << ligne1 << " " << toto << " " << ligne2 << " " << i1 << endl;
    return;
}

void AfficheMessage(string ligne, int i1, double toto1, double toto2) 
{
    cout << ligne << " " << i1 << " " << toto1 << " " << toto2 << endl;
    return;
}

void AfficheMessage(int i1, double toto) 
{
    cout << i1 << " " << toto << endl;
    return;
}

void AfficheMessage(int i1, double toto1, int i2, double toto2) 
{
    cout << i1 << " " << toto1 << " " << i2 << " " << toto2 << endl;
    return;
}

void AfficheMessage(int i1, double toto1, int i2, double toto2, int i3, double toto3) 
{
    cout << i1 << " " << toto1 << " " << i2 << " " << toto2 << " " << i3 << " " << toto3 << endl;
    return;
}

void AfficheMessage(int i1, int i2, double toto) 
{
    cout << i1 << " " << i2 << " " << toto << endl;
    return;
}

void AfficheMessage(int i1, int i2) 
{
    cout << i1 << " " << i2 << endl;
    return;
}

void AfficheMessage(string ligne, MODE toto) 
{
    cout << ligne << " " << toto << endl;
    return;
}

//====================================
// Echange :
// --------
// �change deux indices dans une liste
//------------------------------------
//##### Modif. le 23/09/2005 : pass� en inline

//=======================
// Tris :
//------------------
// Tri par insertion
//------------------

void TriInsertion(const int tab[], int liste[], int n) 
{
    for (int i = 1; i < n; i++)
        for (int j = i; (j > 0) &&
                (tab[liste[j]] > tab[liste[j - 1]]); j--)
            Echange(liste, j, j - 1);
    return;
}
//--------------------
// Tri rapide it�ratif
//--------------------

void TriRapideOptimal(const int tab[], int liste[], int debut, int fin) 
{
    int listsize = fin - debut + 1;
    int pivotindex, l, r;
    int top = 0;
    int pivot;
    Pile soustas[100];

    soustas[top].gauche = debut;
    soustas[top].droite = fin;

    while (top >= 0) 
    {
        // Pop soustas
        debut = soustas[top].gauche;
        fin = soustas[top].droite;
        top--;

        //Find pivot
        pivotindex = (debut + fin) / 2; // Pivot = milieu de l'intervalle
        pivot = tab[liste[pivotindex]];
        Echange(liste, pivotindex, debut); // Stick pivot at beginning

        // Partition
        l = debut - 1;
        r = fin;
        do {
            while (tab[liste[++l]] > pivot);
            while (r && (pivot > tab[liste[++l]]));
            Echange(liste, l, r);
        } while (l < r);

        Echange(liste, l, r);
        Echange(liste, l, debut);

        //Load up soustas
        if ((l - debut) > SEUIL_TRI) 
        { // Left partition
            top++;
            soustas[top].gauche = debut;
            soustas[top].droite = l - 1;
        }

        if ((fin - l) > SEUIL_TRI) 
        { // Right partition
            top++;
            soustas[top].gauche = l + 1;
            soustas[top].droite = fin;
        }
    }
    TriInsertion(tab, liste, listsize); //Final Insertion Sort
    return;
}
