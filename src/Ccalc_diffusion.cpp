#include "Ccalc_diffusion.h"
#include <iostream>
#include <iomanip>
using namespace std;

//-------------------------------------------
// fonctions de la classe CDiffusion :
//
// NB: les mÈthodes de diffusion sont dÈfinies
// dans "Equations.h" et "Equations.cpp"
//-------------------------------------------

// constructeur

CDiffusion::CDiffusion(CModele* modele, CMaillage* maillage, CFlux* flux) 
{
    pas3 = maillage->GetPas3();


    // Les objets importants
    //	MonMaillage = maillage; // Pas besoin du maillage tant qu'on ne recalcule pas de pentes
    MesFlux = flux;

    // Recuperation des parametres d'erosion par diffusion pour les sediments lacustres
    difflac = modele->GetParamDiffLac();

    // Choix de l'equation d'erosion par diffusion pour les sediments
    diffsed = modele->GetParamDiffSed();
    MaDiffusionSed = &Diffusion; // janvier 2013
    FluxDiffEntrant = 0; // janvier 2013

    // ParamËtres de diffusion pour le bedrock
    nblitho = modele->GetNbLitho();
    diffbedrock = new ParamDiff*[nblitho];
    MaDiffusionBedrock = new PtrFct_Diff[nblitho]; // janvier 2013
    BedrockOptionSeuil = new bool[nblitho]; // janvier 2013
    for (int i = 0; i < nblitho; i++) 
    {
        MaDiffusionBedrock[i] = &Diffusion; // janvier 2013
        diffbedrock[i] = (modele->GetLitho(i)).diff;
        switch (diffbedrock[i]->Type_diff) 
        {
            case non_lineaire:
                BedrockOptionSeuil[i] = true; // janvier 2013
                break;
            case lineaire:
                BedrockOptionSeuil[i] = false; // janvier 2013
                break;
            case pas_diff:
                BedrockOptionSeuil[i] = false;
                break;
        }
    }

    // ParamËtres de longueur de transport pour les sediments aeriens
    switch (diffsed->Type_diff) 
    {
        case non_lineaire:
            MaLongueurTransport = &Longueur_NonLineaire; // janvier 2013
            SedOptionSeuil = true;
            break;
        case lineaire:
            MaLongueurTransport = &Longueur_Lineaire; // janvier 2013
            SedOptionSeuil = false;
            break;
        case pas_diff:
            SedOptionSeuil = false;
            MaLongueurTransport = NULL;
    }

    // ParamËtres de longueur de transport pour les sediments sous l'eau // ajout decembre 2018
    switch (difflac->Type_diff) 
    {
        case non_lineaire:
            MaLongueurTransportLac = &Longueur_NonLineaire;
            SedOptionSeuilLac = true;
            break;
        case lineaire:
            MaLongueurTransportLac = &Longueur_Lineaire;
            SedOptionSeuilLac = false;
            break;
        case pas_diff:
            SedOptionSeuilLac = false;
            MaLongueurTransportLac = NULL;
    }



    // Fonction d'actualisation des flux selon OptionIle
    if ((modele->GetOptions()).OptionIle) 
    {
        MonActualiseFlux = &CFlux::ActualiseLesFluxDiffRecus_ILE;
    } 
    else 
    {
        MonActualiseFlux = &CFlux::ActualiseLesFluxDiffRecus;
    }


    MonVoisinage = NULL; // rajout le 13 mars 07, initialisation du pointeur MonVoisinage a Null
}

// destructeur

CDiffusion::~CDiffusion() 
{
    delete [] diffbedrock;
}



// ---------------------------------------------------------------------------------------------------------------------
// Nouvelle approche janvier 2013 fondee sur la difference entre un flux de depot et flux d'erosion et longueur de transport
// donc la valeur est 1 si diffusion simple et (1-(s/Sc)**2) si diffusion non-lineaire. L'erosion est proportionnelle
// a la pente S si le cas "lineaire" est choisi. Si c'est le cas "non-lineaire", l'erosion est proportionnelle a S
// si S<Sc et sinon est egale a (S-Sc)dx**3

void CDiffusion::DiffuseLesSedSeulsTEST(const int &nummaille, CMaille* maille) 
{
    // On recupere le flux de sediment entrant genere par transport gravitaire ("diffusif")
    FluxDiffEntrant = MesFlux->GetVolumeDiff(nummaille);
    // le voisinage local pour la diffusion
    MonVoisinage = maille->GetVoisinageBas();
    // le stock de sediment presents sur la maille
    double stocksed = MesFlux->GetStockSed(nummaille);


    // -----
    // DEPOT
    // -----
    double longueurtransport = MaLongueurTransport(MonVoisinage->pentemax, diffsed);
    double fractionfluxentrant = 1. / longueurtransport;
    double depot = fractionfluxentrant * FluxDiffEntrant;

    // -------
    // EROSION
    // -------
    double erosionsedtotal = 0.;
    // On teste s'il existe une pente excedant la pente critique
    if (MonVoisinage->pentemax > diffsed->Seuil) 
    {
        erosionsedtotal = Diffusion_RetourPenteCritique(MonVoisinage->pentemax, diffsed);
    }        // Sinon on erode normalement proportionnellement a la pente max et distribuant vers les mailles plus basses
    else 
    {
        double MonKtemp = diffsed->Ktemp;
        erosionsedtotal = MaDiffusionSed(MonVoisinage->pentemax, MonKtemp);
    }
    if (erosionsedtotal > stocksed) 
    {
        // pour ne pas depasser la quantite de sediments disponibles
        erosionsedtotal = stocksed;
    }

    // ---------
    // TRANSFERT
    // ---------
    double transfertvers [MonVoisinage->nb];
    double transferttotal = FluxDiffEntrant - depot;
    for (int i = 0; i < MonVoisinage->nb; i++) 
    {
        transfertvers[i] = (erosionsedtotal + transferttotal) * MonVoisinage->coeff[i];
    }

    // ----------------------
    // ACTUALISATION DES FLUX
    // ----------------------
    // Stock de sediments presents sur la maille
    MesFlux->AddStockSed(nummaille, depot - erosionsedtotal);
    // Actualisation des sediments transferes et donc recus par les voisins bas
    (MesFlux->*MonActualiseFlux)(maille, MonVoisinage, transfertvers);
    // Actualisation du Depot et Erosion sur la maille
    MesFlux->ActualiseErosionDepot(nummaille, depot, erosionsedtotal);
    // Actualisation du Transfert sur la maille
    MesFlux->ActualiseErosionDepotDiff(nummaille, depot, erosionsedtotal); //Ajout Pierre
    MesFlux->ActualiseVolumeDiffExporte(nummaille, transfertvers, maille); //Ajout Pierre
    //MesFlux->Setdiffusivite(nummaille,diffsed->Ktemp);  // decembre 2014
    return;
}

void CDiffusion::DiffuseLesSedSeulsLacTEST(const int &nummaille, CMaille* maille) 
{
    // On recupere le flux de sediment entrant genere par transport gravitaire ("diffusif")
    FluxDiffEntrant = MesFlux->GetVolumeDiff(nummaille);
    // le voisinage local pour la diffusion
    MonVoisinage = maille->GetVoisinageBas();
    // le stock de sediment presents sur la maille
    double stocksed = MesFlux->GetStockSed(nummaille);

    // -----
    // DEPOT
    // -----
    double longueurtransport = MaLongueurTransportLac(MonVoisinage->pentemax, difflac);
    double fractionfluxentrant = 1. / longueurtransport;
    double depot = fractionfluxentrant * FluxDiffEntrant;

    // -------
    // EROSION
    // -------
    double erosionsedtotal = 0.;
    // On teste s'il existe une pente excedant la pente critique
    if (MonVoisinage->pentemax > difflac->Seuil) 
    {
        erosionsedtotal = Diffusion_RetourPenteCritique(MonVoisinage->pentemax, difflac);
    }        // Sinon on erode normalement proportionnellement a la pente max et distribuant vers les mailles plus basses
    else 
    {
        double MonKtemp = difflac->Ktemp;
        erosionsedtotal = MaDiffusionSed(MonVoisinage->pentemax, MonKtemp);
        // Si il n'y a pas assez de sediment disponible on pondere l'erosion
    }
    if (erosionsedtotal > stocksed) 
    {
        // pour ne pas depasser la quantite de sediments disponibles
        erosionsedtotal = stocksed;
    }
    // ---------
    // TRANSFERT
    // ---------
    double transfertvers [MonVoisinage->nb];
    double transferttotal = FluxDiffEntrant - depot;
    for (int i = 0; i < MonVoisinage->nb; i++) 
    {
        transfertvers[i] = (erosionsedtotal + transferttotal) * MonVoisinage->coeff[i];
    }

    // ----------------------
    // ACTUALISATION DES FLUX
    // ----------------------
    // Stock de sediments presents sur la maille
    MesFlux->AddStockSed(nummaille, depot - erosionsedtotal);
    // Actualisation des sediments transferes et donc recus par les voisins bas
    (MesFlux->*MonActualiseFlux)(maille, MonVoisinage, transfertvers);
    // Actualisation du Depot et Erosion sur la maille
    MesFlux->ActualiseErosionDepot(nummaille, depot, erosionsedtotal);
    // Actualisation du Transfert sur la maille
    MesFlux->ActualiseErosionDepotDiff(nummaille, depot, erosionsedtotal); //Ajout Pierre
    MesFlux->ActualiseVolumeDiffExporte(nummaille, transfertvers, maille); //Ajout Pierre
    //MesFlux->Setdiffusivite(nummaille, difflac->Ktemp);  // decembre 2014
    return;
}

void CDiffusion::DiffuseToutTEST(const int &nummaille, CMaille* maille) 
{

    // On recupere le flux de sediment entrant genere par transport gravitaire ("diffusif")
    FluxDiffEntrant = MesFlux->GetVolumeDiff(nummaille);
    // le voisinage local pour la diffusion
    MonVoisinage = maille->GetVoisinageBas();
    // le stock de sediment presents sur la maille
    double stocksed = MesFlux->GetStockSed(nummaille);
    // une diffusivite moyenne pour les cailloux
    //double diffusivite=0.; // decembre 2014

    // -----
    // DEPOT
    // -----
    double longueurtransport = MaLongueurTransport(MonVoisinage->pentemax, diffsed);
    double fractionfluxentrant = 1. / longueurtransport;
    double depot = fractionfluxentrant * FluxDiffEntrant;
    // -------
    // EROSION
    // -------
    double erosionsedtotal = 0.; // erosion totale de sed sur la maille
    double erosionbedrocktotal = 0.; // erosion totale de bedrock sur la maille
    double penteactualisee = 0.;
    // structure :
    //    erosion sediment
    //	  (si optionseuil et  si S>Sc on erode pour equilibrer la pente, sinon on erode selon la loi de diffusion)
    //	  si plus de sediment erosion du bedrock
    //	  (pour chaque couche : si optionseuil et si S>Sc on erode pour equilibrer la pente, sinon on erode selon la loi de diffusion)
    //
    // Erosion des Sediments
    if (SedOptionSeuil && MonVoisinage->pentemax > diffsed->Seuil)
        // On teste s'il existe une pente excedant la pente critique (cas non lineaire)
    {
        erosionsedtotal = Diffusion_RetourPenteCritique(MonVoisinage->pentemax, diffsed);
    } 
    else 
    {
        // Sinon on erode normalement proportionnellement a la pente max et on
        // redistribue proportionnellement a la pente locale
        // Erosion des sediments
        double MonKtemp = diffsed->Ktemp;
        erosionsedtotal = MaDiffusionSed(MonVoisinage->pentemax, MonKtemp);
    }

    //diffusivite =  diffsed->Ktemp; // decembre 2014

    if (erosionsedtotal > stocksed)
        // Si le volume erodable excede le stock de sediment
        // Erosion du Bedrock
    {
        // on erode juste le stock de sediment
        facteur_distrib = stocksed / erosionsedtotal;
        erosionsedtotal = stocksed;
        //diffusivite *=  stocksed / erosionsedtotal; // decembre 2014
        // on erode le bedrock en commencant par la premiere couche
        double erosioncouche = 0.;
        // les paramètres de diffusion de la première couche de bedrock
        int indicelitho = MesFlux->GetNumLitho(nummaille);
        ParamDiff* difflitho = diffbedrock[indicelitho];
        // le volume local de bedrock de cette première couche
        double MonVolumeCouche = MesFlux->GetVolumeCouche(nummaille);
        // on compare la pente max actualisee en enlevant les sediments qui viennent d'etre erodes
        penteactualisee = MonVoisinage->pentemax - erosionsedtotal / maille->GetPas3VoisinBas(0);

        if (BedrockOptionSeuil[indicelitho] && penteactualisee > difflitho->Seuil) 
        {
            // On erode pour equilibrer la pente
            erosioncouche = Diffusion_RetourPenteCritique(penteactualisee, difflitho);
            erosionbedrocktotal += erosioncouche;
        } 
        else
            // On erode normalement selon la loi de diffusion
        {
            // Pondération du coefficient de diffusion selon le temps déjà passé à diffuser les sédiments
            facteur_temps = 1. - facteur_distrib;
            double MonKtemp = difflitho->Ktemp * facteur_temps;
            erosioncouche = MaDiffusionBedrock[indicelitho](penteactualisee, MonKtemp);
            erosionbedrocktotal += erosioncouche;
        }
        //diffusivite += difflitho->Ktemp * facteur_temps; // decembre 2014
        // Ensuite on descend dans la pile des couches tant qu'on peut eroder
        while (erosioncouche >= MonVolumeCouche) 
        {

            // on determine le facteur de ponderation des flux erodes dans chaque direction
            facteur_distrib = MonVolumeCouche / erosioncouche;
            // on corrige de ce qui a ete erode en trop
            erosionbedrocktotal = erosionbedrocktotal - erosioncouche + MonVolumeCouche;
            // Actualisation de la  couche superficielle de bedrock de la maille
            MonVolumeCouche = MesFlux->IncrementeCoucheLitho(nummaille, maille);
            // on recupere les paramètres de diffusion de la nouvelle couche
            indicelitho = MesFlux->GetNumLitho(nummaille);
            difflitho = diffbedrock[indicelitho];
            penteactualisee = MonVoisinage->pentemax - (erosionsedtotal + erosionbedrocktotal) / maille->GetPas3VoisinBas(0);
            if (BedrockOptionSeuil[indicelitho] && penteactualisee > difflitho->Seuil) 
            {
                erosioncouche = Diffusion_RetourPenteCritique(penteactualisee, difflitho);
                erosionbedrocktotal += erosioncouche;
            } 
            else 
            {
                // Pondération du coefficient de diffusion selon le temps déjà passé à diffuser les sédiments
                facteur_temps = 1. - facteur_distrib;
                double MonKtemp = difflitho->Ktemp * facteur_temps;
                erosioncouche = MaDiffusionBedrock[indicelitho](penteactualisee, MonKtemp);
                erosionbedrocktotal += erosioncouche;
            }
            //diffusivite +=  difflitho->Ktemp * facteur_temps; // decembre 2014
        }
        // on actualise l'epaisseur de la première couche de la maille
        MesFlux->SoustraitVolumeCouche(nummaille, erosioncouche);
    }

    // ---------
    // TRANSFERT
    // ---------
    double transfertvers [MonVoisinage->nb];
    double transferttotal = FluxDiffEntrant - depot;
    for (int i = 0; i < MonVoisinage->nb; i++) 
    {
        transfertvers[i] = (erosionsedtotal + erosionbedrocktotal + transferttotal) * MonVoisinage->coeff[i];
    }

    // ----------------------
    // ACTUALISATION DES FLUX
    // ----------------------
    // Stock de sediments presents sur la maille
    MesFlux->AddStockSed(nummaille, depot - erosionsedtotal);
    // Actualisation des sediments transferes et donc recus par les voisins bas
    (MesFlux->*MonActualiseFlux)(maille, MonVoisinage, transfertvers);
    // Actualisation du Depot et Erosion sur la maille
    MesFlux->ActualiseErosionDepot(nummaille, depot, erosionbedrocktotal + erosionsedtotal);
    // Actualisation du Transfert sur la maille
    MesFlux->ActualiseErosionDepotDiff(nummaille, depot, erosionbedrocktotal + erosionsedtotal); //Ajout Pierre
    MesFlux->ActualiseVolumeDiffExporte(nummaille, transfertvers, maille); //Ajout Pierre
    //MesFlux->Setdiffusivite(nummaille, diffusivite);  // decembre 2014
    return;
}

void CDiffusion::DiffuseToutLacTEST(const int &nummaille, CMaille* maille) 
{

    // On recupere le flux de sediment entrant genere par transport gravitaire ("diffusif")
    FluxDiffEntrant = MesFlux->GetVolumeDiff(nummaille);
    // le voisinage local pour la diffusion
    MonVoisinage = maille->GetVoisinageBas();
    // le stock de sediment presents sur la maille
    double stocksed = MesFlux->GetStockSed(nummaille);

    // -----
    // DEPOT
    // -----
    double longueurtransport = MaLongueurTransportLac(MonVoisinage->pentemax, difflac);
    double fractionfluxentrant = 1. / longueurtransport;
    double depot = fractionfluxentrant * FluxDiffEntrant;
    // -------
    // EROSION
    // -------
    double erosionsedtotal = 0.; // erosion totale de sed sur la maille
    double erosionbedrocktotal = 0.; // erosion totale de bedrock sur la maille
    double penteactualisee = 0.;
    // structure :
    //    erosion sediment
    //	  (si optionseuil et  si S>Sc on erode pour equilibrer la pente, sinon on erode selon la loi de diffusion)
    //	  si plus de sediment erosion du bedrock
    //	  (pour chaque couche : si optionseuil et si S>Sc on erode pour equilibrer la pente, sinon on erode selon la loi de diffusion)
    //
    // Erosion des Sediments
    if (SedOptionSeuilLac && MonVoisinage->pentemax > difflac->Seuil)
        // On teste s'il existe une pente excedant la pente critique (cas non lineaire)
    {
        erosionsedtotal = Diffusion_RetourPenteCritique(MonVoisinage->pentemax, difflac);
    } 
    else 
    {
        // Sinon on erode normalement proportionnellement a la pente max et on
        // redistribue proportionnellement a la pente locale
        // Erosion des sediments
        double MonKtemp = difflac->Ktemp;
        erosionsedtotal = MaDiffusionSed(MonVoisinage->pentemax, MonKtemp);
    }

    if (erosionsedtotal > stocksed)
        // Si le volume erodable excede le stock de sediment
        // Erosion du Bedrock
    {
        // on erode juste le stock de sediment
        facteur_distrib = stocksed / erosionsedtotal;
        erosionsedtotal = stocksed;
        // on erode le bedrock en commencant par la premiere couche
        double erosioncouche = 0.;
        // les paramètres de diffusion de la première couche de bedrock
        int indicelitho = MesFlux->GetNumLitho(nummaille);
        ParamDiff* difflitho = diffbedrock[indicelitho];
        // le volume local de bedrock de cette première couche
        double MonVolumeCouche = MesFlux->GetVolumeCouche(nummaille);
        // on compare la pente max actualisee en enlevant les sediments qui viennent d'etre erodes
        penteactualisee = MonVoisinage->pentemax - erosionsedtotal / maille->GetPas3VoisinBas(0);
        if (BedrockOptionSeuil[indicelitho] && penteactualisee > difflitho->Seuil) 
        {
            // On erode pour equilibrer la pente
            erosioncouche = Diffusion_RetourPenteCritique(penteactualisee, difflitho);
            erosionbedrocktotal += erosioncouche;
        } 
        else
            // On erode normalement selon la loi de diffusion
        {
            // Pondération du coefficient de diffusion selon le temps déjà passé à diffuser les sédiments
            facteur_temps = 1. - facteur_distrib;
            double MonKtemp = difflitho->Ktemp * facteur_temps;
            erosioncouche = MaDiffusionBedrock[indicelitho](penteactualisee, MonKtemp);
            erosionbedrocktotal += erosioncouche;
        }

        // Ensuite on descend dans la pile des couches tant qu'on peut eroder
        while (erosioncouche >= MonVolumeCouche) 
        {
            // on determine le facteur de ponderation des flux erodes dans chaque direction
            facteur_distrib = MonVolumeCouche / erosioncouche;
            // on corrige de ce qui a ete erode en trop
            erosionbedrocktotal = erosionbedrocktotal - erosioncouche + MonVolumeCouche;
            // Actualisation de la  couche superficielle de bedrock de la maille
            MonVolumeCouche = MesFlux->IncrementeCoucheLitho(nummaille, maille);

            // on recupere les paramètres de diffusion de la nouvelle couche
            indicelitho = MesFlux->GetNumLitho(nummaille);
            difflitho = diffbedrock[indicelitho];
            penteactualisee = MonVoisinage->pentemax - (erosionsedtotal + erosionbedrocktotal) / maille->GetPas3VoisinBas(0);
            if (BedrockOptionSeuil[indicelitho] && penteactualisee > difflitho->Seuil) 
            {
                erosioncouche = Diffusion_RetourPenteCritique(penteactualisee, difflitho);
                erosionbedrocktotal += erosioncouche;
            } 
            else 
            {
                // Pondération du coefficient de diffusion selon le temps déjà passé à diffuser les sédiments
                facteur_temps = 1. - facteur_distrib;
                double MonKtemp = difflitho->Ktemp * facteur_temps;
                erosioncouche = MaDiffusionBedrock[indicelitho](penteactualisee, MonKtemp);
                erosionbedrocktotal += erosioncouche;
            }
        }
        // on actualise l'epaisseur de la première couche de la maille
        MesFlux->SoustraitVolumeCouche(nummaille, erosioncouche);
    }

    // ---------
    // TRANSFERT
    // ---------
    double transfertvers [MonVoisinage->nb];
    double transferttotal = FluxDiffEntrant - depot;
    for (int i = 0; i < MonVoisinage->nb; i++) 
    {
        transfertvers[i] = (erosionsedtotal + erosionbedrocktotal + transferttotal) * MonVoisinage->coeff[i];

    }

    // ----------------------
    // ACTUALISATION DES FLUX
    // ----------------------
    // Stock de sediments presents sur la maille
    MesFlux->AddStockSed(nummaille, depot - erosionsedtotal);
    // Actualisation des sediments transferes et donc recus par les voisins bas
    (MesFlux->*MonActualiseFlux)(maille, MonVoisinage, transfertvers);
    // Actualisation du Depot et Erosion sur la maille
    MesFlux->ActualiseErosionDepot(nummaille, depot, erosionbedrocktotal + erosionsedtotal);
    // Actualisation du Transfert sur la maille
    MesFlux->ActualiseErosionDepotDiff(nummaille, depot, erosionbedrocktotal + erosionsedtotal); //Ajout Pierre
    MesFlux->ActualiseVolumeDiffExporte(nummaille, transfertvers, maille); //Ajout Pierre
    //MesFlux->Setdiffusivite(nummaille, diffsed->Ktemp);  // decembre 2014
    return;
}

// --------------------------------------------------------------------------------------
// On n'erode rien (car on est dans un trou ou parceque la diffusion n'est pas activee dans les lac par exemple)
// mais on depose tous les sediments arrivant par "diffusion" de plus haut
// --------------------------------------------------------------------------------------

void CDiffusion::DeposeLesSediments(const int &nummaille, CMaille* maille) 
{

    // On recupere le flux de sediment entrant genere par transport gravitaire ("diffusif")
    FluxDiffEntrant = MesFlux->GetVolumeDiff(nummaille);

    // -----
    // DEPOT (on depose tout ce qui entre)
    // -----
    double depot = 0.;
    depot = FluxDiffEntrant;

    // ----------------------
    // ACTUALISATION DES FLUX
    // ----------------------
    // Stock de sediments presents sur la maille
    MesFlux->AddStockSed(nummaille, depot);

    // Actualisation du Depot et Erosion sur la maille
    MesFlux->ActualiseErosionDepot(nummaille, depot, 0.);
    return;
}



