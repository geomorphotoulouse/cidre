#include "Ccalc_bilaneau.h"
#include <time.h>
using namespace std;


//-------------------------------------------
//
// fonctions de la classe CBilanEau :
//
//-------------------------------------------

// constructeur

CBilanEau::CBilanEau(CModele* modele, CMaillage* maillage, CFlux* flux)
{
    nbmailles = maillage->GetNbMailles();
    
    pas2 = maillage->GetPas2();
    pas3 = maillage->GetPas3(); //30 juin 2008
    // Les objets importants
    MonMaillage = maillage;
    MesFlux = flux;

    NumLac = new int[nbmailles];
    ptrNumLac = &NumLac;
    DejaPasse = new bool[nbmailles];
    BordDuLac.reserve(TAILLEMAX_LAC);
    MilieuDuLac.reserve(TAILLEMAX_LAC);
    // Calcul de la hauteur d'eau :	
    MonCalculHauteurEau = &CFlux::CalculHauteurEauCourante; // 30 juin 2008
    
    MonGetAlti = &CMaille::GetAltiEau; // 2023

    // Selon l'option de calcul de pente :
    if (modele->GetOptions().OptionIle) 
    {
        MonBordDeLac = &CMaille::BordDeLac_ILE;
        MonSuivantBas = &CMaillage::SuivantBas_ILE;
        MonSuivantPlat = &CMaillage::SuivantPlat_ILE;
        MonTesteSiBas = &CMaillage::TesteSiBas_ILE;

        MonDeverse = &CFlux::DeverseSurMesVoisins_ILE;
        MonDeverse_FilsLac = &CFlux::DeverseSurMesVoisins_FilsLac_ILE;
        MonDeverse_Exutoire = &CFlux::DeverseSurMesVoisins_Exutoire_ILE;
        tabnumvoisin = new int[8];

    } 
    else 
    {
        MonBordDeLac = &CMaille::BordDeLac;
        MonSuivantBas = &CMaillage::SuivantBas;
        MonSuivantPlat = &CMaillage::SuivantPlat;
        MonTesteSiBas = &CMaillage::TesteSiBas;

        MonDeverse = &CFlux::DeverseSurMesVoisins;
        MonDeverse_FilsLac = &CFlux::DeverseSurMesVoisins_FilsLac;
        MonDeverse_Exutoire = &CFlux::DeverseSurMesVoisins_Exutoire;
        tabnumvoisin = new int[8];

    }

    tempslac = 0.;
}

// destructeur

CBilanEau::~CBilanEau() 
{
    delete [] NumLac;
    delete [] DejaPasse;
    delete [] tabnumvoisin;
}

// autres fonctions

//---------------------------------------
// Calcul du bilan d'eau pour le maillage
//---------------------------------------
//
// Les pentes ont �t� calcul�es en tout point lors des effondrements
// (m�me s'il n'y en a pas).
// Il faut maintenant reparcourir le maillage pour voir s'il y a des lacs.
// Quand il y a un lac, il faudra recommencer le bilan en eau � partir de l'exutoire si y a
//
// C'est le cas le plus g�n�ral. On peut avoir un niveau de base eustatique
// r�gional et si une altitude se trouve sous ce niveau de base alors on est forc�ment sous l'eau.
// On peut aussi avoir des lacs qui se forment � d'autres altitudes.

int CBilanEau::BilanEau() 
{
    int ind, redepart;
    int i;
    int k;
    int nbsuivants;
    CMaille* MaMaille;

    // Initialisation des grilles DejaPasse et NumLac, et du nb de lacs
    for (i = 0; i < nbmailles; i++) 
    {
        DejaPasse[i] = false;
        NumLac[i] = 0;
    }
    nblacs = 0;


    //////////////////////////////////////////////////////////////////////////////////////////
    tempslac += dt;
    //////////////////////////////////////////////////////////////////////////////////////////


    // Boucle "while" sur la liste des mailles class�es par altitudes
    // il faut pouvoir revenir en arri�re dans la liste, 
    // quand on tombe sur un lac A EXUTOIRE (si lac ferm�, pas besoin !)
    i = 0;
    while (i < nbmailles) 
    {
        ind = MonMaillage->GetNumListe(i);
        MaMaille = MonMaillage->GetMaille(ind);

        if (!MonMaillage->MailleTerre(ind)) // si on est sous la mer
        {
            NumLac[ind] = -1; // decembre 2018. On consid�re que sous le niveau de base impos� c'est la mer
            // et on affecte a numlac la valeur correspondante de -1
            MaMaille->SetAltiEau(MonMaillage->GetNiveauDeBase());
            i++;
        } 
        else 
        {
            ////// Si c'est le premier passage sur la maille
            if (!DejaPasse[ind]) 
            {
                // Note le num�ro de la maille i dans le classement par altitude
                MonMaillage->SetNumListeInv(i, ind);
                DejaPasse[ind] = true;

                // Si cette maille appartient d�j� � un lac (plat en fait), le passage sert juste
                // � noter le num�ro dans la liste inverse...
                //##### Modif le 30/05/2006 : si le point a d�j� �t� num�rot�, c'est forc�ment en plat, donc forc�ment en <0, �a suffit :
                //			if ((NumLac[ind] < 0) && (NumLac[ind] >= -nblacs))
                if (NumLac[ind] < 0)
                    i++;
                    // ... si elle a des voisins...
                else if (MaMaille->GetNbVoisinsBas() > 0) 
                {
                    // Elle propage son eau et ajuste son flux d'eau (courante), et FluxEau � 0, et Zeau � Z si besoin
                    (MesFlux->*MonDeverse)(ind, MaMaille);
                    //##### Modif le 10/05/2006 : Si cette maille est un descendant de lac, il faut changer son NumLac de -10000 � 0
                    if (NumLac[ind] == -10000) NumLac[ind] = 0;
                    // Le suivant sera le suivant dans la liste des altitudes
                    i++;
                }                    // ... si pas de voisins, c'est le fond d'un lac (un de plus)
                else 
                {
                    nblacs++;
                    // On calcule le lac, renvoie true si exutoire, avec le num�ro d'o� on repart :
                    if (CalculeLac(nblacs, ind, MaMaille, redepart))
                        i = redepart;
                        // Sinon (lac ferm�) on continue la liste tranquillement.
                    else
                        i++;

                }
            }                ////// OU BIEN : si c'est une maille qui re�oit de l'eau d'un lac = "fils-lac")
            else if (NumLac[ind] == -10000) 
            {
                // Si cette maille a des voisins...
                if (MaMaille->GetNbVoisinsBas() > 0) 
                {
                    // Elle propage son eau...
                    // ... et ajuste son flux d'eau (courante), et FluxEau � 0
                    nbsuivants = (MesFlux->*MonDeverse_FilsLac)(ind, MaMaille, tabnumvoisin);
                    for (k = 0; k < nbsuivants; k++) 
                    {
                        NumLac[tabnumvoisin[k]] = -10000;
                    }
                    // L'eau du lac d�vers� est transmise
                    NumLac[ind] = 0;
                    // Le suivant sera le suivant dans la liste des altitudes
                    i++;
                }                    // ... si pas de voisins, c'est le fond d'un lac (un de plus)
                else 
                {
                    nblacs++;
                    // On calcule le lac, renvoie true si exutoire, avec le num�ro d'o� on repart :
                    if (CalculeLac(nblacs, ind, MaMaille, redepart))
                        i = redepart;
                        // Sinon (lac ferm�) on continue la liste tranquillement.
                    else
                        i++;

                }
            }                ////// OU BIEN : si c'est une maille exutoire d'un lac (� proprement parler)
            else if (NumLac[ind] == 10000) 
            {
                // Si cette maille a des voisins...
                //##### Modif le 25/07/2005 : un exutoire a forc�ment des voisins bas
                // Elle propage son eau...
                // ... et ajuste son flux d'eau (courante), et FluxEau � 0
                nbsuivants = (MesFlux->*MonDeverse_Exutoire)(ind, MaMaille, tabnumvoisin,
                        ptrNumLac, nblacs);
                for (k = 0; k < nbsuivants; k++)
                    NumLac[tabnumvoisin[k]] = -10000;
                // Le suivant sera le suivant dans la liste des altitudes
                i++;
                // L'eau du lac exut� est transmise
                // le num reste > 0 pour recalculer les pentes ensuite
                NumLac[ind] = 9999;
            }                ////// OU BIEN : si on est d�j� pass� mais qu'on ne re�oit rien d'un lac
            else i++;
        }

    }
    return nblacs;
}

//-----------------------
// D�termination d'un LAC
//-----------------------
//
// On part d'une maille du fond (0 voisin bas).
// Renvoie "true" si le lac est ouvert et a donc un exutoire
//

bool CBilanEau::CalculeLac(const int numlac, const int i, CMaille* maille,
        int& redepart) 
{

    int isuivant;
    double zsuivant;
    int j, jexut;
    unsigned int k;
    bool TrouveExutoire = false;
    int nbexutoire, iexutoire[8];

    int nbmailles_lac = 0;
    CMaille* MaMaille;
    // L'altitude du lac est celle du niveau de l'eau
    // (notamment si on remplit � nouveau un lac d�j� existant)
    double AltiLac = maille->GetAltiEau();
    // L'altitude du fond lac est aussi celle de l'eau s'il y en a d�j� un
    double AltiFond = maille->GetAltiEau();
    // dvol_lac est l'incr�ment du volume du lac au fur et � mesure qu'on le remplit,
    // ie chaque fois qu'on ajoute un point (le "suivant" : le plus bas sur le bord)
    double dvol_lac = 0.;
    // Le volume d'eau � r�partir dans le lac (au d�but pour une seule cellule)
    double vol_exces = dt * MesFlux->GetFluxEau(i);

    // Vide les listes des mailles du p�rim�tre et du milieu du lac
    BordDuLac.clear();
    MilieuDuLac.clear();
    // L'altitude de d�part est celle de la premi�re maille
    zsuivant = AltiLac;
    // Le point de d�part est le premier de la liste
    isuivant = i;

    // Boucle do car on fait la boucle au moins une fois (!)
    do {

        vol_exces -= dvol_lac*nbmailles_lac;
        AltiLac = zsuivant;

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// On ajoute le point qu'on a trouv� � la liste des points de bord du lac
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        NumLac[isuivant] = -numlac;
        BordDuLac.push_back(isuivant);
        nbmailles_lac++;
        // Si la maille ajout�e n'a pas encore �t� parcourue dans le bilan d'eau
        // (ie si on est sur un plat et que cette maille est APRES dans la liste tri�e par altitude)
        if (!DejaPasse[isuivant])
            // On ajoute l'eau re�ue par cette maille au volume du lac
            vol_exces += dt * MesFlux->GetFluxEau(isuivant) / nbmailles_lac;

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// On cherche le "suivant", le plus bas des points qui sont autour du lac
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        isuivant = -1;
        zsuivant = EPAISSEUR_INF;
        int mon_isuivant;
        double mon_zsuivant;
        for (k = 0; k < BordDuLac.size(); k++) 
        {
            //##### Modif le 30/05/2006 : il faut distinguer le suivant bas local de celui du lac :
            mon_isuivant = -1;
            mon_zsuivant = EPAISSEUR_INF;
            MaMaille = MonMaillage->GetMaille(BordDuLac[k]);
            (MonMaillage->*MonSuivantBas)(MaMaille, ptrNumLac, numlac, mon_isuivant, mon_zsuivant);
            // Si la maille n'a aucun voisin tq : NumLac[numvoisin] != -numlac
            // --> on n'est pas au bord mais au milieu
            if (mon_isuivant == -1) 
            {
                MilieuDuLac.push_back(BordDuLac[k]);
                BordDuLac.erase(BordDuLac.begin() + k);
                k--;
            }                //##### Modif le 05/05/2006 : si on est dans un lac au bord,
                //		on finit par ne rien trouver donc il faut sortir l� !
            else if (mon_isuivant < -1)
                // C'est comme pour un lac ferm� (il se vide dehors)
                return false;

            //##### Modif le 30/05/2006 : il faut distinguer le suivant bas local de celui du lac :
            // Si ce suivant bas l� est plus bas que le pr�c�dent, on le garde :
            if (mon_zsuivant < zsuivant) 
            {
                isuivant = mon_isuivant;
                zsuivant = mon_zsuivant;
            }
        }

        dvol_lac = zsuivant - AltiLac;
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// SI : le point suivant est assez haut, le lac est ferm� : on sort
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        if (dvol_lac * nbmailles_lac >= vol_exces) break;
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// SINON : chaque fois qu'on a trouv� un point � ajouter � la liste,
        ////// on regarde si ce n'est pas un EXUTOIRE
        ////// (exutoire = maille du bord du lac qui a un voisin ext�rieur au lac
        ////// d'altitude strictement inf�rieure � celle du lac)
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MaMaille = MonMaillage->GetMaille(isuivant);
        if (!MaMaille->MailleLac())
            TrouveExutoire = (MonMaillage->*MonTesteSiBas)(MaMaille, ptrNumLac, numlac, zsuivant);
        // Fin de la boucle do...while : on teste si on n'a pas trouv� d'exutoire
        // (le lac d�borde encore, on a d�j� v�rifi� qu'il n'�tait pas ferm�)
        
    } while (!TrouveExutoire);
    //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ////// Cas 1 : le lac est ferm�
    //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    // C'est le premier cas � tester car on peut avoir un lac ferm�
    // tout en ayant trouv� pour dernier point de la liste un 'exutoire' !
    // (mais qui ne sera pas dans le lac puisqu'il ne d�borde plus)
    // (NB: si on n'a pas trouv� d'exutoire c'est donc qu'on est sorti de la boucle par le test "lac ferm�")
    if (!TrouveExutoire) 
    {
        AltiLac += vol_exces / nbmailles_lac;
        // On parcourt les mailles du lac qui sont au "bord" (mais dans l'eau)
        for (k = 0; k < BordDuLac.size(); k++) 
        {
            j = BordDuLac[k];
            MaMaille = MonMaillage->GetMaille(j);
            //##### Modif le 14/10/2005 : on garde en m�moire le volume de lac (sur la maille)
            // On n'annule pas le flux d'eau, il peut reservir, si le lac persiste au pas de temps suivant
            //##### Modif le 15/05/2006 : il faut diviser par dt, car FluxEau est un flux
            MesFlux->SetFluxEau_AnnuleCourante(j, (MaMaille->SetAltiEau_GetVolumeLac(AltiLac)) / dt);
            // Il faut noter les points qui sont autour (hors-lac) et qui sont donc : au bord
            (MaMaille->*MonBordDeLac)(ptrNumLac, numlac);
        }
        // Fin du bord du lac, on fait le milieu
        for (k = 0; k < MilieuDuLac.size(); k++) 
        {
            j = MilieuDuLac[k];
            MaMaille = MonMaillage->GetMaille(j);
            //##### Modif le 14/10/2005 : on garde en m�moire le volume de lac (sur la maille)
            // On n'annule pas le flux d'eau, il peut reservir, si le lac persiste au pas de temps suivant
            //##### Modif le 15/05/2006 : il faut diviser par dt, car FluxEau est un flux
            MesFlux->SetFluxEau_AnnuleCourante(j, (MaMaille->SetAltiEau_GetVolumeLac(AltiLac)) / dt);
        }
        return false;
    }        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ////// Cas 2 : on a trouv� un exutoire (lac ouvert qui d�borde)
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    else 
    {
        //##### Modif le 31/08/2005 : jusqu'ici on n'a pu trouver qu'un seul exutoire
        //##### Modif le 21/06/2005 : l'exutoire proprement dit est isuivant, pas numvoisin.
        iexutoire[0] = isuivant;
        nbexutoire = 1;
        //##### Modif le 21/06/2005 : le lac est rempli jusqu'� l'exutoire !
        vol_exces -= dvol_lac*nbmailles_lac;
        AltiLac = zsuivant;


        //+++++++++++++++++++++++++++++++++++++++++
        ////// Si le lac N'est PAS un simple plat :
        //+++++++++++++++++++++++++++++++++++++++++
        if (AltiLac > AltiFond) 
        {
            // On parcourt les mailles du lac qui sont au bord
            for (k = 0; k < BordDuLac.size(); k++) 
            {
                j = BordDuLac[k];
                MaMaille = MonMaillage->GetMaille(j);
                //##### Modif le 05/05/2006 : peut-il vraiment y avoir des mailles sans eau, dans la mesure
                //	o� il s'agit d'un lac � exutoire ? et que cet exutoire n'est pas dans la liste : OUI,
                //	on imagine par hasard 2 points � la m�me altitude, l'un exutoire et l'autre bord� de
                //	falaises � l'ext�rieur
                if (MaMaille->GetAlti() < AltiLac) 
                {
                    //##### Modif le 14/10/2005 : on garde en m�moire le volume de lac (sur la maille)
                    // On n'annule pas le flux d'eau, il peut reservir, si le lac persiste au pas de temps suivant
                    //##### Modif le 15/05/2006 : il faut diviser par dt, car FluxEau est un flux
                    MesFlux->SetFluxEau_AnnuleCourante(j,
                            (MaMaille->SetAltiEau_GetVolumeLac(AltiLac)) / dt);
                    // Il faut noter les points qui sont autour (hors-lac) et qui sont donc : au bord
                    (MaMaille->*MonBordDeLac)(ptrNumLac, numlac);
                }                    ////// Si z = AltiLac
                else 
                {
                    NumLac[j] = numlac;
                }

            }
            // Fin du bord du lac, on fait le milieu
            for (k = 0; k < MilieuDuLac.size(); k++) 
            {
                j = MilieuDuLac[k];
                MaMaille = MonMaillage->GetMaille(j);
                //##### Modif le 14/10/2005 : on garde en m�moire le volume de lac (sur la maille)
                // On n'annule pas le flux d'eau, il peut reservir, si le lac persiste au pas de temps suivant
                //##### Modif le 15/05/2006 : il faut diviser par dt, car FluxEau est un flux
                MesFlux->SetFluxEau_AnnuleCourante(j,
                        (MaMaille->SetAltiEau_GetVolumeLac(AltiLac)) / dt);

            }
            //##### Modif le 31/08/2005 : il y a peut-�tre plus d'un exutoire
            // On s'occupe ensuite des exutoires
            redepart = nbmailles;
            for (jexut = 0; jexut < nbexutoire; jexut++) 
            {
                j = iexutoire[jexut];
                // Les exutoires sont marqu�s de fa�on � ce que
                // leur pente soit recalcul�e avec z_eau, ainsi les mailles du lac (� la m�me
                // altitude) ne seront pas consid�r�es comme 'voisins bas'
                NumLac[j] = 10000;
                // On actualise son flux (d�bordement du lac) ; l'eau courante sera actualis�e quand on repassera dessus !
                MesFlux->SetFluxEau(j, vol_exces / nbexutoire / dt);
                if (MonMaillage->GetNumListeInv(j) < redepart)
                    redepart = MonMaillage->GetNumListeInv(j);
            }
            return true;
        }
            //+++++++++++++++++++++++++++++++++++++++++
            ////// Si le lac est en fait un PLAT : (il n'a pas de bords)
            //+++++++++++++++++++++++++++++++++++++++++
        else 
        {
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ////// On ajoute le dernier point qu'on a trouv�
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            NumLac[isuivant] = -numlac;
            BordDuLac.push_back(isuivant);
            nbmailles_lac++;
            // Si la maille ajout�e n'a pas encore �t� parcourue dans le bilan d'eau
            // (ie si on est sur un plat et que cette maille est APRES dans la liste tri�e par altitude)
            if (!DejaPasse[isuivant])
                // On ajoute l'eau re�ue par cette maille au volume du lac
                vol_exces += dt * MesFlux->GetFluxEau(isuivant) / nbmailles_lac;

            for (k = 0; k < BordDuLac.size(); k++) 
            {
                j = BordDuLac[k];
                MesFlux->AnnuleFluxEau_etCourante(j);
                MonMaillage->GetMaille(j)->Ajuste_ZEau_a_Z();
            }

            for (k = 0; k < MilieuDuLac.size(); k++) 
            {
                j = MilieuDuLac[k];
                MesFlux->AnnuleFluxEau_etCourante(j);
                MonMaillage->GetMaille(j)->Ajuste_ZEau_a_Z();
            }
            ////// On s'occupe ensuite du ou des exutoire(s) :
            redepart = MonMaillage->GetNumListeInv(i) + 1;
            for (jexut = 0; jexut < nbexutoire; jexut++) 
            {
                j = iexutoire[jexut];
                // Les exutoires sont marqu�s de fa�on � ce que
                // leur pente soit recalcul�e avec z_eau, ainsi les mailles du lac (� la m�me
                // altitude) ne seront pas consid�r�es comme 'voisins bas'
                NumLac[j] = 10000;
                // On actualise leur flux d'eau
                MesFlux->SetFluxEau(j, vol_exces / double(nbexutoire) / dt);
                // On note celui qui est le plus haut dans la liste par altitudes
                // parmi les mailles d�j� parcourues... d'o� numredepart = i
                // dans le cas d'un plat de 2 mailles par exemple o� l'on ne serait pas
                // encore pass� sur la 2e qui est l'exutoire !
                if (DejaPasse[j] && (MonMaillage->GetNumListeInv(j) < redepart))
                    redepart = MonMaillage->GetNumListeInv(j);
            }
            return true;
        }
    }
}

//----------------------------------------------
// Surcharge dans le cas ou on NEGLIGE les lacs
//----------------------------------------------
//
// Les pentes ont �t� calcul�es en tout point lors des effondrements
// (m�me s'il n'y en a pas).
// Quand il y a un lac, c'est comme s'il y avait un puits...
//

int CBilanEau::BilanEau_SansLacs() 
{
    int ind;
    CMaille* MaMaille;

    nblacs = 0;
    for (int i = 0; i < nbmailles; i++) 
    {
        ind = MonMaillage->GetNumListe(i);
        MaMaille = MonMaillage->GetMaille(ind);
        // Note le num�ro de la maille i dans le classement par altitude
        MonMaillage->SetNumListeInv(i, ind);
        NumLac[ind] = 0; // ajout le 27 avril 2007. Les numeros indicatifs de lac n'etaient pas initialises a zero, donc
        // tout lac identifie a un moment donne l'etait pour le restant du calcul !!

        if (!MonMaillage->MailleTerre(ind)) // si on est sous la mer
        {
            NumLac[ind] = -1; // decembre 2018. On consid�re que sous le niveau de base impos� c'est la mer
            // et on affecte a numlac la valeur correspondante de -1
            MaMaille->SetAltiEau(MonMaillage->GetNiveauDeBase());
        } 
        else 
        {
            if (MaMaille->GetNbVoisinsBas() > 0) 
            {
                // Elle propage son eau et ajuste son flux d'eau (courante)
                (MesFlux->*MonDeverse)(ind, MaMaille);
                // Le suivant sera le suivant dans la liste des altitudes
            } 
            else 
            {
                NumLac[ind] = -2; // car -1 c'est pour la mer - modif 2019
                nblacs++;
                // Le suivant sera le suivant dans la liste des altitudes
            }
        }
    }
    return nblacs;
}


//----------------------------------
// Surcharge dans le cas d'une TERRE
// (pas de niveau de base � tester)
// On est donc sur que toutes les mailles sont au-dessus du niveau de base.
// C'est le cas purement continental, possiblement avec des lacs, mais sans mer
//----------------------------------
//

int CBilanEau::BilanEau_TERRE() 
{
    int ind, redepart;
    int i;
    int k;
    int nbsuivants;
    CMaille* MaMaille;

    //////////////////////////////////////////////////////////////////////////////////////////
    tempslac += dt;
    //////////////////////////////////////////////////////////////////////////////////////////
    for (i = 0; i < nbmailles; i++) 
    {
        DejaPasse[i] = false;
        NumLac[i] = 0;
    }
    nblacs = 0;
    i = 0;
    while (i < nbmailles) 
    {
        ind = MonMaillage->GetNumListe(i);

        MaMaille = MonMaillage->GetMaille(ind);

        if (!DejaPasse[ind]) 
        {
            MonMaillage->SetNumListeInv(i, ind);
            DejaPasse[ind] = true;
            if ((NumLac[ind] < 0) && (NumLac[ind] >= -nblacs))
                i++;
            else if (MaMaille->GetNbVoisinsBas() > 0) 
            {
                (MesFlux->*MonDeverse)(ind, MaMaille);
                //##### Modif le 10/05/2006 : Si cette maille est un descendant de lac, il faut changer son NumLac de -10000 � 0
                if (NumLac[ind] == -10000) NumLac[ind] = 0;
                i++;
            } 
            else 
            {
                nblacs++;
                if (CalculeLac(nblacs, ind, MaMaille, redepart))
                    i = redepart;
                else
                    i++;
            }
        } else if (NumLac[ind] == -10000) 
        {
            if (MaMaille->GetNbVoisinsBas() > 0) 
            {
                nbsuivants = (MesFlux->*MonDeverse_FilsLac)(ind, MaMaille, tabnumvoisin);
                for (k = 0; k < nbsuivants; k++) 
                {
                    NumLac[tabnumvoisin[k]] = -10000;
                }
                NumLac[ind] = 0;
                i++;
            } 
            else 
            {
                nblacs++;

                if (CalculeLac(nblacs, ind, MaMaille, redepart))
                    i = redepart;
                else
                    i++;

            }
        } 
        else if (NumLac[ind] == 10000) 
        {
            nbsuivants = (MesFlux->*MonDeverse_Exutoire)(ind, MaMaille, tabnumvoisin,
                    ptrNumLac, nblacs);
            for (k = 0; k < nbsuivants; k++) 
            {
                NumLac[tabnumvoisin[k]] = -10000;
            }
            i++;
            NumLac[ind] = 9999;
        } else i++;
    }

    return nblacs;
}

//-----------------------------------------------
// Surcharge dans le cas o� on NEGLIGE les lacs,
// dans le cas d'une TERRE
// (pas de niveau de base � tester)
//-----------------------------------------------
//

int CBilanEau::BilanEau_TERRE_SansLacs() 
{
    int ind;
    CMaille* MaMaille;
    nblacs = 0; // ajout le 21 juin 2007
   
    // Boucle sur la liste des mailles classees par altitudes
    for (int i = 0; i < nbmailles; i++) 
    {
        ind = MonMaillage->GetNumListe(i);
        MaMaille = MonMaillage->GetMaille(ind);
        NumLac[ind] = 0; // ajout le 22 juin 2007.
        // Note le numero de la maille i dans le classement par altitude
        MonMaillage->SetNumListeInv(i, ind);

        if (MaMaille->GetNbVoisinsBas() > 0) 
        {
            // Elle propage son eau et ajuste son flux d'eau (courante)
            (MesFlux->*MonDeverse)(ind, MaMaille);
        } 
        else 
        {
            NumLac[ind] = -2; // car -1 c'est pour la mer - modif 2019
            nblacs++;

        }

    }

    return nblacs;
}


//-----------------------------------------------
// Surcharge dans le cas o� on NEGLIGE les lacs,
// dans le cas d'une TERRE et en Calculant les hauteurs d'eau pour inondation
// (pas de niveau de base � tester)
// 30 juin 2008
// Modifs 2022 et 2023
//-----------------------------------------------
//

int CBilanEau::BilanEau_TERRE_SansLacs_CalculHeau()
{
    int ind;
    CMaille* MaMaille;
   
    
    nblacs = 0;
    for (int i = 0; i < nbmailles; i++)
    {
        ind = MonMaillage->GetNumListeEau(i); // dans le classement des mailles en fonction de leur hauteur d'eau
        MaMaille = MonMaillage->GetMaille(ind);

        // Note le numero de la maille i dans le classement par altitude
        MonMaillage->SetNumListeEauInv(i, ind);
        NumLac[ind] = 0;
       
        if (MaMaille->GetNbVoisinsBasEau() > 0)
        {
            // La maille propage son eau et ajuste son flux d'eau (courante)
            (MesFlux->DeverseSurMesVoisinsEau)(ind, MaMaille);
        }
        else
        {
            NumLac[ind] = -2; // car -1 c'est pour la mer - modif 2019
            nblacs++;
            // creation d'un robot qui va remplir les trous et eventuellement propager l'eau restante vers un exutoire 2023
            CWaterbot waterbot(MesFlux, MonMaillage, MaMaille, ind, dt);
            bool ilrestedeleau = false;
            bool exutoire = false;
            do {
                ilrestedeleau = waterbot.CombleLeTrouEtActualiseVolumeEauTransporte();
                if (ilrestedeleau)
                {
                    waterbot.DeplaceLeWaterbot();
                    exutoire = waterbot.TesteSiExutoire();
                }
            } while (ilrestedeleau == true && exutoire == false);
            // l'eau restante est gardee pour le prochain pas de temps
            if (exutoire) waterbot.ActualiseFluxEauEnAttente();
        }
       
    }
    
    // On reclasse les altitudes dans l'ordre decroissant de la surface d'eau 2023
    (MonMaillage->TriParInsertion_deMailles_Eau)();
    
    // On recalcule le voisinage car il a pu changer après l'action des waterbot 2023

    for (int i = 0; i < nbmailles; i++)
    {
        MaMaille = MonMaillage->GetMaille(i);
        PentesMaille_MultFlow_SurfaceEau(MaMaille, MonMaillage, MonGetAlti, pas3);
        if (MaMaille->GetNbVoisinsBasEau() == 0){MesFlux->SetEauCourante(i,0.);} // pour eviter qu'un trou puisse avoir une eau courante a l'issue du parcours de Waterbot. Les sediments se deposent dans les trous de la surface d'eau. Faut-il creer un volumeSedEnAttente a l'avenir ?
        // Calcul de la hauteur d'eau sur la maille et  de l'altitude de la surface de l'eau sur les pixels secs (z_ref = z )
        if (!MaMaille->MailleLacHeau())
        {
            double hauteureau = (MesFlux->*MonCalculHauteurEau)(i, MaMaille, dt);
            MaMaille->SetAltiEau(MaMaille->GetAlti() + hauteureau);
        }

    }
    
    return nblacs;
}

//-----------------------------------------------------------------
// Surcharge dans le cas sans lac et ou on calcule une hauteur d'eau, avec la possibilite d'une mer (niv base).
// D'un pas de temps a l'autre, l'eau coule sur la surface de l'eau.
// La seule diff�rence avec le cas ou on descend le long de la pente terrestre, c'est qu'on doit calculer le voisinage des
// mailles selon leur hauteur d'eau. Les puits d'eau sont trait�s en les remplissant avec la valeur moyenne des mailles au-dessus en excluant la plus haute
// (meilleur compromis après plusieurs tentatives). L'eau courante est celle qui circule sur la surface terrestre ou sur la surface de l'eau immobile des trous.
// Dans bilsed, l'erosion est calcul�e en fonction de la pente hydraulique, de sorte que les s�diments peuvent remonter des pentes dans les trous.
// Modifs 2022 et 2023
//-----------------------------------------------

int CBilanEau::BilanEau_SansLacs_CalculHeau() 
{

    int ind;
    CMaille* MaMaille;
    double altieau = 0.;

    nblacs = 0;
    for (int i = 0; i < nbmailles; i++) 
    {
        ind = MonMaillage->GetNumListeEau(i); // dans le classement des mailles en fonction de leur hauteur d'eau
        MaMaille = MonMaillage->GetMaille(ind);
        // Note le num�ro de la maille i dans le classement par altitude
        MonMaillage->SetNumListeEauInv(i, ind);
        NumLac[ind] = 0;

        if (!MonMaillage->MailleTerre(ind)) // si on est sous la mer
        {
            NumLac[ind] = -1; // decembre 2018. On consid�re que sous le niveau de base impos� c'est la mer
            // et on affecte a numlac la valeur correspondante de -1
        } 
        else 
        {
            // S'il existe des voisins plus bas, deversement du flux d'eau proportionnellement
            if (MaMaille->GetNbVoisinsBasEau() > 0)
            {
                // La maille propage son eau et ajuste son flux d'eau (courante)
                (MesFlux->DeverseSurMesVoisinsEau)(ind, MaMaille);
            }
            else
            {
                NumLac[ind] = -2; // car -1 c'est pour la mer - modif 2019
                nblacs++;
                // creation d'un robot qui va remplir les trous et eventuellement propager l'eau restante vers un exutoire 2023
                CWaterbot waterbot(MesFlux, MonMaillage, MaMaille, ind, dt);
                bool ilrestedeleau = false;
                bool exutoire = false;
                do {
                    ilrestedeleau = waterbot.CombleLeTrouEtActualiseVolumeEauTransporte();

                    if (ilrestedeleau)
                    {
                        waterbot.DeplaceLeWaterbot();
                        exutoire = waterbot.TesteSiExutoire();
                    }
                } while (ilrestedeleau == true && exutoire == false);
                // l'eau restante est gardee pour le prochain pas de temps
                if (exutoire) waterbot.ActualiseFluxEauEnAttente();
      
            }
        }
    }
    
    // On reclasse les altitudes dans l'ordre decroissant de la surface d'eau 2023
    (MonMaillage->TriParInsertion_deMailles_Eau)();
    
    // On recalcule le voisinage car il a pu changer après l'action des waterbot 2023
    for (int i = 0; i < nbmailles; i++)
    {
        MaMaille = MonMaillage->GetMaille(i);
        PentesMaille_MultFlow_SurfaceEau(MaMaille, MonMaillage, MonGetAlti, pas3);
        if (MaMaille->GetNbVoisinsBasEau() == 0){MesFlux->SetEauCourante(i,0.);} // pour eviter qu'un trou puisse avoir une eau courante a l'issue du parcours de Waterbot. Les sediments se deposent dans les trous de la surface d'eau. Faut-il creer un volumeSedEnAttente a l'avenir ?
        
        
    }

    return nblacs;

}


void CBilanEau::AfficheMaillage() 
{
    int i, j, k = -1;

    ofstream FichierZ;
    FichierZ.open("cidre_zeau.log", ios::out | ios::trunc);
    if (FichierZ.bad()) 
    {
        cerr << "Erreur lors de l'ouverture du fichier " << "'cidre_zeau.log'" << endl;
        FichierZ.close();
        return;
    }

    AfficheMessage("Altitude de l'eau");
    for (i = 0; i < MonMaillage->GetNbLig(); i++) 
    {
        for (j = 0; j < MonMaillage->GetNbCol(); j++) 
        {
            k++;
            FichierZ << setprecision(3) << setw(9)
                    << MonMaillage->GetMaille(k)->GetAltiEau() / pas2 << "  ";
        }
        FichierZ << endl;
    }
    FichierZ.close();

    k = -1;

    ofstream FichierLac;
    FichierLac.open("cidre_numlac.log", ios::out | ios::trunc);
    if (FichierLac.bad()) 
    {
        cerr << "Erreur lors de l'ouverture du fichier " << "'cidre_numlac.log'" << endl;
        FichierLac.close();
        return;
    }
    AfficheMessage("Grille de lacs");
    for (i = 0; i < MonMaillage->GetNbLig(); i++) 
    {
        for (j = 0; j < MonMaillage->GetNbCol(); j++) 
        {
            k++;
            FichierLac << setw(4) << NumLac[k] << "  ";
        }
        FichierLac << endl;
    }
    FichierLac.close();

    //	system("pause");
    return;
}
