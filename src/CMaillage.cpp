#include "CMaillage.h"

//-------------------------------------------
//
// fonctions de la classe CMaillage :
//
//-------------------------------------------

// constructeur

CMaillage::CMaillage() 
{
    int i;
    nblig = 50;
    nbcol = 50;
    nbmailles = 2500;
    double tmppas = 10.;
    pas2 = tmppas*tmppas;
    pas3 = new double[8];
    for (i = 0; i < 4; i++) pas3[i] = tmppas * pas2;
    for (i = 4; i < 8; i++) pas3[i] = tmppas * pas2 * sqrt(2.);
    TabMaille = NULL;
    Desordre = NULL;
    Liste = NULL;
    ListeInv = NULL;
    ListeEau = NULL; // 30 juin 2008
    ListeEauInv = NULL; // 30 juin 2008
    NiveauDeBase = 0.;
    temps_cycleustatic = 0.;
    TabPrecip = NULL;
    TabUplift = NULL;
    Uplift = NULL;
    jdecro = -1;
}

// copy constructeur

CMaillage::CMaillage(const CMaillage &maillage) 
{
    int i;
    nblig = maillage.nblig;
    nbcol = maillage.nbcol;
    nbmailles = maillage.nbmailles;
    pas3 = new double[8];
    for (i = 0; i < 8; i++) pas3[i] = maillage.pas3[i];
    pas2 = maillage.pas2;
    TabMaille = new CMaille*[nbmailles];
    Desordre = new int[nbmailles];
    Liste = new int[nbmailles];
    ListeInv = new int[nbmailles];
    ListeEau = new int[nbmailles]; // 30 juin 2008
    ListeEauInv = new int[nbmailles]; // 30 juin 2008
    for (i = 0; i < nbmailles; i++) 
    {
        TabMaille[i] = maillage.TabMaille[i];
        Desordre[i] = maillage.Desordre[i];
        Liste[i] = maillage.Liste[i];
        ListeEau[i] = maillage.ListeEau[i]; // 30 juin 2008
    }
    TabPrecip = maillage.TabPrecip;
    TabUplift = maillage.TabUplift;
    Uplift = maillage.Uplift;
    jdecro = maillage.jdecro;
}

// destructeur

CMaillage::~CMaillage() 
{
    int i;

    if (TabMaille != NULL) 
    {
        //for (i=0; i < nbmailles; i++) delete TabMaille[i];
        delete [] TabMaille;
    }
    if (Desordre != NULL) delete [] Desordre;
    if (Liste != NULL) delete [] Liste;
    if (ListeInv != NULL) delete [] ListeInv;
    if (ListeEau != NULL) delete [] ListeEau; // 30 juin 2008
    if (ListeEauInv != NULL) delete [] ListeEauInv; // 30 juin 2008
    if (TabPrecip != NULL) 
    {
        for (i = 0; i < nbperiodes; i++) delete [] TabPrecip[i];
        delete [] TabPrecip;
    }
    if (TabUplift != NULL) 
    {
        for (i = 0; i < nbperiodes; i++) delete [] TabUplift[i];
        delete [] TabUplift;
    }
    if (pas3 != NULL) delete [] pas3;
}

//==================
// Autres fonctions
//==================

//-------------------------------------------------------------------
// Remplit les altitudes initiales du maillage � partir d'un fichier
//-------------------------------------------------------------------

bool CMaillage::LitAltitudes(const string FichierIn) 
{
    double z;

    filebuf MonFichier;
    // Ouverture du flux d'entr�e
    if (MonFichier.open(FichierIn.c_str(), ios::in) == NULL) 
    {
        cerr << "The elevation file does not exist or can not be read !! " << FichierIn << endl;
        return false;
    } 
    else 
    {
        istream FichierEntree(&MonFichier);
        AfficheMessage("> Reading the initial elevations file : ", FichierIn, "");

        for (int k = 0; k < nbmailles; k++) 
        {
            FichierEntree >> z;
            TabMaille[k]->InitialiseLesAlti(z * pas2);
        }
    }
    // Fermeture du fichier
    MonFichier.close();
    AfficheMessage("          *** Initial elevations file read ***\n");
    return true;
}

//---------------------------------------------------------------------------
// Remplit les volumes de s�diments du maillage avec une �paisseur uniforme
//---------------------------------------------------------------------------

void CMaillage::SetSedimentUniforme(double epaisseur) 
{
    double volume = epaisseur*pas2;

    for (int k = 0; k < nbmailles; k++)
        TabMaille[k]->SetStockInitSed(volume);
    return;
}

//---------------------------------------------------------------------------
// Initialise la pile strati de chaque maille
//---------------------------------------------------------------------------

void CMaillage::InitialiseCouches(int nbstrati) 
{
    for (int k = 0; k < nbmailles; k++) 
    {
        TabMaille[k]->AllouePileCouches(nbstrati);
    }
    return;
}

//---------------------------------------------------------------------------
// Remplit les epaisseurs de bedrock du maillage de mani�re uniforme
//---------------------------------------------------------------------------

void CMaillage::SetBedrockUniforme() 
{
    for (int k = 0; k < nbmailles; k++)
        TabMaille[k]->SetCouche(0, EPAISSEUR_INF, 0);
    return;
}

//---------------------------------------------------------------------------
// Surcharge s'il y a plusieurs couches de bedrock, d'�paisseurs uniformes
//---------------------------------------------------------------------------

void CMaillage::SetBedrockUniforme(double epaisseur, int numcouche, int indlitho) 
{
    for (int k = 0; k < nbmailles; k++)
        TabMaille[k]->SetCouche(numcouche, epaisseur * pas2, indlitho);
    return;
}

//-----------------------------------------------------------------------
// Remplit les volumes de s�diments du maillage � partir d'un fichier
//-----------------------------------------------------------------------

bool CMaillage::LitEpaisseurs(const string FichierIn) 
{
    double epaisseur;
    int k = -1;

    filebuf MonFichier;
    // Ouverture du flux d'entr�e
    if (MonFichier.open(FichierIn.c_str(), ios::in) == NULL) 
    {
        cerr << "The file does not exist of cannot be read !! " << FichierIn << endl;
        MonFichier.close();
        return false;
    } 
    else 
    {
        istream FichierEntree(&MonFichier);
        AfficheMessage("> Reading the initial sediment thickness file : ", FichierIn, "");

        for (int j = 0; j < nbcol; j++) 
        {
            for (int i = 0; i < nblig; i++) 
            {
                FichierEntree >> epaisseur;
                k++;
                TabMaille[k]->SetStockInitSed(epaisseur * pas2);
            }
        }
        // Fermeture du fichier
        MonFichier.close();
        AfficheMessage("          *** Initial sediment thickness file read ***\n");
        return true;
    }
}

//------------------------------------------------------------------------------
// Remplit les �paisseurs de la couche strati du maillage � partir d'un fichier
// numcouche : num�ro de la couche dans la pile, numlitho : num�ro de la litho associ�e
//------------------------------------------------------------------------------

bool CMaillage::LitEpaisseurs(const string FichierIn, int numcouche, int numlitho) 
{
    double epaisseur;
    int k = -1;

    filebuf MonFichier;
    // Ouverture du flux d'entr�e
    if (MonFichier.open(FichierIn.c_str(), ios::in) == NULL) 
    {
        cerr << "The file does not exist or cannot be read !! !! " << FichierIn << endl;
        MonFichier.close();
        return false;
    } 
    else 
    {
        istream FichierEntree(&MonFichier);
        AfficheMessage("> Reading the initial bedrock strata thickness file : ", FichierIn, "");

        for (int j = 0; j < nbcol; j++) 
        {
            for (int i = 0; i < nblig; i++) 
            {
                FichierEntree >> epaisseur;
                k++;
                TabMaille[k]->SetCouche(numcouche, epaisseur*pas2, numlitho);
            }
        }

        // Fermeture du fichier
        MonFichier.close();
        AfficheMessage("          +++ Initial bedrock strata thickness file read +++\n");
        return true;
    }
}

//------------------------------------------------------------------------------
// Alloue la place des grilles de taux de soul�vement, une par p�riode
// en les initialisant � 0 partout tout le temps
//------------------------------------------------------------------------------

void CMaillage::AlloueGrilleUplift() 
{
    TabUplift = new GrilleDble[nbperiodes];
    for (int i = 0; i < nbperiodes; i++) 
    {
        TabUplift[i] = new double[nbmailles];
        for (int k = 0; k < nbmailles; k++) 
        {
            TabUplift[i][k] = 0.;
        }
    }
    Uplift = new double[nbmailles];
    return;
}

//----------------------------------------------------------------------------------
// Remplit la grille de soul�vement par une valeur suivant un type, pour une p�riode
//----------------------------------------------------------------------------------

bool CMaillage::SetGrilleUplift(int numperiode, ParamTecto tecto, double* uplift) 
{
    int i, j, k;
    int j1, j2;
    double alpha, uplift_ligne;

    double* vol_uplift = new double[tecto.nbzone];
    for (k = 0; k < tecto.nbzone; k++)
        vol_uplift[k] = uplift[k] * pas2;

    j1 = tecto.numzone[0];
    if (j1 < 1) 
    {
        j1 = 1;
        AfficheMessage("Wrong number of first line of tectonic zone , set to", j1);
    }
    if (j1 > nblig) 
    {
        AfficheMessage("Problem in the numbering of tectonic zone, the first limit to large ", j1);
        return false;
    }
    for (k = 0; k < tecto.nbzone; k++) 
    {
        j2 = tecto.numzone[k + 1];
        if (j2 > nblig) 
        {
            j2 = nblig;
            AfficheMessage("Wrong number of last line of tectonic zone , set to", j2);
        }
        if (j2 <= j1) 
        {
            AfficheMessage("Problem in the numbering of tectonic zone: empty zone");
            return false;
        }
        for (j = j1; j <= j2; j++) 
        {
            // Taux de soul�vement : - uniforme
            if (tecto.type_uplift[k] == creneau) 
            {
                for (i = 1; i <= nbcol; i++)
                    TabUplift[numperiode][(j - 1) * nbcol + i - 1] = vol_uplift[k];
            }                // ... gaussien
            else if (tecto.type_uplift[k] == gaussienne) 
            {
                alpha = double(j - j1) / double(j2 - j1);
                uplift_ligne = vol_uplift[k] * exp(-alpha * alpha);
                for (i = 1; i <= nbcol; i++)
                    TabUplift[numperiode][(j - 1) * nbcol + i - 1] = uplift_ligne;
            }                // ... lin�airement croissant (de 0 [en j1] � taux_uplift [en j2])
            else if (tecto.type_uplift[k] == croissant) 
            {
                alpha = double(j - j1) / double(j2 - j1);
                uplift_ligne = vol_uplift[k] * alpha;
                for (i = 1; i <= nbcol; i++)
                    TabUplift[numperiode][(j - 1) * nbcol + i - 1] = uplift_ligne;
            } 
            else // lineairement decroissant
            {
                alpha = double(j - j1) / double(j2 - j1);
                uplift_ligne = vol_uplift[k] - vol_uplift[k] * alpha;
                for (i = 1; i <= nbcol; i++)
                    TabUplift[numperiode][(j - 1) * nbcol + i - 1] = uplift_ligne;
            }
        }
        j1 = j2;
        if (j2 == nblig) return true;
    }

    delete [] vol_uplift;
    return true;
}

//------------------------------------------------------------------------------
// Remplit la grille de soul�vement d'une p�riode � partir d'un fichier
// numperiode : num�ro de la periode
//------------------------------------------------------------------------------

bool CMaillage::LitGrilleUplift(const string FichierIn, int numperiode) 
{
    double uplift;
    int k = -1;

    filebuf MonFichier;
    // Ouverture du flux d'entr�e
    if (MonFichier.open(FichierIn.c_str(), ios::in) == NULL) 
    {
        cerr << "The file does not exist or cannot be read !! " << FichierIn << endl;
        MonFichier.close();
        return false;
    } 
    else 
    {
        istream FichierEntree(&MonFichier);
        AfficheMessage("> Reading the tectonic file : ", FichierIn);

        for (int j = 0; j < nbcol; j++) 
        {
            for (int i = 0; i < nblig; i++) 
            {
                FichierEntree >> uplift;
                k++;
                TabUplift[numperiode][k] = uplift*pas2;
            }
        }
        // Fermeture du fichier
        MonFichier.close();
        AfficheMessage("          +++ Tectonic file read +++\n");
        return true;
    }
}

//-----------------------------------------------------------------------
// Actualise la grille de soul�vement selon la p�riode et le pas de temps
// et le niveau de base en fct de la p�riode
//-----------------------------------------------------------------------

void CMaillage::ActualiseUplift(const int &numperiode, const double &dt) 
{
    for (int k = 0; k < nbmailles; k++)
        Uplift[k] = TabUplift[numperiode][k] * dt;
    nivbase.NivBase = nivbase.niveau[numperiode];
    NiveauDeBase = nivbase.NivBase * pas2; // multiplication par pas2 en decembre 2018
    return;
}

//----------------------------------------------------------------------
// initialise les dimensions du maillage et alloue le tableau de mailles
// + calcule la topologie des mailles : les num�ros des voisins
//---------------------------------------------------------------

void CMaillage::InitialiseEspaceTopologie(int nv_nbcol, int nv_nblig, double nv_pas)
//
//			-------------------			-------------------------------
//			|  7  |  3  |  6  |			|k-nbcol-1| k-nbcol |k-nbcol+1|
//			-------------------			-------------------------------
//			|  4  |     |  2  |			|   k-1   |    k    |   k+1   |
//			-------------------			-------------------------------
//			|  8  |  1  |  5  |			|k+nbcol-1| k+nbcol |k+nbcol+1|
//			-------------------			-------------------------------
//
///// NB: ne pas oublier que les indices du tableau vont de 0 � nblig*ncol-1
///// donc il faut ajouter des "-1" partout dans le calcul des num�ros
//
{
    int i, j, k;

    // Les mailles d'abord, le pas
    // ---------------------------
    nbcol = nv_nbcol;
    nblig = nv_nblig;
    nbmailles = nbcol*nblig;
    double tmppas3 = nv_pas * nv_pas*nv_pas;
    for (i = 0; i < 4; i++) pas3[i] = tmppas3;
    for (i = 4; i < 8; i++) pas3[i] = tmppas3 * sqrt(2.);
    pas2 = nv_pas*nv_pas;
    TabMaille = new CMaille*[nbmailles];
    Liste = new int[nbmailles];
    ListeInv = new int[nbmailles];
    ListeEau = new int[nbmailles]; // 30 juin 2008
    ListeEauInv = new int[nbmailles]; // 30 juin 2008
    for (i = 0; i < nbmailles; i++) 
    {
        TabMaille[i] = new CMaille;
        Liste[i] = i;
        ListeInv[i] = -1;
        ListeEau[i] = i; // 30 juin 2008
        ListeEauInv[i] = -1; // 30 juin 2008
    }

    // La liste non tri�e par d�faut :
    // -----------------------------
    Desordre = new int[nbmailles];
    int* tmptab;
    tmptab = new int[nbmailles];
    for (i = 0; i < nbmailles; i++) 
    {
        tmptab[i] = rand() % nbmailles;
        Desordre[i] = i;
    }
    TriInsertion(tmptab, Desordre, nbmailles);
    delete [] tmptab;

    // Les Voisins :
    // -----------
    // Sans distinction, en tout point pareil
    float x, y;
    float pas_fl = (float) nv_pas;
    x = y = pas_fl / 2.;
    for (i = 1; i <= nbcol; i++) 
    {
        for (j = nblig; j >= 1; j--) 
        {
            k = ((j - 1) * nbcol + i) - 1;
            TabMaille[k]->SetNumMaille(k);
            TabMaille[k]->SetCoordonnees(x, y);
            TabMaille[k]->SetNumVoisin(1, k + nbcol);
            TabMaille[k]->SetNumVoisin(2, k + 1);
            TabMaille[k]->SetNumVoisin(3, k - nbcol);
            TabMaille[k]->SetNumVoisin(4, k - 1);
            TabMaille[k]->SetNumVoisin(5, k + nbcol + 1);
            TabMaille[k]->SetNumVoisin(6, k - nbcol + 1);
            TabMaille[k]->SetNumVoisin(7, k - nbcol - 1);
            TabMaille[k]->SetNumVoisin(8, k + nbcol - 1);

            y += pas_fl;
        }
        x += pas_fl;
        y = pas_fl / 2.;
    }
    return;
}

//----------------------------------------------------------------------
// initialise les dimensions du maillage et alloue le tableau de mailles
// + calcule la topologie des mailles : les num�ros des voisins
// + determine les voisins lateraux perpendiculaires a chacune des 8 directions (pour erosion laterale)
// 7 aout 2008
//---------------------------------------------------------------

void CMaillage::InitialiseEspaceTopologieVoisinLateral(int nv_nbcol, int nv_nblig, double nv_pas)
//
//			-------------------			-------------------------------
//			|  7  |  3  |  6  |			|k-nbcol-1| k-nbcol |k-nbcol+1|
//			-------------------			-------------------------------
//			|  4  |     |  2  |			|   k-1   |    k    |   k+1   |
//			-------------------			-------------------------------
//			|  8  |  1  |  5  |			|k+nbcol-1| k+nbcol |k+nbcol+1|
//			-------------------			-------------------------------
//
///// NB: ne pas oublier que les indices du tableau vont de 0 � nblig*ncol-1
///// donc il faut ajouter des "-1" partout dans le calcul des num�ros
//
{
    int i, j, k;

    // Les mailles d'abord, le pas
    // ---------------------------
    nbcol = nv_nbcol;
    nblig = nv_nblig;
    nbmailles = nbcol*nblig;
    double tmppas3 = nv_pas * nv_pas*nv_pas;
    for (i = 0; i < 4; i++) pas3[i] = tmppas3;
    for (i = 4; i < 8; i++) pas3[i] = tmppas3 * sqrt(2.);
    pas2 = nv_pas*nv_pas;
    TabMaille = new CMaille*[nbmailles];
    Liste = new int[nbmailles];
    ListeInv = new int[nbmailles];
    ListeEau = new int[nbmailles]; // 30 juin 2008
    ListeEauInv = new int[nbmailles]; // 30 juin 2008
    for (i = 0; i < nbmailles; i++) 
    {
        TabMaille[i] = new CMaille;
        Liste[i] = i;
        ListeInv[i] = -1;
        ListeEau[i] = i; // 30 juin 2008
        ListeEauInv[i] = -1; // 30 juin 2008
    }

    // La liste non tri�e par d�faut :
    // -----------------------------
    Desordre = new int[nbmailles];
    int* tmptab;
    tmptab = new int[nbmailles];
    for (i = 0; i < nbmailles; i++) 
    {
        tmptab[i] = rand() % nbmailles;
        Desordre[i] = i;
    }
    //TriInsertion(tmptab,Desordre,nbmailles); // commente le 23 mars. Tres long pour grandes grilles et utilit� pas verifiee
    delete [] tmptab;

    // Les Voisins :
    // -----------
    // Sans distinction, en tout point pareil
    float x, y;
    float pas_fl = (float) nv_pas;
    x = y = pas_fl / 2.;
    for (i = 1; i <= nbcol; i++) 
    {
        for (j = nblig; j >= 1; j--) 
        {
            k = ((j - 1) * nbcol + i) - 1;
            TabMaille[k]->SetNumMaille(k);
            TabMaille[k]->SetCoordonnees(x, y);
            TabMaille[k]->SetNumVoisin(1, k + nbcol);
            TabMaille[k]->SetNumVoisin(2, k + 1);
            TabMaille[k]->SetNumVoisin(3, k - nbcol);
            TabMaille[k]->SetNumVoisin(4, k - 1);
            TabMaille[k]->SetNumVoisin(5, k + nbcol + 1);
            TabMaille[k]->SetNumVoisin(6, k - nbcol + 1);
            TabMaille[k]->SetNumVoisin(7, k - nbcol - 1);
            TabMaille[k]->SetNumVoisin(8, k + nbcol - 1);

            y += pas_fl;
        }
        x += pas_fl;
        y = pas_fl / 2.;
    }

    // voisins lateraux maintenant :
    for (i = 1; i <= nbcol; i++) 
    {
        for (j = nblig; j >= 1; j--) 
        {
            k = ((j - 1) * nbcol + i) - 1;

            TabMaille[k]->SetNumVoisinLateral(1, 1, k - 1);
            TabMaille[k]->SetNumVoisinLateral(1, 2, k + 1);
            TabMaille[k]->SetNumVoisinLateral(2, 1, k + nbcol);
            TabMaille[k]->SetNumVoisinLateral(2, 2, k - nbcol);
            TabMaille[k]->SetNumVoisinLateral(3, 1, k + 1);
            TabMaille[k]->SetNumVoisinLateral(3, 2, k - 1);
            TabMaille[k]->SetNumVoisinLateral(4, 1, k - nbcol);
            TabMaille[k]->SetNumVoisinLateral(4, 2, k + nbcol);
            TabMaille[k]->SetNumVoisinLateral(5, 1, k + nbcol);
            TabMaille[k]->SetNumVoisinLateral(5, 2, k + 1);
            TabMaille[k]->SetNumVoisinLateral(6, 1, k + 1);
            TabMaille[k]->SetNumVoisinLateral(6, 2, k - nbcol);
            TabMaille[k]->SetNumVoisinLateral(7, 1, k - nbcol);
            TabMaille[k]->SetNumVoisinLateral(7, 2, k - 1);
            TabMaille[k]->SetNumVoisinLateral(8, 1, k - 1);
            TabMaille[k]->SetNumVoisinLateral(8, 2, k + nbcol);
        }
    }

    return;
}

//-------------------------------------------------------------------
// Alloue les voisinages "bas" selon le type de calcul de pente
//-------------------------------------------------------------------

void CMaillage::AlloueLesVoisinages(PENTE type_pente) 
{
    int k;
    switch (type_pente) 
    {
            // plus qu'un seul type : multiple flow . decembre 2018
        case multiple:
            for (k = 1; k <= nbmailles; k++) 
            {
                TabMaille[k - 1]->AlloueVoisinageMultiple();
                TabMaille[k - 1]->AlloueVoisinageMultipleEau(); // 30 juin 2008
            }
            break;
    }
    return;
}

//---------------------------------------------------------------
// calcule la topologie des mailles : les num�ros des voisins
//---------------------------------------------------------------

void CMaillage::AffineTopologie(TLimites Limites)
//
//		J=1  ---------------- 
//		 |  |     BORD 3     |
//		 |  | B            B |
//		 |  | O            O |
//		 |  | R            R |
//		 |  | D            D |
//		 |  |	             |
//		 |  | 1            2 |
//		 v  |     BORD 4     |
//		 NY  ---------------- 
//		   I=1 -----------> NX
//
///// NB: ne pas oublier que les indices du tableau vont de 0 � nblig*ncol-1
///// donc il faut ajouter des "-1" partout dans le calcul des num�ros
//
{
    int i, j, k;

    // Sym�trie : les num�ros voisins hors-bord sont
    // sym�triques, en NEGATIF par rapport � l'axe :

    // Colonne 1
    i = 1;
    for (j = 1; j <= nblig; j++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(Limites.Type_bord[0]);
        TabMaille[k]->SetBord();
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[0]);
        // On est sur le bord OUEST :
        //		[4] = -[2] ; [7] = -[6] ; [8] = -[5]
        TabMaille[k]->SetNumVoisin(4, -(k + 1));
        TabMaille[k]->SetNumVoisin(7, -(k - nbcol + 1));
        TabMaille[k]->SetNumVoisin(8, -(k + nbcol + 1));
    }

    // Derni�re colonne (nbcol)
    i = nbcol;
    for (j = 1; j <= nblig; j++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(Limites.Type_bord[1]);
        TabMaille[k]->SetBord();
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[1]);
        // On est sur le bord EST :
        //		[2] = -[4] ; [5] = -[8] ; [6] = -[7]
        TabMaille[k]->SetNumVoisin(2, -(k - 1));
        TabMaille[k]->SetNumVoisin(5, -(k + nbcol - 1));
        TabMaille[k]->SetNumVoisin(6, -(k - nbcol - 1));
    }


    // Ligne 1 :
    j = 1;
    for (i = 1; i <= nbcol; i++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(Limites.Type_bord[2]);
        TabMaille[k]->SetBord();
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[2]);
        // On est sur le bord NORD :
        //		[3] = -[1] ; [6] = -[5] ; [7] = -[8]
        TabMaille[k]->SetNumVoisin(3, -(k + nbcol));
        TabMaille[k]->SetNumVoisin(6, -(k + nbcol + 1));
        TabMaille[k]->SetNumVoisin(7, -(k + nbcol - 1));
    }

    // Derni�re ligne (nblig) :
    j = nblig;
    for (i = 1; i <= nbcol; i++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(Limites.Type_bord[3]);
        TabMaille[k]->SetBord();
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[3]);
        // On est sur le bord SUD :
        //		[1] = -[3] ; [5] = -[6] ; [8] = -[7]
        TabMaille[k]->SetNumVoisin(1, -(k - nbcol));
        TabMaille[k]->SetNumVoisin(5, -(k - nbcol + 1));
        TabMaille[k]->SetNumVoisin(8, -(k - nbcol - 1));
    }


    //	BOUCLES
    //	-------
    int k1;
    int k2;
    // Bords boucl�s EST-OUEST
    if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[1] == boucle)) 
    {
        k1 = 0;
        k2 = nbcol - 1;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord OUEST (colonne 1, i=1)
            TabMaille[k1]->SetNumVoisin(4, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 - nbcol);
            TabMaille[k1]->SetNumVoisin(8, k2 + nbcol);
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k2]->SetNumVoisin(2, k1);
            TabMaille[k2]->SetNumVoisin(5, k1 + nbcol);
            TabMaille[k2]->SetNumVoisin(6, k1 - nbcol);
            // Passage � la ligne suivante
            k1 += nbcol;
            k2 += nbcol;
        }
    }        // Bords boucl�s NORD-SUD
    else if ((Limites.Type_bord[2] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = 0;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nbcol; i++) {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nbcol - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisin(3, k2);
            TabMaille[k1]->SetNumVoisin(6, k2 + 1);
            TabMaille[k1]->SetNumVoisin(7, k2 - 1);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisin(1, k1);
            TabMaille[k2]->SetNumVoisin(5, k1 + 1);
            TabMaille[k2]->SetNumVoisin(8, k1 - 1);
            // Passage � la ligne suivante
            k1 += 1;
            k2 += 1;
        }
    }        // Bords boucl�s NORD-OUEST
    else if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[2] == boucle)) 
    {
        k1 = 0;
        k2 = 0;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisin(3, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 - nbcol);
            TabMaille[k1]->SetNumVoisin(6, k2 + nbcol);
            // Bord OUEST (colonne 1, i=1)
            TabMaille[k2]->SetNumVoisin(4, k1);
            TabMaille[k2]->SetNumVoisin(7, k1 - 1);
            TabMaille[k2]->SetNumVoisin(8, k1 + 1);
            // Passage � la ligne suivante
            k1 += 1;
            k2 += nbcol;
        }
    }        // Bords boucl�s NORD-EST
    else if ((Limites.Type_bord[1] == boucle) && (Limites.Type_bord[2] == boucle)) 
    {
        k1 = nbcol - 1;
        k2 = nbcol - 1;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisin(3, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 + nbcol);
            TabMaille[k1]->SetNumVoisin(6, k2 - nbcol);
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k2]->SetNumVoisin(2, k1);
            TabMaille[k2]->SetNumVoisin(6, k1 + 1);
            TabMaille[k2]->SetNumVoisin(5, k1 - 1);
            // Passage � la ligne suivante
            k1 -= 1;
            k2 += nbcol;
        }
    }        // Bords boucl�s SUD-OUEST
    else if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = (nblig - 1) * nbcol;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord OUEST (colonne 1, i=1)
            TabMaille[k1]->SetNumVoisin(4, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 + 1);
            TabMaille[k1]->SetNumVoisin(8, k2 - 1);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisin(1, k1);
            TabMaille[k2]->SetNumVoisin(5, k1 - nbcol);
            TabMaille[k2]->SetNumVoisin(8, k1 + nbcol);
            // Passage � la ligne suivante
            k1 -= nbcol;
            k2 += 1;
        }
    }        // Bords boucl�s SUD-EST
    else if ((Limites.Type_bord[1] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = nbcol - 1;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k1]->SetNumVoisin(4, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 - 1);
            TabMaille[k1]->SetNumVoisin(8, k2 + 1);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisin(1, k1);
            TabMaille[k2]->SetNumVoisin(5, k1 + nbcol);
            TabMaille[k2]->SetNumVoisin(8, k1 - nbcol);
            // Passage � la ligne suivante
            k1 += nbcol;
            k2 += 1;
        }
    }


    // 4 flux nuls : idem - l'exutoire est alti_fixe
    if (Limites.Type_bord[0] == flux_nul && Limites.Type_bord[1] == flux_nul
        && Limites.Type_bord[2] == flux_nul && Limites.Type_bord[3] == flux_nul) 
    {
        i = Limites.ij_exut[0];
        j = Limites.ij_exut[1];
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(alti_fixe);
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[0]);
    }

    // Coin en HAUT � GAUCHE : i=1; j=1;
    k = 0;
    TabMaille[k]->SetNumVoisin(7, -(k + nbcol + 1));

    // Coin en HAUT � DROITE : i=nbcol; j=1;
    k = nbcol - 1;
    TabMaille[k]->SetNumVoisin(6, -(k + nbcol - 1));

    // Coin en BAS � GAUCHE : i=1; j=nblig;
    k = (nblig - 1) * nbcol;
    TabMaille[k]->SetNumVoisin(8, -(k - nbcol + 1));

    // Coin en BAS � DROITE : i=nbcol; j=nblig;
    k = (nblig * nbcol) - 1;
    TabMaille[k]->SetNumVoisin(5, -(k - nbcol - 1));

    return;
}

//---------------------------------------------------------------
// calcule la topologie des mailles : les num�ros des voisins
// et ceux des voisins lateraux pour les bords
// 7 aout 2008
//---------------------------------------------------------------

void CMaillage::AffineTopologieVoisinLateral(TLimites Limites)
//
//		J=1  ---------------- 
//		 |  |     BORD 3     |
//		 |  | B            B |
//		 |  | O            O |
//		 |  | R            R |
//		 |  | D            D |
//		 |  |	             |
//		 |  | 1            2 |
//		 v  |     BORD 4     |
//		 NY  ---------------- 
//		   I=1 -----------> NX
//
///// NB: ne pas oublier que les indices du tableau vont de 0 � nblig*ncol-1
///// donc il faut ajouter des "-1" partout dans le calcul des num�ros
//
{
    int i, j, k;

    //-----------------------------------
    // D'abord pour les voisins  
    //-----------------------------------


    // Sym�trie : les num�ros voisins hors-bord sont
    // sym�triques, en NEGATIF par rapport � l'axe :

    // Colonne 1
    i = 1;
    for (j = 1; j <= nblig; j++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(Limites.Type_bord[0]);
        TabMaille[k]->SetBord();
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[0]);
        // On est sur le bord OUEST :
        //		[4] = -[2] ; [7] = -[6] ; [8] = -[5]
        TabMaille[k]->SetNumVoisin(4, -(k + 1));
        TabMaille[k]->SetNumVoisin(7, -(k - nbcol + 1));
        TabMaille[k]->SetNumVoisin(8, -(k + nbcol + 1));
    }

    // Derni�re colonne (nbcol)
    i = nbcol;
    for (j = 1; j <= nblig; j++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(Limites.Type_bord[1]);
        TabMaille[k]->SetBord();
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[1]);
        // On est sur le bord EST :
        //		[2] = -[4] ; [5] = -[8] ; [6] = -[7]
        TabMaille[k]->SetNumVoisin(2, -(k - 1));
        TabMaille[k]->SetNumVoisin(5, -(k + nbcol - 1));
        TabMaille[k]->SetNumVoisin(6, -(k - nbcol - 1));
    }

    // temporaire pour Pyrenees
    /*for (j=1; j <= 85; j++)
    {
            k = ( (j-1)*nbcol + nbcol )  - 1;
            TabMaille[k]->SetTypeBord(flux_nul);
    }*/
    // fin temporaire pour Pyrenees

    // Ligne 1 :
    j = 1;
    for (i = 1; i <= nbcol; i++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(Limites.Type_bord[2]);
        TabMaille[k]->SetBord();
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[2]);
        // On est sur le bord NORD :
        //		[3] = -[1] ; [6] = -[5] ; [7] = -[8]
        TabMaille[k]->SetNumVoisin(3, -(k + nbcol));
        TabMaille[k]->SetNumVoisin(6, -(k + nbcol + 1));
        TabMaille[k]->SetNumVoisin(7, -(k + nbcol - 1));
    }

    // Derni�re ligne (nblig) :
    j = nblig;
    for (i = 1; i <= nbcol; i++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(Limites.Type_bord[3]);
        TabMaille[k]->SetBord();
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[3]);
        // On est sur le bord SUD :
        //		[1] = -[3] ; [5] = -[6] ; [8] = -[7]
        TabMaille[k]->SetNumVoisin(1, -(k - nbcol));
        TabMaille[k]->SetNumVoisin(5, -(k - nbcol + 1));
        TabMaille[k]->SetNumVoisin(8, -(k - nbcol - 1));
    }


    //	BOUCLES
    //	-------
    int k1;
    int k2;
    // Bords boucl�s EST-OUEST
    if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[1] == boucle)) 
    {
        k1 = 0;
        k2 = nbcol - 1;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord OUEST (colonne 1, i=1)
            TabMaille[k1]->SetNumVoisin(4, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 - nbcol);
            TabMaille[k1]->SetNumVoisin(8, k2 + nbcol);
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k2]->SetNumVoisin(2, k1);
            TabMaille[k2]->SetNumVoisin(5, k1 + nbcol);
            TabMaille[k2]->SetNumVoisin(6, k1 - nbcol);
            // Passage � la ligne suivante
            k1 += nbcol;
            k2 += nbcol;
        }
    }        // Bords boucl�s NORD-SUD
    else if ((Limites.Type_bord[2] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = 0;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nbcol; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nbcol - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisin(3, k2);
            TabMaille[k1]->SetNumVoisin(6, k2 + 1);
            TabMaille[k1]->SetNumVoisin(7, k2 - 1);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisin(1, k1);
            TabMaille[k2]->SetNumVoisin(5, k1 + 1);
            TabMaille[k2]->SetNumVoisin(8, k1 - 1);
            // Passage � la ligne suivante
            k1 += 1;
            k2 += 1;
        }
    }        // Bords boucl�s NORD-OUEST
    else if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[2] == boucle)) 
    {
        k1 = 0;
        k2 = 0;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisin(3, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 - nbcol);
            TabMaille[k1]->SetNumVoisin(6, k2 + nbcol);
            // Bord OUEST (colonne 1, i=1)
            TabMaille[k2]->SetNumVoisin(4, k1);
            TabMaille[k2]->SetNumVoisin(7, k1 - 1);
            TabMaille[k2]->SetNumVoisin(8, k1 + 1);
            // Passage � la ligne suivante
            k1 += 1;
            k2 += nbcol;
        }
    }        // Bords boucl�s NORD-EST
    else if ((Limites.Type_bord[1] == boucle) && (Limites.Type_bord[2] == boucle)) 
    {
        k1 = nbcol - 1;
        k2 = nbcol - 1;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisin(3, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 + nbcol);
            TabMaille[k1]->SetNumVoisin(6, k2 - nbcol);
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k2]->SetNumVoisin(2, k1);
            TabMaille[k2]->SetNumVoisin(6, k1 + 1);
            TabMaille[k2]->SetNumVoisin(5, k1 - 1);
            // Passage � la ligne suivante
            k1 -= 1;
            k2 += nbcol;
        }
    }        // Bords boucl�s SUD-OUEST
    else if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = (nblig - 1) * nbcol;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nblig; i++) {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord OUEST (colonne 1, i=1)
            TabMaille[k1]->SetNumVoisin(4, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 + 1);
            TabMaille[k1]->SetNumVoisin(8, k2 - 1);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisin(1, k1);
            TabMaille[k2]->SetNumVoisin(5, k1 - nbcol);
            TabMaille[k2]->SetNumVoisin(8, k1 + nbcol);
            // Passage � la ligne suivante
            k1 -= nbcol;
            k2 += 1;
        }
    }        // Bords boucl�s SUD-EST
    else if ((Limites.Type_bord[1] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = nbcol - 1;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nblig; i++) 
        {
            // Les coins de la grille sont sur d'autres bords
            if ((i > 0) && (i < (nblig - 1))) 
            {
                TabMaille[k1]->SetTypeBord(pas_bord);
                TabMaille[k2]->SetTypeBord(pas_bord);
                TabMaille[k1]->SetPasBord();
                TabMaille[k2]->SetPasBord();
            }
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k1]->SetNumVoisin(4, k2);
            TabMaille[k1]->SetNumVoisin(7, k2 - 1);
            TabMaille[k1]->SetNumVoisin(8, k2 + 1);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisin(1, k1);
            TabMaille[k2]->SetNumVoisin(5, k1 + nbcol);
            TabMaille[k2]->SetNumVoisin(8, k1 - nbcol);
            // Passage � la ligne suivante
            k1 += nbcol;
            k2 += 1;
        }
    }


    // 4 flux nuls : idem - l'exutoire est alti_fixe
    if (Limites.Type_bord[0] == flux_nul && Limites.Type_bord[1] == flux_nul
        && Limites.Type_bord[2] == flux_nul && Limites.Type_bord[3] == flux_nul) 
    {
        i = Limites.ij_exut[0];
        j = Limites.ij_exut[1];
        k = ((j - 1) * nbcol + i) - 1;
        TabMaille[k]->SetTypeBord(alti_fixe);
        TabMaille[k]->SetBaseBord(pas2 * Limites.base[0]);
    }

    // Coin en HAUT � GAUCHE : i=1; j=1;
    k = 0;
    TabMaille[k]->SetNumVoisin(7, -(k + nbcol + 1));

    // Coin en HAUT � DROITE : i=nbcol; j=1;
    k = nbcol - 1;
    TabMaille[k]->SetNumVoisin(6, -(k + nbcol - 1));

    // Coin en BAS � GAUCHE : i=1; j=nblig;
    k = (nblig - 1) * nbcol;
    TabMaille[k]->SetNumVoisin(8, -(k - nbcol + 1));

    // Coin en BAS � DROITE : i=nbcol; j=nblig;
    k = (nblig * nbcol) - 1;
    TabMaille[k]->SetNumVoisin(5, -(k - nbcol - 1));


    //-----------------------------------
    // Pour les voisins lateraux maintenant
    //-----------------------------------

    // Sym�trie : les num�ros voisins hors-bord sont
    // sym�triques, en NEGATIF par rapport � l'axe :

    // Colonne 1
    i = 1;
    for (j = 1; j <= nblig; j++) 
    {
        k = ((j - 1) * nbcol + i) - 1;
        // On est sur le bord OUEST :
        //		
        TabMaille[k]->SetNumVoisinLateral(1, 1, -(k + 1));
        TabMaille[k]->SetNumVoisinLateral(3, 2, -(k + 1));
        TabMaille[k]->SetNumVoisinLateral(7, 2, -(k + 1));
        TabMaille[k]->SetNumVoisinLateral(8, 1, -(k + 1));
    }

    // Derni�re colonne (nbcol)
    i = nbcol;
    for (j = 1; j <= nblig; j++) 
    {
        k = ((j - 1) * nbcol + i) - 1;

        // On est sur le bord EST :
        TabMaille[k]->SetNumVoisinLateral(1, 2, -(k - 1));
        TabMaille[k]->SetNumVoisinLateral(3, 1, -(k - 1));
        TabMaille[k]->SetNumVoisinLateral(5, 2, -(k - 1));
        TabMaille[k]->SetNumVoisinLateral(6, 1, -(k - 1));
    }

    // Ligne 1 :
    j = 1;
    for (i = 1; i <= nbcol; i++) 
    {
        k = ((j - 1) * nbcol + i) - 1;

        // On est sur le bord NORD :
        TabMaille[k]->SetNumVoisinLateral(2, 2, -(k + nbcol));
        TabMaille[k]->SetNumVoisinLateral(4, 1, -(k + nbcol));
        TabMaille[k]->SetNumVoisinLateral(6, 2, -(k + nbcol));
        TabMaille[k]->SetNumVoisinLateral(7, 1, -(k + nbcol));
    }

    // Derni�re ligne (nblig) :
    j = nblig;
    for (i = 1; i <= nbcol; i++) 
    {
        k = ((j - 1) * nbcol + i) - 1;

        // On est sur le bord SUD :
        TabMaille[k]->SetNumVoisinLateral(2, 1, -(k - nbcol));
        TabMaille[k]->SetNumVoisinLateral(4, 2, -(k - nbcol));
        TabMaille[k]->SetNumVoisinLateral(5, 1, -(k - nbcol));
        TabMaille[k]->SetNumVoisinLateral(8, 2, -(k - nbcol));
    }



    //	BOUCLES
    //	-------

    // Bords boucl�s EST-OUEST
    if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[1] == boucle)) 
    {
        k1 = 0;
        k2 = nbcol - 1;
        for (i = 0; i < nblig; i++) 
        {

            // Bord OUEST (colonne 1, i=1)
            TabMaille[k1]->SetNumVoisinLateral(7, 2, k2);
            TabMaille[k1]->SetNumVoisinLateral(8, 1, k2);
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k2]->SetNumVoisinLateral(5, 2, k1);
            TabMaille[k2]->SetNumVoisinLateral(6, 1, k1);
            // Passage � la ligne suivante
            k1 += nbcol;
            k2 += nbcol;
        }
    }        // Bords boucl�s NORD-SUD
    else if ((Limites.Type_bord[2] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = 0;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nbcol; i++) 
        {

            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisinLateral(6, 2, k2);
            TabMaille[k1]->SetNumVoisinLateral(7, 1, k2);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisinLateral(5, 1, k1);
            TabMaille[k2]->SetNumVoisinLateral(8, 2, k1);
            // Passage � la ligne suivante
            k1 += 1;
            k2 += 1;
        }
    }        // Bords boucl�s NORD-OUEST
    else if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[2] == boucle)) 
    {
        k1 = 0;
        k2 = 0;
        for (i = 0; i < nblig; i++) 
        {

            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisinLateral(7, 1, k2);
            TabMaille[k1]->SetNumVoisinLateral(6, 2, k2);
            // Bord OUEST (colonne 1, i=1)
            TabMaille[k2]->SetNumVoisinLateral(7, 2, k1);
            TabMaille[k2]->SetNumVoisinLateral(8, 1, k1);
            // Passage � la ligne suivante
            k1 += 1;
            k2 += nbcol;
        }
    }        // Bords boucl�s NORD-EST
    else if ((Limites.Type_bord[1] == boucle) && (Limites.Type_bord[2] == boucle)) 
    {
        k1 = nbcol - 1;
        k2 = nbcol - 1;

        for (i = 0; i < nblig; i++) 
        {
            // Bord NORD (ligne 1, j=1)
            TabMaille[k1]->SetNumVoisinLateral(7, 1, k2);
            TabMaille[k1]->SetNumVoisinLateral(6, 2, k2);
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k2]->SetNumVoisinLateral(6, 1, k1);
            TabMaille[k2]->SetNumVoisinLateral(5, 2, k1);
            // Passage � la ligne suivante
            k1 -= 1;
            k2 += nbcol;
        }
    }        // Bords boucl�s SUD-OUEST
    else if ((Limites.Type_bord[0] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = (nblig - 1) * nbcol;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nblig; i++) 
        {
            // Bord OUEST (colonne 1, i=1)
            TabMaille[k1]->SetNumVoisinLateral(7, 2, k2);
            TabMaille[k1]->SetNumVoisinLateral(8, 1, k2);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisinLateral(5, 1, k1);
            TabMaille[k2]->SetNumVoisinLateral(8, 2, k1);
            // Passage � la ligne suivante
            k1 -= nbcol;
            k2 += 1;
        }
    }        // Bords boucl�s SUD-EST
    else if ((Limites.Type_bord[1] == boucle) && (Limites.Type_bord[3] == boucle)) 
    {
        k1 = nbcol - 1;
        k2 = (nblig - 1) * nbcol;
        for (i = 0; i < nblig; i++) 
        {
            // Bord EST (derni�re colonne, i=nbcol)
            TabMaille[k1]->SetNumVoisinLateral(7, 1, k2);
            TabMaille[k1]->SetNumVoisinLateral(8, 2, k2);
            // Bord SUD (derni�re ligne, j=nblig)
            TabMaille[k2]->SetNumVoisinLateral(5, 1, k1);
            TabMaille[k2]->SetNumVoisinLateral(8, 2, k1);
            // Passage � la ligne suivante
            k1 += nbcol;
            k2 += 1;
        }
    }

    // Coin en HAUT � GAUCHE : i=1; j=1;
    k = 0;
    TabMaille[k]->SetNumVoisinLateral(1, 1, -(k + 1));
    TabMaille[k]->SetNumVoisinLateral(2, 2, -(k + nbcol));
    TabMaille[k]->SetNumVoisinLateral(3, 2, -(k + 1));
    TabMaille[k]->SetNumVoisinLateral(4, 1, -(k + nbcol));
    TabMaille[k]->SetNumVoisinLateral(6, 2, -(k + nbcol));
    TabMaille[k]->SetNumVoisinLateral(7, 1, -(k + nbcol));
    TabMaille[k]->SetNumVoisinLateral(7, 2, -(k + 1));
    TabMaille[k]->SetNumVoisinLateral(8, 1, -(k + 1));

    // Coin en HAUT � DROITE : i=nbcol; j=1;
    k = nbcol - 1;
    TabMaille[k]->SetNumVoisinLateral(1, 2, -(k - 1));
    TabMaille[k]->SetNumVoisinLateral(2, 2, -(k + nbcol));
    TabMaille[k]->SetNumVoisinLateral(3, 1, -(k - 1));
    TabMaille[k]->SetNumVoisinLateral(4, 1, -(k + nbcol));
    TabMaille[k]->SetNumVoisinLateral(5, 2, -(k - 1));
    TabMaille[k]->SetNumVoisinLateral(6, 1, -(k - 1));
    TabMaille[k]->SetNumVoisinLateral(6, 2, -(k + nbcol));
    TabMaille[k]->SetNumVoisinLateral(7, 1, -(k + nbcol));

    // Coin en BAS � GAUCHE : i=1; j=nblig;
    k = (nblig - 1) * nbcol;
    TabMaille[k]->SetNumVoisinLateral(1, 1, -(k + 1));
    TabMaille[k]->SetNumVoisinLateral(2, 1, -(k - nbcol + 1));
    TabMaille[k]->SetNumVoisinLateral(3, 2, -(k - nbcol + 1));
    TabMaille[k]->SetNumVoisinLateral(4, 2, -(k - nbcol));
    TabMaille[k]->SetNumVoisinLateral(5, 1, -(k - nbcol));
    TabMaille[k]->SetNumVoisinLateral(7, 2, -(k + 1));
    TabMaille[k]->SetNumVoisinLateral(8, 1, -(k + 1));
    TabMaille[k]->SetNumVoisinLateral(8, 2, -(k - nbcol));

    // Coin en BAS � DROITE : i=nbcol; j=nblig;
    k = (nblig * nbcol) - 1;
    TabMaille[k]->SetNumVoisinLateral(1, 2, -(k - 1));
    TabMaille[k]->SetNumVoisinLateral(2, 1, -(k - 1));
    TabMaille[k]->SetNumVoisinLateral(3, 1, -(k - nbcol - 1));
    TabMaille[k]->SetNumVoisinLateral(4, 2, -(k - nbcol - 1));
    TabMaille[k]->SetNumVoisinLateral(5, 1, -(k - nbcol));
    TabMaille[k]->SetNumVoisinLateral(5, 2, -(k - 1));
    TabMaille[k]->SetNumVoisinLateral(6, 1, -(k - 1));
    TabMaille[k]->SetNumVoisinLateral(8, 2, -(k - nbcol));


    return;
}

//------------------------------------------------------------------------
// dans le cas d'une �le, on force les mailles des bords au niveau de base
//------------------------------------------------------------------------

void CMaillage::BordureIle() 
{
    // k = ( (j-1)*nbcol + i )  - 1;
    int k;
    // Colonne 1 : i=1, j=1,nblig
    for (k = 0; k < nbmailles; k += nbcol)
        TabMaille[k]->SetLesAlti(NiveauDeBase);

    // Derni�re colonne : i=nbcol, j=1,nblig
    for (k = nbcol - 1; k < nbmailles; k += nbcol)
        TabMaille[k]->SetLesAlti(NiveauDeBase);

    // Ligne 1 : i=1,nbcol, j=1
    for (k = 0; k < nbcol; k++)
        TabMaille[k]->SetLesAlti(NiveauDeBase);

    // Derni�re ligne (nblig) : i=1,nbcol, j=nblig
    for (k = (nblig - 1) * nbcol; k < nbmailles; k++)
        TabMaille[k]->SetLesAlti(NiveauDeBase);
    return;
}

//----------------------------------------
// trie la liste des mailles par altitudes
//----------------------------------------
// Tri rapide it�ratif
//

void CMaillage::TriRapideOptimal_deMailles() 
{
    int debut = 0;
    int fin = nbmailles - 1;
    int pivotindex, l, r;
    int top = 0;
    CMaille* maillepivot;

    int i, j;

    //	InitialiseTri_deMailles();
    for (i = 0; i < nbmailles; i++) Liste[i] = Desordre[i];

    //##### Modif. le 12/04/2006 :
    //	Au passage, on initialise la liste inverse (liste des classements)
    for (i = 0; i < nbmailles; i++) ListeInv[i] = -1;

    soustas[top].gauche = debut;
    soustas[top].droite = fin;

    while (top >= 0) 
    {
        // Pop soustas
        debut = soustas[top].gauche;
        fin = soustas[top].droite;
        top--;

        //Find pivot
        pivotindex = (debut + fin) / 2; // Pivot = milieu de l'intervalle
        maillepivot = TabMaille[Liste[pivotindex]];
        Echange(Liste, pivotindex, debut); // Stick pivot at beginning

        // Partition
        l = debut - 1;
        r = fin;
        do {
            while (TabMaille[Liste[++l]]->PlusHaut(maillepivot));
            while (r && (maillepivot->PlusHaut(TabMaille[Liste[--r]])));
            Echange(Liste, l, r);
        } while (l < r);

        Echange(Liste, l, r);
        Echange(Liste, l, debut);

        //Load up soustas
        if ((l - debut) > SEUIL_TRI) 
        { // Left partition
            top++;
            soustas[top].gauche = debut;
            soustas[top].droite = l - 1;
        }

        if ((fin - l) > SEUIL_TRI) 
        { // Right partition
            top++;
            soustas[top].gauche = l + 1;
            soustas[top].droite = fin;
        }
    }
    //	TriInsertion_deMailles(); //Final Insertion Sort
    for (i = 1; i < nbmailles; i++)
        for (j = i; (j > 0) &&
                (TabMaille[Liste[j]]->PlusHaut(TabMaille[Liste[j - 1]])); j--)
            Echange(Liste, j, j - 1);
    return;
}

void CMaillage::TriParInsertion_deMailles() 
{

    int i, j;

    //	Au passage, on initialise la liste inverse (liste des classements)
    for (i = 0; i < nbmailles; i++) ListeInv[i] = -1;

    for (i = 1; i < nbmailles; i++)
        for (j = i; (j > 0) &&
                (TabMaille[Liste[j]]->PlusHaut(TabMaille[Liste[j - 1]])); j--)
            Echange(Liste, j, j - 1);
    return;
}



//----------------------------------------
// trie la liste des mailles par altitudes
//----------------------------------------
// Tri rapide it�ratif
//idem pour les altitudes de la surface de l'eau

void CMaillage::TriRapideOptimal_deMailles_Eau() // 30 juin 2008
{
    int debut = 0;
    int fin = nbmailles - 1;
    int pivotindex, l, r;
    int top = 0;
    CMaille* maillepivot;

    int i, j;

    //	InitialiseTri_deMailles();
    for (i = 0; i < nbmailles; i++) ListeEau[i] = Desordre[i];

    //##### Modif. le 12/04/2006 :
    //	Au passage, on initialise la liste inverse (liste des classements)
    for (i = 0; i < nbmailles; i++) ListeEauInv[i] = -1;

    soustas[top].gauche = debut;
    soustas[top].droite = fin;

    while (top >= 0) 
    {
        // Pop soustas
        debut = soustas[top].gauche;
        fin = soustas[top].droite;
        top--;

        //Find pivot
        pivotindex = (debut + fin) / 2; // Pivot = milieu de l'intervalle
        maillepivot = TabMaille[ListeEau[pivotindex]];
        Echange(ListeEau, pivotindex, debut); // Stick pivot at beginning

        // Partition
        l = debut - 1;
        r = fin;
        do {
            while (TabMaille[ListeEau[++l]]->PlusHaut_Eau(maillepivot));
            while (r && (maillepivot->PlusHaut_Eau(TabMaille[ListeEau[--r]])));
            Echange(ListeEau, l, r);
        } while (l < r);

        Echange(ListeEau, l, r);
        Echange(ListeEau, l, debut);

        //Load up soustas
        if ((l - debut) > SEUIL_TRI) 
        { // Left partition
            top++;
            soustas[top].gauche = debut;
            soustas[top].droite = l - 1;
        }

        if ((fin - l) > SEUIL_TRI) 
        { // Right partition
            top++;
            soustas[top].gauche = l + 1;
            soustas[top].droite = fin;
        }
    }
    //	TriInsertion_deMailles(); //Final Insertion Sort
    for (i = 1; i < nbmailles; i++)
        for (j = i; (j > 0) &&
                (TabMaille[ListeEau[j]]->PlusHaut_Eau(TabMaille[ListeEau[j - 1]])); j--)
            Echange(ListeEau, j, j - 1);
    return;
}

//----------------------------------------
// trie la liste des mailles par altitudes
//----------------------------------------
// Tri rapide it�ratif
//idem pour les altitudes de la surface de l'eau

void CMaillage::TriParInsertion_deMailles_Eau() // 2018
{
    int i, j;

    //	Au passage, on initialise la liste inverse (liste des classements)
    for (i = 0; i < nbmailles; i++) ListeEauInv[i] = -1;

    //	TriInsertion_deMailles();
    for (i = 1; i < nbmailles; i++)
        for (j = i; (j > 0) &&
                (TabMaille[ListeEau[j]]->PlusHaut_Eau(TabMaille[ListeEau[j - 1]])); j--)
            Echange(ListeEau, j, j - 1);
    return;
}


//--------------------------
// Initialisation des d�cros
//--------------------------

void CMaillage::InitialiseDecro(ParamTecto param_tecto) 
{
    jdecro = param_tecto.LigneDecro;
    return;
}

//-----------------------
// Soul�vement tectonique
//-----------------------

void CMaillage::Souleve() 
{
    for (int k = 0; k < nbmailles; k++)
        TabMaille[k]->AddLesAlti(Uplift[k]);
    return;
}

//-------------
// Decrochement
//-------------

void CMaillage::Decroche() 
{
    double tmpdb[3];
    TCouche** tmppile;
    int j, k;

    for (j = 1; j < jdecro; j++) 
    {
        //##### Modif. le 10/03/2006 : pb de num�ro ? la maille du bout de la ligne (i=nbcol)
        //		k = j*nbcol;
        k = j * nbcol - 1;
        tmpdb[0] = TabMaille[k]->GetAlti();
        tmpdb[1] = TabMaille[k]->GetAltiEau();
        tmpdb[2] = TabMaille[k]->GetAltiInit();
        //##### Modif. le 23/01/2006 : il faut aussi d�caler la pile strati !
        tmppile = TabMaille[k]->GetPileStrati();

        //		for (i=nbcol; i > 1; i--)
        for (k = j * nbcol - 1; k > (j - 1) * nbcol; k--) 
        {
            //			k = (j-1)*nbcol+i-1;
            //			k--;
            TabMaille[k]->SetAlti(TabMaille[k - 1]->GetAlti());
            TabMaille[k]->SetAltiEau(TabMaille[k - 1]->GetAltiEau());
            TabMaille[k]->SetAltiInit(TabMaille[k - 1]->GetAltiInit());
            TabMaille[k]->SetPileStrati(TabMaille[k - 1]->GetPileStrati());
        }
        TabMaille[k]->SetAlti(tmpdb[0]);
        TabMaille[k]->SetAltiEau(tmpdb[1]);
        TabMaille[k]->SetAltiInit(tmpdb[2]);
        TabMaille[k]->SetPileStrati(tmppile);
    }
    return;
}

//-----------------------------------------------
// Cherche parmi les voisins le point le plus bas
//-----------------------------------------------

void CMaillage::SuivantBas(CMaille* maille, GrilleInt* ptr, const int &numlac,
        int& isuivant, double& zsuivant) 
{
    double altivoisin;
    int j, numvoisin;
    ////// Cas d'une maille qui N'EST PAS au bord du maillage
    if (maille->GetPasBord()) 
    {
        // On parcourt les voisins de cette maille de bord de lac
        for (j = 0; j < 8; j++) 
        {
            numvoisin = maille->GetNumVoisin(j);
            // On teste si la maille n'appartient pas d�j� au lac
            if ((*ptr)[numvoisin] != -numlac) 
            {
                altivoisin = TabMaille[numvoisin]->GetAltiEau();
                if (altivoisin < zsuivant) 
                {
                    zsuivant = altivoisin;
                    isuivant = numvoisin;
                }
            }
        }
    }        ////// Cas d'une maille qui EST au bord du maillage
    else {
        // On parcourt les voisins de cette maille de bord de lac
        for (j = 0; j < 8; j++) 
        {
            numvoisin = maille->GetNumVoisin(j);
            // Comme on est au bord de la grille, on regarde si le voisin n'est pas dehors
            if (numvoisin >= 0) 
            {
                // On teste si la maille n'appartient pas d�j� au lac
                if ((*ptr)[numvoisin] != -numlac) 
                {
                    altivoisin = TabMaille[numvoisin]->GetAltiEau();
                    if (altivoisin < zsuivant) 
                    {
                        zsuivant = altivoisin;
                        isuivant = numvoisin;
                    }
                }
            }                //##### Modif. le 05/05/2006 : si on est au bord, on sort !
                //	(c'est malsain de conserver un lac au bord de la grille... on n'a qu'� dire que �a d�borde dehors)
                // Il faut le noter dans ce cas, car si on est en bord-boucle, il n'y a pas de pb
            else 
            {
                isuivant = -1000;
                zsuivant = -EPAISSEUR_INF;
                return;
            }
        }
    }
    return;
}
//---------------------------------------------------------------
// Cherche parmi les voisins le point le plus bas : cas d'une ILE
//---------------------------------------------------------------

void CMaillage::SuivantBas_ILE(CMaille* maille, GrilleInt* ptr, const int &numlac,
        int& isuivant, double& zsuivant) 
{
    double altivoisin;
    int j, numvoisin;
    // On parcourt les voisins de cette maille de bord de lac
    for (j = 0; j < 8; j++) 
    {
        numvoisin = maille->GetNumVoisin(j);
        // On teste si la maille n'appartient pas d�j� au lac
        if ((*ptr)[numvoisin] != -numlac) 
        {
            altivoisin = TabMaille[numvoisin]->GetAltiEau();
            if (altivoisin < zsuivant) 
            {
                zsuivant = altivoisin;
                isuivant = numvoisin;
            }
        }
    }
    return;
}
//--------------------------------------------------
// Cherche parmi les voisins un autre � la m�me alti
//--------------------------------------------------

bool CMaillage::SuivantPlat(CMaille* maille, GrilleInt* ptr, const int &numlac,
        const double &zlac, int& isuivant) 
{
    int j, numvoisin;
    ////// Cas d'une maille qui N'EST PAS au bord du maillage
    if (maille->GetPasBord()) 
    {
        // On parcourt les voisins de cette maille de bord de plat
        for (j = 0; j < 8; j++) 
        {
            numvoisin = maille->GetNumVoisin(j);
            // On teste si la maille n'appartient pas d�j� au plat
            if ((*ptr)[numvoisin] != -numlac) 
            {
                // On teste si la maille est bien � la m�me altitude
                if (TabMaille[numvoisin]->GetAltiEau() == zlac) 
                {
                    isuivant = numvoisin;
                    return true;
                }
            }
        }
    }        ////// Cas d'une maille qui EST au bord du maillage
    else 
    {
        // On parcourt les voisins de cette maille de bord de lac
        for (j = 0; j < 8; j++) 
        {
            numvoisin = maille->GetNumVoisin(j);
            // Comme on est au bord de la grille, on regarde si le voisin n'est pas dehors
            if (numvoisin >= 0) 
            {
                // On teste si la maille n'appartient pas d�j� au lac
                if ((*ptr)[numvoisin] != -numlac) 
                {
                    // On teste si la maille est bien � la m�me altitude
                    if (TabMaille[numvoisin]->GetAltiEau() == zlac) 
                    {
                        isuivant = numvoisin;
                        return true;
                    }
                }
            }                //##### Modif. le 05/05/2006 : si on est au bord, on sort !
                //	(c'est malsain de conserver un lac au bord de la grille... on n'a qu'� dire que �a d�borde dehors)
                // Il faut le noter dans ce cas, car si on est en bord-boucle, il n'y a pas de pb
            else 
            {
                isuivant = -1000;
                return true;
            }
        }
    }
    return false;
}
//------------------------------------------------------------------
// Cherche parmi les voisins un autre � la m�me alti : cas d'une ILE
//------------------------------------------------------------------

bool CMaillage::SuivantPlat_ILE(CMaille* maille, GrilleInt* ptr, const int &numlac,
        const double &zlac, int& isuivant) 
{
    int j, numvoisin;
    // On parcourt les voisins de cette maille de bord de plat
    for (j = 0; j < 8; j++) 
    {
        numvoisin = maille->GetNumVoisin(j);
        // On teste si la maille n'appartient pas d�j� au plat
        if ((*ptr)[numvoisin] != -numlac) 
        {
            // On teste si la maille est bien � la m�me altitude
            if (TabMaille[numvoisin]->GetAltiEau() == zlac) 
            {
                isuivant = numvoisin;
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------
// Cherche si un des voisins est plus bas
//---------------------------------------

bool CMaillage::TesteSiBas(CMaille* maille, GrilleInt* ptr, const int &numlac,
        const double &z) 
{
    int j, numvoisin;
    ////// Cas d'une maille qui N'EST PAS au bord du maillage
    if (maille->GetPasBord()) 
    {
        for (j = 0; j < 8; j++) 
        {
            numvoisin = maille->GetNumVoisin(j);
            if ((*ptr)[numvoisin] != -numlac) 
            {
                if (TabMaille[numvoisin]->GetAltiEau() < z)
                    return true;
            }
        }
    } 
    else 
    {
        for (j = 0; j < 8; j++) 
        {
            numvoisin = maille->GetNumVoisin(j);
            if (numvoisin >= 0) 
            {
                if ((*ptr)[numvoisin] != -numlac) 
                {
                    if (TabMaille[numvoisin]->GetAltiEau() < z)
                        return true;
                }
            }                // Si Flux nul, on ne peut pas exuter par l�...
            else if (!maille->TesteBordFluxNul()) 
            {
                //##### Modif le 06/10/2005 : si on n'est pas au bord d'un flux-nul,
                // automatiquement l'eau peut se barrer dehors et point-barre !!!
                /*
                                                double altivoisin;
                                                if (maille->TesteBordAltiFixe())
                                                        altivoisin = maille->GetBaseBord();
                                                else
                                                        altivoisin = TabMaille[-numvoisin]->GetAlti();
                                // ...sinon, on peut !
                                                if (altivoisin < z)
                 */
                return true;
            }
        }
    }
    return false;
}
//-------------------------------------------------------
// Cherche si un des voisins est plus bas : cas d'une ILE
//-------------------------------------------------------

bool CMaillage::TesteSiBas_ILE(CMaille* maille, GrilleInt* ptr, const int &numlac,
        const double &z)
{
    int j, numvoisin;
    for (j = 0; j < 8; j++) 
    {
        numvoisin = maille->GetNumVoisin(j);
        if ((*ptr)[numvoisin] != -numlac) 
        {
            if (TabMaille[numvoisin]->GetAltiEau() < z)
                return true;
        }
    }
    return false;
}
//--------------------------------------------------
// Lit la courbe eustatique niv=f(t) dans un fichier
//--------------------------------------------------

bool CMaillage::LitEustatique(const double &dtMin) 
{
    ifstream FichierEntree((nivbase.fich_nivbase).c_str(), ios::in);
    if (!FichierEntree) 
    {
        cerr << "The file does not exist or cannot be read !! "
                << nivbase.fich_nivbase << endl;
        return false;
    }

    int i, j;
    string ligne;

    // Compte le nombre de lignes
    nivbase.nbdates = 0;
    while (getline(FichierEntree, ligne))
        ++nivbase.nbdates;
    FichierEntree.close();

    // Allocation du tableau
    if (nivbase.date != NULL) delete [] nivbase.date;
    if (nivbase.niveau != NULL) delete [] nivbase.niveau;
    nivbase.date = new double[nivbase.nbdates];
    nivbase.niveau = new double[nivbase.nbdates];

    // Lecture de la courbe eustatique
    AfficheMessage("> Reading the base-level file : ", nivbase.fich_nivbase);
    FichierEntree.open((nivbase.fich_nivbase).c_str(), ios::in);
    for (i = 0; i < nivbase.nbdates; i++) 
    {
        getline(FichierEntree, ligne);
        LitLigne(ligne, nivbase.date[i], nivbase.niveau[i]);
        nivbase.niveau[i] *= pas2;
    }
    FichierEntree.close();
    AfficheMessage("          *** Base-level file read ***\n");

    // V�rification des valeurs
    i = 0;
    //	- dates n�gatives
    if (nivbase.date[0] < 0.) 
    {
        do i++; while (nivbase.date[i] < 0);
        for (j = i; j < nivbase.nbdates; j++) 
        {
            nivbase.date[j] = nivbase.date[j - i];
            nivbase.niveau[j] = nivbase.niveau[j - i];
        }
        nivbase.nbdates -= i;
    }
    //	- succession d�croissante de dates
    for (i = 1; i < nivbase.nbdates; i++) 
    {
        if (nivbase.date[i] <= nivbase.date[i - 1]) {
            AfficheMessage("Problem of ordering in dates");
            for (j = i; j < nivbase.nbdates - 1; j++) 
            {
                nivbase.date[j] = nivbase.date[j + 1];
                nivbase.niveau[j] = nivbase.niveau[j + 1];
            }
            i--;
        }
    }
    //	- dur�e du cycle trop faible
    if (nivbase.nbdates < 3) 
    {
        AfficheMessage("Eustatic cycle too rough !!");
        return false;
    }
    if ((nivbase.date[nivbase.nbdates] - nivbase.date[0]) < 5. * dtMin) 
    {
        AfficheMessage("Duration of eustatic cycle too short !!");
        return false;
    }
    return true;
}

void CMaillage::AfficheMaillage() 
{
    int i, j, k = -1;
    AfficheMessage("Altitude du maillage (eau)");
    for (i = 0; i < nblig; i++) 
    {
        for (j = 0; j < nbcol; j++) 
        {
            k++;
            cout << TabMaille[k]->GetAltiEau() / pas2 << "  ";
        }
        cout << endl;
    }
    return;
}


void CMaillage::NeFaitRien(){} // 2023
