#include <iostream>
#include <string>
#include <cstdlib>
#include "CWaterbot.h"


using namespace std;


////////////////////
////CONSTRUCTEUR////
//////////////////// 

CWaterbot::CWaterbot(CFlux *flux, CMaillage *maillage, CMaille *maille, const int ind, const double dt) // 2023
{
    m_MesFlux = flux;
    m_MaMaille = maille;
    m_MonMaillage = maillage;
    
    // Variables qui definissent le waterbot
    
    m_numeroMaille = ind;
    numinit = ind;
    m_numMailleAppartientLac = new int[m_MonMaillage->GetNbMailles()];
    m_numMailleAppartientLac[m_numeroMaille] = -2; // -2 veut dire dans le lac
    m_numeroMaillePrecedent = -1;
    m_numeroMailleSuivant = -2;
    m_dt = dt;
    m_fauxExutoire = false;

    // On recupere le volume d'eau transporte sur cette maille a la creation du waterbot :
    m_volumeEauTransporte = 0.; // c'est bien FluxEau et pas EauCourante qui n'est pas definie (=0)
                            // dans un trou tandis que FluEau est mis a 0 et transforme en EauCourante quand il y a des voisins bas

    m_pas3 = m_MonMaillage->GetPas3();
    MonGetAlti = &CMaille::GetAltiEau;
    
}

// destructeur

CWaterbot::~CWaterbot()
{
}


bool CWaterbot::CombleLeTrouEtActualiseVolumeEauTransporte()
{
    bool result = false;
    double difference = 0.;

    m_numMailleAppartientLac[m_numeroMaille] = -2; //  on est dans un trou ou une surface plane
    // On recupere son eau accumulee dans le pas de temps qu'on ajoute au volume transporte par le waterbot
    m_volumeEauTransporte += m_MesFlux->GetFluxEau(m_numeroMaille) * m_dt;
    if (m_MesFlux->GetFluxEau(m_numeroMaille)>0.) m_MesFlux->SetEauCourante(m_numeroMaille, m_MesFlux->GetFluxEau(m_numeroMaille));
    m_MesFlux->SetFluxEau(m_numeroMaille,0.); // sinon  on compterait 2 fois sont flux d'eau dans m_volumeEauTransporte si on repassait sur cette maille
    
    
    difference = m_MaMaille->GetAltiMeanVoisinEau() - m_MaMaille->GetAltiEau();
    
    if (m_volumeEauTransporte > difference) // comble le trou et renvoie un flag disant qu'il faut continuer
    {
        m_MaMaille->SetAltiEau(m_MaMaille->GetAltiMeanVoisinEau());
        m_MaMaille->SetAltiRef(m_MaMaille->GetAltiMeanVoisinEau());
        m_volumeEauTransporte -=  difference;
        // On recalcule le voisinage de sorte que cette maille puisse ne plus etre  un trou
        // lors du bilan sedimentaire;
        PentesMaille_MultFlow_SurfaceEau(m_MaMaille, m_MonMaillage, MonGetAlti, m_pas3);
        result = true;
    }
    else // remplit le trou avec ce qu'il y a comme eau et renvoie un flag disant qu'il faut s'arreter
    {
        m_MaMaille->SetAltiEau(m_MaMaille->GetAltiEau() + m_volumeEauTransporte);
        m_MaMaille->SetAltiRef(m_MaMaille->GetAltiEau());
        m_MesFlux->SetEauCourante(m_numeroMaille,0.);
        result = false;
    }

    return result;
}



void CWaterbot::DeplaceLeWaterbot()
{
    
    // Le suivant dans le cheminement du waterbot, calcule avant de recalculer le voisinage (important)
    if ((m_MaMaille->GetVoisinageBasEau())->nb >0) // peut arriver si le precedent est un faux exutoire se deversant dans un trou ou dans le lac en cours de comblement
    {
        int indice = (m_MaMaille->GetVoisinageBasEau())->directionaltilaplusbasse;
        m_numeroMailleSuivant = m_MaMaille->GetNumVoisinBasEau(indice);
    }
    else {m_numeroMailleSuivant = m_MaMaille->GetNumVoisinJusteAuDessusEau();}
    
    if (m_numeroMailleSuivant < 0) return; // on etait en fait sur un exutoire sur la maille precedente, au bord du maillage
    
    // Mais pour eviter le ping-pong entre deux mailles de memes altitudes :
    if (m_numeroMailleSuivant==m_numeroMaillePrecedent && (m_MonMaillage->GetMaille(m_numeroMailleSuivant))->GetAltiEau() == (m_MonMaillage->GetMaille(m_numeroMaille))->GetAltiEau())
    {m_numeroMailleSuivant = m_MaMaille->GetNumVoisinDeuxiemeJusteAuDessusEau(); }
        
    // On actualise la  maille en cours
    m_MaMaille = m_MonMaillage->GetMaille(m_numeroMailleSuivant);
    m_numeroMaillePrecedent = m_numeroMaille;
    m_numeroMaille = m_numeroMailleSuivant;
        
}



bool CWaterbot::TesteSiExutoire()
{
    bool result = false;
    if (m_numeroMailleSuivant < 0) return true; // on etait en fait sur un exutoire sur la maille precedente, au bord du maillage

    // Recalcule les pentes sur la surface d'eau actualisée autour du pixel. MonGetAlti renvoie l'alti de la surface d'eau
    PentesMaille_MultFlow_SurfaceEau(m_MaMaille, m_MonMaillage, MonGetAlti, m_pas3);

    // Si cette maille a au moins un voisin bas et n'est pas une maille deja traversee par le
    // waterbot, alors c'est un exutoire
    if ((m_MaMaille->GetVoisinageBasEau())->nb > 0 && m_numMailleAppartientLac[m_numeroMaille] != -2)
    {
        result = true;
        // on teste quand meme si la maille plus basse de cet exutoire n'est pas un trou
        int indice = (m_MaMaille->GetVoisinageBasEau())->directionaltilaplusbasse;
        m_numeroMailleSuivant = m_MaMaille->GetNumVoisinBasEau(indice);
        if (m_numeroMailleSuivant > 0) // pour ne pas prendre en compte les mailles en dehors du maillage
        {
            m_MaMaille = m_MonMaillage->GetMaille(m_numeroMailleSuivant);
            PentesMaille_MultFlow_SurfaceEau(m_MaMaille, m_MonMaillage, MonGetAlti, m_pas3);
            
            if ((m_MaMaille->GetVoisinageBasEau())->nb == 0)
            {   // le suivant est donc un trou et on va repositionner le waterbot  sur ce pixel pour qu'il continue son chemin de comblement.
                m_numeroMaille = m_numeroMailleSuivant;
                result = false;
            }
        }
    }
    else
    {
        result = false;
    }
    return result;
}

void CWaterbot::ActualiseFluxEauEnAttente()
{
    double flux = 0.;

    if (m_numeroMailleSuivant < 0) // on etait sur un exutoire sur la maille precedente, au bord du maillage
    {

        m_MesFlux->AddFluxEau_OutWaterbot(m_volumeEauTransporte/m_dt);
        return;
    }
    else
    {
        flux = m_MesFlux->GetFluxEauEnAttente(m_numeroMaille);
        m_MesFlux->SetFluxEauEnAttente(m_numeroMaille,flux + m_volumeEauTransporte/m_dt);
    }
}
