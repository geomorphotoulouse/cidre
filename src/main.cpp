//#include "CCalculePentes.h"
#include "CMaillage.h"
#include "CModele.h"
#include "CCalcul.h"
#include "Utilitaires.h"
#include "Options.h"
#include <omp.h>    // OpenMP option pour la parallelisation des grains
#include <iostream> // pour les manipulations d'I/O sur cin/cout
using namespace std;

int main() 
{
    CModele* MonModele = new CModele;
    CMaillage* MonMaillage = new CMaillage;
    bool flag;

    flag = MonModele->LitModele("input_cidre.txt");
    if (!flag)
    {
        AfficheMessage("The reading of input parameters file failed");
        return 0;
    }

    flag = MonModele->ConstruitModele(MonMaillage);
    if (!flag)
    {
        AfficheMessage("The building of the model with the specified options failed");
        return 0;
    }

    CCalcul* MonCalcul = new CCalcul(MonModele, MonMaillage);

    if ((MonModele->GetOptions()).OptionAvecCailloux) 
    {
        MonCalcul->RunLeCalculAvecCaillou();
    } 
    else 
    {
        MonCalcul->RunLeCalculSansCaillou();
    }

    delete MonModele;
    delete MonMaillage;
    delete MonCalcul;

    //puts("Appuyer sur la touche \"Entree\" pour finir...");
    //getchar();
    return 1;
}
