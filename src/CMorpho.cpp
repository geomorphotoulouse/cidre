#include "CMorpho.h"
//#include <direct.h> // pour windows
#include <sys/types.h> // pour gcc
#include <sys/stat.h> // pour gcc

//-------------------------------------------
//
// fonctions de la classe CMorpho :
//
//-------------------------------------------

// constructeur

CMorpho::CMorpho(CMaillage* maillage, CFlux* flux, TSorties resultats) 
{
    // Les objets importants
    MonMaillage = maillage;
    MesFlux = flux;

    nbcol = maillage->GetNbCol();
    nblig = maillage->GetNbLig();
    nbmailles = maillage->GetNbMailles();
    pas = sqrt(maillage->GetPas2());
    pasdiag = pas * sqrt(2.);
    unsurpas2 = 1. / maillage->GetPas2();
    Desordre = maillage->GetDesordre();
    if (resultats.DirSortie.size() == 0) 
    {
        RadicalRiviere = resultats.FichierRiviere;
        RadicalBassin = resultats.FichierBassin;
    } 
    else 
    {
        //CreateDirectory(resultats.DirSortie.c_str(),NULL); // pour windows
        //mkdir(resultats.DirSortie.c_str(),755); // pour gcc sous mac
        RadicalRiviere = "./" + resultats.DirSortie + "/" + resultats.FichierRiviere;
        RadicalBassin = "./" + resultats.DirSortie + "/" + resultats.FichierBassin;
    }
    NbRivieres = resultats.nbrivieres;

    ListeFlux = new int[nbmailles];
    DejaPasse = new bool[nbmailles];
    int nbmax_mailles_riv = 3 * myintmax(nbcol, nblig);
    for (int i = 0; i < NRIVIERES_MAX; i++) 
    {
        TabRiv[i].reserve(nbmax_mailles_riv);
        TabRivDiag[i].reserve(nbmax_mailles_riv);
    }
    NRivieres = 0;
}

// destructeur

CMorpho::~CMorpho() 
{
    delete [] ListeFlux;
    delete [] DejaPasse;
}

//==================
// Autres fonctions
//==================

//-------------------------------------------
// trie la liste des mailles par aire drain�e
//-------------------------------------------

void CMorpho::TriParAireDrainee() 
{
    int debut = 0;
    int fin = nbmailles - 1;
    int pivotindex, l, r;
    int top = 0;
    double fluxpivot;

    int i, j;

    //	Initialisation : toutes les mailles dans le d�sordre
    for (i = 0; i < nbmailles; i++) ListeFlux[i] = Desordre[i];

    soustas[top].gauche = debut;
    soustas[top].droite = fin;

    while (top >= 0) 
    {
        // Pop soustas
        debut = soustas[top].gauche;
        fin = soustas[top].droite;
        top--;

        //Find pivot
        pivotindex = (debut + fin) / 2; // Pivot = milieu de l'intervalle
        fluxpivot = MesFlux->GetEauCourante(ListeFlux[pivotindex]);
        Echange(ListeFlux, pivotindex, debut); // Stick pivot at beginning

        // Partition
        l = debut - 1;
        r = fin;
        do {
            while (MesFlux->GetEauCourante(ListeFlux[++l]) > fluxpivot);
            while (r && (fluxpivot > MesFlux->GetEauCourante(ListeFlux[--r])));
            Echange(ListeFlux, l, r);
        } while (l < r);

        Echange(ListeFlux, l, r);
        Echange(ListeFlux, l, debut);

        //Load up soustas
        if ((l - debut) > SEUIL_TRI) 
        { // Left partition
            top++;
            soustas[top].gauche = debut;
            soustas[top].droite = l - 1;
        }

        if ((fin - l) > SEUIL_TRI) 
        { // Right partition
            top++;
            soustas[top].gauche = l + 1;
            soustas[top].droite = fin;
        }
    }
    //	TriInsertion_deMailles(); //Final Insertion Sort
    for (i = 1; i < nbmailles; i++)
        for (j = i; (j > 0) &&
                (MesFlux->DrainePlus(ListeFlux[j], ListeFlux[j - 1])); j--)
            Echange(ListeFlux, j, j - 1);
    return;
}

//--------------------------------------
// calcule les "nb" rivi�res principales
//--------------------------------------

void CMorpho::RivieresPrincipales() 
{
    int indflux = 0;
    int compteur = 0;
    int i, indmaille, numvoisin, isuivant;
    CMaille* MaMaille;
    double MonAlti;
    double fluxmax, fluxloc, fluxvoisin;
    int dirdiagonale = 0;
    bool Recoupement;

    // Tri par aire drain�e
    TriParAireDrainee();

    // Initialisation de la grille d'indicateur de passage
    for (i = 0; i < nbmailles; i++)
        DejaPasse[i] = false;

    while (compteur < NbRivieres) 
    {
        // La maille avec la plus grande aire drain�e...
        indmaille = ListeFlux[indflux];
        // ...sur laquelle on n'est pas encore pass�.
        while (DejaPasse[indmaille]) 
        {
            if (indflux < nbmailles)
                indmaille = ListeFlux[++indflux];
            else
                break;
        }

        // On vide le vecteur de la rivi�re, au cas o�
        TabRiv[compteur].clear();
        TabRivDiag[compteur].clear();
        Recoupement = false;
        // On assigne la premi�re maille
        isuivant = indmaille;
        do {
            if (DejaPasse[isuivant]) 
            {
                Recoupement = true;
                break;
            }
            // On ajoute la maille � la liste de la rivi�re
            TabRiv[compteur].push_back(isuivant);
            TabRivDiag[compteur].push_back(dirdiagonale);
            DejaPasse[isuivant] = true;

            MaMaille = MonMaillage->GetMaille(isuivant);
            MonAlti = MaMaille->GetAlti();
            fluxloc = MesFlux->GetEauCourante(isuivant);
            fluxmax = -1.;
            isuivant = -1;
            // Pour chaque maille qu'on ajoute � la rivi�re, on parcourt les voisins
            // pour trouver la suivante
            for (i = 0; i < 4; i++) 
            {
                numvoisin = MaMaille->GetNumVoisin(i);
                if (numvoisin < 0) continue;
                fluxvoisin = MesFlux->GetEauCourante(numvoisin);
                if ((MonMaillage->GetMaille(numvoisin)->GetAlti() >= MonAlti)
                        && (fluxvoisin > fluxmax) && (fluxvoisin < fluxloc)) {
                    isuivant = numvoisin;
                    fluxmax = fluxvoisin;
                    dirdiagonale = 0;
                }
            }
            for (i = 4; i < 8; i++) 
            {
                numvoisin = MaMaille->GetNumVoisin(i);
                if (numvoisin < 0) continue;
                fluxvoisin = MesFlux->GetEauCourante(numvoisin);
                if ((MonMaillage->GetMaille(numvoisin)->GetAlti() >= MonAlti)
                        && (fluxvoisin > fluxmax) && (fluxvoisin < fluxloc)) 
                {
                    isuivant = numvoisin;
                    fluxmax = fluxvoisin;
                    dirdiagonale = 1;
                }
            }
        }            // Si on n'a pas trouv� de maille plus haute, on a fini
        while (isuivant > 0);

        //		if (!Recoupement)
        //		{
        compteur++;
        //			AfficheMessage(" n-ieme riviere ",compteur);
        //		}
        indflux++;
        if (indflux >= nbmailles) break;
    }
    NRivieres = compteur;
    return;
}

//--------------------------------------
// sauve les "nb" rivi�res principales
//--------------------------------------

bool CMorpho::SauveRivieres(int num) 
{
    // D�termination des rivi�res
    RivieresPrincipales();

    // Construction du nom du fichier de sortie
    string FichierRiviere;
    ostringstream NumFichier;
    NumFichier << num;
    if (num < 10)
        FichierRiviere = RadicalRiviere + "00" + NumFichier.str();
    else if (num < 100)
        FichierRiviere = RadicalRiviere + "0" + NumFichier.str();
    else
        FichierRiviere = RadicalRiviere + NumFichier.str();
    AfficheMessage("Sortie des rivieres dans le fichier : ", FichierRiviere);
    //
    // Ecriture dans un flux de sortie
    //
    ofstream FichierSortie;
    FichierSortie.open(FichierRiviere.c_str(), ios::out | ios::trunc);

    // Ouverture du flux de sortie
    if (FichierSortie.bad()) 
    {
        cerr << "Impossible d'ouvrir un flux de sortie !! " << FichierRiviere << endl;
        FichierSortie.close();
        return false;
    } 
    else 
    {
        int i;
        unsigned int j;
        TCoord xy, xy0;
        float z;
        double distlong;
        for (i = 0; i < NRivieres; i++) 
        {
            if (i > 0) FichierSortie << ">" << endl;
            distlong = 0.;
            MonMaillage->GetMaille(TabRiv[i][0])->GetCoordonnees3D(xy0, z);
            FichierSortie << xy0.x << " " << xy0.y << " " << z * unsurpas2 << " " << distlong << endl;
            for (j = 1; j < TabRiv[i].size(); j++) 
            {
                MonMaillage->GetMaille(TabRiv[i][j])->GetCoordonnees3D(xy, z);
                FichierSortie << xy.x << " " << xy.y << " " << z*unsurpas2;
                if (TabRivDiag[i][j] == 1)
                    distlong += pasdiag;
                else
                    distlong += pas;
                //				distlong += Distance(xy0,xy);
                //				xy0 = xy;
                FichierSortie << " " << distlong << endl;
                //				FichierSortie.write((char*) &y[0],sizeof (float));
            }
        }

        // Fermeture du fichier
        FichierSortie.close();

        return true;
    }
}
