#include "Ccalc_erosionfluv.h"
#include <math.h>
#include <iostream>
using namespace std;

//-------------------------------------------
// fonctions de la classe CErosionFluviale :
//-------------------------------------------

// constructeur

CErosionFluviale::CErosionFluviale(CModele* modele, CFlux* flux) 
{
    // Les objets importants
    MesFlux = flux;
    MonModele = modele; // 25 octobre 2007

    // Param√ãtres d'erosion pour les s√àdiments
    incisSed = modele->GetParamIncisSed();
    largeur = modele->GetParamLargeur();

    //  pas d'espace
    pas2 = modele->GetTopoInit().pas * modele->GetTopoInit().pas; // 7 aout 2008
    
    
    // Choix du voisinage (altitude du sol ou de l'eau)
    if (modele->GetOptions().OptionHauteurEau) // 2023
    {
        MonGetVoisinage = &CMaille::GetVoisinageBasEau;
        MonGetDirectionVoisinsBas = &CMaille::GetDirectionVoisinsBasEau;
    }
    else
    {
        MonGetVoisinage = &CMaille::GetVoisinageBas;
        MonGetDirectionVoisinsBas = &CMaille::GetDirectionVoisinsBas;
    }
   

    // Choix du calcul du shear stress
    if (incisSed->Seuil > 0.) 
    {
        if (largeur->OuiNon) 
        {
            if (incisSed->NS == 1.) 
            {
                if (incisSed->MQ == 0.5) MonCalculDeShearStress = &CalculeLeShearStress_M0_5_N1; // decembre 2018
                else if (incisSed->MQ == 1.) MonCalculDeShearStress = &CalculeLeShearStress_M1_N1; // decembre 2018
            } 
            else if (incisSed->MQ == 0.5) 
            {
                if (incisSed->MQ == 0.5) MonCalculDeShearStress = &CalculeLeShearStress_M0_5_N0_5; // decembre 2018
            } 
            else if (incisSed->MQ == 1.5 && incisSed->MQ == 1.5) MonCalculDeShearStress = &CalculeLeShearStress_M1_5_N1_5; // decembre 2018
            else
                MonCalculDeShearStress = &CalculeLeShearStress; // 25 octobre 2007
        } 
        else 
        {
            if (incisSed->NS == 1.) 
            {
                if (incisSed->MQ == 0.5) MonCalculDeShearStress = &CalculeLeShearStressSansLargeur_M0_5_N1; // decembre 2018
                else if (incisSed->MQ == 1.) MonCalculDeShearStress = &CalculeLeShearStressSansLargeur_M1_N1; // decembre 2018
            } 
            else if (incisSed->MQ == 0.5) 
            {
                if (incisSed->MQ == 0.5) MonCalculDeShearStress = &CalculeLeShearStressSansLargeur_M0_5_N0_5; // decembre 2018
            } 
            else if (incisSed->MQ == 1.5 && incisSed->MQ == 1.5) MonCalculDeShearStress = &CalculeLeShearStressSansLargeur_M1_5_N1_5; // decembre 2018
            else
                MonCalculDeShearStress = &CalculeLeShearStressSansLargeur; // 25 octobre 2007
        }
    } 
    else 
    {
        MonCalculDeShearStress = &NeCalculePasLeShearStress; // 25 octobre 2007
    }

    // Choix du calcul de la largeur de l'ecoulement
    if (largeur->OuiNon) 
    {
        MonCalculDeLargeurDecoulement = &LargeurFonctionDuFluxEau; // 25 octobre 2007
    } 
    else 
    {
        MonCalculDeLargeurDecoulement = &NeRecalculePasLaLargeur; // 25 octobre 2007
    }

    // Choix de l'equation de l'erosion des sediment
    // si le seuil de transport est nul ...

    if (incisSed->Seuil == 0.) // si le seuil d'erosion est nul ... - modifs janvier 2013  ... et juin 2014
    {
        if (incisSed->NS == 1. && not largeur->OuiNon) 
        {
            if (incisSed->MQ == 0.5)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M0_5N1;
            else if (incisSed->MQ == 1.)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M1N1;
            else if (incisSed->MQ == 1.5)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M1_5N1;
            else
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_N1;
        } 
        else if (incisSed->NS == 1. && largeur->OuiNon) 
        {
            if (incisSed->MQ == 0.5)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M0_5N1_AvecLargeurRiv;
            else if (incisSed->MQ == 1.)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M1N1_AvecLargeurRiv;
            else if (incisSed->MQ == 1.5)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M1_5N1_AvecLargeurRiv;
            else
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_N1_AvecLargeurRiv;
        } 
        else if (largeur->OuiNon)
            MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_AvecLargeurRiv;
        else
            MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil;
    }
        // si le seuil de detachement n est pas nul ...

    else 
    {
        if (incisSed->Exposantshearstress == 1. && not largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_Exposantshearstress1;
        } 
        else if (incisSed->Exposantshearstress == 1.5 && not largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_Exposantshearstress1_5;
        } 
        else if (incisSed->Exposantshearstress == 2. && not largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_Exposantshearstress2;
        } 
        else if (incisSed->Exposantshearstress == 1. && largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress1;
        } 
        else if (incisSed->Exposantshearstress == 1.5 && largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress1_5;
        } 
        else if (incisSed->Exposantshearstress == 2. && largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress2;
        } 
        else if (largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_AvecLargeurRiv;
        } 
        else 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil;
        }
    }


    // Parametres de detachement pour le bedrock
    nblitho = modele->GetNbLitho();
    incisbedrock = new ParamErosion*[nblitho];
    MonDetachePotentiel = new PtrFct_Detach[nblitho];


    for (int i = 0; i < nblitho; i++) 
    {
        incisbedrock[i] = (modele->GetLitho(i)).incis;

        // si le seuil de transport est nul ...

        if (incisbedrock[i]->Seuil == 0.) // si le seuil de transport est nul ... - modifs janvier 2013
        {
            if (incisbedrock[i]->NS == 1. && not largeur->OuiNon) 
            {
                if (incisbedrock[i]->MQ == 0.5)
                    MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_M0_5N1;
                else if (incisbedrock[i]->MQ == 1.)
                    MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_M1N1;
                else if (incisbedrock[i]->MQ == 1.5)
                    MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_M1_5N1;
                else
                    MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_N1;
            } 
            else if (incisbedrock[i]->NS == 1. && largeur->OuiNon) 
            {
                if (incisbedrock[i]->MQ == 0.5)
                    MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_M0_5N1_AvecLargeurRiv;
                else if (incisbedrock[i]->MQ == 1.)
                    MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_M1N1_AvecLargeurRiv;
                else if (incisbedrock[i]->MQ == 1.5)
                    MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_M1_5N1_AvecLargeurRiv;
                else
                    MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_N1_AvecLargeurRiv;
            } 
            else if (largeur->OuiNon)
                MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil_AvecLargeurRiv;
            else
                MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil;
        }
            // si le seuil de detachement n est pas nul ...

        else 
        {
            if (incisbedrock[i]->Exposantshearstress == 1. && not largeur->OuiNon) 
            {
                MonDetachePotentiel[i] = &CapaciteErosion_AvecSeuil_Exposantshearstress1;
            } 
            else if (incisbedrock[i]->Exposantshearstress == 1.5 && not largeur->OuiNon) 
            {
                MonDetachePotentiel[i] = &CapaciteErosion_AvecSeuil_Exposantshearstress1_5;
            } 
            else if (incisbedrock[i]->Exposantshearstress == 2. && not largeur->OuiNon) 
            {
                MonDetachePotentiel[i] = &CapaciteErosion_AvecSeuil_Exposantshearstress2;
            } 
            else if (incisbedrock[i]->Exposantshearstress == 1. && largeur->OuiNon) 
            {
                MonDetachePotentiel[i] = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress1;
            } 
            else if (incisbedrock[i]->Exposantshearstress == 1.5 && largeur->OuiNon) 
            {
                MonDetachePotentiel[i] = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress1_5;
            } 
            else if (incisbedrock[i]->Exposantshearstress == 2. && largeur->OuiNon) 
            {
                MonDetachePotentiel[i] = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress2;
            } 
            else if (largeur->OuiNon) 
            {
                MonDetachePotentiel[i] = &CapaciteErosion_AvecSeuil_AvecLargeurRiv;
            } 
            else 
            {
                MonDetachePotentiel[i] = &CapaciteErosion_SansSeuil;
            }
        }
    }

    // Choix de l'equation de la longueur de transport - janvier 2013
    MaLongueurTransport = &Longueur_transport;


    // Actualisation des flux - janvier 2013
    if ((modele->GetOptions()).OptionIle) 
    {
        MonActualiseFlux = &CFlux::ActualiseLesFluxTransportFluvial_ILE;
    } else
    {
        MonActualiseFlux = &CFlux::ActualiseLesFluxTransportFluvial;
    }




    MonVoisinage = NULL; // janvier 2013
    FluxFluvEntrant = 0.; // janvier 2013
    facteur_distrib = 0.; // janvier 2013
    facteur_temps = 0.; // janvier 2013

    return;
}

// destructeur

CErosionFluviale::~CErosionFluviale() {
    delete [] incisbedrock;

}

//=============================================================
//						CALCULS D'EROSION
//=============================================================
//

void CErosionFluviale::DetachTransport_MultFlow_MultiCouche(const int &nummaille, CMaille* maille) // 2018
    // modif en 2023 pour n'avoir qu'une seule fonction en rajoutant le
    // pointeur de fonction MonGetVoisinage qui va dependre de l'option hauteur d'eau ou pas
{

 
    // le voisinage local (voisins plus bas) pour cette maille
    //MonVoisinage = maille->GetVoisinageBas();
    MonVoisinage = (maille->*MonGetVoisinage)(); // modif 2023
    // Recuperation du FLUX d'eau courante
    double MonFluxEau = MesFlux->GetEauCourante(nummaille);
    // On recupere le flux de sediment entrant genere par transport fluvial
    FluxFluvEntrant = MesFlux->GetVolumeFluv(nummaille);
    // le stock de sediments presents sur la maille
    double stocksed = MesFlux->GetStockSed(nummaille);
    // largeur flux (sert pour depot et erosion)
    double width = MonCalculDeLargeurDecoulement(MonFluxEau, largeur, (MonModele->GetTopoInit()).pas); //pb conceptuel ici entre le calcul d'une hauteur d'eau si largeur de l'ecoulement = dx, et d'une largeur d'écoulement calculée par ailleurs ici si on choisit l'option de la calculer comme la sqrt du debit

    // -----
    // DEPOT
    // -----
    double longueurtransport = MaLongueurTransport(MonFluxEau, incisSed->CoeffLongTransporttemp); // modif 2022 CoeffLongTransporttemp
    double fractionfluxentrant = mymin(width / longueurtransport, 1.); // car on calcule wdx (Qs/LQ), comme pour erosion, car (Qs/LQ) a la
    // dimension d'une vitesse (de depot). Le 1/dx est deja incorpore dans le L dans Reformulation().
    double depot = fractionfluxentrant * FluxFluvEntrant;

    // -------
    // EROSION
    // -------
    double erosionsedtotal = 0.; // erosion totale de sed sur la maille
    double erosionbedrocktotal = 0.; // erosion totale de bedrock sur la maille
    double shearstress = 0.; // shearstress (pas necessairement calcule)

    // structure :
    //    erosion sediment
    //	  On erode selon la loi d'erosion pour les sediments
    //	  si plus de sediment erosion du bedrock de couche en couche

    // Erosion des Sediments
    shearstress = MonCalculDeShearStress(MonVoisinage->pentemax, MonFluxEau, incisSed, width);
    erosionsedtotal = MonErosSedFluvPotentiel(MonVoisinage->pentemax, MonFluxEau, incisSed, incisSed->Ktemp, width, shearstress);

    if (erosionsedtotal > stocksed)
    // Si le volume erodable excede le stock de sediment
    // Erosion du Bedrock
    {

        // on erode juste le stock de sediment
        facteur_distrib = stocksed / erosionsedtotal;
        erosionsedtotal = stocksed;
        // on erode le bedrock en commencant par la premiere couche
        double erosioncouche = 0.;
        // les param√®tres  de la premi√®re couche de bedrock
        int indicelitho = MesFlux->GetNumLitho(nummaille);
        ParamErosion* detachement = incisbedrock[indicelitho];
        // le volume local de bedrock de cette premi√®re couche
        double MonVolumeCouche = MesFlux->GetVolumeCouche(nummaille);
        // On erode maintenant le bedrock
        // Pond√©ration du coefficient d'erosion selon le temps d√©j√† pass√© √† diffuser les s√©diments
        facteur_temps = 1. - facteur_distrib;
        double MonKtemp = detachement->Ktemp * facteur_temps;

        erosioncouche = MonDetachePotentiel[indicelitho](MonVoisinage->pentemax,
                MonFluxEau, detachement, MonKtemp, width, shearstress);

        erosionbedrocktotal += erosioncouche;


        // Ensuite on descend dans la pile des couches tant qu'on peut eroder
        while (erosioncouche >= MonVolumeCouche) 
        {
            // on determine le facteur de ponderation du flux erode
            facteur_distrib = MonVolumeCouche / erosioncouche;
            erosionbedrocktotal = erosionbedrocktotal - erosioncouche + MonVolumeCouche;
            // Actualisation de la  couche superficielle de bedrock de la maille
            MonVolumeCouche = MesFlux->IncrementeCoucheLitho(nummaille, maille);
            // on recupere les parametres de diffusion de la nouvelle couche
            indicelitho = MesFlux->GetNumLitho(nummaille);
            detachement = incisbedrock[indicelitho];
            // Ponderation du coefficient de transport selon le temps d√©j√† pass√© √† eroder les s√©diments
            facteur_temps = 1. - facteur_distrib;
            double MonKtemp = detachement->Ktemp * facteur_temps;
            erosioncouche = MonDetachePotentiel[indicelitho](MonVoisinage->pentemax,
                    MonFluxEau, detachement, MonKtemp, width, shearstress);
            erosionbedrocktotal += erosioncouche;
        }
        // on actualise l'epaisseur de la premi√®re couche de la maille

        MesFlux->SoustraitVolumeCouche(nummaille, erosioncouche);
    }

    // ---------
    // TRANSFERT
    // ---------

    double transfertvers [MonVoisinage->nb];
    double transferttotal = FluxFluvEntrant - depot;
    for (int i = 0; i < MonVoisinage->nb; i++)
    {
        transfertvers[i] = (erosionsedtotal + erosionbedrocktotal + transferttotal) * MonVoisinage->coeff[i];
    }
    // ----------------------
    // ACTUALISATION DES FLUX
    // ----------------------

    // Stock de sediments presents sur la maille
    MesFlux->AddStockSed(nummaille, depot - erosionsedtotal);
    // Actualisation des sediments transferes et donc recus par les voisins bas
    (MesFlux->*MonActualiseFlux)(nummaille, maille, MonVoisinage, transfertvers);
    // Actualisation du Depot et Erosion sur la maille
    MesFlux->ActualiseErosionDepot(nummaille, depot, erosionbedrocktotal + erosionsedtotal);
    
    return;
}



//-----------
// EROSION LATERALE
// 7 aout 2008
//-----------

void CErosionFluviale::ErosionLaterale(const int &nummaille, CMaille* maille, CMaillage* MonMaillage) 
{
    int i, j;
    int voisinlateral, directionvoisin;
    double altivoisinlateral = 0.;
    double altimaille = 0.;
    double denivelee = 0.;
    double erosionlateralesed = 0.;
    double erosionlateralebedrock = 0.;
    double propsed = 0.;
    double propbedrock = 0.;

    // le voisinage local pour l'√àrosion
    Tvoisinage_bas* MonVoisinage = (maille->*MonGetVoisinage)(); // modif 2023;

    // Altitude topographique de la maille
    altimaille = maille->GetAlti(); // c'est bien cette altitude du sol qu'on veut meme si on propage tout sur la surface de l'eau



    // Pour chaque direction vers une maille plus basse, on traite les 2 voisins lateraux :
    for (i = 0; i < MonVoisinage->nb; i++) 
    {
        directionvoisin = (maille->*MonGetDirectionVoisinsBas)(i) - 1; // -1 car le tableau qui donne le numero du voisin commence a 0 tandis que la direction va de 1 a 8 // modif2023
        double volerosionlaterale = 0.;
        for (j = 0; j < 2; j++) 
        {
            // 1er voisin lateral dans le sens anti-horaire : j=0
            // 2eme voisin lateral dans le sens anti-horaire : j=1
            double erosion = 0.;
            voisinlateral = maille->GetNumVoisinLateral(directionvoisin, j);
            if (voisinlateral > 0) 
            {
                altivoisinlateral = (MonMaillage->GetMaille(voisinlateral)->GetAlti)();
                denivelee = altivoisinlateral - altimaille;
                if (altivoisinlateral > altimaille) 
                {
                    propsed = mymin(MesFlux->GetStockSed(voisinlateral) / denivelee, 1.);
                    propbedrock = 1. - propsed;

                    erosionlateralesed = incisSed->KErosionLaterale * MesFlux->GetVolumeFluvExporte(nummaille, directionvoisin)
                            * propsed;

                    erosionlateralebedrock = incisbedrock[MesFlux->GetNumLitho(nummaille)]->KErosionLaterale
                            * MesFlux->GetVolumeFluvExporte(nummaille, directionvoisin)
                            * propbedrock;

                    erosion = erosionlateralesed + erosionlateralebedrock;

                    MesFlux->AddVolumeFluv(nummaille, erosion); // on actualise le flux de sediments entrant sur la maille nummaille
                    MesFlux->ActualiseErosionDepot(voisinlateral, 0, erosion); // actualise l'erosion totale sur la maille laterale - janvier 2013
                    MesFlux->AddDeltaErosion(voisinlateral, -erosion); //juin 2014. Pour calculer correctement l'erosion moyenne dans CSortie
                    if (erosionlateralesed > MesFlux->GetStockSed(voisinlateral)) //juin 2014
                    {
                        MesFlux->AnnuleStockSed(voisinlateral);
                    } 
                    else 
                    {
                        MesFlux->AddStockSed(voisinlateral, -erosionlateralesed);
                    }
                    MonMaillage->GetMaille(voisinlateral)->AddLesAlti(-erosion); // on modifie l'altitude de la maille laterale
                    MesFlux->ActualiseVolumeFluvExporte(erosion, voisinlateral, nummaille); //Ajout Pierre, pour mettre à jour VolumeFluvExporte
                    volerosionlaterale += erosion;
                    MesFlux->ActualiseErosionDepot(voisinlateral, 0., -erosion);
                }
            }
        }
        // On depose ces sediments sur la maille numaille, comme si la berge s'ecroulait.
        // On pourrait aussi choisir de les faire partir.
        // Il seront de toute facon pris en compte au prochain pas de temps - janvier 2013
        MesFlux->ActualiseErosionDepot(nummaille, volerosionlaterale, 0);
        MesFlux->AddStockSed(nummaille, volerosionlaterale);
    }


    return;
}




