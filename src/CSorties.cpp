#include "CSorties.h"
#include <iomanip>
//#include <direct.h> // pour windows
//#include <windows.h> // pour windows
#include <sys/types.h> // pour gcc
#include <sys/stat.h> // pour gcc
//#include <sys/types.h> // pour macosx
//#include <dirent.h> // pour macosx
#include <algorithm>
using namespace std;

//-------------------------------------------
//
// fonctions de la classe CSorties :
//
//-------------------------------------------

// constructeur : nbmax = effondr.Nmax

CSorties::CSorties(CModele* modele, CMaillage* maillage, CFlux* flux, CBilanEau* bileau) 
{
    nblig = maillage->GetNbLig();
    nbcol = maillage->GetNbCol();
    nbmailles = maillage->GetNbMailles();
    pas = modele->GetTopoInit().pas;
    coteoctogone = modele->GetTopoInit().coteoctogone; // 25 octobre 2007
    pas2 = maillage->GetPas2();
    pas3 = maillage->GetPas3();
    unsurpas2 = 1. / maillage->GetPas2();
    sortieFluxChimique = false;
    sortieGrillesCailloux = false;


    // Les objets importants
    MonMaillage = maillage;
    MesFlux = flux;
    MonBilanEau = bileau;
    MonModele = modele;

    TSorties resultats = modele->GetParamSorties();
    dt_denud = resultats.dt_denud;
    dt_bassin = resultats.dt_bassin;
    ind_bassin = resultats.ind_bassin;

    if (resultats.DirSortie.size() == 0) 
    {
        FichierDenud = resultats.FichierDenud;
        RadicalBassin = resultats.FichierBassin;
        FichierFluxChimique = resultats.FichierFluxChimique;
        FichierMoyenneEcartType = resultats.FichierMoyenneEcartType;
        // Si les r�sultats sont stock�s dans le r�pertoire courant,
        // il faut nommer le fichier restart de sortie diff�rement
        if (!(modele->GetOptions()).OptionReprise)
            FichierRestart = resultats.FichierRestart;
        else 
        {
            FichierReprise = resultats.FichierRestart;
            FichierRestart = "new-" + FichierRestart;
        }
    } 
    else
    {
        //CreateDirectory(resultats.DirSortie.c_str(),NULL); // pour windows
        mkdir(resultats.DirSortie.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // pour gcc sous mac
        FichierDenud = "./" + resultats.DirSortie + "/" + resultats.FichierDenud; // pour windows on doit enlever "./"+ et remplacer "/" par "\\"
        RadicalBassin = "./" + resultats.DirSortie + "/" + resultats.FichierBassin; // pour windows on doit enlever "./"+ et remplacer "/" par "\\"
        FichierFluxChimique = "./" + resultats.DirSortie + "/" + resultats.FichierFluxChimique; // pour windows on doit enlever "./"+ et remplacer "/" par "\\"
        FichierMoyenneEcartType = "./" + resultats.DirSortie + "/" + resultats.FichierMoyenneEcartType; // pour windows on doit enlever "./"+ et remplacer "/" par "\\"


        // Le fichier de reprise DOIT �tre dans le r�pertoire courant
        FichierReprise = resultats.FichierRestart;
        FichierRestart = "./" + resultats.DirSortie + "/" + resultats.FichierRestart; // pour windows on doit enlever "./"+ et remplacer "/" par "\\"

    }

    numzone = resultats.numzone;
    fich_masque = resultats.fich_masque;

    Masque = NULL;

    MaMorpho = new CMorpho(maillage, flux, resultats);
    // Sauvegarde ou non des rivi�res principales
    if (resultats.OptionRiviere) 
    {
        MonSauveDivers = &CMorpho::SauveRivieres;
    } 
    else 
    {
        MonSauveDivers = &CMorpho::SauveRien;
    }

    // Param�tres de transport pour les s�diments : sert si on calcule le max de capa
    incisSed = modele->GetParamIncisSed();
    largeur = modele->GetParamLargeur(); // ajout  25 octobre 2007
    width = 0.; // ajout  25 octobre 2007
    shearstress = 0; // ajout  25 octobre 2007
    // Choix du calcul du shear stress
    if (incisSed->Seuil > 0.) 
    {
        if (largeur->OuiNon) 
        {
            
            MonCalculDeShearStress = &CalculeLeShearStress; // 25 octobre 2007

        } 
        else 
        {
            MonCalculDeShearStress = &CalculeLeShearStressSansLargeur; // 25 octobre 2007
        }
    } 
    else 
    {
        MonCalculDeShearStress = &NeCalculePasLeShearStress; // 25 octobre 2007
    }

    // Choix du calcul de la largeur de l'ecoulement
    if (largeur->OuiNon) 
    {
        MonCalculDeLargeurDecoulement = &LargeurFonctionDuFluxEau; // 25 octobre 2007
    } 
    else 
    {
        MonCalculDeLargeurDecoulement = &NeRecalculePasLaLargeur; // 25 octobre 2007
    }

    // Choix de l'equation de la capacite de transport
    if (incisSed->Seuil == 0.) // si le seuil d'erosion est nul ... - modifs janvier 2013  ... et juin 2014
    {
        if (incisSed->NS == 1. && not largeur->OuiNon) 
        {
            if (incisSed->MQ == 0.5)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M0_5N1;
            else if (incisSed->MQ == 1.)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M1N1;
            else if (incisSed->MQ == 1.5)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M1_5N1;
            else
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_N1;
        } 
        else if (incisSed->NS == 1. && largeur->OuiNon) 
        {
            if (incisSed->MQ == 0.5)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M0_5N1_AvecLargeurRiv;
            else if (incisSed->MQ == 1.)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M1N1_AvecLargeurRiv;
            else if (incisSed->MQ == 1.5)
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_M1_5N1_AvecLargeurRiv;
            else
                MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_N1_AvecLargeurRiv;
        } 
        else if (largeur->OuiNon)
            MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil_AvecLargeurRiv;
        else
            MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil;
    }
        // si le seuil de detachement n est pas nul ...

    else 
    {
        if (incisSed->Exposantshearstress == 1. && not largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_Exposantshearstress1;
        } 
        else if (incisSed->Exposantshearstress == 1.5 && not largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_Exposantshearstress1_5;
        } 
        else if (incisSed->Exposantshearstress == 2. && not largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_Exposantshearstress2;
        } 
        else if (incisSed->Exposantshearstress == 1. && largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress1;
        } 
        else if (incisSed->Exposantshearstress == 1.5 && largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress1_5;
        } 
        else if (incisSed->Exposantshearstress == 2. && largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_AvecLargeurRiv_Exposantshearstress2;
        } 
        else if (largeur->OuiNon) 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_AvecSeuil_AvecLargeurRiv;
        } 
        else 
        {
            MonErosSedFluvPotentiel = &CapaciteErosion_SansSeuil;
        }
    }

}


// destructeur

CSorties::~CSorties() 
{
    if (Masque != NULL) delete [] Masque;
    delete MaMorpho;
}

// autres fonctions

//---------------------------
// Actualise le numero de la periode
//---------------------------

void CSorties::ActualiseNumPeriod(const int &numperiod) 
{
    NumPeriod = numperiod;
}

//---------------------------
// Initialisation des sorties
//---------------------------

void CSorties::InitialiseSorties() 
{
    temps_denud = 0.;
    temps_bassin = 0.;
    // Ecriture dans un flux de sortie des taux de d�nudation
    // Ouverture du flux de sortie
    FichierSortieDenud.open(FichierDenud.c_str(), ios::out | ios::trunc);
    if (FichierSortieDenud.bad()) 
    {
        cerr << "Erreur lors de l'ouverture du fichier " << FichierDenud << endl;
        FichierSortieDenud.close();
        return;
    }
    FichierSortieDenud << "  Temps  | Taux d'erosion | Alti moyenne | Pente moyenne |"
            << "Nb de lacs | Nb d'effondrements  |  Reg. prod. rate.  | RunOff moyen | Couche sed moy   " << endl; // modifie le 01/02/11


    return;
}

void CSorties::InitialiseSortiesCOSMO()
// 2023 surcharge pour calculer les taux de denudation moyen pour chaque cosmo
{
    temps_denud = 0.;
    temps_bassin = 0.;
    // Ecriture dans un flux de sortie des taux de d�nudation
    // Ouverture du flux de sortie
    FichierSortieDenud.open(FichierDenud.c_str(), ios::out | ios::trunc);
    if (FichierSortieDenud.bad())
    {
        cerr << "Erreur lors de l'ouverture du fichier " << FichierDenud << endl;
        FichierSortieDenud.close();
        return;
    }
    FichierSortieDenud << "  Temps  | Taux d'erosion | Alti moyenne | Pente moyenne |"
            << "Nb de lacs | Nb d'effondrements  |  Reg. prod. rate.  | RunOff moyen | Couche sed moy | Denud10Be | Denud26Al | Denud14C | Denud21Ne | nbgrainsout" << endl; // modifie le 01/02/11


    return;
}

//---------------------------
// Initialisation des sorties avec les Flux Chimiques
//---------------------------

void CSorties::InitialiseSortiesCailloux() 
{
    temps_denud = 0.;
    temps_bassin = 0.;
    // Ecriture dans un flux de sortie des flux chimiques
    // Ouverture du flux de sortie

    FichierSortieFluxChimique.open(FichierFluxChimique.c_str(), ios::out | ios::trunc);
    if (FichierSortieFluxChimique.bad()) 
    {
        cerr << "Erreur lors de l'ouverture du fichier " << FichierFluxChimique << endl;
        FichierSortieFluxChimique.close();
        return;
    }
    FichierSortieFluxChimique << "time  | [Ca]/dt  | [Na]/dt  | [Si]/dt  | [K]/dt  | [Mg]/dt  | [S]/dt   | integrated_dissolved_vol  | dissolved_vol/unit_of_clast_volume/nb_of_clasts/dt  | nbDissolvedClasts" << endl;
    FichierSortieFluxChimique << "[Cations]/dt in moles/year ; dt is the last time step ; the dissolved_vol are m3/year and 1/year resp.  |  |  |  |  |  |  |  |  | " << endl;

    FichierSortieMoyenneEcartType.open(FichierMoyenneEcartType.c_str(), ios::out | ios::trunc);
    if (FichierSortieMoyenneEcartType.bad()) 
    {
        cerr << "Erreur lors de l'ouverture du fichier " << FichierMoyenneEcartType << endl;
        FichierSortieMoyenneEcartType.close();
        return;
    }
    FichierSortieMoyenneEcartType << "time | deviation standart | mean downstream distance" << endl;

    return;
}

void CSorties::InitialiseSortiesCaillouxWithoutDissolution()
{
    temps_denud = 0.;
    temps_bassin = 0.;
    // Ecriture dans un flux de sortie des flux chimiques
    // Ouverture du flux de sortie
    
    
    FichierSortieMoyenneEcartType.open(FichierMoyenneEcartType.c_str(), ios::out | ios::trunc);
    if (FichierSortieMoyenneEcartType.bad())
    {
        cerr << "Erreur lors de l'ouverture du fichier " << FichierMoyenneEcartType << endl;
        FichierSortieMoyenneEcartType.close();
        return;
    }
    FichierSortieMoyenneEcartType << "time | deviation standart | mean downstream distance" << endl;
    
    return;
}

//---------------------------
// Initialisation des sorties
//---------------------------

void CSorties::InitialiseSorties_Zone(ParamTecto tecto) 
{
    int i, j;
    int j1 = tecto.numzone[numzone - 1];
    int j2 = tecto.numzone[numzone];
    // Fabrication du masque � partir de la zone :
    if (Masque != NULL) delete [] Masque;
    Masque = new bool[nbmailles];
    for (i = 1; i <= nbcol; i++) 
    {
        for (j = 1; j < j1; j++)
            Masque[(j - 1) * nbcol + i - 1] = false;
        for (j = j1; j <= j2; j++)
            Masque[(j - 1) * nbcol + i - 1] = true;
        for (j = j2 + 1; j <= nblig; j++)
            Masque[(j - 1) * nbcol + i - 1] = false;
    }

    temps_denud = 0.;
    temps_bassin = 0.;
    // Ecriture dans un flux de sortie des taux de d�nudation
    // Ouverture du flux de sortie
    FichierSortieDenud.open(FichierDenud.c_str(), ios::out | ios::trunc);
    if (FichierSortieDenud.bad()) 
    {
        cerr << "Erreur lors de l'ouverture du fichier " << &FichierSortieDenud << endl;
        FichierSortieDenud.close();
        return;
    }
    FichierSortieDenud << "Temps  | Taux d'erosion  | Alti moyenne | Pente moyenne | "
            << "Nb de lacs | Nb d'effondrements |  Reg. prod. rate.  | RunOff moyen | Couche sed moy | Taux d'erosion masque  | Alti moyenne masque |  "
            << "Pente moyenne masque|  Reg. prod. rate. masque  | RunOff moyen masque | Couche sed moy masque " << endl;


    return;
}

//---------------------------
// Initialisation des sorties
//---------------------------

void CSorties::InitialiseSorties_Masque() 
{
    int k;
    double valeur;

    if (Masque != NULL) delete [] Masque;
    Masque = new bool[nbmailles];

    filebuf MonFichier;
    // Ouverture du flux d'entr�e
    if (MonFichier.open(fich_masque.c_str(), ios::in) == NULL) 
    {
        cerr << "Le fichier n'existe pas, ou n'est pas lisible !! " << fich_masque << endl;
        return;
    } 
    else 
    {
        istream FichierEntree(&MonFichier);
        AfficheMessage("> Lecture du fichier masque : ", fich_masque, "");

        for (k = 0; k < nbmailles; k++) 
        {
            FichierEntree >> valeur;
            if (valeur > 0.)
                Masque[k] = true;
            else
                Masque[k] = false;
        }
    }
    // Fermeture du fichier
    MonFichier.close();

    temps_denud = 0.;
    temps_bassin = 0.;
    // Ecriture dans un flux de sortie des taux de d�nudation
    // Ouverture du flux de sortie
    FichierSortieDenud.open(FichierDenud.c_str(), ios::out | ios::trunc);
    if (FichierSortieDenud.bad()) 
    {
        cerr << "Erreur lors de l'ouverture du fichier " << &FichierSortieDenud << endl;
        FichierSortieDenud.close();
        return;
    }
    FichierSortieDenud << "Temps  | Taux d'erosion  | Alti moyenne | Pente moyenne | "
            << "Nb de lacs | Nb d'effondrements |  Reg. prod. rate.  | RunOff moyen | Couche sed moy | Taux d'erosion masque  | Alti moyenne masque |  "
            << "Pente moyenne masque |  Reg. prod. rate. masque  | RunOff moyen masque | Couche sed moy masque " << endl;


    return;
}

//----------------------
// Fermeture des sorties
//----------------------

void CSorties::FinitSorties() 
{
    // Fermeture du fichier de denudation
    FichierSortieDenud.close();
    AfficheMessage("*** Fichier de sortie de denudation ecrit ***");
    return;
}

void CSorties::FinitSortiesCailloux() 
{
    // Fermeture du fichier de flux chimique
    FichierSortieFluxChimique.close();
    AfficheMessage("*** Fichier de sortie des flux chimiques ecrit ***");
    return;

    // Fermeture du fichier de repartition des clasts
    FichierSortieMoyenneEcartType.close();
    AfficheMessage("*** Fichier de sortie de repartition des clasts ecrit ***");
    return;
}

void CSorties::FinitSortiesCaillouxWithoutDissolution()
{
    // Fermeture du fichier de repartition des clasts
    FichierSortieMoyenneEcartType.close();
    AfficheMessage("*** Fichier de sortie de repartition des clasts ecrit ***");
    return;
}

//---------------------------------------------
// Ecriture des sorties : dans un fichier binaire
//---------------------------------------------
// Renvoie TRUE s'il y a eu une sortie dans "denudation"
// 2023 ajout de const vector<CCaillou> & listeCaillou comme argument par compatibilité avec la definition du prototype de la fonction

bool CSorties::IncrementeBIN(const double &temps, const double &dt,
        const int &NbEffondrements, const int &NbLacs, const vector<CCaillou> & listeCaillou)
{
    int i, k;
    bool flag = false;
    double* pente;
    pente = new double[nbmailles];
    int* airedrainee;
    airedrainee = new int[nbmailles];
    double fractempspluie = MonModele->GetFracTempsPluie(NumPeriod);
    int nfile = 7; // 2022
    if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionHauteurEau) nfile = 8;
    
    
    ////////////////////////
    ////// Sortie PARAMETRES MOYENS SUR LA GRILLE
    ////////////////////////
    temps_denud += dt;
    if (temps_denud >= dt_denud) 
    {
        temps_denud -= dt_denud;
        sortieFluxChimique = true;
        int nbcalc = 0;

        double altiloc = 0.;
        double tauxderosion = 0.;
        double altimoy = 0.;
        double regprodrate = 0.; // le 01/02/11
        double runoffmoy = 0.; // le 01/02/11
        double epaisseursedmoy = 0; // le 01/02/11
        double pentemoy = 0; // 19/01/12

        // CALCUL DES PENTES EN STEEPEST DESCENT //  19/01/12
        //---------------------------------------------------
        for (k = 0; k < nbmailles; k++) 
        {
            pente[k] = 0.;
        }
        for (i = 0; i < nbmailles; i++) 
        {
            k = MonMaillage->GetNumListe(i);
            PentesMaille_SteepDesc(MonMaillage->GetMaille(k), MonMaillage, &CMaille::GetAlti, pas3);
            pente[k] = MonMaillage->GetMaille(k)->GetPente(0);
        }
        //---------------------------------------------------
        
        for (k = 0; k < nbmailles; k++)
        {
            altiloc = MonMaillage->GetMaille(k)->GetAlti();
            if (MonMaillage->MailleTerre(k)) 
            {
                nbcalc++;
                altimoy += altiloc;
                pentemoy += pente[k]; // 19/01/12
                tauxderosion += MesFlux->GetDeltaErosion(k);
                regprodrate += (MesFlux->GetRegolitheCree(k) / pas2) / dt; //modifie le 01/02/11
                runoffmoy += MesFlux->GetEauCourante(k) / pas2; //modifie le 01/02/11
                epaisseursedmoy += (MesFlux->GetStockSed(k) - MesFlux->GetDepot(k)) / pas2; // on enleve le dernier depot, qui depend de dt.
                if (epaisseursedmoy < 0.) epaisseursedmoy = 0.;
            }
        }
        
        
        altimoy /= nbcalc*pas2;
        tauxderosion /= -nbcalc * dt*pas2;
        regprodrate /= nbcalc; //ajoute le 01/02/11
        runoffmoy /= nbcalc; //ajoute le 01/02/11
        epaisseursedmoy /= nbcalc;
        pentemoy /= nbcalc; // 19/01/12
        FichierSortieDenud.setf(ios::fixed);
        FichierSortieDenud
                << setfill(' ') << setw(8) << (int) temps << " | "
                << setprecision(9) << setw(11) << (double) tauxderosion << "    | "
                << setprecision(2) << setw(9) << (double) altimoy << "    | "
                << setprecision(6) << setw(9) << (double) pentemoy << "    | "
                << setfill(' ') << setw(7) << int(double(NbLacs) / dt_denud) << "    | "
                << setfill(' ') << setw(11) << (int) NbEffondrements << "    | "
                << setprecision(9) << setw(9) << (double) regprodrate << "    | "
                << setprecision(2) << setw(9) << (double) runoffmoy * fractempspluie << "    | "
                << setprecision(2) << setw(9) << (double) epaisseursedmoy << endl;
        flag = true;
    }

    //////////////////////////////
    ////// Sortie DES GRILLES
    //////////////////////////////
    temps_bassin += dt;
    if (temps_bassin >= dt_bassin) 
    {
        temps_bassin -= dt_bassin;
        ind_bassin++;
        AfficheMessage("Grid outputs in : ", RadicalBassin, ind_bassin);

        sortieGrillesCailloux = true;
        // Sauvegarde des rivi�res
        (MaMorpho->*MonSauveDivers)(ind_bassin);

        // Sauvegarde des grilles
        string FichierGrille[nfile]; // modif 2022
        string RadicalFichier;
        ostringstream NumFichier;
        ofstream FichierSortie[nfile]; // modif 2022

        // Fabrication des noms de fichiers de sortie
        NumFichier << ind_bassin;
        if (ind_bassin < 10)
            RadicalFichier = RadicalBassin + "00" + NumFichier.str();
        else if (ind_bassin < 100)
            RadicalFichier = RadicalBassin + "0" + NumFichier.str();
        else
            RadicalFichier = RadicalBassin + NumFichier.str();
        FichierGrille[0] = RadicalFichier + ".z";
        FichierGrille[1] = RadicalFichier + ".sed";
        FichierGrille[2] = RadicalFichier + ".water";
        FichierGrille[3] = RadicalFichier + ".eros";
        FichierGrille[4] = RadicalFichier + ".area"; //  19/01/12
        FichierGrille[5] = RadicalFichier + ".slope"; //  19/01/12
        FichierGrille[6] = RadicalFichier + ".rain"; // 2018
        if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionHauteurEau) FichierGrille[7] = RadicalFichier + ".hwater"; // 2022 2023


        // Ecriture dans un flux de sortie
        for (i = 0; i < nfile; i++) // modif 2022
        {
            FichierSortie[i].open(FichierGrille[i].c_str(), ios::out | ios::trunc | ios::binary);
            if (FichierSortie[i].bad()) 
            {
                cerr << "Impossible to output files !! " << FichierGrille[i] << endl;
                FichierSortie[i].close();
                return false;
            }
        }
        
        // CALCUL DES PENTES EN STEEPEST DESCENT ET DES AIRES DRAINEES. //  19/01/12
        //-------------------------------------------------------------------------
        int ksuiv = 0;
        for (k = 0; k < nbmailles; k++) 
        {
            airedrainee[k] = 1;
            pente[k] = 0.;
        }
        for (i = 0; i < nbmailles; i++) 
        {
            k = MonMaillage->GetNumListe(i);
            PentesMaille_SteepDesc(MonMaillage->GetMaille(k), MonMaillage, &CMaille::GetAlti, pas3);
            pente[k] = MonMaillage->GetMaille(k)->GetPente(0);
            ksuiv = MonMaillage->GetMaille(k)->GetNumVoisinBas();
            if (ksuiv >= 0) airedrainee[ksuiv] += airedrainee[k];
        }
        //-------------------------------------------------------------------------

        float var[nfile]; // modif 2022
        // Ecriture des r�sultats
        for (k = 0; k < nbmailles; k++) 
        {
            //%%%%% ALTI
            var[0] = (float) unsurpas2 * MonMaillage->GetMaille(k)->GetAlti();
            //%%%%% STOCK SED
            var[1] = (float) unsurpas2 * mymax(MesFlux->GetStockSed(k) - MesFlux->GetDepot(k), 0.); // on enleve le dernier depot, qui depend de dt.
            //%%%%% EAU COURANTE
            var[2] = (float) MesFlux->GetEauCourante(k) * fractempspluie;
            //if (MesFlux->GetEauCourante(k)<0) cout <<k << endl;
            //%%%%% DELTA EROSION
            var[3] = (float) unsurpas2 * MesFlux->GetDeltaErosion(k) / dt; // ajout le 9 mai 2007 : division par dt pour avoir taux d'erosion
            //%%%%% AIRES DRAINEES
            var[4] = (float) airedrainee[k]; //  19/01/12
            //%%%%% PENTES
            var[5] = (float) pente[k]; //  19/01/12
            //%%%%% RAINFALL
            var[6] = (float) MesFlux->GetRainfall(k) * unsurpas2; //  2018
            //%%%%% WATER HEIGHT
            if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionSansLacs) var[7] = (float) unsurpas2 * (MonMaillage->GetMaille(k)->GetAltiEau()-MonMaillage->GetMaille(k)->GetAlti()); //  2022 2023
            
            // Ecriture dans les fichiers
            for (i = 0; i < nfile; i++) // modif 2022
                FichierSortie[i].write((char*) &var[i], sizeof (float));
        }
        // Fermeture des fichiers
        for (i = 0; i < nfile; i++) // modif 2022
            FichierSortie[i].close();
        
    }
    delete [] pente;
    delete [] airedrainee;

    return flag;
}

//---------------------------------------------
// Ecriture des sorties : dans un fichier binaire
//---------------------------------------------
// Renvoie TRUE s'il y a eu une sortie dans "denudation"
// Surcharge dans le cas du calcul du taux de denudation issue de la moyenne en cosmo
// 2023
bool CSorties::IncrementeBINCOSMO(const double &temps, const double &dt,
        const int &NbEffondrements, const int &NbLacs, const vector<CCaillou> & listeCaillou)
{
    int i, k;
    bool flag = false;
    double* pente;
    pente = new double[nbmailles];
    int* airedrainee;
    airedrainee = new int[nbmailles];
    double fractempspluie = MonModele->GetFracTempsPluie(NumPeriod);
    int nfile = 7; // 2022
    if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionHauteurEau) nfile = 8;

    
    ////////////////////////
    ////// Sortie PARAMETRES MOYENS SUR LA GRILLE
    ////////////////////////
    temps_denud += dt;

    if (temps_denud >= dt_denud)
    {
        temps_denud -= dt_denud;
        sortieFluxChimique = true;
        int nbcalc = 0;

        double altiloc = 0.;
        double tauxderosion = 0.;
        double altimoy = 0.;
        double regprodrate = 0.; // le 01/02/11
        double runoffmoy = 0.; // le 01/02/11
        double epaisseursedmoy = 0; // le 01/02/11
        double pentemoy = 0; // 19/01/12
        

        ////// CALCUL DU TAUX DE DENUDATION MOYEN A PARTIR DES ISOTOPES COSMOGENIQUES  2023
        //---------------------------------------------------
        double pressure = 0.;
        double pressurepressure = 0.;
        double spallation = 0.;
        double slowmuonCapture = 0.;
        double fastmuon = 0.;
        double meanspallation = 0.;
        double meanslowmuonCapture = 0.;
        double meanfastmuon = 0.;
        //double meansurfaceProductionRate[4];
        double meanCosmoDenudationRate[4];
        double meanConcentration[4];

        
        vector<double> lambda;
        vector<double> productionRateSeeLevel;
        vector<CCaillou> listedeCaillou = listeCaillou;
        int nbCailloux(int(listedeCaillou.size()));
        
        
        if (MonModele->GetOptions().CosmonucleideEtudie[0])
        {
            lambda.push_back(Lambda10Be);
            productionRateSeeLevel.push_back(ProductionRateSeeLevel10Be);
        }
        if (MonModele->GetOptions().CosmonucleideEtudie[1])
        {
            lambda.push_back(Lambda26Al);
            productionRateSeeLevel.push_back(ProductionRateSeeLevel26Al);
        }
        if (MonModele->GetOptions().CosmonucleideEtudie[2])
        {
            lambda.push_back(Lambda14C);
            productionRateSeeLevel.push_back(ProductionRateSeeLevel14C);
        }
        if (MonModele->GetOptions().CosmonucleideEtudie[3])
        {
            lambda.push_back(Lambda21Ne);
            productionRateSeeLevel.push_back(ProductionRateSeeLevel21Ne);
        }
        for (int j=0 ; j<4 ; j++)
        {
            meanConcentration[j] = 0. ;
        }
        
        // concentration moyenne de l'ensemble des cailloux pour chaque isotope etudie
        int ncaillouxsortis = 0;
        for (i = 0; i < nbCailloux; i++)
        {
            if (!listeCaillou[i].getDansLaGrille() && listeCaillou[i].GetDeadInTheLastIteration())
            {
                ncaillouxsortis++;
                vector<double> cosmoconc=listeCaillou[i].getCosmoConcentration();
                int inucl = -1;
                for (int j=0 ; j<4 ; j++)
                {
                    if (MonModele->GetOptions().CosmonucleideEtudie[j])
                    {
                        inucl += 1;
                        meanConcentration[j] += listeCaillou[i].getCosmoConcentration()[j];
                    }
                    else
                    {
                        meanConcentration[j] = 0.;
                    }
                }
            }
        }
        
        
        if (ncaillouxsortis > 0)
        {
            for (k = 0; k < nbmailles; k++)
            {
                    altiloc = MonMaillage->GetMaille(k)->GetAlti() / pas2;
                    pressure = 1013.25 * pow(1.- (0.00002255 * altiloc),5.257);
                    pressurepressure = pressure*pressure;
                    spallation += aStone + bStone*exp(-pressure/150.) + cStone*pressure + dStone*pressurepressure
                    + eStone*pressurepressure*pressure;
                    //muonCapture = MStone *exp((1013.25-pressure)/242.);
                    slowmuonCapture += exp((1013.25-pressure)/260.); // 2023
                    fastmuon += exp((1013.25-pressure)/510.); // 2023

               
            }
            int inucl = -1;
            for (int i=0 ; i<4 ; i++)
            {
                meanConcentration[i] = meanConcentration[i] / ncaillouxsortis;
                if (MonModele->GetOptions().CosmonucleideEtudie[i])
                {
                    inucl += 1;
                    meanspallation = spallation * FracNeutronSLHL * productionRateSeeLevel[inucl]   / nbmailles;
                    meanslowmuonCapture = slowmuonCapture * FracSlowMuonSLHL * productionRateSeeLevel[inucl]   / nbmailles;
                    meanfastmuon = fastmuon * FracFastMuonSLHL * productionRateSeeLevel[inucl]   / nbmailles;
                    meanCosmoDenudationRate[i] = (meanspallation * AttenuationNeutron +  meanslowmuonCapture * AttenuationSlowMuon +  meanfastmuon * AttenuationFastMuon) /(rho * meanConcentration[i]); // en m/a
                }
                else
                {
                    meanCosmoDenudationRate[i] =-1.; // -1 signifie que le taux de denud n'est pas calcule
                }
            }
            
        }
        else
        {
            for (int i=0 ; i<4 ; i++)
            {
                meanCosmoDenudationRate[i] =-1.; // -1 signifie que le taux de denud n'est pas calcule
            }
        }
        
        
        

        // CALCUL DES PENTES EN STEEPEST DESCENT //  19/01/12
        //---------------------------------------------------
        for (k = 0; k < nbmailles; k++)
        {
            pente[k] = 0.;
        }
        for (i = 0; i < nbmailles; i++)
        {
            k = MonMaillage->GetNumListe(i);
            PentesMaille_SteepDesc(MonMaillage->GetMaille(k), MonMaillage, &CMaille::GetAlti, pas3);
            pente[k] = MonMaillage->GetMaille(k)->GetPente(0);
        }
        //---------------------------------------------------
        
        
        
        for (k = 0; k < nbmailles; k++)
        {
            altiloc = MonMaillage->GetMaille(k)->GetAlti();
            if (MonMaillage->MailleTerre(k))
            {
                nbcalc++;
                altimoy += altiloc;
                pentemoy += pente[k]; // 19/01/12
                tauxderosion += MesFlux->GetDeltaErosion(k);
                regprodrate += (MesFlux->GetRegolitheCree(k) / pas2) / dt; //modifie le 01/02/11
                runoffmoy += MesFlux->GetEauCourante(k) / pas2; //modifie le 01/02/11
                epaisseursedmoy += (MesFlux->GetStockSed(k) - MesFlux->GetDepot(k)) / pas2; // on enleve le dernier depot, qui depend de dt.
                if (epaisseursedmoy < 0.) epaisseursedmoy = 0.;
            }
        }
        
        
        altimoy /= nbcalc*pas2;
        tauxderosion /= -nbcalc * dt*pas2;
        regprodrate /= nbcalc; //ajoute le 01/02/11
        runoffmoy /= nbcalc; //ajoute le 01/02/11
        epaisseursedmoy /= nbcalc;
        pentemoy /= nbcalc; // 19/01/12
        FichierSortieDenud.setf(ios::fixed);
        FichierSortieDenud
                << setfill(' ') << setw(8) << (int) temps << " | "
                << setprecision(9) << setw(11) << (double) tauxderosion << "    | "
                << setprecision(2) << setw(9) << (double) altimoy << "    | "
                << setprecision(6) << setw(9) << (double) pentemoy << "    | "
                << setfill(' ') << setw(7) << int(double(NbLacs) / dt_denud) << "    | "
                << setfill(' ') << setw(11) << (int) NbEffondrements << "    | "
                << setprecision(9) << setw(9) << (double) regprodrate << "    | "
                << setprecision(2) << setw(9) << (double) runoffmoy * fractempspluie << "    | "
                << setprecision(2) << setw(9) << (double) epaisseursedmoy << "    | "
                << setprecision(9) << setw(11) << (double) meanCosmoDenudationRate[0]  << "    | "
                << setprecision(9) << setw(11) << (double) meanCosmoDenudationRate[1]<< "    | "
                << setprecision(9) << setw(11) << (double) meanCosmoDenudationRate[2]<< "    | "
                << setprecision(9) << setw(11) << (double) meanCosmoDenudationRate[3] << "    | "
        << setprecision(9) << setw(11) << (int) ncaillouxsortis << "    | "<< endl;
        flag = true;
    }

    //////////////////////////////
    ////// Sortie DES GRILLES
    //////////////////////////////
    temps_bassin += dt;
    if (temps_bassin >= dt_bassin)
    {
        temps_bassin -= dt_bassin;
        ind_bassin++;
        AfficheMessage("Grid outputs in : ", RadicalBassin, ind_bassin);

        sortieGrillesCailloux = true;
        // Sauvegarde des rivi�res
        (MaMorpho->*MonSauveDivers)(ind_bassin);

        // Sauvegarde des grilles
        string FichierGrille[nfile]; // modif 2022
        string RadicalFichier;
        ostringstream NumFichier;
        ofstream FichierSortie[nfile]; // modif 2022

        // Fabrication des noms de fichiers de sortie
        NumFichier << ind_bassin;
        if (ind_bassin < 10)
            RadicalFichier = RadicalBassin + "00" + NumFichier.str();
        else if (ind_bassin < 100)
            RadicalFichier = RadicalBassin + "0" + NumFichier.str();
        else
            RadicalFichier = RadicalBassin + NumFichier.str();
        FichierGrille[0] = RadicalFichier + ".z";
        FichierGrille[1] = RadicalFichier + ".sed";
        FichierGrille[2] = RadicalFichier + ".water";
        FichierGrille[3] = RadicalFichier + ".eros";
        FichierGrille[4] = RadicalFichier + ".area"; //  19/01/12
        FichierGrille[5] = RadicalFichier + ".slope"; //  19/01/12
        FichierGrille[6] = RadicalFichier + ".rain"; // 2018
        if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionHauteurEau) FichierGrille[7] = RadicalFichier + ".hwater"; // 2022 2023


        // Ecriture dans un flux de sortie
        for (i = 0; i < nfile; i++) // modif 2022
        {
            FichierSortie[i].open(FichierGrille[i].c_str(), ios::out | ios::trunc | ios::binary);
            if (FichierSortie[i].bad())
            {
                cerr << "Impossible to output files !! " << FichierGrille[i] << endl;
                FichierSortie[i].close();
                return false;
            }
        }
        
        // CALCUL DES PENTES EN STEEPEST DESCENT ET DES AIRES DRAINEES. //  19/01/12
        //-------------------------------------------------------------------------
        int ksuiv = 0;
        for (k = 0; k < nbmailles; k++)
        {
            airedrainee[k] = 1;
            pente[k] = 0.;
        }
        for (i = 0; i < nbmailles; i++)
        {
            k = MonMaillage->GetNumListe(i);
            PentesMaille_SteepDesc(MonMaillage->GetMaille(k), MonMaillage, &CMaille::GetAlti, pas3);
            pente[k] = MonMaillage->GetMaille(k)->GetPente(0);
            ksuiv = MonMaillage->GetMaille(k)->GetNumVoisinBas();
            if (ksuiv >= 0) airedrainee[ksuiv] += airedrainee[k];
        }
        //-------------------------------------------------------------------------

        float var[nfile]; // modif 2022
        // Ecriture des r�sultats
        for (k = 0; k < nbmailles; k++)
        {
            //%%%%% ALTI
            var[0] = (float) unsurpas2 * MonMaillage->GetMaille(k)->GetAlti();
            //%%%%% STOCK SED
            var[1] = (float) unsurpas2 * mymax(MesFlux->GetStockSed(k) - MesFlux->GetDepot(k), 0.); // on enleve le dernier depot, qui depend de dt.
            //%%%%% EAU COURANTE
            var[2] = (float) MesFlux->GetEauCourante(k) * fractempspluie;
            //if (MesFlux->GetEauCourante(k)<0) cout <<k << endl;
            //%%%%% DELTA EROSION
            var[3] = (float) unsurpas2 * MesFlux->GetDeltaErosion(k) / dt; // ajout le 9 mai 2007 : division par dt pour avoir taux d'erosion
            //%%%%% AIRES DRAINEES
            var[4] = (float) airedrainee[k]; //  19/01/12
            //%%%%% PENTES
            var[5] = (float) pente[k]; //  19/01/12
            //%%%%% RAINFALL
            var[6] = (float) MesFlux->GetRainfall(k) * unsurpas2; //  2018
            //%%%%% WATER HEIGHT
            if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionSansLacs) var[7] = (float) unsurpas2 * (MonMaillage->GetMaille(k)->GetAltiEau()-MonMaillage->GetMaille(k)->GetAlti()); //  2022 2023
            
            // Ecriture dans les fichiers
            for (i = 0; i < nfile; i++) // modif 2022
                FichierSortie[i].write((char*) &var[i], sizeof (float));
        }
        // Fermeture des fichiers
        for (i = 0; i < nfile; i++) // modif 2022
            FichierSortie[i].close();
        
    }
    delete [] pente;
    delete [] airedrainee;

    return flag;
}

//-----------------------------
// Surcharge s'il y a un masque
//-----------------------------

bool CSorties::IncrementeBIN_Masque(const double &temps, const double &dt,
        const int &NbEffondrements, const int &NbLacs, const vector<CCaillou> & listeCaillou) // modif 2023
{
    bool flag = false;
    int i, k;
    double* pente;
    pente = new double[nbmailles];
    int* airedrainee;
    airedrainee = new int[nbmailles];
    int ksuiv;
    double fractempspluie = MonModele->GetFracTempsPluie(NumPeriod);
    int nfile = 7; // 2022
    if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionHauteurEau) nfile = 8;

    ////////////////////////
    ////// Sortie DENUDATION
    ////////////////////////
    temps_denud += dt;
    if (temps_denud >= dt_denud) 
    {
        temps_denud -= dt_denud;
        sortieFluxChimique = true;

        int nbcalc = 0;

        double tauxloc;
        double altiloc = 0.;
        double tauxderosion = 0.;
        double altimoy = 0.;
        double pentemoy = 0.; // 19/01/12
        double regprodrate = 0.; // le 01/02/11
        double runoffmoy = 0.; // le 01/02/11
        double epaisseursedmoy = 0; // le 01/02/11

        double tauxderosion_masque = 0.;
        double altimoy_masque = 0.; // 19/01/12
        double pentemoy_masque = 0.; // 19/01/12
        double regprodrate_masque = 0.; // le 01/02/11
        double runoffmoy_masque = 0.; // le 01/02/11
        double epaisseursedmoy_masque = 0; // le 01/02/11
        int nbmasque = 0;
        

       


        // CALCUL DES PENTES EN STEEPEST DESCENT  //  19/01/12
        //------------------------------------------------------

        for (k = 0; k < nbmailles; k++) 
        {
            pente[k] = 0.;
        }
        for (i = 0; i < nbmailles; i++) 
        {
            k = MonMaillage->GetNumListe(i);
            PentesMaille_SteepDesc(MonMaillage->GetMaille(k), MonMaillage, &CMaille::GetAlti, pas3);
            pente[k] = MonMaillage->GetMaille(k)->GetPente(0);
            
        }
        //--------------------------------------------------------

        for (k = 0; k < nbmailles; k++) 
        {
            altiloc = MonMaillage->GetMaille(k)->GetAlti();
            tauxloc = MesFlux->GetDeltaErosion(k);
            tauxderosion += tauxloc;
            altimoy += altiloc;
            pentemoy += pente[k];
            regprodrate += (MesFlux->GetRegolitheCree(k) / pas2) / dt; //modifie le 01/02/11
            runoffmoy += MesFlux->GetEauCourante(k) / pas2; //modifie le 01/02/11
            epaisseursedmoy += MesFlux->GetStockSed(k) / pas2;
            nbcalc++;
            if (Masque[k]) 
            {
                tauxderosion_masque += tauxloc;
                altimoy_masque += altiloc;
                pentemoy_masque += pente[k];
                regprodrate_masque += (MesFlux->GetRegolitheCree(k) / pas2) / dt; //modifie le 01/02/11
                runoffmoy_masque += MesFlux->GetEauCourante(k) / pas2; //modifie le 01/02/11
                epaisseursedmoy_masque += (MesFlux->GetStockSed(k) - MesFlux->GetDepot(k)) / pas2; // on enleve le dernier depot, qui depend de dt.
                if (epaisseursedmoy_masque < 0.) epaisseursedmoy_masque = 0.;
                nbmasque++;
            }
            
        }

        

        altimoy /= nbcalc*pas2;
        tauxderosion /= -nbcalc * dt*pas2;
        regprodrate /= nbcalc; //ajoute le 01/02/11
        runoffmoy /= nbcalc; //ajoute le 01/02/11
        epaisseursedmoy /= nbcalc;
        pentemoy /= nbcalc; // 19/01/12
        altimoy_masque /= nbmasque*pas2;
        tauxderosion_masque /= -nbmasque * dt*pas2;
        regprodrate_masque /= nbmasque; //ajoute le 01/02/11
        runoffmoy_masque /= nbmasque; //ajoute le 01/02/11
        epaisseursedmoy_masque /= nbmasque;
        pentemoy_masque /= nbmasque; // 19/01/12

        FichierSortieDenud.setf(ios::fixed);
        FichierSortieDenud
                << setfill(' ') << setw(8) << (int) temps << " | "
                << setprecision(6) << setw(11) << (double) tauxderosion << "    | "
                << setprecision(2) << setw(9) << (double) altimoy << "    | "
                << setprecision(6) << setw(9) << (double) pentemoy << "    | "
                << setfill(' ') << setw(7) << int(double(NbLacs) / dt_denud) << "    | "
                << setfill(' ') << setw(11) << NbEffondrements << "    | "
                << setprecision(6) << setw(9) << (double) regprodrate << "    | "
                << setprecision(2) << setw(9) << (double) runoffmoy * fractempspluie << "    | "
                << setprecision(2) << setw(9) << (double) epaisseursedmoy << " | "
                << setprecision(6) << setw(11) << (double) tauxderosion_masque << "    | "
                << setprecision(2) << setw(9) << (double) altimoy_masque << "    | "
                << setprecision(6) << setw(9) << (double) pentemoy_masque << "    | "
                << setprecision(6) << setw(9) << (double) regprodrate_masque << "    | "
                << setprecision(2) << setw(9) << (double) runoffmoy_masque * fractempspluie << "    | "
                << setprecision(2) << setw(9) << (double) epaisseursedmoy_masque << "    | "
                << endl;
        flag = true;

    }

    //////////////////////////////
    ////// Sortie BASSIN (grilles)
    //////////////////////////////
    temps_bassin += dt;
    if (temps_bassin >= dt_bassin) 
    {
        temps_bassin -= dt_bassin;
        ind_bassin++;
        AfficheMessage("Sortie des grilles dans les fichiers : ", RadicalBassin, ind_bassin);

        // Sauvegarde des rivi�res
        (MaMorpho->*MonSauveDivers)(ind_bassin);

        // Sauvegarde des grilles
        string FichierGrille[nfile]; // modif 2022
        string RadicalFichier;
        ostringstream NumFichier;
        ofstream FichierSortie[nfile]; // modif 2022

        // Fabrication des noms de fichiers de sortie
        NumFichier << ind_bassin;
        if (ind_bassin < 10)
            RadicalFichier = RadicalBassin + "00" + NumFichier.str();
        else if (ind_bassin < 100)
            RadicalFichier = RadicalBassin + "0" + NumFichier.str();
        else
            RadicalFichier = RadicalBassin + NumFichier.str();
        FichierGrille[0] = RadicalFichier + ".z";
        FichierGrille[1] = RadicalFichier + ".sed";
        FichierGrille[2] = RadicalFichier + ".water";
        FichierGrille[3] = RadicalFichier + ".eros";
        FichierGrille[4] = RadicalFichier + ".area"; //  19/01/12
        FichierGrille[5] = RadicalFichier + ".slope"; //  19/01/12
        FichierGrille[6] = RadicalFichier + ".rain"; // 2018
        if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionSansLacs) FichierGrille[7] = RadicalFichier + ".hwater"; // 2022


        // Ecriture dans un flux de sortie
        for (i = 0; i < nfile; i++) // modif 2022
        {
            FichierSortie[i].open(FichierGrille[i].c_str(), ios::out | ios::trunc | ios::binary);
            if (FichierSortie[i].bad()) 
            {
                cerr << "Impossible d'ouvrir un flux de sortie !! " << FichierGrille[i] << endl;
                FichierSortie[i].close();
                return false;
            }
        }

        // CALCUL DES PENTES EN STEEPEST DESCENT ET DES AIRES DRAINEES. //  19/01/12
        //-------------------------------------------------------------------------
        for (k = 0; k < nbmailles; k++) 
        {
            airedrainee[k] = 1;
            pente[k] = 0.;
        }
        for (i = 0; i < nbmailles; i++) 
        {
            k = MonMaillage->GetNumListe(i);
            PentesMaille_SteepDesc(MonMaillage->GetMaille(k), MonMaillage, &CMaille::GetAlti, pas3);
            pente[k] = MonMaillage->GetMaille(k)->GetPente(0);
            ksuiv = MonMaillage->GetMaille(k)->GetNumVoisinBas();
            if (ksuiv >= 0) airedrainee[ksuiv] += airedrainee[k];
        }
        //-------------------------------------------------------------------------

        float var[nfile]; // modif 2022
        // Ecriture des r�sultats
        for (k = 0; k < nbmailles; k++) 
        {
            //%%%%% ALTI
            var[0] = (float) unsurpas2 * MonMaillage->GetMaille(k)->GetAlti();
            //%%%%% STOCK SED
            var[1] = (float) unsurpas2 * mymax(MesFlux->GetStockSed(k) - MesFlux->GetDepot(k), 0.);
            //%%%%% EAU COURANTE
            var[2] = (float) MesFlux->GetEauCourante(k) * fractempspluie;
            //%%%%% DELTA EROSION
            var[3] = (float) unsurpas2 * MesFlux->GetDeltaErosion(k) / dt; // ajout le 9 mai 2007 : division par dt pour avoir taux d'erosion
            //%%%%% AIRES DRAINEES
            var[4] = (float) airedrainee[k]; //  19/01/12
            //%%%%% PENTES
            var[5] = (float) pente[k]; //  19/01/12
            //%%%%% RAINFALL
            var[6] = (float) MesFlux->GetRainfall(k) * unsurpas2; //  2018
            //%%%%% WATER ELEVATION
            if (MonModele->GetOptions().OptionHauteurEau || !MonModele->GetOptions().OptionSansLacs) var[7] = (float) unsurpas2 * (MonMaillage->GetMaille(k)->GetAltiEau()-MonMaillage->GetMaille(k)->GetAlti()); //  2022
            

            // Ecriture dans les fichiers
            for (i = 0; i < nfile; i++) // modif 2022
                FichierSortie[i].write((char*) &var[i], sizeof (float));
        }
        // Fermeture des fichiers
        for (i = 0; i < nfile; i++)
            FichierSortie[i].close();
    }
    delete [] pente;
    delete [] airedrainee;
    return flag;
}



//----------------------------
// Ecriture du fichier RESTART
//----------------------------

bool CSorties::EcritRestart(const double &temps) 
{
    //
    // Ecriture dans un flux de sortie
    //
    ofstream FichierSortie;
    FichierSortie.open(FichierRestart.c_str(), ios::out);
    // Ouverture du flux de sortie
    if (FichierSortie.bad()) 
    {
        cerr << "Impossible d'ouvrir un flux de sortie !! " << FichierRestart << endl;
        FichierSortie.close();
        return false;
    } 
    else 
    {
        FichierSortie.setf(ios::scientific);
        FichierSortie << temps << "  " << temps_denud << "  "
                << temps_bassin << " " << ind_bassin << endl;
        for (int k = 0; k < nbmailles; k++) 
        {
            FichierSortie << setprecision(10);
            FichierSortie << MonMaillage->GetMaille(k)->GetAlti() / pas2 << "  ";
            FichierSortie << MonMaillage->GetMaille(k)->GetAltiInit() / pas2 << "  ";
            FichierSortie << MonMaillage->GetMaille(k)->GetAltiEau() / pas2 << "  ";
            FichierSortie << MesFlux->GetStockSed(k) << "  ";
            FichierSortie << MesFlux->GetVolumeCouche(k) << "  ";
            FichierSortie << MesFlux->GetNumCouche(k) << "  ";
            FichierSortie << MesFlux->GetNumLitho(k) << endl;
        }
        // Fermeture du fichier
        FichierSortie.close();
    }
    return true;
}
//----------------------------
// Lecture du fichier RESTART
//----------------------------

bool CSorties::Reprise(double &temps) 
{
    //	string ligne;
    double temps_lu, temps_denud_lu, temps_bassin_lu, x;
    int num;
    filebuf MonFichier;
    // Ouverture du flux d'entr�e
    if (MonFichier.open(FichierReprise.c_str(), ios::in) == NULL) 
    {
        cerr << "The file does not exist or cannot be read !! " << FichierReprise << endl;
        MonFichier.close();
        return false;
    } 
    else 
    {
        istream FichierEntree(&MonFichier);
        AfficheMessage("> Reading the restart file : ", FichierReprise);

        // En fait, on ne lit dans le fichier Restart que le num�ro de la derni�re sortie
        // Le temps initial effectif est celui qui figure dans le fichier de param�tres
        FichierEntree >> temps_lu >> temps_denud_lu >> temps_bassin_lu >> ind_bassin;
        for (int k = 0; k < nbmailles; k++) 
        {
            FichierEntree >> x;
            MonMaillage->GetMaille(k)->SetAlti(x * pas2);
            FichierEntree >> x;
            MonMaillage->GetMaille(k)->SetAltiInit(x * pas2);
            FichierEntree >> x;
            MonMaillage->GetMaille(k)->SetAltiEau(x * pas2);
            FichierEntree >> x;
            MesFlux->SetStockSed(k, x);
            FichierEntree >> x;
            MesFlux->SetVolumeCouche(k, x);
            FichierEntree >> num;
            MesFlux->SetNumCouche(k, num);
            FichierEntree >> num;
            MesFlux->SetNumLitho(k, num);
        }
        // Fermeture du fichier
        MonFichier.close();
        AfficheMessage("          *** Restart file read ***\n");
        return true;
    }
}

void CSorties::EcritListeCailloux(const int &temps, const vector<CCaillou> & listeCaillou)
{
    int nbCaillou(int(listeCaillou.size()));
    //int intervalleSortie(dt_bassin);
    //if((temps)%intervalleSortie==0)                             //On test si on est � la date o� doivent sortir des fichiers
    if (sortieGrillesCailloux) 
    {
        string FichierListe;
        //string FichierGrille;
        //string FichierBinaire;
        ostringstream NumFichier;


        NumFichier << ind_bassin; // Fabrication des noms de fichiers de sortie
        if (ind_bassin < 10) 
        {
            FichierListe = "RESULTS/ListeCaillou00" + NumFichier.str();

        } 
        else if (ind_bassin < 100) 
        {
            FichierListe = "RESULTS/ListeCaillou0" + NumFichier.str();

        } 
        else 
        {
            FichierListe = "RESULTS/ListeCaillou" + NumFichier.str();

        }


        //////////////////////////////////////////////////
        ////SORIT DE LA LISTE DES DONNEES DES CAILLOUX////
        //////////////////////////////////////////////////


        ofstream FluxListe;
        FluxListe.open(FichierListe.c_str() ,  ios::binary);
        bool EcrireDonneesCosmo=MonModele->GetOptions().OptionCosmo;
        if (FluxListe) //On teste si tout est OK
        {
            FluxListe << "          REPARTITION DES CAILLOUX AU BOUT DE " << temps << " ANS" << endl << endl;
            FluxListe << "n  Dead_or_alive  Icell Jcell   Age     Prof    Elevation      Radius_(inlcude_vacuum)         Minerals_and_their_relative_frequency  Dissolves_or_not  Total_mineral_volume  Thermo_chrono_Age Age_montagne" ;
            
            // Debut Ajout youssouf
            if (EcrireDonneesCosmo)
            {
                if (MonModele->GetOptions().CosmonucleideEtudie[0])
                {
                    FluxListe <<   "   Concentration_10Be";
                }
                if (MonModele->GetOptions().CosmonucleideEtudie[1])
                {
                    FluxListe <<   "   Concentration_26Al";
                }
                if (MonModele->GetOptions().CosmonucleideEtudie[2])
                {
                    FluxListe <<   "   Concentration_14C";
                }
                if (MonModele->GetOptions().CosmonucleideEtudie[3])
                {
                    FluxListe <<   "   Concentration_21Ne";
                }
                    FluxListe << endl;
            }
            else
            {
                FluxListe << endl;
            }
            // Fin Ajout Youssouf
          
            double i, j;

            for (int k(0); k < nbCaillou; k++)
            {
                

                FluxListe << listeCaillou[k].getNumeroCaillou() << " ";

                FluxListe << listeCaillou[k].getDansLaGrille() << " ";

                int numMaille = listeCaillou[k].getNumeroMaille();
                i = 1 + numMaille % nbcol;
                j = 1 + numMaille / nbcol;

                FluxListe << i << " ";

                FluxListe << j << " ";

                FluxListe << listeCaillou[k].getAge() << " ";

                FluxListe << listeCaillou[k].getProfondeur() / pas2 << " ";

                FluxListe << MonMaillage->GetMaille(listeCaillou[k].getNumeroMaille())->GetAlti() / pas2 << " ";

                FluxListe << listeCaillou[k].getRayon() << " ";

                double Voltot = 0.;

                for (int m(0); m < listeCaillou[k].getNbMineraux(); m++) 
                {
                    FluxListe << listeCaillou[k].getNomMineral(m) << " " << listeCaillou[k].getProportionMineral(m) << "%" << " ";
                    Voltot += listeCaillou[k].getVolumeMineral(m);
                }
                if (listeCaillou[k].getProfondeur() < MesFlux->GetStockSed(numMaille)) 
                {
                    FluxListe << 1 << " ";
                } 
                else 
                {
                    FluxListe << 0 << " ";
                }
                FluxListe << Voltot << " ";

                FluxListe << listeCaillou[k].getThermoChronoAge() << " ";

                FluxListe << listeCaillou[k].getAgeMontagne() << " "; // temporaire pour distribution ages montagne
                
                // Debut Ajout youssouf
                if (EcrireDonneesCosmo) //Ecrire les données cosmo uniquement si on les a calculees
                {
                    vector<double> Cosmo=listeCaillou[k].getCosmoConcentration();
                    for (int i = 0; i < listeCaillou[k].getNbCosmonucleide() ; i++)
                    {
                        FluxListe <<" "<<Cosmo[i];

                    }
                }
                // Fin ajout youssouf
                FluxListe << endl;
                
            }
        }
        else 
        {
            cout << "ERROR: Problem during writting of clasts lists output" << endl;
        }
        FluxListe.close();
        

    
    }
         
    sortieGrillesCailloux = false;
}

void CSorties::MoyenneEcartTypeCailloux(const double &temps, const vector<CCaillou> & listeCaillou) 
{
    int nbCaillou(int(listeCaillou.size()));


    if (sortieFluxChimique) // tout ce qui concerne les valeurs temporelles sortent en meme temps que la denudation
    {
        ////////Calcul des valeurs////////
        double i, j;
        int numMaille;
        double MI(0);
        double MJ(0);
        double variance(0);
        double sigma(0);

        int nbMin(listeCaillou[0].getNbMineraux());
        double M1[nbMin];
        for (int m(0); m < nbMin; m++) 
        {
            M1[m] = 0;
        }
        double volTot(0);

        int nbcol = MonMaillage->GetNbCol();

        for (int k(0); k < nbCaillou; k++) 
        {
            numMaille = listeCaillou[k].getNumeroMaille();
            i = 1 + numMaille % nbcol;
            j = 1 + numMaille / nbcol;
            MI += i / nbCaillou;
            MJ += j / nbCaillou;
        }

        for (int m(0); m < nbMin; m++) 
        {
            M1[m] += listeCaillou[0].getVolumeMineral(m);
        }
        double r(listeCaillou[0].getRayon());
        volTot = 4. * PI * r * r * r / 3.;

        for (int k(0); k < nbCaillou; k++) 
        {
            numMaille = listeCaillou[k].getNumeroMaille();
            i = 1 + numMaille % nbcol;
            j = 1 + numMaille / nbcol;

            variance += pas * pas * ((MI - i)*(MI - i)+(MJ - j)*(MJ - j)) / nbCaillou;
        }
        sigma = sqrt(variance);


        ////////Ecriture des valeurs////////

        FichierSortieMoyenneEcartType << temps << " | " << sigma << " | " << MJ * pas << endl;
        ;


    }

}

void CSorties::FluxChimique(CModele *modele, const vector<CCaillou> & listeCaillou, const double &dt, const double &temps) 
{
    int nbCailloux(int(listeCaillou.size()));


    if (sortieFluxChimique) 
    {

        ////////Cr�ation de la liste des min�raux////////

        vector<string> listeNomMineraux;

        for (int i(0); i < modele->GetNbLitho(); i++) 
        {
            for (int j(0); j < (modele->GetLitho(i)).nbMineral; j++) 
            {
                bool existeDeja(0);
                string mineral(modele->GetLitho(i).mineraux[j]);
                for (int k(0); k < listeNomMineraux.size(); k++) 
                {
                    if (listeNomMineraux[k] == mineral) 
                    {
                        existeDeja = 1;
                    }
                }

                if (!existeDeja) 
                {
                    listeNomMineraux.push_back(mineral);
                }
            }
        }

        int nbTotMineraux(int(listeNomMineraux.size()));
        double quantiteDissoute[nbTotMineraux];
        for (int i(0); i < nbTotMineraux; i++) 
        {
            quantiteDissoute[i] = 0;
        }


        ////////Stockage des quantit�e de min�raux dissoutes (des volumes en m3 et une densite sans dimension)////////

        double quantitedissouteparcaillou = 0.;
        double quantiteDissouteParUniteVolumeCaillou = 0.;
        int nbCaillouxDissouts = 0.;

        for (int i(0); i < nbCailloux; i++) 
        {
            quantitedissouteparcaillou = 0.;
            double rinit = listeCaillou[i].getRayonDebutPasdeTemps();
            double r = listeCaillou[i].getRayon();
            if (r < rinit) // on ne traite que les cailloux qui se sont dissouts sur le pas de temps
            {
                nbCaillouxDissouts = nbCaillouxDissouts + 1;
                for (int j(0); j < listeCaillou[i].getNbMineraux(); j++) 
                {
                    for (int k(0); k < nbTotMineraux; k++) 
                    {
                        if (listeCaillou[i].getNomMineral(j) == listeNomMineraux[k]) 
                        {
                            quantiteDissoute[k] += listeCaillou[i].getQuantiteDissoute(j);
                            quantitedissouteparcaillou += listeCaillou[i].getQuantiteDissoute(j);
                        }
                    }
                }
            }

            double vol = (4. / 3.) * PI * rinit * rinit * rinit;
            quantiteDissouteParUniteVolumeCaillou += quantitedissouteparcaillou / vol;
        }
        quantiteDissouteParUniteVolumeCaillou = quantiteDissouteParUniteVolumeCaillou / double(nbCaillouxDissouts);

        ////////Calcul des quantitees d'atomes dissoutes (des moles)////////

        double Ca(0), Na(0), Si(0), K(0), Mg(0), S(0);

        for (int i(0); i < nbTotMineraux; i++) 
        {
            if (listeNomMineraux[i] == "albite")// NaAlSi3O8
            {
                Na += quantiteDissoute[i] / 0.0001002;
                Si += quantiteDissoute[i] / 0.0001002;
            } 
            else if (listeNomMineraux[i] == "quartz")//SiO2
            {
                Si += quantiteDissoute[i] / 0.00002269;
            } 
            else if (listeNomMineraux[i] == "muscovite")// KAl2(AlSi3O10)(OH)2
            {
                Si += 3. * quantiteDissoute[i] / 0.00014226;
                K += quantiteDissoute[i] / 0.00014226;
            }
            else if (listeNomMineraux[i] == "biotite")//� verifier//K(Mg,Fe)3(OH,F)2(Si3AlO10)
            {
                Si += 3. * quantiteDissoute[i] / 0.00015487;
                K += quantiteDissoute[i] / 0.00015487;
                Mg += 0.5 * quantiteDissoute[i] / 0.00015487;
            } 
            else if (listeNomMineraux[i] == "kaolinite")//Al2 Si2 O5 (OH)4
            {
                Si += 2. * quantiteDissoute[i] / 0.00009853;
            } 
            else if (listeNomMineraux[i] == "apatite")//Ca5 (PO4)3 (F,Cl,OH)
            {
                Ca += 5. * quantiteDissoute[i] / 0.00015698;
            } 
            else if (listeNomMineraux[i] == "montmorillonite")//� verifier// (Na,Ca)0,3 (Al,Mg)2 Si4 O10 (OH)2 nH2 O
            {
                Ca += 0.5 * 1.5 * quantiteDissoute[i] / 0.00015698;
                Na += 0.5 * 1.5 * quantiteDissoute[i] / 0.00015698;
                Si += 0.5 * 1.5 * quantiteDissoute[i] / 0.00015698;
                Mg += 0.5 * 1.5 * quantiteDissoute[i] / 0.00015698;
                //volume molaire : 0.00023365
            } 
            else if (listeNomMineraux[i] == "K-feldspar") // KAlSi3O8
            {
                Si += 3. * quantiteDissoute[i] / 0.00010888;
                K += quantiteDissoute[i] / 0.00010888;
                //volume molaire : 0.00010888
            } 
            else if (listeNomMineraux[i] == "Hornblende") // Ca2(Mg, Fe, Al)5 (Al, Si)8O22(OH)2
            {
                //Si += 8.*quantiteDissoute[i]/0.00010888;
                //Mg += 5.*quantiteDissoute[i]/0.00010888;
                //Ca += 2.*quantiteDissoute[i]/0.00010888;
                //volume molaire : 0.00010888
            } 
            else if (listeNomMineraux[i] == "zircon1") 
            {
                Ca += 0.;
                Na += 0.;
                Si += 0.;
                Mg += 0.;
            } 
            else if (listeNomMineraux[i] == "zircon2") 
            {
                Ca += 0.;
                Na += 0.;
                Si += 0.;
                Mg += 0.;
            } 
            else if (listeNomMineraux[i] == "zircon3") 
            {
                Ca += 0.;
                Na += 0.;
                Si += 0.;
                Mg += 0.;
            } 
            else if (listeNomMineraux[i] == "zircon4") 
            {
                Ca += 0.;
                Na += 0.;
                Si += 0.;
                Mg += 0.;
            } 
            else if (listeNomMineraux[i] != "autre") 
            {
                cout << "Problem : the mineral " << listeNomMineraux[i] << " is not in the list in CSortie.cpp ..." << endl << endl;
            }
        }


        ////////Ecriture des valeurs////////

        FichierSortieFluxChimique << temps << " |  " << Ca / dt << " |  " << Na / dt << " |  " << Si / dt << "  | " << K / dt << " |  " << Mg / dt << " |  " << S / dt << " |  " << quantiteDissouteParUniteVolumeCaillou << endl;

        sortieFluxChimique = false;

    }
}

void CSorties::FluxChimiqueIntegre(CModele *modele, const vector<CCaillou> & listeCaillou, const double &dt, const double &temps) 
{

    if (sortieFluxChimique) 
    {

        // On initialise ...
        vector<CCaillou> listedeCaillou = listeCaillou;
        int numaille = 0;
        int nbCailloux(int(listedeCaillou.size()));
        double dissoutParUniteVolumeCailloutotal = 0.;
        double dissouttotalintegre = 0.;
        int nbCaillouxDissouts = 0.;
        double volSedEmpty = 0.;
        double volSedAvec = 0.;
        //double rayonmoy = 0.;
        //double count = 0.;

        double CaIntegre(0), NaIntegre(0), SiIntegre(0), KIntegre(0), MgIntegre(0), SIntegre(0); // Concentrations totales integrees sur les mailles


        // ... puis on repere les mailles contenant des cailloux dans le regolithe
        // en excluant les cailloux "morts" stockes sur la derniere ligne du bas (par convention)

        vector<vector<CCaillou> > grille(nbmailles);
        for (int i(0); i < nbCailloux; i++) 
        {
            numaille = listedeCaillou[i].getNumeroMaille();
            if (listedeCaillou[i].getProfondeur() <= MesFlux->GetStockSed(numaille) && listedeCaillou[i].getDansLaGrille()) 
            {
                grille[numaille].push_back(listedeCaillou[i]);
            }
        }


        // Maintenant on parcourt les mailles. Pour celles qui contiennent un ou des cailloux
        // vivants et dissolvables on classe les cailloux par profondeur croissante.

        for (int i(0); i < nbmailles; i++) 
        {
            //double volumesanscailloux= MesFlux->GetStockSed(i) ; // sert pour l'integration a l'echelle de la grille

            bool dansdepot = false;
            bool danssaprol = false;
            double rmax = 0.; // rayon max des cailloux du dernier depot

            if (!grille[i].empty()) // s'il y a un ou des cailloux vivants et dissolvables sur la maille
            {
                int nbcaillouxsurmaille = int(grille[i].size());
                int Liste[nbcaillouxsurmaille];
                double dissoutparmaille(0.);
                double proftranchemin(MesFlux->GetDepot(i)); // le haut de la tranche qu'on initialise a l'interface dernier depot - saprolite
                double proftranchemax(0.); // le bas de la tranche
                double sommevolInitdepot(0.); // La somme des volumes initiaux des cailloux de la couche de depot
                // pour la ponderation des flux dissouts issus des cailloux


                TriRapideOptimal_deCailloux(grille[i], Liste, nbcaillouxsurmaille);

                for (int j(0); j < nbcaillouxsurmaille; j++) 
                {
                    if (grille[i][Liste[j]].getProfondeur() <= MesFlux->GetDepot(i)) 
                    {
                        double rinit = grille[i][Liste[j]].getRayonInitial();
                        sommevolInitdepot += (4. / 3.) * PI * rinit * rinit * rinit;
                        if (grille[i][Liste[j]].getRayon() > rmax) rmax = grille[i][Liste[j]].getRayon();

                    }
                }

                for (int j(0); j < nbcaillouxsurmaille; j++) 
                {
                    double rinitdt = grille[i][Liste[j]].getRayonDebutPasdeTemps();
                    double volinitdt = (4. / 3.) * PI * rinitdt * rinitdt * rinitdt; // volume au debut du pas de temps
                    double profcaillou = grille[i][Liste[j]].getProfondeur();
                    double profcaillousuivant(0.);
                    double couche(0.);


                    if (profcaillou <= MesFlux->GetDepot(i) && !grille[i][Liste[j]].getFantome()) 
                    {
                        // On repere ceux qui sont dans la derniere couche de depot (sol mobile) ou en surface, et on fait la
                        // moyenne ponderee de leur vitesse de dissolution.
                        double dissoutparcailloux = 0.;
                        double Ca(0.), Na(0.), Si(0.), K(0.), Mg(0.), S(0.);
                        nbCaillouxDissouts += 1;

                        // Rmq : pas de cailloux fantome dans le dernier depot car tue definitivement des qu'il est detache
                        for (int k(0); k < grille[i][Liste[j]].getNbMineraux(); k++) 
                        {
                            double dissoutparmineral = grille[i][Liste[j]].getQuantiteDissoute(k);
                            dissoutparcailloux += dissoutparmineral;
                            FluxCations(Ca, Na, Si, K, Mg, S, grille[i][Liste[j]].getNomMineral(k), dissoutparmineral);
                        }

                        dissoutParUniteVolumeCailloutotal += dissoutparcailloux / volinitdt;

                        couche = 2. * rmax * pas2; // on considere que la couche qui se dissout est d'epaisseur correspondant au cailloux le plus gros
                        // du depot, ceci pour eviter que le flux dissout integre ne depende trop de dt, lui-meme controlant
                        // l'epaisseur du dernier depot (si dt grand, erosion amont grand et depot local grand).
                        //proftranchemin = MesFlux->GetDepot(i);

                        dissoutparmaille += couche * dissoutparcailloux / sommevolInitdepot;
                        // Rmq : A la suite on a CationIntegre = couche * (MoleCation/volClast) * (volClast/SommeVolClasts)
                        // * (SommeVolClasts / SomevolinitialClast), ce dernier terme servant
                        // a tenir compte de la diminution de porosite dans les sediments
                        CaIntegre += couche * Ca / sommevolInitdepot;
                        NaIntegre += couche * Na / sommevolInitdepot;
                        SiIntegre += couche * Si / sommevolInitdepot;
                        KIntegre += couche * K / sommevolInitdepot;
                        MgIntegre += couche * Mg / sommevolInitdepot;
                        SIntegre += couche * S / sommevolInitdepot;

                        dansdepot = true;

                    } 
                    else if (profcaillou < MesFlux->GetStockSed(i) && profcaillou > MesFlux->GetDepot(i)) 
                    {
                        double dissoutparcailloux = 0.;
                        double Ca(0.), Na(0.), Si(0.), K(0.), Mg(0.), S(0);
                        nbCaillouxDissouts += 1;

                        if (grille[i][Liste[j]].getFantome()) 
                        {
                            dissoutparcailloux = 0.;
                            volinitdt = 1.; // pour eviter une division par zero un peu plus bas
                        } 
                        else 
                        {
                            for (int k(0); k < grille[i][Liste[j]].getNbMineraux(); k++) 
                            {
                                double dissoutparmineral = grille[i][Liste[j]].getQuantiteDissoute(k);
                                dissoutparcailloux += dissoutparmineral;
                                FluxCations(Ca, Na, Si, K, Mg, S, grille[i][Liste[j]].getNomMineral(k), dissoutparmineral);
                            }
                        }

                        dissoutParUniteVolumeCailloutotal += dissoutparcailloux / volinitdt;

                        if (j == nbcaillouxsurmaille - 1) // si c'est le caillou le + profond
                        {
                            proftranchemax = MesFlux->GetStockSed(i);
                        } 
                        else 
                        {
                            profcaillousuivant = grille[i][Liste[j + 1]].getProfondeur();
                            proftranchemax = 0.5 * (profcaillou + profcaillousuivant);
                        }
                        // Rmq : tranche est un volume, comme toutes les autres "profondeurs"
                        double rinit = grille[i][Liste[j]].getRayonInitial();
                        double volinit = (4. / 3.) * PI * rinit * rinit * rinit; // volume au tout debut de la vie du cailloux
                        double tranche = proftranchemax - proftranchemin;

                        proftranchemin = proftranchemax;
                        dissoutparmaille += tranche * dissoutparcailloux / volinit;
                        CaIntegre += tranche * Ca / volinit;
                        NaIntegre += tranche * Na / volinit;
                        SiIntegre += tranche * Si / volinit;
                        KIntegre += tranche * K / volinit;
                        MgIntegre += tranche * Mg / volinit;
                        SIntegre += tranche * S / volinit;

                        danssaprol = true;

                    }
                }

                dissouttotalintegre += dissoutparmaille;
            }

            grille[i].clear(); // on vide le vecteur pour etre sur

            if (dansdepot) 
            {
                volSedAvec += 2. * rmax * pas2;
            }

            if (MesFlux->GetStockSed(i) - MesFlux->GetDepot(i) > 0.) 
            {
                if (danssaprol) 
                {
                    volSedAvec += MesFlux->GetStockSed(i) - MesFlux->GetDepot(i);
                } 
                else if (!dansdepot) 
                {
                    volSedEmpty += MesFlux->GetStockSed(i) - MesFlux->GetDepot(i);
                }
            }
        }

        //Ecriture des valeurs s'il reste des cailloux sur la grille qui se dissolvent
        if (nbCaillouxDissouts > 0) 
        {
            double facteurintegration = (volSedAvec + volSedEmpty) / volSedAvec / dt;
            if (facteurintegration < 0) cout << volSedAvec << " " << volSedEmpty << " " << endl;
            FichierSortieFluxChimique << temps << "|" << CaIntegre * facteurintegration << "|" << NaIntegre * facteurintegration << "|" << SiIntegre * facteurintegration << "|" << KIntegre * facteurintegration << "|" << MgIntegre * facteurintegration << "|" << SIntegre * facteurintegration << "|" << dissouttotalintegre * facteurintegration << "|" << (dissoutParUniteVolumeCailloutotal / double(nbCaillouxDissouts)) / dt << "|" << nbCaillouxDissouts << endl;

        }
        sortieFluxChimique = false;
        // fin if sortieFluxChimique
    }
    return;
}

void CSorties::FluxChimiqueIntegreTEST(CModele *modele, const vector<CCaillou> & listeCaillou, const double &dt, const double &temps) 
{

    if (sortieFluxChimique) 
    {

        // On initialise ...

        int numaille = 0;
        int nbCailloux(int(listeCaillou.size()));
        double dissoutParUniteVolumeCailloutotal = 0.;
        double dissouttotalintegre = 0.;
        int nbCaillouxDissouts = 0.;
        double volSedEmpty = 0.;
        double volSedAvec = 0.;
        double CaIntegre(0), NaIntegre(0), SiIntegre(0), KIntegre(0), MgIntegre(0), SIntegre(0); // Concentrations totales integrees sur les mailles


        // ... puis on repere les mailles contenant des cailloux dans le regolithe
        // en excluant les cailloux "morts" stockes sur la derniere ligne du bas (par convention)

        vector<vector<CCaillou> > grille(nbmailles);
        for (int i(0); i < nbCailloux; i++) 
        {
            numaille = listeCaillou[i].getNumeroMaille();
            if (listeCaillou[i].getProfondeur() <= MesFlux->GetStockSed(numaille) && listeCaillou[i].getDansLaGrille()) 
            {
                grille[numaille].push_back(listeCaillou[i]);
            }
        }


        // Maintenant on parcourt les mailles. Pour celles qui contiennent un ou des cailloux
        // vivants et dissolvables on classe les cailloux par profondeur croissante.

        for (int i(0); i < nbmailles; i++) 
        {
            //double volumesanscailloux= MesFlux->GetStockSed(i) ; // sert pour l'integration a l'echelle de la grille

            bool dansdepot = false;
            bool danssaprol = false;

            if (!grille[i].empty()) // s'il y a un ou des cailloux vivants et dissolvables sur la maille
            {
                int nbcaillouxsurmaille = int(grille[i].size());
                int Liste[nbcaillouxsurmaille];
                double dissoutparmaille(0.);
                double sommevolcaillouxdepot(0.); // La somme des volumes initiaux des cailloux de la couche du dernier depot
                double sommevolcaillouxsaprol(0.); // La somme des volumes initiaux des cailloux sous la couche de depot (plus ou moins le saprolite)
                TriRapideOptimal_deCailloux(grille[i], Liste, nbcaillouxsurmaille);

                for (int j(0); j < nbcaillouxsurmaille; j++) 
                {
                    double rinit = grille[i][Liste[j]].getRayonInitial(); // rayon au tout debut de la vie du caillou
                    if (grille[i][Liste[j]].getProfondeur() < MesFlux->GetDepot(i)) 
                    {
                        sommevolcaillouxdepot += (4. / 3.) * PI * rinit * rinit * rinit;
                    } 
                    else if (grille[i][Liste[j]].getProfondeur() < MesFlux->GetStockSed(i)) 
                    {
                        sommevolcaillouxsaprol += (4. / 3.) * PI * rinit * rinit * rinit;
                    }
                }

                for (int j(0); j < nbcaillouxsurmaille; j++) 
                {
                    double rinitdt = grille[i][Liste[j]].getRayonDebutPasdeTemps(); //  rayon au debut du pas de temps
                    double volinitdt = (4. / 3.) * PI * rinitdt * rinitdt * rinitdt; // volume au debut du pas de temps
                    double profcaillou = grille[i][Liste[j]].getProfondeur();
                    double couche(0.);

                    if (profcaillou == 0.) // le cailloux est en surface. Il n'est pas dans une couche de depot a proprement parle
                    {

                        // On repere ceux qui sont dans la derniere couche de depot (sol mobile) et on fait la
                        // moyenne ponderee de leur vitesse de dissolution.
                        double dissoutparcailloux = 0.;
                        double Ca(0.), Na(0.), Si(0.), K(0.), Mg(0.), S(0.);
                        nbCaillouxDissouts += 1;

                        for (int k(0); k < grille[i][Liste[j]].getNbMineraux(); k++) 
                        {
                            double dissoutparmineral = grille[i][Liste[j]].getQuantiteDissoute(k);
                            // if (dissoutparmineral<0) cout << dissoutparmineral<<endl;
                            dissoutparcailloux += dissoutparmineral;
                            FluxCations(Ca, Na, Si, K, Mg, S, grille[i][Liste[j]].getNomMineral(k), dissoutparmineral);
                        }
                        dissoutParUniteVolumeCailloutotal += dissoutparcailloux / volinitdt;

                        dissoutparmaille += dissoutparcailloux;
                        // Rmq : A la suite on a CationIntegre = couche * (MoleCation/volClast) * (volClast/SommeVolClasts)
                        CaIntegre += Ca;
                        NaIntegre += Na;
                        SiIntegre += Si;
                        KIntegre += K;
                        MgIntegre += Mg;
                        SIntegre += S;

                    } 
                    else if (profcaillou < MesFlux->GetDepot(i)) 
                    {
                        // On repere ceux qui sont dans la derniere couche de depot (sol mobile) et on fait la
                        // moyenne ponderee de leur vitesse de dissolution.
                        double dissoutparcailloux = 0.;
                        double Ca(0.), Na(0.), Si(0.), K(0.), Mg(0.), S(0.);
                        nbCaillouxDissouts += 1;

                        for (int k(0); k < grille[i][Liste[j]].getNbMineraux(); k++) 
                        {
                            double dissoutparmineral = grille[i][Liste[j]].getQuantiteDissoute(k);
                            dissoutparcailloux += dissoutparmineral;
                            FluxCations(Ca, Na, Si, K, Mg, S, grille[i][Liste[j]].getNomMineral(k), dissoutparmineral);
                        }
                        dissoutParUniteVolumeCailloutotal += dissoutparcailloux / volinitdt;

                        couche = MesFlux->GetDepot(i);
                        dissoutparmaille += couche * dissoutparcailloux / sommevolcaillouxdepot;
                        // Rmq : A la suite on a CationIntegre = couche * (MoleCation/volClast) * (volClast/SommeVolClasts)
                        // * (SommeVolClasts/SommeVolClastsInitial) ce dernier terme est pour tenir compte de la porosite
                        CaIntegre += couche * Ca / sommevolcaillouxdepot;
                        NaIntegre += couche * Na / sommevolcaillouxdepot;
                        SiIntegre += couche * Si / sommevolcaillouxdepot;
                        KIntegre += couche * K / sommevolcaillouxdepot;
                        MgIntegre += couche * Mg / sommevolcaillouxdepot;
                        SIntegre += couche * S / sommevolcaillouxdepot;

                        //volSedAvec += couche;
                        dansdepot = true;
                    } 
                    else if (profcaillou <= MesFlux->GetStockSed(i))
                        // Pour les cailloux qui sont dans le saprolite, on calcule de la meme facon.
                        // On separe le dernier depot du saprolite pour eviter le cas ou un depot plein de
                        // clasts vient recouvrir un saprolite sans clast. Le flux dissout issu du saprolite sera zero,
                        // pas de bol, mais c'est mieux que d'appliquer le flux dissous des clasts deposes, qui peut correspondre
                        // a une autre histoire beaucoup plus en amont. Cette couche de saprolite est de toute facon prise en compte
                        // quand on integre tout a l'echelle de la grille puisque on utilise le volume total de saprolite pour
                        // estimer le flux total dissout de la grille.
                    {
                        double dissoutparcailloux = 0.;
                        double Ca(0.), Na(0.), Si(0.), K(0.), Mg(0.), S(0);
                        nbCaillouxDissouts += 1;

                        for (int k(0); k < grille[i][Liste[j]].getNbMineraux(); k++) 
                        {
                            double dissoutparmineral = grille[i][Liste[j]].getQuantiteDissoute(k);
                            dissoutparcailloux += dissoutparmineral;
                            FluxCations(Ca, Na, Si, K, Mg, S, grille[i][Liste[j]].getNomMineral(k), dissoutparmineral);
                        }
                        dissoutParUniteVolumeCailloutotal += dissoutparcailloux / volinitdt;

                        couche = MesFlux->GetStockSed(i) - MesFlux->GetDepot(i);
                        dissoutparmaille += couche * dissoutparcailloux / sommevolcaillouxsaprol;
                        // Rmq : A la suite on a CationIntegre = couche * (MoleCation/volClast) * (volClast/SommeVolClasts)
                        // * (SommeVolClasts/SommeVolClastsInitial) ce dernier terme est pour tenir compte de la porosite
                        CaIntegre += couche * Ca / sommevolcaillouxsaprol;
                        NaIntegre += couche * Na / sommevolcaillouxsaprol;
                        SiIntegre += couche * Si / sommevolcaillouxsaprol;
                        KIntegre += couche * K / sommevolcaillouxsaprol;
                        MgIntegre += couche * Mg / sommevolcaillouxsaprol;
                        SIntegre += couche * S / sommevolcaillouxsaprol;

                        //volSedAvec += couche;
                        danssaprol = true;
                    }
                }

                dissouttotalintegre += dissoutparmaille;
            }

            //volSedEmpty += volumesanscailloux;

            grille[i].clear(); // on vide le vecteur pour etre sur

            if (dansdepot) 
            {
                volSedAvec += MesFlux->GetDepot(i);
            } 
            else 
            {
                volSedEmpty += MesFlux->GetDepot(i);
            }

            if (danssaprol) 
            {
                volSedAvec += MesFlux->GetStockSed(i) - MesFlux->GetDepot(i);
            } 
            else 
            {
                volSedEmpty += mymax(MesFlux->GetStockSed(i) - MesFlux->GetDepot(i), 0.);
            } // sur une maille sans cailloux, il est possible que
            // le saprolite soit d'epaisseur nulle mais pas le dernier depot

        }

        //Ecriture des valeurs s'il reste des cailloux sur la grille qui se dissolvent )
        if (nbCaillouxDissouts > 0) 
        {

            double facteurintegration = (volSedAvec + volSedEmpty) / volSedAvec / dt;
            FichierSortieFluxChimique << temps << "|" << CaIntegre * facteurintegration << "|" << NaIntegre * facteurintegration << "|" << SiIntegre * facteurintegration << "|" << KIntegre * facteurintegration << "|" << MgIntegre * facteurintegration << "|" << SIntegre * facteurintegration << "|" << dissouttotalintegre * facteurintegration << "|" << (dissoutParUniteVolumeCailloutotal / double(nbCaillouxDissouts)) / dt << "|" << nbCaillouxDissouts << endl;
        }
        sortieFluxChimique = false;
        // fin if sortieFluxChimique
    }
    return;
}


//----------------------------------------
// trie la liste des mailles par altitudes
//----------------------------------------
// Tri rapide it�ratif pour classer les cailloux par profondeur croissante
//

void CSorties::TriRapideOptimal_deCailloux(vector<CCaillou> & listecaillouxsurmaille, int Liste[], const int &nbcailloux) 
{
    int debut = 0;
    int fin = nbcailloux - 1;
    int pivotindex, l, r;
    int top = 0;
    CCaillou* cailloupivot;
    Pile soustas[100];
    int i, j;

    //	Initialise
    for (i = 0; i < nbcailloux; i++) Liste[i] = i;

    soustas[top].gauche = debut;
    soustas[top].droite = fin;

    while (top >= 0) 
    {
        // Pop soustas
        debut = soustas[top].gauche;
        fin = soustas[top].droite;
        top--;

        //Find pivot
        pivotindex = (debut + fin) / 2; // Pivot = milieu de l'intervalle
        cailloupivot = &listecaillouxsurmaille[Liste[pivotindex]];
        Echange(Liste, pivotindex, debut); // Stick pivot at beginning

        // Partition
        l = debut - 1;
        r = fin;
        do {
            while (listecaillouxsurmaille[Liste[++l]].getProfondeur() > cailloupivot->getProfondeur());
            while (r && (cailloupivot->getProfondeur() > listecaillouxsurmaille[Liste[--r]].getProfondeur()));
            Echange(Liste, l, r);
        } while (l < r);

        Echange(Liste, l, r);
        Echange(Liste, l, debut);

        //Load up soustas
        if ((l - debut) > SEUIL_TRI) 
        { // Left partition
            top++;
            soustas[top].gauche = debut;
            soustas[top].droite = l - 1;
        }

        if ((fin - l) > SEUIL_TRI) 
        { // Right partition
            top++;
            soustas[top].gauche = l + 1;
            soustas[top].droite = fin;
        }
    }

    for (i = 1; i < nbcailloux; i++)
        for (j = i; (j > 0) &&
                (listecaillouxsurmaille[Liste[j]].getProfondeur() < listecaillouxsurmaille[Liste[j - 1]].getProfondeur()); j--)
            Echange(Liste, j, j - 1);
    return;
}

//----------------------------------------
// Calcule les flux de cations issus de la dissolution des cailloux pour differents mineraux constituant les cailloux
//----------------------------------------

void CSorties::FluxCations(double &Ca, double &Na, double &Si, double &K, double &Mg, double &S, string NomMineral, const double &dissoutparmineral) 
{
    if (NomMineral == "albite")// NaAlSi3O8
    {
        Na += dissoutparmineral / 0.0001002;
        Si += dissoutparmineral / 0.0001002;
    } 
    else if (NomMineral == "quartz")//SiO2
    {
        Si += dissoutparmineral / 0.00002269;
    } 
    else if (NomMineral == "muscovite")// KAl2(AlSi3O10)(OH)2
    {
        Si += 3. * dissoutparmineral / 0.00014226;
        K += dissoutparmineral / 0.00014226;
    } 
    else if (NomMineral == "biotite")//� verifier//K(Mg,Fe)3(OH,F)2(Si3AlO10)
    {
        Si += 3. * dissoutparmineral / 0.00015487;
        K += dissoutparmineral / 0.00015487;
        Mg += 0.5 * dissoutparmineral / 0.00015487;
    } 
    else if (NomMineral == "kaolinite")//Al2 Si2 O5 (OH)4
    {
        Si += 2. * dissoutparmineral / 0.00009853;
    } 
    else if (NomMineral == "apatite")//Ca5 (PO4)3 (F,Cl,OH)
    {
        Ca += 5. * dissoutparmineral / 0.00015698;
    } 
    else if (NomMineral == "montmorillonite")//� verifier// (Na,Ca)0,3 (Al,Mg)2 Si4 O10 (OH)2 nH2 O
    {
        Ca += 0.5 * 1.5 * dissoutparmineral / 0.00015698;
        Na += 0.5 * 1.5 * dissoutparmineral / 0.00015698;
        Si += 0.5 * 1.5 * dissoutparmineral / 0.00015698;
        Mg += 0.5 * 1.5 * dissoutparmineral / 0.00015698;
        //volume molaire : 0.00023365
    } 
    else if (NomMineral == "illite") //� verifier// (K,H3O)(Al,Mg,Fe)2(Si,Al)4O10[(OH)2,(H2O)]
    {

        //volume molaire : 0.00014158
    } 
    else if (NomMineral == "K-feldspar") // KAlSi3O8
    {
        //Si += 3.*quantiteDissoute[i]/0.00010888;
        //K += quantiteDissoute[i]/0.00010888;
        //volume molaire : 0.00010888
    } 
    else if (NomMineral == "Hornblende") // Ca2(Mg, Fe, Al)5 (Al, Si)8O22(OH)2
    {
        //Si += 3.*quantiteDissoute[i]/0.00010888;
        //K += quantiteDissoute[i]/0.00010888;
        //volume molaire : 0.00010888
    } 
    else if (NomMineral == "zircon1") 
    {
        Ca += 0.;
        Na += 0.;
        Si += 0.;
        Mg += 0.;
    } 
    else if (NomMineral == "zircon2") 
    {
        Ca += 0.;
        Na += 0.;
        Si += 0.;
        Mg += 0.;
    } 
    else if (NomMineral == "zircon3") 
    {
        Ca += 0.;
        Na += 0.;
        Si += 0.;
        Mg += 0.;
    } 
    else if (NomMineral == "zircon4") 
    {
        Ca += 0.;
        Na += 0.;
        Si += 0.;
        Mg += 0.;
    } 
    else if (NomMineral != "autre") 
    {
        cout << "Problem : the mineral " << NomMineral << " is not in the list in CSortie.cpp line CSortie.cpp FluxCations()" << endl << endl;
    }

}

//----------------------------------------
// Ne fait rien, en particulier ne calcule pas le flux dissout integre
//----------------------------------------
void CSorties::NeFaitRien(CModele *modele, const vector<CCaillou> & listeCaillou, const double &dt, const double &temps)
{
    ;
}
