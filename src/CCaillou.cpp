#include <iostream>
#include <string>
#include <cstdlib>
#include "CCaillou.h"

#define RED "\e[0;31m"
#define WI "\e[0m"
using namespace std;


////////////////////
////CONSTRUCTEUR////
////////////////////

CCaillou::CCaillou(CModele *modele, CClimatStochastique *flux, CMaillage *maillage) 
{
    //Variables qui dÈfinissent le caillou

    m_numeroCaillou = 0.;
    m_numeroMaille = 0.;
    m_numeroMailleInit = 0.;
    m_profondeur = 0.;
    m_dansLaGrille = true; //1 tant qu'il est dans la grille, 0 quand il sort
    m_DeadInTheLastIteration = false; // 2023
    m_fantome = false; // devient fantome quand il est totalement dissout, ce qui sert au moment de l'integration dans le regolithe (sapro ou depot de bassin)
    m_age = 0.; //
    m_age_montagne = 0.;
    m_pas = modele->GetTopoInit().pas;
    m_pas2 = m_pas*m_pas;
    m_pas3 = m_pas * m_pas*m_pas;
    m_dt = 0.;



    m_rayon = 0.; //valeur ‡ entrer en m
    m_rayonInit = 0.;
    m_rayonDebutPasdeTemps = 0.; // 
    m_nbMineral = 0.;
    m_MineralBase = "autre";

    m_ThermoChronoAge = 0.;

    //Variables ‡ rÈcuperer qui serviront pour le dÈplacement et l'Èvolution du caillou

    m_quantiteDepose = 0;
    m_quantiteErode = 0;
    m_quantiteTransfere = 0;
    for (int i(0); i < 8; i++) 
    {
        m_repartition[i] = 0;
    }

    m_profAlteInit = 0;
    m_profInit = 0;
    m_erosionInit = 0;
    climat = modele->GetParamClimat();
    for (int i(0); i < modele->GetNbPeriodes(); i++) 
    {
        m_tempSurface.push_back(modele->GetTemperatureSurface(i));
    }
    m_MesFlux = flux; // decembre 2014
    m_MonModele = modele; // il faudrait utiliser cette instance dans toutes les routines ! (janvier 2016)
    m_MonMaillage = maillage; // 2023 "MonMaillage" supprime de l'appel de nombreuses fonctions car le maillage est deja defini ici  lors de la creation d'un  caillou

    m_salterepremierefois = true;
    m_serodepremierefois = true;


    m_Rugosite = modele->GetRugositeMineral();
   
    InitialiserCosmonucleides(m_MonModele); // Ajout youssouf
 
    

}

// destructeur

CCaillou::~CCaillou()
{
}


///////////////////////////////////////////////////////////
////LECTURE DES CARACTERISTIQUES INITIALES DES CAILLOUX////
///////////////////////////////////////////////////////////

int CCaillou::compterCailloux() 
{
    ifstream monFluxEntree("InitialClastList");
    int nombreLigne = -1; //-1 pour enlever la ligne de texte du dÈbut

    if (monFluxEntree) 
    {
        string ligne;
        while (getline(monFluxEntree, ligne)) //Tant qu'on n'est pas ‡ la fin, on lit
        {
            nombreLigne++;
        }
    } 
    else 
    {
        cout << "ERROR: Impossible to open the initial clast list. Verify that it exists or remove the clast option in the input_cidre.txt" << endl;
        exit(EXIT_FAILURE);
    }

    return nombreLigne;
}

void CCaillou::initialiserCailloux(const double n, const double x, const double y, const double z, const double r, string nom, const double age) {


    m_numeroCaillou = n;
    m_numeroMaille = x - 1 + (y - 1)*(m_MonMaillage->GetNbCol());
    m_numeroMailleInit = m_numeroMaille;
    m_profondeur = z * m_pas2; //la surface d'une maille pour avoir des profondeurs exprimÈes en volume
    m_rayon = r;
    m_rayonInit = m_rayon;
    m_MineralBase = nom;
    m_ThermoChronoAge = age;

}

void CCaillou::initialiserMineraux() 
{
    if (m_MonModele->GetOptions().OptionMineraux) //Si la composition des cailloux est liÈe ‡ sa position initiale
    {
        //////////On dÈtermine la litho ‡ laquelle appartient le caillou//////////

        int numLitho(-1);
        if (fabs(m_profondeur) / (m_pas2) < m_MonModele->GetStrati().epaiss_sed) 
        {
            numLitho = rand() % (m_MonModele->GetNbLitho()); //Si le caillou est dÈj‡ dans la couche de sÈdiment on tire sa composition au hasard parmi toutes les lithologies qui existent dans le modËle
        } 
        else 
        {
            int numCouche(0);
            double epaisseur(m_MonModele->GetStrati().epaiss_sed);
            while (numLitho == -1) 
            {
                epaisseur += m_MonMaillage->GetMaille(m_numeroMaille)->GetEpaisseur(numCouche);

                if (fabs(m_profondeur) < epaisseur) 
                {
                    numLitho = m_MonModele->GetStrati().TabCouche[numCouche].indlitho;
                }
                numCouche++;
            }
        }
        //////////Initialisation des caracteristiques des cailloux//////////

        m_nbMineral = (m_MonModele->GetLitho(numLitho)).nbMineral;

        for (int i(0); i < m_nbMineral; i++) 
        {
            m_nomMineral.push_back(m_MonModele->GetLitho(numLitho).mineraux[i]);
            m_proportionMineral.push_back(m_MonModele->GetLitho(numLitho).proportionMineral[i]);
            m_rayonMineral.push_back(m_rayon);
            m_proportionMineralInit.push_back(m_proportionMineral[i]);
            m_quantiteDissoute.push_back(0);
            m_volumeMineral.push_back(4. * PI * m_rayon * m_rayon * m_rayon * m_proportionMineral[i] / 300.);
        }
    } 
    else //si la composition des cailloux est dÈterminÈe par InitialClastList
    {
        m_nbMineral = 1;
        m_nomMineral.push_back(m_MineralBase);
        m_proportionMineral.push_back(100);
        m_rayonMineral.push_back(m_rayon);
        m_proportionMineralInit.push_back(100);
        m_quantiteDissoute.push_back(0);
        m_volumeMineral.push_back(4. * PI * m_rayon * m_rayon * m_rayon / 3.);
    }
    caracteristiqueMineraux(); //Initialisation des parametres physiques des minÈraux
}

void CCaillou::caracteristiqueMineraux() 
{
    m_ki.clear(); // necessaire pour la ressucitation des cailloux, sinon la taille des vecteurs augmente a chaque recussitation et ca ralentit le code.
    m_energieActivation.clear();
    m_volumeMolaire.clear();

    for (int i(0); i < m_nomMineral.size(); i++) 
    {
        //Origine des donnees
        //ki : au cas par cas
        //energie d'activation : papier godderis2006_WITCH
        //volume molaire : fichier molar_init.for de Yves Godderis

        if (m_nomMineral[i] == "albite") 
        {
            m_ki.push_back(pow(10., -12.26)); //Livre Geomorphology : The Mechanics and Chemisty of Landscapes (pour ph = 5)
            m_energieActivation.push_back(66000); //valeur entre Eah et Eaw
            m_volumeMolaire.push_back(0.0001002);
        } 
        else if (m_nomMineral[i] == "quartz") 
        {
            m_ki.push_back(pow(10., -13.39)); //Livre Geomorphology : The Mechanics and Chemisty of Landscapes (pour ph = 5)
            m_energieActivation.push_back(85000); //valeur de Eaw = Eaoh
            m_volumeMolaire.push_back(0.00002269);
        } 
        else if (m_nomMineral[i] == "muscovite") 
        {
            m_ki.push_back(pow(10., -13.07)); //Livre Geomorphology : The Mechanics and Chemisty of Landscapes (pour ph = 5)
            m_energieActivation.push_back(22000); //valeur de Eah = Eaoh
            m_volumeMolaire.push_back(0.00014226);
        } 
        else if (m_nomMineral[i] == "biotite") 
        {
            m_ki.push_back(pow(10., -10.88)); //papier godderis2006_WITCH, valeur de pkh
            m_energieActivation.push_back(35000); //valeur de Eah = Eaw
            m_volumeMolaire.push_back(0.00015487);
        } 
        else if (m_nomMineral[i] == "kaolinite") 
        {
            m_ki.push_back(pow(10., -13.28)); //Livre Geomorphology : The Mechanics and Chemisty of Landscapes (pour ph = 5)
            m_energieActivation.push_back(54000); //valeur entre Eah et Eaw
            m_volumeMolaire.push_back(0.00009853);
        } 
        else if (m_nomMineral[i] == "apatite") 
        {
            m_ki.push_back(pow(10., -5.08)); //papier godderis2006_WITCH, valeur de pkh
            m_energieActivation.push_back(34700); //valeur de Eah
            m_volumeMolaire.push_back(0.00015698);
        } 
        else if (m_nomMineral[i] == "montmorillonite") 
        {
            m_ki.push_back(pow(10., -11.00)); //papier godderis2006_WITCH, valeur entre pkh et pkw
            m_energieActivation.push_back(48200); //valeur entre Eah et Eaw
            m_volumeMolaire.push_back(0.00023365);
        } 
        else if (m_nomMineral[i] == "illite") 
        {
            m_ki.push_back(pow(10., -13.00)); //papier godderis2006_WITCH, valeur entre pkh et pkw
            m_energieActivation.push_back(20000); //valeur entre Eah et Eaw
            m_volumeMolaire.push_back(0.00014158);
        } 
        else if (m_nomMineral[i] == "K-feldspar") 
        {
            m_ki.push_back(pow(10., -8.65)); //papier Helgeson et al. 1984
            m_energieActivation.push_back(73000); //d'apres rapport USGS OFR_2004_1068 valeur entre acide et basic processes
            m_volumeMolaire.push_back(0.00010888); // sur internet
        } 
        else if (m_nomMineral[i] == "zircon1") 
        {
            m_ki.push_back(pow(10., -15.)); //au pif pour que cela soit très faible
            m_energieActivation.push_back(100000); //au pif pour que la dissolution soit faible
            m_volumeMolaire.push_back(0.000003927); // sur internet
        } 
        else if (m_nomMineral[i] == "zircon2") 
        {
            m_ki.push_back(pow(10., -15.)); //au pif pour que cela soit très faible
            m_energieActivation.push_back(100000); //au pif pour que la dissolution soit faible
            m_volumeMolaire.push_back(0.000003927); // sur internet
        } 
        else if (m_nomMineral[i] == "zircon3") 
        {
            m_ki.push_back(pow(10., -15.)); //au pif pour que cela soit très faible
            m_energieActivation.push_back(100000); //au pif pour que la dissolution soit faible
            m_volumeMolaire.push_back(0.000003927); // sur internet
        } 
        else if (m_nomMineral[i] == "zircon4") 
        {
            m_ki.push_back(pow(10., -15.)); //au pif pour que cela soit très faible
            m_energieActivation.push_back(100000); //au pif pour que la dissolution soit faible
            m_volumeMolaire.push_back(0.000003927); // sur internet
        } 
        else if (m_nomMineral[i] == "Hornblende") 
        {
            //m_ki.push_back(pow(10., -8.65));//papier Helgeson et al. 1984
            //m_energieActivation.push_back(73000);//d'apres rapport USGS OFR_2004_1068 valeur entre acide et basic processes
            //m_volumeMolaire.push_back(0.00010888);// sur internet
        } 
        else if (m_nomMineral[i] == "autre") 
        {
            m_ki.push_back(pow(10., -9));
            m_energieActivation.push_back(10000);
            m_volumeMolaire.push_back(0.00001);
        } 
        else 
        {
            cout << "Problem : the mineral " << m_nomMineral[i] << " is not in the list..." << endl << endl;
        }
    }
}

void CCaillou::RessusciteCaillou(CModele *modele, const int numperiode)
{
    
   
    m_dansLaGrille = true;
    m_fantome = false;
    m_rayon = m_rayonInit; // rayon remis a sa valeur initiale
    m_numeroMaille = m_numeroMailleInit; // position remise a sa position initiale sur la grille
    
    setPasDeTemps(modele->GetPasTemps(numperiode)); // on met à jour le pas de temps 2023
    
    double random = hasardNombre();
    
    double profmin = modele->GetProfRessusciteCaillouxMin() * m_pas2; // test 2023
    double profmax = modele->GetProfRessusciteCaillouxMax() * m_pas2; // test 2023
    m_profondeur = m_MesFlux->GetStockSed(m_numeroMaille)-m_MesFlux->GetDepot(m_numeroMaille) + profmin + (profmax-profmin) * random; //  2023
    if (m_profondeur<0.) m_profondeur = 0.; // peut arriver si la difference entre StockSed et depot

    m_age = 0.; // revival !
    m_totalage = 0.; // revival ! 2023
    

    if (modele->GetOptions().OptionMineraux) //Si la composition des cailloux est liÈe ‡ sa position initiale
    {
        //On dÈtermine la litho ‡ laquelle appartient le caillou, qui est celle juste sous les sediments.
        int indicelitho = m_MesFlux->GetNumLitho(m_numeroMaille);

        //Initialisation des caracteristiques des cailloux
        m_nbMineral = (modele->GetLitho(indicelitho)).nbMineral;

        for (int i(0); i < m_nbMineral; i++) 
        {
            m_nomMineral[i] = modele->GetLitho(indicelitho).mineraux[i];
            m_proportionMineral[i] = modele->GetLitho(indicelitho).proportionMineral[i];
            m_rayonMineral[i] = m_rayon;
            m_proportionMineralInit[i] = m_proportionMineral[i];
            m_quantiteDissoute[i] = 0.;
            m_volumeMineral[i] = 4. * PI * m_rayon * m_rayon * m_rayon * m_proportionMineral[i] / 300.;
        }
    } 
    else //si la composition des cailloux est dÈterminÈe par InitialClastList
    {
        m_nbMineral = 1;
        m_nomMineral[0] = m_MineralBase;
        m_proportionMineral[0] = 100;
        m_rayonMineral[0] = m_rayon;
        m_proportionMineralInit[0] = 100;
        m_quantiteDissoute[0] = 0;
        m_volumeMineral[0] = 4. * PI * m_rayon * m_rayon * m_rayon / 3.;
    }

    caracteristiqueMineraux(); //Initialisation des parametres physiques des minÈraux

}

void CCaillou::RessusciteCaillouCOSMO(CModele *modele, const int numperiode)
// surcharge si on calcule les cosmos
{
    

    m_dansLaGrille = true;
    m_fantome = false;
    m_rayon = m_rayonInit; // rayon remis a sa valeur initiale
    double random = hasardNombre();

    
    m_numeroMaille = m_numeroMailleInit;
    
    setPasDeTemps(modele->GetPasTemps(numperiode)); // on met à jour le pas de temps 2023
    
    double profmin = modele->GetProfRessusciteCaillouxMin() * m_pas2 ; //  2023
    double profmax = modele->GetProfRessusciteCaillouxMax() * m_pas2 ; //  2023
    m_profondeur = m_MesFlux->GetStockSed(m_numeroMaille)-m_MesFlux->GetDepot(m_numeroMaille) + profmin + (profmax-profmin) * random; //  2023
    if (m_profondeur<0.) m_profondeur = 0.; // peut arriver si la difference entre StockSed et depot

    m_age = 0.; // revival !
    m_totalage = 0; // revival ! 2023
    
    // Concentration en cosmo
    ComputeInitialSteadyStateCosmoConcentration();
    for (int i=0 ; i<m_NbCosmonucleide ; i++)     //2023
    {
        m_IntermediateIntegral[i] = 0.;
    }


    if (modele->GetOptions().OptionMineraux) //Si la composition des cailloux est liÈe ‡ sa position initiale
    {
        //On dÈtermine la litho ‡ laquelle appartient le caillou, qui est celle juste sous les sediments.
        int indicelitho = m_MesFlux->GetNumLitho(m_numeroMaille);

        //Initialisation des caracteristiques des cailloux
        m_nbMineral = (modele->GetLitho(indicelitho)).nbMineral;

        for (int i(0); i < m_nbMineral; i++)
        {
            m_nomMineral[i] = modele->GetLitho(indicelitho).mineraux[i];
            m_proportionMineral[i] = modele->GetLitho(indicelitho).proportionMineral[i];
            m_rayonMineral[i] = m_rayon;
            m_proportionMineralInit[i] = m_proportionMineral[i];
            m_quantiteDissoute[i] = 0.;
            m_volumeMineral[i] = 4. * PI * m_rayon * m_rayon * m_rayon * m_proportionMineral[i] / 300.;
        }
    }
    else //si la composition des cailloux est dÈterminÈe par InitialClastList
    {
        m_nbMineral = 1;
        m_nomMineral[0] = m_MineralBase;
        m_proportionMineral[0] = 100;
        m_rayonMineral[0] = m_rayon;
        m_proportionMineralInit[0] = 100;
        m_quantiteDissoute[0] = 0;
        m_volumeMineral[0] = 4. * PI * m_rayon * m_rayon * m_rayon / 3.;
    }

    caracteristiqueMineraux(); //Initialisation des parametres physiques des minÈraux

}


//////////////////
////ACCESSEURS////
//////////////////

int CCaillou::getNumeroCaillou() const 
{
    return m_numeroCaillou;
}

int CCaillou::getNumeroMaille() const 
{
    return m_numeroMaille;
}

int CCaillou::getNumeroMailleInit() const
{
    return m_numeroMailleInit;
}

double CCaillou::getProfondeur() const 
{
    return m_profondeur;
}

bool CCaillou::getDansLaGrille() const 
{
    return m_dansLaGrille;
}

void CCaillou::SetDeadInTheLastIterationTrue()  // 2023
{
    m_DeadInTheLastIteration=true;
}


void CCaillou::SetDeadInTheLastIterationFalse()  // 2023
{
    m_DeadInTheLastIteration=false;
}

bool CCaillou::GetDeadInTheLastIteration()  const // 2023
{
    return m_DeadInTheLastIteration;
}


bool CCaillou::getFantome() const 
{
    return m_fantome;
}

double CCaillou::getAge() const // modif 2023
{
    return m_age;
}

double CCaillou::getTotalAge() const //  2023
{
    return m_totalage;
}


double CCaillou::getAgeMontagne() const // temporaire pour distribution ages montagne
{
    return m_age_montagne;
}

double CCaillou::getThermoChronoAge() const 
{
    return m_ThermoChronoAge;
}

double CCaillou::getRayon() const 
{
    return m_rayon;
}

double CCaillou::getRayonDebutPasdeTemps() const 
{
    return m_rayonDebutPasdeTemps;
}

double CCaillou::getRayonInitial() const 
{
    return m_rayonInit;
}

int CCaillou::getNbMineraux() const 
{
    return int(m_nomMineral.size());
}

string CCaillou::getNomMineral(int num) const 
{
    return m_nomMineral[num];
}

double CCaillou::getVolumeMineral(int num) const 
{
    return m_volumeMineral[num];
}

double CCaillou::getProportionMineral(int num) const 
{
    return m_proportionMineral[num];
}

double CCaillou::getQuantiteDissoute(int num) const 
{
    return m_quantiteDissoute[num];
}



/////////////////////////////////////////////////////
////MISE A JOUR DES DONNEES NECESSAIRES AU CALCUL////
/////////////////////////////////////////////////////

void CCaillou::miseAZero() 
{
    m_quantiteDepose = 0.;
    m_quantiteErode = 0.;
    m_quantiteDeposeTotale = 0.;
    m_quantiteErodeTotale = 0.;
    m_quantiteTransfere = 0.;


    for (int i(0); i < 8; i++) 
    {
        m_repartition[i] = 0.;
    }

    m_listeTemp.clear();
    m_listeRunoff.clear();

}

void CCaillou::setFluxDiff() 
{
    m_quantiteDepose = m_MesFlux->GetDepotDiff(m_numeroMaille); //On rÈcupËre les infos sur la quantitÈ de sÈdiment qui se dÈpose par diffusion sur la maille du caillou
    m_quantiteErode = m_MesFlux->GetErosionDiff(m_numeroMaille); //On rÈcupËre les infos sur la quantitÈ de sÈdiment qui s'est ÈrodÈ par diffusion sur la maille du caillou
    m_quantiteDeposeTotale = m_MesFlux->GetDepot(m_numeroMaille); // idem mais quantite totale (diffusion+fluvial)
    m_quantiteErodeTotale = m_MesFlux->GetErosion(m_numeroMaille); // idem mais quantite totale (diffusion+fluvial)

    double transfertVersDiff[8]; //Tableau local qui va servir d'intermediaire pour stocker des infos
    m_MesFlux->GetVolumeDiffExporte(m_numeroMaille, transfertVersDiff); //On remplit ce tableau afin d'avoir les proportions de propagation de sÈdiment selon les directions


    m_quantiteTransfere = 0.;
    for (int i(0); i < 8; i++) //On rÈcupËre les infos sur la quantitÈ de sÈdiment qui a transfÈrÈ sur la maille du caillou
    {
        m_quantiteTransfere += transfertVersDiff[i];
        m_repartition[i] = 0.;
    }

    if (m_quantiteTransfere > 0) 
    {
        for (int i(0); i < 8; i++) 
        {
            m_repartition[i] = transfertVersDiff[i] / m_quantiteTransfere;
        }
    }
    m_quantiteTransfere -= m_quantiteErode;
}

void CCaillou::setFluxFluv() 
{
    m_quantiteDepose = m_MesFlux->GetDepot(m_numeroMaille) - m_MesFlux->GetDepotDiff(m_numeroMaille); //On rÈcupËre les infos sur la quantitÈ de sÈdiment qui se dÈpose sur la maille du caillou
    m_quantiteErode = m_MesFlux->GetErosion(m_numeroMaille) - m_MesFlux->GetErosionDiff(m_numeroMaille); //On rÈcupËre les infos sur la quantitÈ de sÈdiment qui s'est ÈrodÈ sur la maille du caillou
    m_quantiteDeposeTotale = m_MesFlux->GetDepot(m_numeroMaille);
    m_quantiteErodeTotale = m_MesFlux->GetErosion(m_numeroMaille);


    m_quantiteTransfere = 0.;
    for (int i(0); i < 8; i++) //On rÈcupËre les infos sur la quantitÈ de sÈdiment qui a transfÈrÈ sur la maille du caillou
    {
        m_quantiteTransfere += m_MesFlux->GetVolumeFluvExporte(m_numeroMaille, i); //Pour l'instant on stocke tout ce qui sort de la maille
        m_repartition[i] = 0.;
    }

    if (m_quantiteTransfere > 0.) //On recupËre la rÈpartition des flux sortant sur la maille du caillou
    {
        for (int i(0); i < 8; i++) 
        {
            m_repartition[i] = m_MesFlux->GetVolumeFluvExporte(m_numeroMaille, i) / m_quantiteTransfere;
        }
    }
    m_quantiteTransfere -= m_quantiteErode; //Finalement on enleve la partie due ‡ l'Èrosion
}

void CCaillou::setPasDeTemps(const int dt) 
{
    m_dt = dt;
}

void CCaillou::setProfAlteInit(const double prof) 
{
    m_profAlteInit = prof;
}


/////////////////////////
////ENSEMBLE DU CALCUL///
/////////////////////////

void CCaillou::suiviCaillouDiff(const int nbCaillou, const int numPeriode)
{
    setFluxDiff(); //On met a jour les infos necessaires au calcul
    m_profInit = m_profondeur; //necessaire pour l'Èvolution des quantitÈs de minÈraux dans les cailloux
    m_erosionInit = m_quantiteErodeTotale; //idem. Totale car on veut savoir ensuite le temps que passe le cailloux
    //sur la premiere maille avant de partir
    double templocale = 273.15 + m_tempSurface[numPeriode];
    m_listeTemp.push_back(templocale); //cette liste sert juste a compter les mailles
    //partcourrue (cf suiviCaillouDiffTemp pour justification)
    m_listeRunoff.push_back(1.); //L'alteration ne depend donc pas du runoff

    bool seDeplace(false);
    seDeplace = estErode(); //On teste si le caillou se deplace ou pas

    bool changeMaille(false);
    if (seDeplace) //Si oui on teste si il change de maille
    {
        changeMaille = directionDiff(); //On determine si le caillou chang de maille et la direction dans laquelle il se dirige
    }

    if (changeMaille) //Dans le cas o˘ il change de maille
    {
        bool seDepose;
        do {
            setFluxDiff(); //On met a jour les infos necessaires au calcul
            m_listeTemp.push_back(templocale); //cette liste sert juste a compter les mailles

            seDepose = hasardSedimentation(); //On teste si le caillou se depose

            if (!seDepose) {
                direction(); //Si il ne se dÈpose pas on teste si il change de maille
            }

        } while (!seDepose && m_dansLaGrille); //Tant que le caillou ne se dÈpose pas et qu'il est sur la grille on continue.
        nouvelleProfondeur(seDepose);
        ; //On met ‡ jour la pronfondeur du caillou
    }
}

void CCaillou::suiviCaillouDiffTemp(const int nbCaillou, const int numPeriode)
{
    setFluxDiff(); //On met a jour les infos necessaires au calcul

    m_profInit = m_profondeur; //necessaire pour l'Èvolution des quantitÈs de minÈraux dans les cailloux
    m_erosionInit = m_quantiteErodeTotale; //idem. Totale car on veut savoir ensuite le temps que passe le cailloux
    //sur la premiere maille avant de partir
    double zlocal = m_MonMaillage->GetMaille(m_numeroMaille)->GetAlti(); //idem
    double templocale = 273.15 + mymax(0., m_tempSurface[numPeriode] - climat.DiminutionTemperature * (zlocal / m_pas2)); //idem
    m_listeTemp.push_back(templocale); //idem
    double runofflocal = m_MesFlux->GetEauCourante(m_numeroMaille) / (m_pas2); //Calcul du runoff local
    double runoffmax = (m_MonModele->GetLitho(0)).alter->NbMailleRunoffLimite * m_pas2;
    if (runofflocal > runoffmax) runofflocal = runoffmax;
    m_listeRunoff.push_back(runofflocal); //On stocke ce runoff


    bool seDeplace(false);
    seDeplace = estErode(); //On teste si le caillou se deplace ou pas

    bool changeMaille(false);
    if (seDeplace) //Si oui on teste si il change de maille
    {
        changeMaille = directionDiff(); //On determine si le caillou chang de maille et la direction dans laquelle il se dirige
    }

    if (changeMaille) //Dans le cas o˘ il change de maille
    {
        bool seDepose;
        do {
            setFluxDiff(); //On met a jour les infos necessaires au calcul

            zlocal = m_MonMaillage->GetMaille(m_numeroMaille)->GetAlti(); //Altitude local, necessaire pour la ligne d'en dessous
            templocale = 273.15 + mymax(0., m_tempSurface[numPeriode] - climat.DiminutionTemperature * (zlocal / m_pas2)); //Calcul de la temperature local
            m_listeTemp.push_back(templocale); //On stocke cette tempÈrature
            runofflocal = m_MesFlux->GetEauCourante(m_numeroMaille) / (m_pas2); //Calcul du runoff local
            if (runofflocal > runoffmax) runofflocal = runoffmax;
            m_listeRunoff.push_back(runofflocal); //On stocke ce runoff

            seDepose = hasardSedimentation(); //On teste si le caillou se depose

            if (!seDepose) 
            {
                direction(); //Si il ne se dÈpose pas on teste si il change de maille
            }


        } while (!seDepose && m_dansLaGrille); //Tant que le caillou ne se dÈpose pas et qu'il est sur la grille on continue.
        nouvelleProfondeur(seDepose); //On met ‡ jour la pronfondeur du caillou
    }
}

void CCaillou::suiviCaillouFluv(const int nbCaillou, const int numPeriode)
{
    setFluxFluv(); //On met a jour les infos necessaires au calcul

    double templocale = 273.15 + m_tempSurface[numPeriode];

    bool seDeplace(false);
    seDeplace = estErode(); //On teste si le caillou se deplace ou pas

    bool changeMaille(false);
    if (seDeplace) //Si oui on teste si il change de maille
    {
        changeMaille = directionFluv(); //On determine si le caillou change de maille et la direction dans laquelle il se dirige
    }

    if (changeMaille) //Dans le cas o˘ il change de maille
    {
        bool seDepose;
        do {
            setFluxFluv(); //On met a jour les infos necessaires au calcul
            m_listeTemp.push_back(templocale); //cette liste sert juste a compter les mailles
            //parcourrues (cf suiviCaillouDiffTemp pour justification)
            m_listeRunoff.push_back(1.); //L'alteration ne depend donc pas du runoff
            seDepose = hasardSedimentation(); //On teste si le caillou se depose

            if (!seDepose) 
            {
                direction(); //Si il ne se dÈpose pas on teste si il change de maille
            }

        } while (!seDepose && m_dansLaGrille); //Tant que le caillou ne se dÈpose pas et qu'il est sur la grille on continue.
        nouvelleProfondeur(seDepose); //On met ‡ jour la pronfondeur du caillou
    }

    calculMineraux(seDeplace, m_MesFlux->GetStockSed(m_numeroMaille));
    m_age += m_dt; //On met a jour l'age du caillou
}

void CCaillou::suiviCaillouFluvTemp(const int nbCaillou, const int numPeriode)
{
    setFluxFluv(); //On met a jour les infos necessaires au calcul

    double zlocal = m_MonMaillage->GetMaille(m_numeroMaille)->GetAlti(); //idem
    double templocale = 273.15 + mymax(0., m_tempSurface[numPeriode] - climat.DiminutionTemperature * (zlocal / m_pas2)); //idem
    m_listeTemp.push_back(templocale); //idem
    double runofflocal = m_MesFlux->GetEauCourante(m_numeroMaille) / (m_pas2); //Calcul du runoff local
    double runoffmax = (m_MonModele->GetLitho(0)).alter->NbMailleRunoffLimite * m_pas2;
    if (runofflocal > runoffmax) runofflocal = runoffmax;
    m_listeRunoff.push_back(runofflocal); //On stocke ce runoff


    bool seDeplace(false);
    seDeplace = estErode(); //On teste si le caillou se deplace ou pas

    bool changeMaille(false);
    if (seDeplace) //Si oui on teste si il change de maille
    {
        changeMaille = directionFluv(); //On determine si le caillou chang de maille et la direction dans laquelle il se dirige
    }

    if (changeMaille) //Dans le cas o˘ il change de maille
    {
        bool seDepose;
        do {
            setFluxFluv(); //On met a jour les infos necessaires au calcul

            zlocal = m_MonMaillage->GetMaille(m_numeroMaille)->GetAlti(); //Altitude locale, necessaire pour la ligne d'en dessous
            templocale = 273.15 + mymax(0., m_tempSurface[numPeriode] - climat.DiminutionTemperature * (zlocal / m_pas2)); //Calcul de la temperature locale
            m_listeTemp.push_back(templocale); //On stocke cette tempÈrature
            runofflocal = m_MesFlux->GetEauCourante(m_numeroMaille) / (m_pas2); //Calcul du runoff local
            if (runofflocal > runoffmax) runofflocal = runoffmax;
            m_listeRunoff.push_back(runofflocal); //On stocke ce runoff

            seDepose = hasardSedimentation(); //On teste si le caillou se depose

            if (!seDepose) {
                directionFluv(); //Si il ne se dÈpose pas on teste si il change de maille
            }

        } while (!seDepose && m_dansLaGrille); //Tant que le caillou ne se dÈpose pas et qu'il est sur la grille on continue.
        nouvelleProfondeur(seDepose); //On met ‡ jour la pronfondeur du caillou
    }

    calculMineraux(seDeplace, m_MesFlux->GetStockSed(m_numeroMaille));
    m_age += m_dt; //On met a jour l'age du caillou
}

void CCaillou::suiviCaillouDiffWithoutDissolution(const int nbCaillou, const int numPeriode)
{
    setFluxDiff(); //On met a jour les infos necessaires au calcul
    m_profInit = m_profondeur; //necessaire pour l'Èvolution des quantitÈs de minÈraux dans les cailloux
    m_erosionInit = m_quantiteErodeTotale; //idem. Totale car on veut savoir ensuite le temps que passe le cailloux
    
    
    bool seDeplace(false);
    seDeplace = estErode(); //On teste si le caillou se deplace ou pas
    
    bool changeMaille(false);
    if (seDeplace) //Si oui on teste si il change de maille
    {
        changeMaille = directionDiff(); //On determine si le caillou change de maille et la direction dans laquelle il se dirige
        if (m_serodepremierefois) // 2023
        {
            m_age = 0.;
            m_serodepremierefois = false;
        };
    }
    
    if (changeMaille) //Dans le cas o˘ il change de maille
    {
        bool seDepose;
        do {
            setFluxDiff(); //On met a jour les infos necessaires au calcul
            
            seDepose = hasardSedimentation(); //On teste si le caillou se depose
            
            if (!seDepose) {
                direction(); //Si il ne se dÈpose pas on teste si il change de maille
            }
            
        } while (!seDepose && m_dansLaGrille); //Tant que le caillou ne se dÈpose pas et qu'il est sur la grille on continue.
        nouvelleProfondeur(seDepose);
        ; //On met ‡ jour la pronfondeur du caillou
    }
}

void CCaillou::suiviCaillouFluvWithoutDissolution(const int nbCaillou, const int numPeriode)
{
    setFluxFluv(); //On met a jour les infos necessaires au calcul
    
    bool seDeplace(false);
    seDeplace = estErode(); //On teste si le caillou se deplace ou pas
    
    bool changeMaille(false);
    if (seDeplace) //Si oui on teste si il change de maille
    {
        changeMaille = directionFluv(); //On determine si le caillou change de maille et la direction dans laquelle il se dirige
        if (m_serodepremierefois) // 2023
        {
            m_age = 0.;
            m_serodepremierefois = false;
        };
    }

    if (changeMaille) //Dans le cas o˘ il change de maille
    {
        bool seDepose;
        do {
            setFluxFluv(); //On met a jour les infos necessaires au calcul
            //parcourrues (cf suiviCaillouDiffTemp pour justification)
            seDepose = hasardSedimentation(); //On teste si le caillou se depose
            if (!seDepose)
            {
                direction(); //Si il ne se dÈpose pas on teste si il change de maille
            }
            
        } while (!seDepose && m_dansLaGrille); //Tant que le caillou ne se dÈpose pas et qu'il est sur la grille on continue.
        nouvelleProfondeur(seDepose); //On met ‡ jour la pronfondeur du caillou
    }
    
    m_age += m_dt; //On met a jour l'age du caillou
}


bool CCaillou::estErode() //renvoie 1 si le caillou est ÈrodÈ, 0 sinon et dans ce cas met ‡ jour sa profondeur
{
    bool resultat = false;

    if (m_quantiteErode > m_profondeur) 
    {
        resultat = true;
        if (m_fantome) // si un caillou fantome est erode, on en finit avec lui. Il pourra ensuite etre ressucite sous le regolithe.
        {
            m_dansLaGrille = false;
            m_fantome = false;
            resultat = false;
        }
    } 
    else 
    {
        m_profondeur += m_quantiteDepose - m_quantiteErode;
        resultat = false;
    }

    return resultat;
}

bool CCaillou::directionDiff()
{
    int mouvement;
    int numeroMailleAvant(m_numeroMaille);
    bool changeMaille = false;
    mouvement = hasardDeplacement(); //on dÈtermine la direction dans laquelle il se dÈplace

    if (hasardChangeMaille(mouvement)) 
    {
        m_numeroMaille = m_MonMaillage->GetMaille(m_numeroMaille)->GetNumVoisin(mouvement - 1); //on met ‡ jour le numÈro de la case
        changeMaille = true;
    }

    if (m_numeroMaille < 0) //‡ condition d'etre sur de la dÈfinition maille exterieur au maillage => m_numeroMaille<0
    {
        m_numeroMaille = numeroMailleAvant; //si le saillou est sorti du maillage, on lui rÈaffecte le numero de la derniËre maille o˘ il a ÈtÈ
        m_dansLaGrille = false; //et on le tue.
    }

    return changeMaille;
}

bool CCaillou::directionFluv()
{
    int mouvement;
    int numeroMailleAvant(m_numeroMaille);
    bool changeMaille = false;
    mouvement = hasardDeplacement(); //on dÈtermine la direction dans laquelle il se dÈplace

    if (hasardChangeMaille(mouvement)) //
    {
        m_numeroMaille = m_MonMaillage->GetMaille(m_numeroMaille)->GetNumVoisin(mouvement - 1); //on met ‡ jour le numÈro de la case
        changeMaille = true;
    }

    if (m_numeroMaille < 0) //‡ condition d'etre sur de la dÈfinition maille exterieur au maillage => m_numeroMaille<0
    {
        m_numeroMaille = numeroMailleAvant; //si le saillou est sorti du maillage, on lui reaffecte le numero de la derniËre maille o˘ il a ÈtÈ
        m_dansLaGrille = false; //et on le tue.
    }

    return changeMaille;
    
}

void CCaillou::direction()
{
    int mouvement;
    int numeroMailleAvant(m_numeroMaille);

    mouvement = hasardDeplacement(); //on dÈtermine la direction dans laquelle il se dÈplace
    m_numeroMaille = m_MonMaillage->GetMaille(m_numeroMaille)->GetNumVoisin(mouvement - 1); //on met ‡ jour le numÈro de la case

    if (m_numeroMaille < 0) //‡ condition d'etre sur de la dÈfinition maille exterieur au maillage => m_numeroMaille<0
    {
        m_numeroMaille = numeroMailleAvant; //si le saillou est sorti du maillage, on lui rÈaffecte le numero de la derniËre maille o˘ il a ÈtÈ
        m_dansLaGrille = false; //et on le tue.
    }

}

void CCaillou::nouvelleProfondeur(const bool seDepose) //Mise ‡ jour de la profondeur du caillou (dans le cas o˘ il s'est dÈplacÈ)
{ //Pour l'instant aucune pondÈration
    if (seDepose) //soit aprËs s'Ítre dÈposÈ
    {
        m_profondeur = m_quantiteDepose * hasardNombre();
    } 
    else //soit sans s'Ëtre dÈposÈ
    {
        m_profondeur = 0.;
    }
}

void CCaillou::calculMineraux(const bool seDeplace, const double profAlteFin) 
{

    // Recupere le rayon du cailloux au debut du pas de temps, avant dissolution
    m_rayonDebutPasdeTemps = m_rayon;
    // Calcul du temps que le caillou a passe sur chaque maille

    double dtutile = 0.;

    double B(profAlteFin); // Epaisseur de regolithe a la fin du pas de temps (meme maille car pas de deplacement)
    double D(m_profondeur); // Profondeur du cailloux a la fin du pas de temps
    bool saltere = true;

    dtutile = UNANENSECONDES * m_dt; // 21 mars 2016. On simplifie : certes le clast se dissout durant une fraction du temps mais le regolithe au-dessus qui l'inclut se dissout pendant dt.

    if (B <= D)// si le cailloux est dans le bedrock il ne s'altere pas
    {
        saltere = false;
        m_age = -m_dt; // pour avoir effectivement 0 a la fin du pas de temps.
        for (int i(0); i < m_nbMineral; i++) 
        {
            m_quantiteDissoute[i] = 0.;
        }
    }


    if (saltere) 
    {

        if (m_salterepremierefois) 
        {
            m_age = 0.;
            m_salterepremierefois = false;
        };


        for (int i(0); i < m_nbMineral; i++) //Pour chaque minÈral on fait :
        {
            double r(m_rayonMineral[i]);
            double R(RGAZPARFAIT);
            double Ea(m_energieActivation[i]);
            double Vm(m_volumeMolaire[i]);
            double volumePerdu = m_proportionMineralInit[i] * m_Rugosite * m_listeRunoff[0] *
                    Vm * dtutile * 4. * PI * r * r * m_ki[i] * exp((Ea / R)*(1. / TEMPERATUREREFERENCE - 1. / m_listeTemp[0])) / 100.;

            double volumeinitial = m_volumeMineral[i];
            m_volumeMineral[i] -= volumePerdu; //On met ‡ jour le volume de minÈral
            if (m_volumeMineral[i] < 0.) //Si ce vomume est devenu nÈgatif
            {
                m_rayonMineral[i] = 0.; //On met le volume et le rayon ‡ 0
                volumePerdu = volumeinitial;
                m_volumeMineral[i] = 0.;
            } 
            else //Si le volume est toujours positif
            {
                m_rayonMineral[i] = pow(300. * m_volumeMineral[i] / (4. * PI * m_proportionMineralInit[i]), 1. / 3.); //On met ‡ jour le rayon  du minÈral en question
            }

            m_quantiteDissoute[i] = volumePerdu;
        }

        m_rayon = 0.;
        double volTot(0.);
        for (int i(0); i < m_nbMineral; i++) //Ici on met ‡ jour le vrai rayon du caillou en lui donnant la valeur du plus grand rayon des sphËres de chaque minÈral
        {
            volTot += m_volumeMineral[i]; //Et on calcule le volume total du caillou
            if (m_rayonMineral[i] > m_rayon) 
            {
                m_rayon = m_rayonMineral[i];
            }
        }

        if (volTot <= 0.) 
        {
            m_fantome = true; //Si le caillou a ÈtÈ totalement dissout, on il devient un fantome de taille nulle. Il sera tué des qu'il est erodé
        } 
        else 
        {
            for (int i(0); i < m_nbMineral; i++) 
            {
                m_proportionMineral[i] = 100. * m_volumeMineral[i] / volTot; //On met ‡ jour les proportions relatives de chaque minÈral dans l'ensemble du caillou
            }
        }
    }
}


//Ajout Youssouf 21/01/2021
void CCaillou::InitialiserCosmonucleides(CModele *modele)
{   // Ajout Youssouf
    // On initialise toutes les variables dont on aura besoin pour calculer la concentration en cosmonucleide
    // Debut variables ajoutees pour le calcul de la production de nucleides cosmogeniques 20/10/2021
    m_AltitudeMoyenne= 0.;
    m_Pressure= 0.;
    m_ScalingSpallation=0;
    m_MuonCapture=0;
    m_totalage=0;
    m_numeroMaillePrecedent=m_numeroMaille;
    m_ProfondeurAvant=m_profondeur; // On initialise la profondeur précédente
    m_NbCosmonucleide=0;
    
    // On construit les variables de calcul en fonction des cosmonucleides que nous voulons étudier
    if (m_MonModele->GetOptions().CosmonucleideEtudie[0])
    {
        m_Lambda.push_back(Lambda10Be);
        m_ProductionRateSeeLevel.push_back(ProductionRateSeeLevel10Be);
        m_NbCosmonucleide+=1;
    }
    if (m_MonModele->GetOptions().CosmonucleideEtudie[1])
    {
        m_Lambda.push_back(Lambda26Al);
        m_ProductionRateSeeLevel.push_back(ProductionRateSeeLevel26Al);
        m_NbCosmonucleide+=1;
    }
    if (m_MonModele->GetOptions().CosmonucleideEtudie[2])
    {
        m_Lambda.push_back(Lambda14C);
        m_ProductionRateSeeLevel.push_back(ProductionRateSeeLevel14C);
        m_NbCosmonucleide+=1;
    }
    if (m_MonModele->GetOptions().CosmonucleideEtudie[3])
    {
        m_Lambda.push_back(Lambda21Ne);
        m_ProductionRateSeeLevel.push_back(ProductionRateSeeLevel21Ne);
        m_NbCosmonucleide+=1;
    }
        

    for (int i=0 ; i<m_NbCosmonucleide ; i++)
    {
        m_SurfaceProductionRate.push_back(0.);
        m_ProductionRate.push_back(0.);
        m_IntermediateIntegral.push_back(0.);
        m_InitialCosmoConcentration.push_back(0.); // 2023
        m_CosmoConcentration.push_back(0.);
    }
    //fin ajout Youssouf du 20/10/2021
}


void CCaillou::setTotalage()
{   // Ajout Youssouf
    // Calcul l'âge du caillou en années
    if (m_dansLaGrille)
    {
        m_totalage += m_dt; // On incrémente l'âge du caillou par le pas de temps cidre dt
        // rmq : m_age qui correspond au temps passe depuis le premier mouvement ou depuis le premier passage dans le regolite est incremente lors du deplacement des grains
    }
    else
        // Si le caillou vient de sortir de la grille, on estime le dernier pas de temps dt ou le caillou est reste dans la grille
    {
        double dtDanslaGrille = m_dt;
        if (m_profInit>m_MesFlux->GetDepot(m_numeroMaillePrecedent))
        dtDanslaGrille *=  (m_profInit)/(m_erosionInit); // 2023
        m_dt = dtDanslaGrille;
        m_totalage += m_dt;
    }
}


void CCaillou::setProfondeurMoyenne()
{
    // Ajout Youssouf
    // Calcul la profondeur moyenne du caillou entre deux itérations successives. Cela permet d'avoir un compromis sur l'estimation de la concentration du caillou, afin d'avoir une concentration moyenne sur le long du parcours effectué pendant une itération de cidre
    double ProfondeurMaintenant ;
    ProfondeurMaintenant= m_profondeur;
    m_ProfondeurMoyenne= 0.5*(ProfondeurMaintenant+m_ProfondeurAvant);
    m_ProfondeurAvant=ProfondeurMaintenant;
}


void CCaillou::setAltitudeMoyenne()
{   // Ajout Youssouf
    // Calcul l'altitude moyenne du caillou entre deux itérations successives, cela permet d'avoir un compromis sur l'estimation de la concentration du caillou, afin d'avoir une concentration moyenne sur le long du parcours effectué pendant une itération de cidre

    double AltitudeMaintenant ;
    double AltitudePrecedent ;
    AltitudeMaintenant=(m_MonMaillage->GetMaille(m_numeroMaille)->GetAlti())/m_pas2 ;
    AltitudePrecedent=(m_MonMaillage->GetMaille(m_numeroMaillePrecedent)->GetAlti())/m_pas2 ;
    m_AltitudeMoyenne= 0.5*(AltitudePrecedent+AltitudeMaintenant);

    m_numeroMaillePrecedent=m_numeroMaille; // On stocke le numéro de la maille pour l'utiliser a l'itération suivante
}

void CCaillou::ComputePressure()
{
    //Ajout Youssouf
    // Calcul la pression atmosphérique a l'altitude moyenne du caillou
    m_Pressure = 1013.25 * pow(1.- (0.00002255 * m_AltitudeMoyenne),5.257);
}
void CCaillou::ComputeScalingSpallation()
{   // Ajout Youssouf
    // Facteur de production par spallation
    double pressurepressure = m_Pressure*m_Pressure;
    m_ScalingSpallation = aStone + bStone*exp(-m_Pressure/150.) + cStone*m_Pressure + dStone*pressurepressure
                   + eStone*pressurepressure*m_Pressure;
}
void CCaillou:: ComputeScalingMuons()
{   // Ajout Youssouf
    // Facteur de production par capture de muons
    m_ScalingSlowMuonCapture = exp((1013.25-m_Pressure)/260.); // 2023
    m_ScalingFastMuon = exp((1013.25-m_Pressure)/510.); // 2023
}
void CCaillou::ComputeSurfaceProductionRate()
{   // Ajout Youssouf
    // Calcul le taux de  production en surface.
    // Coeff multiplicatifs pris dans Stone 2000.
    for (int i=0 ; i<m_NbCosmonucleide ; i++)
    {
        m_SurfaceProductionRate[i] = m_ProductionRateSeeLevel[i]*(FracNeutronSLHL * m_ScalingSpallation + FracSlowMuonSLHL * m_ScalingSlowMuonCapture + FracFastMuonSLHL * m_ScalingFastMuon); // 2023
    }
}

void CCaillou::ComputeTotalProductionRate()
{
    // Ajout Youssouf
    // Calcul le taux de production de cosmonucleide total, par combinaison des productions grâce aux muons et a la spallation.
    double depth = rho*(m_rayon-m_ProfondeurMoyenne/m_pas2);
    if (depth >0.) depth= -1.*rho*m_rayon; //2023
    // Profondeur du centre du caillou * la masse volumique de la roche
    double CoeffProductionRate = FracNeutronSLHL * m_ScalingSpallation * exp(depth/AttenuationNeutron) // modif 2023
                    + FracFastMuonSLHL * m_ScalingFastMuon * exp(depth/AttenuationFastMuon)
                    + FracSlowMuonSLHL * m_ScalingSlowMuonCapture * exp(depth/AttenuationSlowMuon);
    
    for (int i=0 ; i<m_NbCosmonucleide ; i++)
    {
        m_ProductionRate[i] = CoeffProductionRate * m_ProductionRateSeeLevel[i]; //* m_SurfaceProductionRate[i]; // modif 2023
    }

}
void CCaillou::ComputeIntermediateIntegral()
{    // Ajout Youssouf
     // Fonction qui exécute un calcul intermédiaire nécessaire pour le calcul de la concentration
    for (int i=0 ; i<m_NbCosmonucleide ; i++)
    {
        m_IntermediateIntegral[i] = m_IntermediateIntegral[i] + exp(m_Lambda[i]*m_totalage) * m_ProductionRate[i] * m_dt;
    }
}
void CCaillou::ComputeCosmoConcentration()
{
    // Ajout Youssouf
    // Calcul de la concentration des différents cosmonucleides
    for (int i=0 ; i<m_NbCosmonucleide ; i++)
    {
      m_CosmoConcentration[i] = exp(-1.*m_Lambda[i]*m_totalage) * (m_InitialCosmoConcentration[i] + m_IntermediateIntegral[i]);
    }
}

void CCaillou::ComputeInitialSteadyStateCosmoConcentration() // 2023
{
    // Calcul de la concentration des différents cosmonucleides au steady state
    double depth = m_rayon-m_profondeur/m_pas2;
    if (depth>0.) depth=-1.*m_rayon;
    double eros =  m_MesFlux->GetDeltaErosion(m_numeroMaille)/m_pas2/m_dt;
    if (eros > 0.) eros = 1.E-7; // si on avait de la sedimentation sur cette maille on fixe une valeur arbitraire très faible
    else eros = -eros;
    double Lneutron = AttenuationNeutron/rho;
    double LFastMuon = AttenuationFastMuon/rho;
    double LSlowMuon = AttenuationSlowMuon/rho;
    
    m_AltitudeMoyenne = m_MonMaillage->GetMaille(m_numeroMaille)->GetAlti()/m_pas2;
    ComputePressure();
    ComputeScalingSpallation();
    ComputeScalingMuons();
    
    for (int i=0 ; i<m_NbCosmonucleide ; i++)
    {
        double Coeff = FracNeutronSLHL * m_ScalingSpallation * exp(depth/Lneutron) * Lneutron / (eros + m_Lambda[i]*Lneutron)
        + FracFastMuonSLHL * m_ScalingFastMuon * exp(depth/LFastMuon) * LFastMuon / (eros + m_Lambda[i]*LFastMuon)
        + FracSlowMuonSLHL * m_ScalingSlowMuonCapture * exp(depth/LSlowMuon) * LSlowMuon / (eros + m_Lambda[i]*LSlowMuon);

        m_InitialCosmoConcentration[i] =  Coeff * m_ProductionRateSeeLevel[i];
    }
}

vector<double> CCaillou::getCosmoConcentration() const
{   // Ajout Youssouf
    return m_CosmoConcentration;
}
int CCaillou::getNbCosmonucleide() const
{   // Ajout Youssouf
    return m_NbCosmonucleide;
}
void CCaillou::ComputeCosmonuclide()
{   // Ajout Youssouf
    // Fonction qui centralise les autres fonctions permettant le calcul de la concentration en cosmonucleide
    setTotalage();
    setAltitudeMoyenne();
    setProfondeurMoyenne();
    ComputePressure();
    ComputeScalingSpallation();
    ComputeScalingMuons();
    //ComputeSurfaceProductionRate(); // 2023 plus besoin
    ComputeTotalProductionRate();
    ComputeIntermediateIntegral();
    ComputeCosmoConcentration();

}


void CCaillou::NeFaitRien(){
    // Ajout Youssouf
    // Si on ne veut pas calculer la concentration en cosmonucleide, on pointe cette fonction
}
// fin ajout youssouf


//////////////////////////
////FONCTION DE HASARD////
//////////////////////////

int CCaillou::hasardDeplacement() //Fonction qui permet de dÈterminer au hasard la direction que prend un caillou dans le cas o˘ il bouge
{ //Pour l'instant seul la quantitÈ de sÈdiment partant dans chaque direction pondËre le tirage au sort
    int resultat;
    double alea;

    alea = hasardNombre();
    if (alea <= m_repartition[0]) 
    {
        resultat = 1;
    } 
    else if (alea <= m_repartition[0] + m_repartition[1]) 
    {
        resultat = 2;
    } 
    else if (alea <= m_repartition[0] + m_repartition[1] + m_repartition[2]) 
    {
        resultat = 3;
    } 
    else if (alea <= m_repartition[0] + m_repartition[1] + m_repartition[2] + m_repartition[3]) 
    {
        resultat = 4;
    } 
    else if (alea <= m_repartition[0] + m_repartition[1] + m_repartition[2] + m_repartition[3] + m_repartition[4]) 
    {
        resultat = 5;
    } 
    else if (alea <= m_repartition[0] + m_repartition[1] + m_repartition[2] + m_repartition[3] + m_repartition[4] + m_repartition[5]) 
    {
        resultat = 6;
    } 
    else if (alea <= m_repartition[0] + m_repartition[1] + m_repartition[2] + m_repartition[3] + m_repartition[4] + m_repartition[5] + m_repartition[6]) 
    {
        resultat = 7;
    } 
    else 
    {
        resultat = 8;
    }

    return resultat; //On renvoie un nombre de 1 ‡ 8 correspondant ‡ la direction dans laquelle se dÈplace le caillou
}

bool CCaillou::hasardSedimentation() 
{ //Fonction qui permet de dÈterminer au hasard si un caillou qui bouge se dÈpose ou pas
    bool resultat(false); //Pour l'instant on pondËre seulement avec le rapport entre quantitÈ transfÈrÈ et quantitÈ dÈposÈ
    double alea;
    double var;

    alea = hasardNombre();

    var = m_quantiteDepose / (m_quantiteDepose + m_quantiteTransfere);

    if (alea < var) 
    {
        resultat = true; //On renvoie 0 si le caillou ne se dÈpose pas
    }
    return resultat;
}

bool CCaillou::hasardChangeMaille(int mouvement) //Dans le cas o˘ le caillou vient d'etre ÈrodÈ on teste si il change de maille
{
    bool resultat(false);
    double alea;
    double var;

    if (m_quantiteErode < 0.0000001) 
    {
        resultat = false;
    } 
    else 
    {
        alea = hasardNombre();
        var = (1.25 / (2. * m_rayon))*(1. - m_profondeur / m_quantiteErode) * m_quantiteErode / m_pas2;

        if (mouvement > 4) 
        {
            var /= sqrt(2.);
        }
        if (alea <= var) 
        {
            resultat = true; //renvoie 1 si il change de maille
        } 
        else 
        {
            m_profondeur = 0;
        }
    }

    return resultat;
}

double CCaillou::hasardNombre() //fonction qui renvoie un nombre au hasard compris dans [0 ; 1]
{
    double resultat;

    double alea1, alea2; //pour info : rand_max = 32 767 et (RAND_MAX+1)*RAND_MAX+RAND_MAX = 1 073 741 823
    alea1 = rand();
    alea2 = rand();

    resultat = (alea1 * (double(RAND_MAX) + 1.) + alea2) / ((double(RAND_MAX) + 2.) * double(RAND_MAX));


    return resultat;
}

 
