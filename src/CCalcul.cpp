#include "CCalcul.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <omp.h>
using namespace std;

//-------------------------------------------
//
// fonctions de la classe CCalcul :
//
//-------------------------------------------

// constructeur ‡ partir d'un modËle et d'un maillage existant

CCalcul::CCalcul(CModele* modele, CMaillage* maillage) 
{
    MonModele = modele;
    MonMaillage = maillage; 
    nbmailles = maillage->GetNbMailles();
    pas3 = maillage->GetPas3();
    pas2 = maillage->GetPas2();
    nbCaillouVivant = 0;
    nbCaillouMortAcetteIteration = 0; // 2023

    //---------------------------------------
    // MODIF ET AJOUT CLIMAT STOCHASTIQUE:
    //---------------------------------------
    MesFlux = new CClimatStochastique(maillage, modele); // L'objet MesFlux est maintenant une instance de la classe CClimatStochastique, qui derive de CFlux
    nbValeurPrecipStoch = modele->GetNbValeurPrecipStoch(); //nombre de valeurs discretes de la distribution des precipitations

    //---------------------------------------
    // FIN MODIF ET AJOUT
    //---------------------------------------
    MesEffondrements = new CEffondr(modele, maillage, MesFlux);
    MonBilanEau = new CBilanEau(modele, maillage, MesFlux);
    MonBilanSed = new CBilanSed(modele, maillage, MesFlux, MonBilanEau);
    MesSorties = new CSorties(modele, maillage, MesFlux, MonBilanEau);


    // Utilise le stock sÈdimentaire initial des mailles
    // pour initialiser StockSed
    MesFlux->InitialiseStockSedLitho();
    // DÈtermine le calcul des prÈcipitations en fonction de l'option choisie
    //---------------------------------------
    // MODIF CLIMAT STOCHASTIQUE:
    //---------------------------------------
    if (modele->TestePrecipStochastique()) 
    {
        MonCalculPluieStoch = &CClimatStochastique::CalculSequencePluieStochPowerlaw;
        MonInitFlux_RecuperePluieStoch = &CClimatStochastique::InitFlux_RecuperePluie_NoSin_NoLin_Stoch;
    } 
    else 
    {
        if (modele->TestePrecipSinus()) 
        {
            if (modele->TestePrecipLinAlti())
                MonInitFlux_RecuperePluie = &CFlux::InitFlux_RecuperePluie_Sin_Lin;
            else
                MonInitFlux_RecuperePluie = &CFlux::InitFlux_RecuperePluie_Sin_NoLin;
        } 
        else 
        {
            if (modele->TestePrecipLinAlti())
                MonInitFlux_RecuperePluie = &CFlux::InitFlux_RecuperePluie_NoSin_Lin;
            else
                if (modele->TestePrecipGaussAlti())
                MonInitFlux_RecuperePluie = &CFlux::InitFlux_RecuperePluie_NoSin_NoLin_Gaussienne; //modifie le 01/02/11
            else
                MonInitFlux_RecuperePluie = &CFlux::InitFlux_RecuperePluie_NoSin_NoLin;
        }
    }
    //---------------------------------------
    // FIN MODIF CLIMAT STOCHASTIQUE
    //---------------------------------------
    // Selon l'option d'effondrements :
    if ((modele->GetOptions()).OptionEffondr) 
    {
        if (modele->GetOptions().OptionIle)
            MonCalculeEffondrements = &CEffondr::EffondreGrille_ILE;
        else if (modele->GetOptions().OptionNivBase)
            MonCalculeEffondrements = &CEffondr::EffondreGrille;
        else
            MonCalculeEffondrements = &CEffondr::EffondreGrille_TERRE;
    } 
    else 
    {
        if (modele->GetOptions().OptionNivBase) 
        {
            //MonCalculeEffondrements = &CEffondr::PenteSansEffondr; // decembre 2018
            MonCalculeEffondrements = &CEffondr::PenteSansEffondr_TERRE;
        }
        else
        {
            MonCalculeEffondrements = &CEffondr::PenteSansEffondr_TERRE;
        }
    }

    // Selon l'option : altÈration ou non :
    if ((modele->GetOptions()).OptionAlter)
        MonActuCoeffs = &CBilanSed::ActualiseCoeffs_AvecAlter;
    else
        MonActuCoeffs = &CBilanSed::ActualiseCoeffs_SansAlter;

    // Selon l'option : tecto ou non :
    if ((modele->GetOptions()).OptionTecto) 
    {
        if ((MonModele->GetParamTecto()).OptionUplift) 
        {
            MonActuTecto = &CMaillage::ActualiseUplift;
            if ((MonModele->GetParamTecto()).OptionDecro)
                MonActiviteTecto = &CFlux::SouleveEtDecroche;
            else
                MonActiviteTecto = &CFlux::Souleve;
        } 
        else 
        {
            MonActuTecto = &CMaillage::PasUplift;
            MonActiviteTecto = &CFlux::Decroche;
        }
    } 
    else 
    {
        MonActuTecto = &CMaillage::PasUplift;
        MonActiviteTecto = &CFlux::PasDeTecto;
    }
    // Selon l'option : niveau de base, et ile ou non :
    if ((modele->GetOptions()).OptionNivBase) 
    {
        if ((modele->GetNivBase()).mode_nivbase == value) 
        {
            if ((modele->GetNivBase()).Sinus) 
            {
                if (modele->GetOptions().OptionIle)
                    MonActuVoisinsNivbase = &CMaillage::ReinitVoisinages_NivBaseSinus_ILE;
                else
                    MonActuVoisinsNivbase = &CMaillage::ReinitVoisinages_NivBaseSinus;
            } 
            else
                MonActuVoisinsNivbase = &CMaillage::ReinitVoisinages_PasNivBase; // ??
        } 
        else 
        {
            if (modele->GetOptions().OptionIle)
                MonActuVoisinsNivbase = &CMaillage::ReinitVoisinages_NivBaseEustatic_ILE;
            else
                MonActuVoisinsNivbase = &CMaillage::ReinitVoisinages_NivBaseEustatic;
        }
        if ((modele->GetOptions()).OptionSansLacs) 
        {
            MonCalc_BilanEau = &CBilanEau::BilanEau_SansLacs;
            if ((modele->GetOptions()).OptionHauteurEau) MonCalc_BilanEau = &CBilanEau::BilanEau_SansLacs_CalculHeau; // 2019
        } 
        else
            MonCalc_BilanEau = &CBilanEau::BilanEau;
    } 
    else 
    {
        MonActuVoisinsNivbase = &CMaillage::ReinitVoisinages_PasNivBase;
        if ((modele->GetOptions()).OptionSansLacs) 
        {
            MonCalc_BilanEau = &CBilanEau::BilanEau_TERRE_SansLacs;
            if ((modele->GetOptions()).OptionHauteurEau) MonCalc_BilanEau = &CBilanEau::BilanEau_TERRE_SansLacs_CalculHeau; // 30 juin 2008
        } 
        else 
        {
            MonCalc_BilanEau = &CBilanEau::BilanEau_TERRE;
        }
    }

    // choix de l'algo de tri selon qu'on calcule les hauteurs d'eau ou pas
    if ((modele->GetOptions()).OptionHauteurEau) 
    {
        MonTriOptimalMailles = &CMaillage::TriRapideOptimal_deMailles_Eau;
        MonTriParInsertionMailles = &CMaillage::TriParInsertion_deMailles_Eau;
    } 
    else 
    {
        MonTriOptimalMailles = &CMaillage::TriRapideOptimal_deMailles;
        MonTriParInsertionMailles = &CMaillage::TriParInsertion_deMailles;
    }

    // Selon l'option : seuil de flux entre processus diffusif et fluviaux
    if ((modele->GetOptions()).OptionSeuilFlux) 
    {
        if ((modele->GetOptions()).OptionHauteurEau)
            MonCalc_BilanSed = &CBilanSed::BilanSedim_Heau_SeuilFlux;
        else
            MonCalc_BilanSed = &CBilanSed::BilanSedim_SeuilFlux;
    } 
    else 
    {
        if ((modele->GetOptions()).OptionHauteurEau)
            MonCalc_BilanSed = &CBilanSed::BilanSedim_Heau;
        else
            MonCalc_BilanSed = &CBilanSed::BilanSedim;
    }
    // Selon l'option : masque :
    if ((modele->GetParamSorties()).OptionZone || (modele->GetParamSorties()).OptionMasque) 
    {
        MonIncrementeSorties = &CSorties::IncrementeBIN_Masque;
    } 
    else if (MonModele->GetOptions().OptionCosmo)
    {
        MonIncrementeSorties = &CSorties::IncrementeBINCOSMO; // 2023 il faudrait rajouter la routine pour le cas masque et cosmo
    }
    else
        MonIncrementeSorties = &CSorties::IncrementeBIN;


    // Selon l'option : précipitation stochastique (AJOUT PIERRE) :
    if (MonModele->TestePrecipStochastique()) 
    {
        MaBoucle = &CCalcul::BouclePeriodeStoch;
        MaBoucleCaillou = &CCalcul::BouclePeriodeStochCaillou;
    } 
    else 
    {
        MaBoucle = &CCalcul::BouclePeriode;
        MaBoucleCaillou = &CCalcul::BouclePeriodeCaillou;
    }

    //---------------------------------------
    // CAILLOUX - Choix avec gradient elev-temperature ou non et dissolution ou non
    //--------------------------------------
    if ((modele->GetOptions()).OptionDissolve)
    {
        MonFluxChimiqueIntegre = &CSorties::FluxChimiqueIntegre;
        MonInitSortiesCailloux = &CSorties::InitialiseSortiesCailloux;
        MonFinitSortiesCailloux = &CSorties::FinitSortiesCailloux;

        if ((modele->GetOptions()).OptionTempRunoff)
        {
            MonSuiviCaillouDiff = &CCaillou::suiviCaillouDiffTemp;
            MonSuiviCaillouFluv = &CCaillou::suiviCaillouFluvTemp;
        }
        else
        {
            MonSuiviCaillouDiff = &CCaillou::suiviCaillouDiff;
            MonSuiviCaillouFluv = &CCaillou::suiviCaillouFluv;
        }
    }
    else
    {
        MonFluxChimiqueIntegre = &CSorties::NeFaitRien;
        MonInitSortiesCailloux = &CSorties::InitialiseSortiesCaillouxWithoutDissolution;
        MonFinitSortiesCailloux = &CSorties::FinitSortiesCaillouxWithoutDissolution;
        MonSuiviCaillouDiff = &CCaillou::suiviCaillouDiffWithoutDissolution;
        MonSuiviCaillouFluv = &CCaillou::suiviCaillouFluvWithoutDissolution;
    }

    //--------------------------------------
    if ((modele->GetOptions()).OptionRessuscite) 
    {
        if ((modele->GetOptions()).OptionCosmo)
            MonRessusciteCaillouMort = &CCalcul::RessusciteCaillouMortCOSMO;
        else
            MonRessusciteCaillouMort = &CCalcul::RessusciteCaillouMort;
    } 
    else 
    {
        MonRessusciteCaillouMort = &CCalcul::NeFaitRien; // modif 2023
    }
    
    //Ajout youssouf 21/10/21
    
    
    if ((modele->GetOptions()).OptionCosmo)
    {
        MonComputeCosmonuclide= &CCaillou::ComputeCosmonuclide;
    }
    else
        MonComputeCosmonuclide= &CCaillou::NeFaitRien;
    //fin ajout

}

// destructeur

CCalcul::~CCalcul() 
{
    delete MesFlux;
    delete MesEffondrements;
    delete MonBilanEau; // 30 juin 2008
    delete MonBilanSed; // 30 juin 2008
    delete MesSorties; // 30 juin 2008
}

//=================
// Autres fonctions
//=================




//================
//	Tout le calcul
//================

void CCalcul::RunLeCalculSansCaillou() 
{

    cout << "THE SIMULATION BEGINS. YOU PROBABLY HAVE TIME TO DO SOMETHING ELSE BEFORE IT TERMINATES ... " << endl;
    cout << "EVERY GRID OUPUT WILL BE INDICATED " << endl;

    // NIVEAU DE BASE (pour l'instant une valeur constante)
    //
    if ((MonModele->GetOptions()).OptionNivBase) 
    {
        if ((MonModele->GetNivBase()).mode_nivbase == value)
            MonMaillage->InitialiseNiveauDeBase_Constant();
        else
            MonMaillage->InitialiseNiveauDeBase_Eustatic();
    }

    // Assigne les bords au niveau de base dans le cas d'une Óle
    if ((MonModele->GetOptions()).OptionIle)
        MonMaillage->BordureIle();

    // INITIALISATION
    //

    //---------------------------------------
    // MODIF CLIMAT STOCHASTIQUE:
    //---------------------------------------
    temps = MonModele->GetFinPeriode(-1);
    if (MonModele->TestePrecipStochastique()) 
    {
        if (!MesFlux->InitialisePrecipStoch(MonModele))
            return;
    } 
    else 
    {
        if (!MesFlux->InitialisePrecip(MonModele))
            return;
    }

    //---------------------------------------
    // FIN MODIF CLIMAT STOCHASTIQUE
    //---------------------------------------

    // LECTURE EVENTUELLE DU RESTART
    //
    if ((MonModele->GetOptions()).OptionReprise) 
    {
        if (!MesSorties->Reprise(temps)) 
        {
            AfficheMessage("Erreur a la reprise du restart");
            return;
        }
    }

    // INITIALISATION DES SORTIES ET DES COMPTEURS DE SORTIES
    //
    if ((MonModele->GetParamSorties()).OptionZone)
        MesSorties->InitialiseSorties_Zone(MonModele->GetParamTecto());
    else if ((MonModele->GetParamSorties()).OptionMasque)
        MesSorties->InitialiseSorties_Masque();
    else
        MesSorties->InitialiseSorties();

    // INITIALISATION DU COMPTEUR DE DECROCHEMENT
    //
    if ((MonModele->GetParamTecto()).OptionDecro) 
    {
        MesFlux->InitialiseDecro(MonModele->GetParamTecto(), sqrt(pas2));
        MonMaillage->InitialiseDecro(MonModele->GetParamTecto());
    }

    NbEffondrements = 0;
    NbLacs = 0;

    // CALCUL PAR PERIODE (MODIF PIERRE)
    //

    for (int i = 0; i < MonModele->GetNbPeriodes(); i++) 
    {
        (((CCalcul*)this)->*CCalcul::MaBoucle)(i);
        MesSorties->ActualiseNumPeriod(i);
    }


    // FERMETURE DES SORTIES ET ECRITURE DU RESTART
    //
    MesSorties->FinitSorties();
    if (!MesSorties->EcritRestart(temps))
        AfficheMessage("Pb dans l'ecriture du restart");

    return;
}

void CCalcul::RunLeCalculAvecCaillou() 
{

    cout << "THE SIMULATION BEGINS. YOU PROBABLY HAVE TIME TO DO SOMETHING ELSE BEFORE IT TERMINATES ... " << endl;
    cout << "EVERY GRID OUPUT WILL BE INDICATED " << endl;


    // NIVEAU DE BASE (pour l'instant une valeur constante)
    //
    if ((MonModele->GetOptions()).OptionNivBase)
    {
        if ((MonModele->GetNivBase()).mode_nivbase == value)
            MonMaillage->InitialiseNiveauDeBase_Constant();
        else
            MonMaillage->InitialiseNiveauDeBase_Eustatic();
    }

    // Assigne les bords au niveau de base dans le cas d'une Óle
    if ((MonModele->GetOptions()).OptionIle)
        MonMaillage->BordureIle();

    // INITIALISATION
    //

    //---------------------------------------
    // MODIF CLIMAT STOCHASTIQUE:
    //---------------------------------------
    temps = MonModele->GetFinPeriode(-1);
    if (MonModele->TestePrecipStochastique()) 
    {
        if (!MesFlux->InitialisePrecipStoch(MonModele))
            return;
    } 
    else 
    {
        if (!MesFlux->InitialisePrecip(MonModele))
            return;
    }

    //---------------------------------------
    // FIN MODIF CLIMAT STOCHASTIQUE
    //---------------------------------------

    // LECTURE EVENTUELLE DU RESTART
    //
    if ((MonModele->GetOptions()).OptionReprise) 
    {
        if (!MesSorties->Reprise(temps)) 
        {
            AfficheMessage("Erreur a la reprise du restart");
            return;
        }
    }

    // INITIALISATION DES SORTIES ET DES COMPTEURS DE SORTIES
    //
    if ((MonModele->GetParamSorties()).OptionZone)
        MesSorties->InitialiseSorties_Zone(MonModele->GetParamTecto());
    else if ((MonModele->GetParamSorties()).OptionMasque)
        MesSorties->InitialiseSorties_Masque();
    else if ((MonModele->GetOptions()).OptionCosmo)
        MesSorties->InitialiseSortiesCOSMO(); // 2023 // Il faudra plus tard faire des surcharges pour les options masques et zone
    else
        MesSorties->InitialiseSorties();

    (MesSorties->*MonInitSortiesCailloux)(); // Il faudra plus tard faire des surcharges pour les options masques et zone

    // INITIALISATION DU COMPTEUR DE DECROCHEMENT
    //
    if ((MonModele->GetParamTecto()).OptionDecro) 
    {
        MesFlux->InitialiseDecro(MonModele->GetParamTecto(), sqrt(pas2));
        MonMaillage->InitialiseDecro(MonModele->GetParamTecto());
    }

    NbEffondrements = 0;
    NbLacs = 0;


    ///////////////////////////////////////////////////////////////
    ////Création de la liste de Cailloux et tout ce qui va avec////
    ///////////////////////////////////////////////////////////////

    srand(int(time(NULL)));
    //BP for validation
    //srand(0);
    
    int nbCaillou(0);
    //vector<CCaillou> listeCaillou;
    nbCaillou = listeCaillou[0].compterCailloux(); //Choisir entre cette otpion
    //nbCaillou = 1;                                          //Ou bien celle là
    CCaillou caillou(MonModele, MesFlux, MonMaillage);
    cout << "> Reading the list of clasts" << endl;
    ifstream monFluxEntrant("InitialClastList"); //Ouverture du fichier en lecture
    string ligne;
    int x, y, n;
    double z, r, age;
    string nom;

    if (monFluxEntrant) 
    {
        for (int i(0); i < nbCaillou; i++) 
        {
            listeCaillou.push_back(caillou);
            getline(monFluxEntrant, ligne); //On lit le nombre de ligne qu'il faut pour dÈplacer le curseur au niveau des valeurs ‡ rÈcuperer
            monFluxEntrant >> n;
            monFluxEntrant >> x;
            monFluxEntrant >> y;
            monFluxEntrant >> z;
            monFluxEntrant >> r;
            monFluxEntrant >> nom;
            monFluxEntrant >> age;
            listeCaillou[i].initialiserCailloux(n, x, y, z, r, nom, age);
            listeCaillou[i].initialiserMineraux();
        }
    } 
    else 
    {
        cout << "ERROR: Impossible to open the initial clast list. Verify that it exists or remove the clast option in the input_cidre.txt" << endl;
        exit(EXIT_FAILURE);
    }
    cout << "\t  ***List of clasts read**" << endl << endl;


    // initialisation de la liste d'indice de cailloux vivants

    nbCaillouVivant = nbCaillou;

    for (int k(0); k < nbCaillouVivant; k++) 
    {
        indiceCaillouVivant.push_back(k);
    }


    // Fin ajout cailloux


    // CALCUL PAR PERIODE
    //

    for (int i = 0; i < MonModele->GetNbPeriodes(); i++) 
    {
        int dt = MonModele->GetPasTemps(i); //Ajout Pierre
        for (int j(0); j < nbCaillou; j++) //Ajout Pierre
        {
            listeCaillou[j].setPasDeTemps(dt); //Pour chaque caillou on met à jour le pas de temps, et on le fait au début de chaque période
        }

        (((CCalcul*)this)->*CCalcul::MaBoucleCaillou)(i, nbCaillou);
    }


    // FERMETURE DES SORTIES ET ECRITURE DU RESTART
    //
    MesSorties->FinitSorties();
    (MesSorties->*MonFinitSortiesCailloux)();
    if (!MesSorties->EcritRestart(temps))
        AfficheMessage("Pb dans l'ecriture du restart");

    return;
}




//==================================
//	Boucle de calcul sur une pÈriode
//==================================

void CCalcul::BouclePeriode(int NumPeriode) 
{
    double TempsMax = MonModele->GetFinPeriode(NumPeriode);
    double dt_periode = MonModele->GetPasTemps(NumPeriode);
    double dt = dt_periode;
    double FracTempsPluie = MonModele->GetFracTempsPluie(NumPeriode); // rajout, cas stochastique ou non
    int count = 0;

    MesFlux->ActualiseNumPeriode(NumPeriode);
    MesFlux->ActualisePluieDeLaPeriode(NumPeriode);
    MonBilanEau->ActualisePasDeTemps(dt);
    (MonMaillage->*MonActuTecto)(NumPeriode, dt);

    // Actualise les coefficients d'Èrosion, diffusion, etc...
    (MonBilanSed->*MonActuCoeffs)(dt, FracTempsPluie);

    while (temps <= TempsMax) 
    {
        //;;;;;;;;;;;;;;;;;
        // INITIALISATIONS
        //;;;;;;;;;;;;;;;;;
        // Initialise les tableaux de flux d'eau et de sÈdiments ET
        // Calcule le volume d'eau reÁue des prÈcipitations pendant le pas de temps
        // ==> "FluxEau" calculÈ partout
        (MesFlux->*MonInitFlux_RecuperePluie)(temps);
        // Initialise les voisinages (nb de voisins), et, Èventuellement
        // Actualise le niveau de base

        (MonMaillage->*MonActuVoisinsNivbase)(temps, dt);
        //;;;;;;;;;;;;;;;;;;;;;;;;
        // PENTES / EFFONDREMENTS
        //;;;;;;;;;;;;;;;;;;;;;;;;
        // Calcule les pentes et les effondrements si option d'effondrement
        // ==> Pentes
        //		AfficheMessage("   --- calcul pentes/effondrements...");
        NbEffondrements += (MesEffondrements->*MonCalculeEffondrements)(dt);
        //;;;;;;;;;;;;;;;;;;
        // TRI PAR ALTITUDE
        //;;;;;;;;;;;;;;;;;;
        // Tri les mailles par altitudes dÈcroissantes, range le classement dans Liste
        // ==> "Liste"
        count = count + 1;
        if (count == 1)
            // le tri rapide est efficace quand les mailles sont tres desordonnées au premier pas de temps.
            // Ensuite c'est le tri par insertion qui est le plus rapide.
            (MonMaillage->*MonTriOptimalMailles)();
        else
            (MonMaillage->*MonTriParInsertionMailles)();
        //;;;;;;;;;;;;;
        // BILAN HYDRO
        //;;;;;;;;;;;;;
        // Calcule le bilan d'eau (les flux et les lacs) en fonction des pentes
        // ==> "EauCourante" calculÈ partout
        //		AfficheMessage("   --- calcul bilan eau...");
        // Actualisation du pas de temps pour calcul des lacs. Le pas de temps est multiplie
        // par la fraction de temps avec pluie car les debits d'eau correspondent a des volumes
        // d'eau calcule sur la duree totale annuelle de pluie. Pour avoir des dÈbits moyens annuels,
        // et calculer des volumes de lac corrects, il faut actualiser avec un pas de temps
        // multiplie par la fraction du temps avec pluie.

        MonBilanEau->ActualisePasDeTemps(dt * FracTempsPluie); // modif pour calculer les volumes de lac avec la bonne valeur de dt
        // Necessaire depuis introduction de FracTempsPluie (stochastique ou non)

        NbLacs += (MonBilanEau->*MonCalc_BilanEau)();
        
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // BILAN EROSION/SEDIMENTATION
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Calcule le bilan sÈdimentaire en fonction des options de processus :
        // altÈration, diffusion, transport fluvial / dÈtachement
        // ==> "" calculÈ partout
        //		AfficheMessage("   --- calcul bilan sed...");

        (MonBilanSed->*MonCalc_BilanSed)();

        //;;;;;;;;;;;;;;;;;;;;;;;;
        // EFFET DE LA TECTONIQUE
        //;;;;;;;;;;;;;;;;;;;;;;;;
        (MesFlux->*MonActiviteTecto)(dt);

        //MonBilanEau->CalculeNouvelleHeau();
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // TEMPS / SORTIES EVENTUELLES
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Actualise les temps
        //		dt_prec = dt;
        temps += dt;
        if ((MesSorties->*MonIncrementeSorties)(temps, dt, NbEffondrements, NbLacs, listeCaillou))
        {
            if (NbLacs > 0)
            {
                AfficheMessage("********** Model Time ", temps + dt, "  --> number of depressions : ", NbLacs);
            }
            NbEffondrements = 0;
            NbLacs = 0;
        }
    }

    NumPeriode++;
    return;
}

//----------------------------------------------------------------
// AJOUT CLIMAT STOCHASTIQUE:
//----------------------------------------------------------------

//==================================
//	Boucle de calcul sur une pÈriode
//==================================

void CCalcul::BouclePeriodeStoch(int NumPeriode) 
{
    double TempsMax = MonModele->GetFinPeriode(NumPeriode);
    double FracTempsPluie = MonModele->GetFracTempsPluie(NumPeriode);
    double dt = 0.;
    double dt_prec = 0.;
    int NumValeurPrecipStoch = 0; // premiere valeur du tableau des precipitations stochastiques
    double ValeurPrecipStoch = 0.;
    double Precipmoyen = MonModele->GetPeriode(NumPeriode).taux_precip;
    double Variabilite = MonModele->GetPeriode(NumPeriode).variabilite;
    MesFlux->ActualiseNumPeriode(NumPeriode);
    int count = 0;

    //;;;;;;;;;;;;;;;;;
    // CALCUL DES VALEURS DE PLUIE (CLIMAT STOCHASTIQUE)
    //;;;;;;;;;;;;;;;;;
    // Calcul de la distribution des valeurs de precipitation,
    // de leur temps de retour et du pas de temps associe.$
    // 50 grilles de precipitations sont preparees avant le calcul iteratif
    // Chaque grille contient une des 50 valeurs de precipitation de la distribution frequentielle.

    (MesFlux->*MonCalculPluieStoch)(NumPeriode, Precipmoyen, Variabilite);
    for (int j = 0; j < nbValeurPrecipStoch; j++) 
    {
        ValeurPrecipStoch = (MesFlux->GetValeurEvenementPluie)(j);
        // rempli la grille de precipitation pour chaque valeur de precipitation
        MesFlux->SetGrillePrecipUniformeStoch(j, ValeurPrecipStoch);
    }

    while (temps <= TempsMax) 
    {
        //		AfficheMessage("Temps ",temps+dt);
        //;;;;;;;;;;;;;;;;;
        // INITIALISATIONS
        //;;;;;;;;;;;;;;;;;
        // Initialise les tableaux de flux d'eau et de sÈdiments ET
        // Calcule le volume d'eau reÁue des prÈcipitations pendant le pas de temps
        // ==> "FluxEau" calculÈ partout
        MesFlux->ActualiseNumValeurPrecipStoch(NumValeurPrecipStoch);
        (MesFlux->*MonInitFlux_RecuperePluieStoch)(temps);


        //;;;;;;;;;;;;;;;;;;;;;;;;;;
        // ACTUALISATION DES COEFFS
        //;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Actualise les coefficients d'Èrosion, diffusion, etc...
        // SI le pas de temps a changÈ !
        //##### Modif le 08/09/2005 :
        //		a priori, le pas de temps est constant, au moins par pÈriode
        // #### 06/01/06 Prise en compte de CLIMAT STOCHASTIQUE, dt varie a chaque pas de temps
        //      dt = nouveau pas de temps associe a l'element suivant de la liste des precipitations stochastiques

        dt = (MesFlux->GetPasTempsEvenementPluie)(NumValeurPrecipStoch);
        // Initialise les voisinages (nb de voisins), et, Èventuellement
        // Actualise le niveau de base
        (MonMaillage->*MonActuVoisinsNivbase)(temps, dt);

        if (dt != dt_prec) 
        {
            (MonBilanSed->*MonActuCoeffs)(dt, FracTempsPluie);
            //			(MonMaillage->*MonActuPrecTect)(NumPeriode,dt);
            (MonMaillage->*MonActuTecto)(NumPeriode, dt);
        }


        //;;;;;;;;;;;;;;;;;;;;;;;;
        // PENTES / EFFONDREMENTS
        //;;;;;;;;;;;;;;;;;;;;;;;;
        // Calcule les pentes et les effondrements si option d'effondrement
        // ==> Pentes
        NbEffondrements += (MesEffondrements->*MonCalculeEffondrements)(dt);

        //;;;;;;;;;;;;;;;;;;
        // TRI PAR ALTITUDE
        //;;;;;;;;;;;;;;;;;;
        // Tri les mailles par altitudes dÈcroissantes, range le classement dans Liste
        // ==> "Liste"
        count = count + 1;
        if (count == 1)
            // le tri rapide est efficace quand les mailles sont tres desordonnées au premier pas de temps.
            //Ensuite c'est le tri par insertion qui est le plus rapide
            (MonMaillage->*MonTriOptimalMailles)();
        else
            (MonMaillage->*MonTriParInsertionMailles)();

        //;;;;;;;;;;;;;
        // BILAN HYDRO
        //;;;;;;;;;;;;;
        // Calcule le bilan d'eau (les flux et les lacs) en fonction des pentes
        // ==> "EauCourante" calculÈ partout
        // Actualisation du pas de temps pour calcul des lacs. Le pas de temps est multiplie
        // par la fraction de temps avec pluie car les debits d'eau correspondent a des volumes
        // d'eau calcule sur la duree totale annuelle de pluie. Pour avoir des dÈbits moyens annuels,
        // et calculer des volumes de lac corrects, il faut actualiser avec un pas de temps
        // multiplie par la fraction du temps avec pluie.
        MonBilanEau->ActualisePasDeTemps(dt * FracTempsPluie);
        NbLacs += (MonBilanEau->*MonCalc_BilanEau)();

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // BILAN EROSION/SEDIMENTATION
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Calcule le bilan sÈdimentaire en fonction des options de processus :
        // altÈration, diffusion, transport fluvial / dÈtachement
        // ==> "" calculÈ partout
        (MonBilanSed->*MonCalc_BilanSed)();

        //;;;;;;;;;;;;;;;;;;;;;;;;
        // EFFET DE LA TECTONIQUE
        //;;;;;;;;;;;;;;;;;;;;;;;;
        (MesFlux->*MonActiviteTecto)(dt);

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // TEMPS / SORTIES EVENTUELLES
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Actualise les temps
        dt_prec = dt;
        temps += dt;
        if ((MesSorties->*MonIncrementeSorties)(temps, dt, NbEffondrements, NbLacs, listeCaillou)) // modif 2023
        {
            NbEffondrements = 0;
            NbLacs = 0;
        }
        // Actualise le numero de la valeur de precipitation de la distribution
        NumValeurPrecipStoch += 1;
        if (NumValeurPrecipStoch == nbValeurPrecipStoch - 1) 
        {
            NumValeurPrecipStoch = 0;
        }
    }

    NumPeriode++;
    return;
}
//----------------------------------------------------------------
// FIN AJOUT CLIMAT STOCHASTIQUE:
//----------------------------------------------------------------

//==================================
//	Boucle de calcul sur une pÈriode
//==================================

void CCalcul::BouclePeriodeCaillou(int NumPeriode, const int &nbCaillou) 
{
    double TempsMax = MonModele->GetFinPeriode(NumPeriode);
    double dt_periode = MonModele->GetPasTemps(NumPeriode);
    double dt = dt_periode;
    double FracTempsPluie = MonModele->GetFracTempsPluie(NumPeriode); // rajout, cas stochastique ou non
    int count = 0;


    MesFlux->ActualiseNumPeriode(NumPeriode);
    MesFlux->ActualisePluieDeLaPeriode(NumPeriode);
    MonBilanEau->ActualisePasDeTemps(dt);
    (MonMaillage->*MonActuTecto)(NumPeriode, dt);

    // Actualise les coefficients d'Èrosion, diffusion, etc...

    (MonBilanSed->*MonActuCoeffs)(dt, FracTempsPluie);


    while (temps <= TempsMax) 
    {

        for (int k(0); k < nbCaillou; k++)
        {
                listeCaillou[indiceCaillouVivant[k]].miseAZero();
            // listeCaillou[indiceCaillouVivant[k]].setProfAlteInit(MesFlux->GetStockSed(listeCaillou[indiceCaillouVivant[k]].getNumeroMaille())); // commente 2023
        }
        //;;;;;;;;;;;;;;;;;
        // INITIALISATIONS
        //;;;;;;;;;;;;;;;;;
        //Mise à 0 des valeurs dans CCaillou
        for (int k(0); k < nbCaillouVivant; k++) 
        {
            listeCaillou[indiceCaillouVivant[k]].miseAZero();
            // listeCaillou[indiceCaillouVivant[k]].setProfAlteInit(MesFlux->GetStockSed(listeCaillou[indiceCaillouVivant[k]].getNumeroMaille())); // commente 2023
        }
        

        // Initialise les tableaux de flux d'eau et de sÈdiments ET
        // Calcule le volume d'eau reÁue des prÈcipitations pendant le pas de temps
        // ==> "FluxEau" calculÈ partout

        (MesFlux->*MonInitFlux_RecuperePluie)(temps);

        // Initialise les voisinages (nb de voisins), et, Èventuellement
        // Actualise le niveau de base

        (MonMaillage->*MonActuVoisinsNivbase)(temps, dt);


        //;;;;;;;;;;;;;;;;;;;;;;;;;;
        // ACTUALISATION DES COEFFS
        //;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Actualise les coefficients d'Èrosion, diffusion, etc...
        // SI le pas de temps a changÈ !
        //##### Modif le 08/09/2005 :
        //		a priori, le pas de temps est constant, au moins par pÈriode
        /*
                        if (dt != dt_prec)
                        {
                                (MonBilanSed->*MonActuCoeffs)(dt);
                                (MonMaillage->*MonActuPrecTect)(NumPeriode,dt);
                        }
         */

        //;;;;;;;;;;;;;;;;;;;;;;;;
        // PENTES / EFFONDREMENTS
        //;;;;;;;;;;;;;;;;;;;;;;;;
        // Calcule les pentes et les effondrements si option d'effondrement
        // ==> Pentes
        //		AfficheMessage("   --- calcul pentes/effondrements...");

        NbEffondrements += (MesEffondrements->*MonCalculeEffondrements)(dt);

        //;;;;;;;;;;;;;;;;;;
        // TRI PAR ALTITUDE
        //;;;;;;;;;;;;;;;;;;
        // Tri les mailles par altitudes dÈcroissantes, range le classement dans Liste
        // ==> "Liste"
        count = count + 1;
        if (count == 1)
            // le tri rapide est efficace quand les mailles sont tres desordonnées au premier pas de temps.
            //Ensuite c'est le tri par insertion qui est le plus rapide
            (MonMaillage->*MonTriOptimalMailles)();
        else
            (MonMaillage->*MonTriParInsertionMailles)();
        
        //;;;;;;;;;;;;;
        // BILAN HYDRO
        //;;;;;;;;;;;;;
        // Calcule le bilan d'eau (les flux et les lacs) en fonction des pentes
        // ==> "EauCourante" calculÈ partout
        //		AfficheMessage("   --- calcul bilan eau...");
        // Actualisation du pas de temps pour calcul des lacs. Le pas de temps est multiplie
        // par la fraction de temps avec pluie car les debits d'eau correspondent a des volumes
        // d'eau calcule sur la duree totale annuelle de pluie. Pour avoir des dÈbits moyens annuels,
        // et calculer des volumes de lac corrects, il faut actualiser avec un pas de temps
        // multiplie par la fraction du temps avec pluie.

        MonBilanEau->ActualisePasDeTemps(dt * FracTempsPluie); // modif pour calculer les volumes de lac avec la bonne valeur de dt
        // Necessaire depuis introduction de FracTempsPluie (stochastique ou non)

        NbLacs += (MonBilanEau->*MonCalc_BilanEau)();


        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // BILAN EROSION/SEDIMENTATION
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Calcule le bilan sÈdimentaire en fonction des options de processus :
        // altÈration, diffusion, transport fluvial / dÈtachement
        // ==> "" calculÈ partout
        //		AfficheMessage("   --- calcul bilan sed...");

        (MonBilanSed->*MonCalc_BilanSed)();

        //;;;;;;;;;;;;;;;;;;;;;;;;
        // EFFET DE LA TECTONIQUE
        //;;;;;;;;;;;;;;;;;;;;;;;;

        (MesFlux->*MonActiviteTecto)(dt);

        
        ///////////////////////////////////
        ////Ajout du suivi des Cailloux////
        ///////////////////////////////////
 
        int OMP_DYN_SCHEDULE = 1;
        
        for (int k(0); k < indiceCaillouMortAcetteIteration.size(); k++)
        {
            listeCaillou[indiceCaillouMortAcetteIteration[k]].SetDeadInTheLastIterationFalse(); //  2023
        }
        indiceCaillouMortAcetteIteration.clear(); //Ajout Youssouf
        nbCaillouMortAcetteIteration = 0; // 2023


        #pragma omp parallel 
        #pragma omp for schedule(dynamic,OMP_DYN_SCHEDULE)
        for (int i = 0; i < nbCaillouVivant; i++)
        {           
            (listeCaillou[indiceCaillouVivant[i]].*MonSuiviCaillouDiff)(nbCaillou, NumPeriode);
        }
       
        MiseAJourListeCaillou();
       

        #pragma omp parallel
        #pragma omp for schedule(dynamic,OMP_DYN_SCHEDULE)
        for (int j = 0; j < nbCaillouVivant; j++)
        {        
            (listeCaillou[indiceCaillouVivant[j]].*MonSuiviCaillouFluv)(nbCaillou, NumPeriode);
        }
        
        MiseAJourListeCaillou();


        // Début Ajout youssouf
        // Les deux boucles for qui suivent sont destinées au calcul de la concentration en cosmonucleides
        #pragma omp parallel
        #pragma omp for schedule(dynamic,OMP_DYN_SCHEDULE)
        for(int j = 0; j < nbCaillouVivant; j++)
        {// Boucle sur l'ensemble des cailloux vivant pour calculer leur concentration
                   
            (listeCaillou[indiceCaillouVivant[j]].*MonComputeCosmonuclide)();
        }

        #pragma omp parallel
        #pragma omp for schedule(dynamic,OMP_DYN_SCHEDULE)
        for(int j = 0; j < nbCaillouMortAcetteIteration; j++)
        {// Permet de calculer la concentration des cailloux qui viennent de mourir à cette itération

            (listeCaillou[indiceCaillouMortAcetteIteration[j]].*MonComputeCosmonuclide)();
            (listeCaillou[indiceCaillouMortAcetteIteration[j]].SetDeadInTheLastIterationTrue)(); // 2023
        }
        

       // Fin ajout Youssouf

        
        // On sort ce qui concerne les cailloux (condition interne a ces 3 routines sur la sortie ou pas)

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // TEMPS / SORTIES EVENTUELLES
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Actualise les temps
        //        dt_prec = dt;
        temps += dt;

        if ((MesSorties->*MonIncrementeSorties)(temps, dt, NbEffondrements, NbLacs, listeCaillou))
        {
            if (NbLacs > 0)
            {
                AfficheMessage("********** Model time ", temps + dt, "  --> number of depressions : ", NbLacs);
            }
            NbEffondrements = 0;
            NbLacs = 0;
        }

        MesSorties->EcritListeCailloux(int(temps), listeCaillou);

        MesSorties->MoyenneEcartTypeCailloux(temps, listeCaillou);

        (MesSorties->*MonFluxChimiqueIntegre)(MonModele, listeCaillou, dt, temps);

        // On ressuscite eventuellement les cailloux morts
        
        if (nbCaillouVivant < nbCaillou)
        {
            (((CCalcul*)this)->*CCalcul::MonRessusciteCaillouMort)(NumPeriode); // modif 2023
        }
        
        
    }

    NumPeriode++;
    return;
}

//----------------------------------------------------------------
// AJOUT CLIMAT STOCHASTIQUE:
//----------------------------------------------------------------

//==================================
//	Boucle de calcul sur une période
//==================================

void CCalcul::BouclePeriodeStochCaillou(int NumPeriode, const int &nbCaillou) 
{
    double TempsMax = MonModele->GetFinPeriode(NumPeriode);
    double FracTempsPluie = MonModele->GetFracTempsPluie(NumPeriode);
    double dt = 0.;
    double dt_prec = 0.;
    int NumValeurPrecipStoch = 0; // premiere valeur du tableau des precipitations stochastiques
    double ValeurPrecipStoch = 0.;
    double Precipmoyen = MonModele->GetPeriode(NumPeriode).taux_precip;
    double Variabilite = MonModele->GetPeriode(NumPeriode).variabilite;
    MesFlux->ActualiseNumPeriode(NumPeriode);
    int count = 0;

    //;;;;;;;;;;;;;;;;;
    // CALCUL DES VALEURS DE PLUIE (CLIMAT STOCHASTIQUE)
    //;;;;;;;;;;;;;;;;;
    // Calcul de la distribution des valeurs de precipitation,
    // de leur temps de retour et du pas de temps associe.$
    // 50 grilles de precipitations sont preparees avant le calcul iteratif
    // Chaque grille contient une des 50 valeurs de precipitation de la distribution frequentielle.

    (MesFlux->*MonCalculPluieStoch)(NumPeriode, Precipmoyen, Variabilite);
    for (int j = 0; j < nbValeurPrecipStoch; j++) 
    {
        ValeurPrecipStoch = (MesFlux->GetValeurEvenementPluie)(j);
        // rempli la grille de precipitation pour chaque valeur de precipitation
        MesFlux->SetGrillePrecipUniformeStoch(j, ValeurPrecipStoch);
    }

    nbCaillouVivant = nbCaillou; //AJOUT PIERRE
    for (int k(0); k < nbCaillouVivant; k++) 
    {
        indiceCaillouVivant.push_back(k);
    }

    while (temps <= TempsMax) 
    {
        //		AfficheMessage("Temps ",temps+dt);
        //;;;;;;;;;;;;;;;;;
        // INITIALISATIONS
        //;;;;;;;;;;;;;;;;;

        //Mise à 0 des valeurs dans CCaillou
        for (int k(0); k < nbCaillouVivant; k++) 
        {
            listeCaillou[indiceCaillouVivant[k]].miseAZero();
            //listeCaillou[indiceCaillouVivant[k]].setProfAlteInit(MesFlux->GetStockSed(listeCaillou[indiceCaillouVivant[k]].getNumeroMaille())); // commente 2023
        }

        // Initialise les tableaux de flux d'eau et de sÈdiments ET
        // Calcule le volume d'eau reÁue des prÈcipitations pendant le pas de temps
        // ==> "FluxEau" calculÈ partout
        MesFlux->ActualiseNumValeurPrecipStoch(NumValeurPrecipStoch);
        (MesFlux->*MonInitFlux_RecuperePluieStoch)(temps);


        //;;;;;;;;;;;;;;;;;;;;;;;;;;
        // ACTUALISATION DES COEFFS
        //;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Actualise les coefficients d'Èrosion, diffusion, etc...
        // SI le pas de temps a changÈ !
        //##### Modif le 08/09/2005 :
        //		a priori, le pas de temps est constant, au moins par pÈriode
        // #### 06/01/06 Prise en compte de CLIMAT STOCHASTIQUE, dt varie a chaque pas de temps
        //      dt = nouveau pas de temps associe a l'element suivant de la liste des precipitations stochastiques

        dt = (MesFlux->GetPasTempsEvenementPluie)(NumValeurPrecipStoch);
        // Initialise les voisinages (nb de voisins), et, Èventuellement
        // Actualise le niveau de base
        (MonMaillage->*MonActuVoisinsNivbase)(temps, dt);

        if (dt != dt_prec) 
        {
            (MonBilanSed->*MonActuCoeffs)(dt, FracTempsPluie);
//			(MonMaillage->*MonActuPrecTect)(NumPeriode,dt);
            (MonMaillage->*MonActuTecto)(NumPeriode, dt);
        }


        //;;;;;;;;;;;;;;;;;;;;;;;;
        // PENTES / EFFONDREMENTS
        //;;;;;;;;;;;;;;;;;;;;;;;;
        // Calcule les pentes et les effondrements si option d'effondrement
        // ==> Pentes
        NbEffondrements += (MesEffondrements->*MonCalculeEffondrements)(dt);

        //;;;;;;;;;;;;;;;;;;
        // TRI PAR ALTITUDE
        //;;;;;;;;;;;;;;;;;;
        // Tri les mailles par altitudes dÈcroissantes, range le classement dans Liste
        // ==> "Liste"
        count = count + 1;
        if (count == 1)
            // le tri rapide est efficace quand les mailles sont tres desordonnées au premier pas de temps.
            //Ensuite c'est le tri par insertion qui est le plus rapide
            (MonMaillage->*MonTriOptimalMailles)();
        else
            (MonMaillage->*MonTriParInsertionMailles)();

        //;;;;;;;;;;;;;
        // BILAN HYDRO
        //;;;;;;;;;;;;;
        // Calcule le bilan d'eau (les flux et les lacs) en fonction des pentes
        // ==> "EauCourante" calculÈ partout
        // Actualisation du pas de temps pour calcul des lacs. Le pas de temps est multiplie
        // par la fraction de temps avec pluie car les debits d'eau correspondent a des volumes
        // d'eau calcule sur la duree totale annuelle de pluie. Pour avoir des dÈbits moyens annuels,
        // et calculer des volumes de lac corrects, il faut actualiser avec un pas de temps
        // multiplie par la fraction du temps avec pluie.
        MonBilanEau->ActualisePasDeTemps(dt * FracTempsPluie);
        NbLacs += (MonBilanEau->*MonCalc_BilanEau)();

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // BILAN EROSION/SEDIMENTATION
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Calcule le bilan sÈdimentaire en fonction des options de processus :
        // altÈration, diffusion, transport fluvial / dÈtachement
        // ==> "" calculÈ partout
        (MonBilanSed->*MonCalc_BilanSed)();

        //;;;;;;;;;;;;;;;;;;;;;;;;
        // EFFET DE LA TECTONIQUE
        //;;;;;;;;;;;;;;;;;;;;;;;;
        (MesFlux->*MonActiviteTecto)(dt);


        ///////////////////////////////////
        ////Ajout du suivi des Cailloux////
        ///////////////////////////////////
        int OMP_DYN_SCHEDULE = 1;

        // reinitialise la liste des grains sortis a l'iteration precedente
        for (int k(0); k < indiceCaillouMortAcetteIteration.size(); k++)
        {
            listeCaillou[indiceCaillouMortAcetteIteration[k]].SetDeadInTheLastIterationFalse(); // 2023
        }
        indiceCaillouMortAcetteIteration.clear(); //Ajout Youssouf

        #pragma omp parallel
        #pragma omp for schedule(dynamic,OMP_DYN_SCHEDULE)
        for (int i=0; i < nbCaillouVivant; i++) 
        {
            (listeCaillou[indiceCaillouVivant[i]].*MonSuiviCaillouDiff)(nbCaillou, NumPeriode);
        }

        MiseAJourListeCaillou();

        #pragma omp parallel
        #pragma omp for schedule(dynamic,OMP_DYN_SCHEDULE)
        for (int i=0; i < nbCaillouVivant; i++) 
        {
            (listeCaillou[indiceCaillouVivant[i]].*MonSuiviCaillouFluv)(nbCaillou, NumPeriode);
        }

        MiseAJourListeCaillou();
        
        // Début Ajout youssouf
        // Les deux boucles for qui suivent sont destinées au calcul de la concentration en cosmonucleides
        #pragma omp parallel
        #pragma omp for schedule(dynamic,OMP_DYN_SCHEDULE)
        for(int j = 0; j < nbCaillouVivant; j++)
        {// Boucle sur l'ensemble des cailloux vivant pour calculer leur concentration
                   
            (listeCaillou[indiceCaillouVivant[j]].*MonComputeCosmonuclide)();
        }
               
        #pragma omp parallel
        #pragma omp for schedule(dynamic,OMP_DYN_SCHEDULE)
        for(int j = 0; j < nbCaillouMortAcetteIteration; j++)
        {// Permet de calculer la concentration des cailloux qui viennent de mourir à cette itération

            (listeCaillou[indiceCaillouMortAcetteIteration[j]].*MonComputeCosmonuclide)();
            (listeCaillou[indiceCaillouMortAcetteIteration[j]].SetDeadInTheLastIterationTrue)(); // 2023
        }
        // Fin ajout Youssouf

        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // TEMPS / SORTIES EVENTUELLES
        //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        // Actualise les temps
        dt_prec = dt;
        temps += dt;

        if ((MesSorties->*MonIncrementeSorties)(temps, dt, NbEffondrements, NbLacs, listeCaillou))  // modif 2023
        {
            NbEffondrements = 0;
            NbLacs = 0;
        }
        // Actualise le numero de la valeur de precipitation de la distribution
        NumValeurPrecipStoch += 1;
        if (NumValeurPrecipStoch == nbValeurPrecipStoch - 1)
        {
            NumValeurPrecipStoch = 0;
        }
        


        MesSorties->EcritListeCailloux(int(temps), listeCaillou);
        MesSorties->MoyenneEcartTypeCailloux(temps, listeCaillou);
        (MesSorties->*MonFluxChimiqueIntegre)(MonModele, listeCaillou, dt, temps);

        if (nbCaillouVivant < nbCaillou) 
        {
            (((CCalcul*)this)->*CCalcul::MonRessusciteCaillouMort)(NumPeriode); // modif 2023
        }

    }

    NumPeriode++;
    return;
}
//----------------------------------------------------------------
// FIN AJOUT CLIMAT STOCHASTIQUE:
//----------------------------------------------------------------

void CCalcul::MiseAJourListeCaillou()  // modif 2023
{
    int nbCaillouMort = 0;
    int k = 0;
    //for (int k = 0; k < indiceCaillouVivant.size(); k++)
    while (k < indiceCaillouVivant.size())
    {
    
        if (!listeCaillou[indiceCaillouVivant[k]].getDansLaGrille())
        {
            nbCaillouMort++;
            indiceCaillouMort.push_back(indiceCaillouVivant[k]);
            indiceCaillouMortAcetteIteration.push_back(indiceCaillouVivant[k]); //Ajout youssouf
            indiceCaillouVivant.erase(indiceCaillouVivant.begin() + k);

        }
        else
        {
            k=k+1; // modif 2023
        }
    }

    nbCaillouMortAcetteIteration += nbCaillouMort; // Ajout youssouf
    nbCaillouVivant -= nbCaillouMort;

    
    
}



void CCalcul::RessusciteCaillouMort(const int NumPeriode) // modif 2023
{
    int nbCailloux(int(listeCaillou.size()));

    for (int k(0); k < indiceCaillouMort.size(); k++)
    {
        // Cette fonction remet le cailloux sur sa maille initiale, avec sa taille initiale
        //  sous l'interface bedrock-regolite a une profondeur speficiee dans input_cidre, avec la mineralogie associee au bedrock ou imposee en entree.
        listeCaillou[indiceCaillouMort[k]].RessusciteCaillou(MonModele,NumPeriode); // modif 2023
        indiceCaillouVivant.push_back(indiceCaillouMort[k]);
    }
    nbCaillouVivant = nbCailloux;
    indiceCaillouMort.clear();
}

void CCalcul::RessusciteCaillouMortCOSMO(const int NumPeriode) // modif 2023
{
    int nbCailloux(int(listeCaillou.size()));

    for (int k(0); k < indiceCaillouMort.size(); k++)
    {
        // Cette fonction remet le cailloux sur sa maille initiale, avec sa taille initiale
        //  sous l'interface bedrock-regolite a une profondeur speficiee dans input_cidre, avec la mineralogie associee au bedrock ou imposee en entree.
        listeCaillou[indiceCaillouMort[k]].RessusciteCaillouCOSMO(MonModele,NumPeriode); // modif 2023
        indiceCaillouVivant.push_back(indiceCaillouMort[k]);
    }
    nbCaillouVivant = nbCailloux;
    indiceCaillouMort.clear();
}

void CCalcul::NeFaitRien(const int NumPeriode) {
    ;
}// modif 2023

