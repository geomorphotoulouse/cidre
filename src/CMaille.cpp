#include "CMaille.h"
#include <iostream> // pour les manipulations d'I/O sur cin/cout
using namespace std;


//-------------------------------------------
//
// fonctions de la classe CMaille :
//
//-------------------------------------------

// constructeur

CMaille::CMaille() 
{
    z_init = -10000.;
    z = z_init;
    z_eau = z_init;
    z_ref = z_init; // 2022
    // Par d�faut, une maille n'est pas sur le bord
    PasBord = true;
    type_bord = pas_bord;
    nivbase = 0.;
    StockInitSed = 0.;
    // Par d�faut, une maille a une seule couche
    PileCouches = new TCouche*[1];
    PileCouches[0] = new TCouche;
    coord.x = 0.;
    coord.y = 0.;
    
    alti_voisin_justeaudessus_eau = z_init; // 2023
    NumVoisinJusteAuDessusEau = 0; //2023
    AltiVoisinLePlusBasEau = z_init; // 2023
    AltiMeanVoisinEau = z_init; // 2023
    altilaplusbasse = z_init; // 2023
}

// copy constructeur

CMaille::CMaille(const CMaille &maille) 
{
    int i;

    nummaille = maille.nummaille;
    z_init = maille.z_init;
    z = maille.z;
    z_eau = maille.z_eau;
    z_ref = maille.z_ref; // 2022

    PasBord = maille.PasBord;
    type_bord = maille.type_bord;
    nivbase = maille.nivbase;


    
    voisins_bas = new Tvoisinage_bas;
    voisins_bas->nb = maille.voisins_bas->nb;
    voisins_bas->ind = new int[voisins_bas->nb];
    voisins_bas->direction = new int[voisins_bas->nb]; // 7 aout 2008
    voisins_bas->pas3 = new double[voisins_bas->nb];
    voisins_bas->pente = new double[voisins_bas->nb];
    voisins_bas->coeff = new double[voisins_bas->nb];
    voisins_bas->pentemoyenne = maille.voisins_bas->pentemoyenne; // 30 juin 2008
    voisins_bas->pentemax = maille.voisins_bas->pentemax;
    voisins_bas->minupstreamslope = maille.voisins_bas->minupstreamslope; // 2022
    voisins_bas->minupstreamslopepas = maille.voisins_bas->minupstreamslopepas; // 2022
    voisins_bas->alti = new double[voisins_bas->nb]; // 2023
    voisins_bas->altilaplusbasse = maille.voisins_bas->altilaplusbasse; // 2023
    voisins_bas->directionaltilaplusbasse = maille.voisins_bas->directionaltilaplusbasse; // 2023
    
    for (i = 0; i < voisins_bas->nb; i++) 
    {
        voisins_bas->ind[i] = maille.voisins_bas->ind[i];
        voisins_bas->pas3[i] = maille.voisins_bas->pas3[i];
        voisins_bas->pente[i] = maille.voisins_bas->pente[i];
        voisins_bas->coeff[i] = maille.voisins_bas->coeff[i];
    }
    // idem pour voisin bas le long de la surface de l'eau 30 juin 2008	
    voisins_bas_eau = new Tvoisinage_bas;
    voisins_bas_eau->nb = maille.voisins_bas_eau->nb;
    voisins_bas_eau->ind = new int[voisins_bas_eau->nb];
    voisins_bas_eau->direction = new int[voisins_bas_eau->nb]; // modif 2022
    voisins_bas_eau->pas3 = new double[voisins_bas_eau->nb];
    voisins_bas_eau->pente = new double[voisins_bas_eau->nb];
    voisins_bas_eau->coeff = new double[voisins_bas_eau->nb];
    voisins_bas_eau->pentemoyenne = maille.voisins_bas_eau->pentemoyenne; // 30 juin 2008
    voisins_bas_eau->pentemax = maille.voisins_bas_eau->pentemax;
    voisins_bas_eau->minupstreamslope = maille.voisins_bas_eau->minupstreamslope; //2022
    voisins_bas_eau->minupstreamslopepas = maille.voisins_bas_eau->minupstreamslopepas; //2022
    voisins_bas_eau->alti = new double[voisins_bas_eau->nb]; // 2023
    voisins_bas_eau->altilaplusbasse = maille.voisins_bas_eau->altilaplusbasse; // 2023
    voisins_bas_eau->directionaltilaplusbasse = maille.voisins_bas_eau->directionaltilaplusbasse; // 2023
    
    for (i = 0; i < voisins_bas_eau->nb; i++) 
    {
        // initialisation
        voisins_bas_eau->ind[i] = maille.voisins_bas_eau->ind[i];
        voisins_bas_eau->direction[i] = maille.voisins_bas_eau->direction[i]; // 7 aout 2008
        voisins_bas_eau->pas3[i] = maille.voisins_bas_eau->pas3[i];
        voisins_bas_eau->pente[i] = maille.voisins_bas_eau->pente[i];
        voisins_bas_eau->coeff[i] = maille.voisins_bas_eau->coeff[i];
    }


    StockInitSed = maille.StockInitSed;

    for (i = 0; i < 8; i++) // 7 aout 2008
    {
        VoisinLateral[i][0] = 0.;
        VoisinLateral[i][1] = 0.;
    }
}

// destructeur

CMaille::~CMaille() 
{ // tous les delete ajoutes le 30 juin 2008
    delete [] PileCouches;
    delete voisins_bas_eau;
    delete voisins_bas;
}

// fonctions

//---------------------------------------------------
// r�cup�re ou assigne la stratigraphie de la maille
//---------------------------------------------------

void CMaille::AllouePileCouches(int nb) 
{
    if (nb > 1) {
        delete PileCouches[0];
        delete [] PileCouches;
        PileCouches = new TCouche*[nb];
        for (int i = 0; i < nb; i++) 
        {
            PileCouches[i] = new TCouche;
        }
    }
    return;
}

//----------------------
// alloue les voisinages
//----------------------

void CMaille::AlloueVoisinageMultiple() 
{
    voisins_bas = new Tvoisinage_bas;
    voisins_bas->nb = 0;
    voisins_bas->ind = new int[8];
    voisins_bas->direction = new int[8]; // 7 aout 2008
    voisins_bas->pas3 = new double[8];
    voisins_bas->pente = new double[8];
    voisins_bas->coeff = new double[8];
    voisins_bas->pentemoyenne = 0.; // 30 juin 2008
    voisins_bas->pentemax = 0.;
    voisins_bas->minupstreamslope = 0.; // 2022
    voisins_bas->minupstreamslopepas = 0.; // 2022
    voisins_bas->minupstreamslope = 0.; // 2022
    voisins_bas->minupstreamslopepas = 0.; // 2022
    voisins_bas->alti = new double[8]; // 2023
    voisins_bas->altilaplusbasse = 0.; // 2023
    voisins_bas->directionaltilaplusbasse = 0; // 2023

}
// idem pour les voisins le long de la surface de l'eau 30 juin 2008

void CMaille::AlloueVoisinageMultipleEau() 
{
    voisins_bas_eau = new Tvoisinage_bas;
    voisins_bas_eau->nb = 0;
    voisins_bas_eau->ind = new int[8];
    voisins_bas_eau->direction = new int[8]; // 7 aout 2008
    voisins_bas_eau->pas3 = new double[8];
    voisins_bas_eau->pente = new double[8];
    voisins_bas_eau->coeff = new double[8];
    voisins_bas_eau->pentemoyenne = 0.; // 30 juin 2008 (inutile en fait)
    voisins_bas_eau->pentemax = 0.;
    voisins_bas_eau->minupstreamslope = 0.; // 2022
    voisins_bas_eau->minupstreamslopepas = 0.; // 2022
    voisins_bas_eau->minupstreamslope = 0.; //2022
    voisins_bas_eau->minupstreamslopepas = 0.; //2022
    voisins_bas_eau->alti = new double[8]; // 2023
    voisins_bas_eau->altilaplusbasse = 0.; // 2023
    voisins_bas_eau->directionaltilaplusbasse = 0; // 2023

}

//-------------------------------------------
// Annote les mailles qui sont au bord du lac
//-------------------------------------------

void CMaille::BordDeLac(GrilleInt* ptr, const int &numlac) 
{
    int l, numvoisin;
    if (PasBord) {
        // On parcourt les voisins de cette maille de bord de lac
        for (l = 0; l < 8; l++) 
        {
            numvoisin = voisin[l];
            if ((*ptr)[numvoisin] != -numlac) (*ptr)[numvoisin] = numlac;
        }
    } 
    else 
    {
        // On parcourt les voisins de cette maille de bord de lac
        for (l = 0; l < 8; l++) 
        {
            numvoisin = voisin[l];
            if (numvoisin >= 0) 
            {
                if ((*ptr)[numvoisin] != -numlac) (*ptr)[numvoisin] = numlac;
            }
        }
    }
    return;
}
//-----------------------------------------------------------
// Annote les mailles qui sont au bord du lac : cas d'une ILE
//-----------------------------------------------------------

void CMaille::BordDeLac_ILE(GrilleInt* ptr, const int &numlac) 
{
    int l, numvoisin;
    // On parcourt les voisins de cette maille de bord de lac
    for (l = 0; l < 8; l++) 
    {
        numvoisin = voisin[l];
        if ((*ptr)[numvoisin] != -numlac) (*ptr)[numvoisin] = numlac;
    }
    return;
}
